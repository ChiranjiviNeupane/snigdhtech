-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `other_information`
--

DROP TABLE IF EXISTS `other_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `other_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blood_group` varchar(255) DEFAULT NULL,
  `driving_liscence_number` varchar(255) DEFAULT NULL,
  `ethnicity` varchar(255) DEFAULT NULL,
  `insurance_policy_number` varchar(255) DEFAULT NULL,
  `major_alergy_or_sickness` varchar(255) DEFAULT NULL,
  `other_professional_training_courses` varchar(255) DEFAULT NULL,
  `pan_number` varchar(255) DEFAULT NULL,
  `past_organisations` varchar(255) DEFAULT NULL,
  `preferences` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `employee_personal_information_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpbl4mxggg4kpebyqdg4v8otxy` (`employee_personal_information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_information`
--

LOCK TABLES `other_information` WRITE;
/*!40000 ALTER TABLE `other_information` DISABLE KEYS */;
INSERT INTO `other_information` VALUES (1,NULL,'x',NULL,'x','','x','x','x','',NULL,'2019-01-21 10:12:18',1,'a@gmail.com'),(4,NULL,'',NULL,'','Allergic Skin','Programming','','','',NULL,'2019-01-17 12:29:58',4,NULL),(5,NULL,'06-40268',NULL,'x','x','programming','x','Airport Service','',NULL,'2019-01-17 12:35:06',5,NULL),(6,NULL,'',NULL,'','','Software','','','',NULL,'2019-01-17 12:35:45',6,NULL),(7,NULL,'-',NULL,'-','-','-','-','-','-',NULL,'2019-01-17 12:44:00',7,NULL),(8,NULL,'',NULL,'','x','Financial','','ABC Inc.','Travelling, Trekking',NULL,'2019-01-17 12:49:34',8,NULL),(9,NULL,'',NULL,'','','','','','',NULL,'2019-01-17 12:50:45',9,NULL),(12,NULL,'xx',NULL,'x','x','Communication and Leadership','x','','',NULL,'2019-01-20 10:39:51',12,NULL),(14,NULL,'xx',NULL,'x','Sensitive skin','Programming','x','x','',NULL,'2019-01-20 10:48:13',14,NULL),(15,NULL,'x',NULL,'x','x','x','x','x','',NULL,'2019-01-20 10:55:44',15,NULL),(16,NULL,'X',NULL,'X','X','X','X','X','',NULL,'2019-01-20 11:10:28',16,NULL),(17,NULL,'x',NULL,'x','','','x','','',NULL,'2019-01-20 11:23:04',17,NULL),(18,NULL,'x',NULL,'x','','','xx','','',NULL,'2019-01-20 11:33:28',18,NULL),(19,NULL,'xx',NULL,'x','','','x','','',NULL,'2019-01-20 12:34:09',19,NULL),(20,NULL,'',NULL,'','','','','','',NULL,'2019-01-21 10:11:15',20,NULL),(21,NULL,'',NULL,'','','','','','',NULL,'2019-01-21 10:14:49',21,NULL),(22,NULL,'01-06-00080249',NULL,'','','Hardware and Graphics Desgin','','Graphic Nepal','',NULL,'2019-01-21 12:33:37',22,NULL),(23,NULL,'06-52495',NULL,'','','Web designing/Programming','','INFOTECH','',NULL,'2019-01-21 12:35:08',23,NULL),(24,NULL,'',NULL,'','','','','','',NULL,'2019-01-21 12:40:14',24,NULL),(25,NULL,'01-06-00156847',NULL,'','','Program Developing','','INFOTECH Nepal','',NULL,'2019-01-21 12:43:18',25,NULL),(27,NULL,'',NULL,'','','Web Designing','','Nerdware','',NULL,'2019-01-22 10:03:07',27,NULL),(28,NULL,'01-06-00302549',NULL,'','','Web Designing','','Aptech','',NULL,'2019-01-22 10:12:58',28,NULL),(29,NULL,'01-06-00256315',NULL,'x','','x','x','','',NULL,'2019-01-22 10:27:53',29,'a@gmail.com'),(31,NULL,'06-457854',NULL,'','','Communication and Leadership','','Laxmi Bikash Bank','',NULL,'2019-01-22 10:37:17',31,NULL),(32,NULL,'',NULL,'','','Hardware and Graphics Design','01-06-00128458','Rajesh Hardware','',NULL,'2019-01-22 10:44:20',32,NULL),(33,NULL,'01-06-00213598',NULL,'x','','Hardware and Graphics Design','x','','',NULL,'2019-01-22 10:57:41',33,NULL),(34,NULL,'x',NULL,'x','','','x','Marcoo Café','',NULL,'2019-01-22 11:10:03',34,NULL),(35,NULL,'01-95485',NULL,'x','','Accounting','x','Kanchajunga Tea State','',NULL,'2019-01-22 11:38:41',35,NULL),(36,NULL,'01-06-00198544',NULL,'','','Program Developing','','','',NULL,'2019-01-22 11:50:56',36,NULL),(37,NULL,'',NULL,'','','Web Designing and Programming','','Etihad Airport Service','',NULL,'2019-01-22 11:58:14',37,NULL),(38,NULL,'06-75984',NULL,'x','x','PHOTOGHAPHIC & PROGRAMMING','x','GLOBALPLATFORM','',NULL,'2019-01-22 12:21:28',38,NULL),(39,NULL,'06-45821',NULL,'x','','Finance and Accounting','x','Nabil Bank','',NULL,'2019-01-22 12:34:05',39,NULL);
/*!40000 ALTER TABLE `other_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:05
