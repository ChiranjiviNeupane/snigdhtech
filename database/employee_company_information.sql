-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company_information`
--

DROP TABLE IF EXISTS `company_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_end_date` varchar(255) DEFAULT NULL,
  `contract_start_date` varchar(255) DEFAULT NULL,
  `contract_type` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `line_manager` varchar(255) DEFAULT NULL,
  `office` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `step` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `employee_personal_information_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmbi4vupxmnyp0213jem8t0tve` (`employee_personal_information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_information`
--

LOCK TABLES `company_information` WRITE;
/*!40000 ALTER TABLE `company_information` DISABLE KEYS */;
INSERT INTO `company_information` VALUES (1,'','','','It','10','chitwaaaaaaaann','','','','1','1','2019-01-17 16:48:33',1,'a@gmail.com'),(4,'','2018-08-15','Permanent','Administration','2','Daniel Prasai','Head','Office Asistant','Office Management','4','Office Premise','2019-01-17 12:29:58',4,NULL),(5,'30/09/2019','01/08/2018','Fixed term','Administration','5','Bishwas Gurung','Head','Admin Officer','General','1','','2019-01-17 12:35:06',5,NULL),(6,'','2018-08-15','Permanent','Administration','2','Daniel Prasai','Head','Driver','Office Management','12','Fleet Management','2019-01-17 12:35:45',6,NULL),(7,'2020-08-14','2018-08-15','Fixed term','Communication','7','Bishwas Gurung','Office','Communication Manager','Public Engament','6','-','2019-01-17 12:44:00',7,NULL),(8,'','15/06/2018','Permanent','Administration','9','Bishwas Gurung','Head','Chief Executive Officer','','3','','2019-01-17 12:49:34',8,NULL),(9,'2018-11-14','2018-08-15','Contract Type','Human Resource','5','Arun Manandhar','Head','Recruitment Officcer','Recruitment','-','Job Grading','2019-01-17 12:50:45',9,NULL),(12,'2019-06-14','2018-06-15','Fixed term','Health','8','Bishwas Gurung','Head','Health Director','','6','','2019-01-20 10:39:51',12,NULL),(14,'2019-08-04','2018-08-05','Short Term','Health','7','Monika Sherpa','Head','Health Manager','Public Health','4','Child Health Division','2019-01-20 10:48:13',14,NULL),(15,'2019-08-04','2018-08-05','Short Term','Education','8','Bishwas Gurung','Head','Education Director','National Examination Board','5','','2019-01-20 10:55:44',15,NULL),(16,'2019-08-04','2018-08-05','Fixed term','Education','7','Ponam Singh Mukhiya','Head','Education Manager','National Examination Board','2','Standardization','2019-01-20 11:10:28',16,NULL),(17,'2020-08-06','2018-08-07','Fixed term','Finance','8','Bishwas Gurung','Head','Finance Director','Finance','2','','2019-01-20 11:23:04',17,NULL),(18,'2020-08-06','2018-08-07','Fixed term','Finance','7','Dhiren Kumar Rai','Head','Finance Manager','Budget Management','2','','2019-01-20 11:33:28',18,NULL),(19,'2019-08-04','2018-08-05','Short Term','Finance','5','Ajit Jung Thapa','Head','Finance Officer','Financial Control and Compliance','3','General Accounting','2019-01-20 12:34:09',19,NULL),(20,'','','Contract Type','chicomp','','chitwaaaaaaaann','Office','','','','','2019-01-21 10:11:15',20,NULL),(21,'2020-11-20','2018-08-12','Fixed term','Finance','4','Ajit Jung Thapa','Head','Assistant Finance Officer','Financial Control and Compliance','4','Monitoring and Review','2019-01-21 10:14:49',21,NULL),(22,'2019-08-21','2018-08-22','Short Term','Program','D','','Head','Asistant Officer','','','A','2019-01-21 12:33:37',22,NULL),(23,'2019-08-04','2018-08-05','Short Term','pROGRAM','D','','Head','Assistant Officer','','1','A','2019-01-21 12:35:08',23,NULL),(24,'2020-08-04','2018-08-05','Fixed term','Transport','B','','Head','Driver','','6','','2019-01-21 12:40:14',24,NULL),(25,'2019-08-14','2018-08-15','Short Term','Program','D','','Head','Assistant Officer','','1','A','2019-01-21 12:43:18',25,NULL),(27,'2019-08-24','2018-08-25','Short Term','Program','D','','Head','Assistant Officer','','1','A','2019-01-22 10:03:07',27,NULL),(28,'2019-08-21','2018-08-22','Short Term','Program','D','','Head','Assistant Officer','','1','A','2019-01-22 10:12:58',28,NULL),(29,'2019-08-20','2018-08-20','Short Term','Program','D','','Head','Assistant Officer','','1','A','2019-01-22 10:24:01',29,NULL),(31,'2019-08-19','2018-08-20','Fixed term','Human Resource','F','','Head','Co-ordinator','','','A','2019-01-22 10:37:17',31,NULL),(32,'2019-08-21','2018-08-22','Short Term','Program','D','','Office','','','1','A','2019-01-22 10:44:20',32,NULL),(33,'2019-08-14','2018-08-15','Short Term','Program','D','','Head','Assistant Officer','','1','A','2019-01-22 10:57:41',33,NULL),(34,'2019-08-04','2018-08-05','Short Term','Communication','D','','Head','','','1','B','2019-01-22 11:10:03',34,NULL),(35,'2018-11-07','2018-08-08','Trainee','Finance','B','','Head','Jr. Assistant','','','B','2019-01-22 11:38:41',35,NULL),(36,'2019-08-14','2018-08-15','Short Term','Program','E','','Head','Officer','','5','A','2019-01-22 11:50:56',36,NULL),(37,'2019-08-04','2018-08-05','Contract Type','Program','D','','Head','Assistant Officer','','1','A','2019-01-22 11:58:14',37,NULL),(38,'2018-08-14','2018-08-15','Short Term','Program','F','','Head','Co-ordinator','','1','B','2019-01-22 12:21:28',38,NULL),(39,'2019-08-14','2018-08-15','Short Term','Finance','G','','Head','Manager','','5','C','2019-01-22 12:34:05',39,NULL);
/*!40000 ALTER TABLE `company_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:03
