-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee_personal_information`
--

DROP TABLE IF EXISTS `employee_personal_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employee_personal_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `academic_qualification` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `identification_name` varchar(255) DEFAULT NULL,
  `identification_number` varchar(255) DEFAULT NULL,
  `maiden_name` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_personal_information`
--

LOCK TABLES `employee_personal_information` WRITE;
/*!40000 ALTER TABLE `employee_personal_information` DISABLE KEYS */;
INSERT INTO `employee_personal_information` VALUES (1,'Bachelor','2019-01-17 12:06:03','2050/01/04','HRD.snigdh@gmail.com','Arun','Citizenship','96549','','Unmarried,','','Nepali','$2a$10$A5qAy88WxvACmk32rIROkeltvh.AUuvZYyFlKQvsBq1cTC3CUrSdG','Male','','Manandhar','','2019-01-21 10:07:00','','a@gmail.com'),(4,'Master','2019-01-17 12:29:58','1982-12-17','OA.snigdh@gmail.com','Sami','Citizenship','52156','','Unmarried,','','Nepali','$2a$10$aqQnymAimb9IpWiJIxv//uCg.hQS.SHT4zTj5BsV4v85Suz/wXpsi','Female','Suffix','Thapa Magar','Miss','2019-01-17 12:29:58','a@gmail.com','a@gmail.com'),(5,'Intermediate','2019-01-17 12:35:06','16/01/1990','AO.snigdh@gmail.com','Daniel','Identification Type','46436','','Unmarried,','','Nepali','$2a$10$iu3puNLP/NJiCywVPZVlJO2iCuqdW.p0w5o0PVpBYiCAp7QEXxZ4S','Male','Jr','Prasai','Mr','2019-01-17 12:35:06','a@gmail.com','a@gmail.com'),(6,'Master','2019-01-17 12:35:45','1982-12-17','fleet.snigdh@gmail.com','Sarita ','Citizenship','63245','','Married,','','Nepali','$2a$10$lPJoF658IKhOOGtAe7mcp.uIburjzRw9ghwbzDEZVgdwd9WG1XHjC','Female','Suffix','Limbu','Mrs','2019-01-17 12:35:45','a@gmail.com','a@gmail.com'),(7,'Intermidiate','2019-01-17 12:44:00','1986-01-31','CM.snigdh@gmail.com','Ujwal','Identification Type','65425','','Unmarried,','','Nepali','$2a$10$TrxNrvcy0QkCFFH54C5JIOaCOaNF8bR/INxZvOCRZ5tIwEjA/VZvK','Male','Suffix','Shrestha','Mr','2019-01-17 16:49:18','a@gmail.com','a@gmail.com'),(8,'Master','2019-01-17 12:49:34','15/08/1979','CEO.snigdh@gmail.com','Bishwas ','Citizenship','45134','','Unmarried,','','Nepali','$2a$10$1gcHCdh1ylYdDfjDiQx93OBuatCWSElMR7t..v6KCyLPxrtsi2xMy','Male','Suffix','Gurung','Mr','2019-01-17 12:49:34','a@gmail.com','a@gmail.com'),(9,'Bachelor','2019-01-17 12:50:45','1986-04-24','RO.snigdh@gmail.com','Yogesh','Citizenship','45621','','Unmarried,','','Nepali','$2a$10$WMxuRUZ9OVlmTXx9qeUGh.pIKIF/GLWJo79kJZEfRw5Ct0/DOOjrC','Male','Suffix','Shrestha','Mr','2019-01-17 12:50:45','a@gmail.com','a@gmail.com'),(12,'Master','2019-01-20 10:39:51','1990-01-16','HD.snigdh@gmail.com','Monika','Citizenship','53345','','Divorced','','Nepali','$2a$10$oVl/My0F/ZIMOxDD9znD6.3XxpXl3f.Lv1NW8nUc0GT.ygh/LRo3i','Female','Suffix','Sherpa','Mrs','2019-01-20 10:57:35','a@gmail.com','a@gmail.com'),(14,'Bachelor','2019-01-20 10:48:13','1987-05-27','HM.snigdh@gmail.com','Pushan','Citizenship','213453','Raj','Divorced','Raj','Nepali','$2a$10$lDCyATl/gZJcesOJkHawoO54hAqzY42gFX9eVFM1CkCk8aFiOlgaC','Male','Suffix','Maharjan','Mr','2019-01-20 10:57:26','a@gmail.com','a@gmail.com'),(15,'Master','2019-01-20 10:55:44','1983-06-19','ED.snigdh@gmail.com','Ponam ','Citizenship','13515','','Unmarried,','Singh','Nepali','$2a$10$AUPZoGXphAtCiXjrdENw0uWWFl8TGhI4OOL5El26zLtVUglTACuqm','Female','Suffix','Mukhiya','Miss','2019-01-20 10:55:44','a@gmail.com','a@gmail.com'),(16,'Bachelor','2019-01-20 11:10:28','1980-07-22','EM.snigdh@gmail.com','Rajani','Citizenship','131355','','Married,','','Nepali','$2a$10$geOEcvvz.kxvhrrVLetih.3VmedEz.B8UyYIXWeFlDlXS4tCshyB6','Female','Suffix','Shrestha','Mrs','2019-01-20 11:10:28','a@gmail.com','a@gmail.com'),(17,'Intermediate','2019-01-20 11:23:04','1972-09-30','FD.snigdh@gmail.com','Dhiren','Citizenship','4543','','Unmarried,','Kumar','Nepali','$2a$10$kfMoBEGvEeCj6A9TB7PNluXNWkOXfVEmIOaMRl59D36YbzIXYQkyy','Male','Suffix','Rai','Mr','2019-01-20 11:23:04','a@gmail.com','a@gmail.com'),(18,'Bachelor','2019-01-20 11:33:28','1975-07-31','FM.snigdh@gmail.com','Ajit','Citizenship','43135','Jung ','Unmarried,','Jung ','Nepali','$2a$10$PmickjipDQupgIa8pEsXQ.ElAAujDK6ccCAOkVSk142h7Jn/7kT.6','Male','II','Thapa','Mr','2019-01-20 12:34:27','a@gmail.com','a@gmail.com'),(19,'Bachelor','2019-01-20 12:34:09','1998-04-03','FO.snigdh@gmail.com','Sanjee','Citizenship','31354','','Unmarried,','','Nepali','$2a$10$nWtW4IEz2c.AhvHlSEwJ6OZ4ZBJYeyKW03VUXm3KXU47auiqy3tMm','Female','Suffix','Pariyar','Miss','2019-01-20 12:34:09','a@gmail.com','a@gmail.com'),(20,'Be','2019-01-21 10:11:15','','a@gmail.com','arun','Identification Type','123456','','Unmarried,','','','$2a$10$mV433GIbA5ZQP7f8N3kvkuh.qmaRlgfXcVGVw6soVwvDLtOLmFPWG','Select Sex','Suffix','','Mr','2019-01-21 10:11:15','HRD.snigdh@gmail.com','HRD.snigdh@gmail.com'),(21,'Master','2019-01-21 10:14:49','1998-07-10','AFO.snigdh@gmail.com','Sumana','Citizenship','15354','Subedi','Married,','','Nepali','$2a$10$MmByAvhcB6WqB32u4sFv4eugyTX3mQ77BKCm21ab6Lx2CEf9cfiLG','Female','Suffix','Acharya','Mrs','2019-01-21 10:14:49','a@gmail.com','a@gmail.com'),(22,'Bachelor','2019-01-21 12:33:37','1989-02-28','ay@snigdh@gmail.com.np','Arbin','Citizenship','84257','','Unmarried,','','Nepali','$2a$10$FkIvwqWMSnq8hu6UfuzK3.GzCSKsyeMiiO5TRKcb62V1qmXFdm4Pq','Male','Suffix','Yakthumba','Mr','2019-01-21 12:33:37','a@gmail.com','a@gmail.com'),(23,'Bachelor','2019-01-21 12:35:08','1998-12-15','sa@snigdh@gmail.com.np','Sudeep ','Citizenship','659821','','Unmarried,','','Nepali','$2a$10$2YGtgQ3WfK7/rWs4IWh/Be4Prg8DjLQ6YKYTToDKKl2ptwkJtuc5i','Male','Suffix','Adhikari','Mr','2019-01-21 12:35:08','a@gmail.com','a@gmail.com'),(24,'Bachelor','2019-01-21 12:40:14','1998-11-22','st1@snigdh@gmail.com.np','Surendra ','Citizenship','98154','','Married,','','Nepali','$2a$10$iKpsZ1LOEsbGxkxgS57ShuF2/pgXQn6wp.Z5TECctjDC6a0rh9QBi','Male','Suffix','Tmalsina','Mr','2019-01-21 12:40:14','a@gmail.com','a@gmail.com'),(25,'Bachelor','2019-01-21 12:43:18','1989-01-15','na@snigdh@gmail.com.np','Nabin','Citizenship','60548','','Unmarried,','','Nepali','$2a$10$Fb7wf/Bkra5m7DzGtKlGhe9MrUAzd5R6daTpnn/0J4AD1gfhajtUi','Male','Suffix','Adhikari','Mr','2019-01-21 12:43:18','a@gmail.com','a@gmail.com'),(27,'bachelor','2019-01-22 10:03:07','1989-03-27','nt@snigdh@gmail.com','Nawaraj','Citizenship','20315-12035','','Unmarried,','','Nepali','$2a$10$ayof2rwQzb.T4/tVELE4q.q4Ygt3CSdEuS8fVlE716tevSL6997my','Male','Suffix','Thapa','Mr','2019-01-22 10:03:07','a@gmail.com','a@gmail.com'),(28,'Bachelor','2019-01-22 10:12:58','1989-03-04','st@snigdh@gmail.com','Suraj','Citizenship','84568','','Unmarried,','','Nepali','$2a$10$jYnitIbdu6l844E.ZYKtLe8gf9jfc8BN4yS42g2oF0RjnK6Ldbw0m','Male','Suffix','Thapa','Mr','2019-01-22 10:12:58','a@gmail.com','a@gmail.com'),(29,'Bachelor','2019-01-22 10:24:01','1989-02-08','ha@snigdh@gmail.com.np','Hari','Citizenship','102325','','Unmarried,','','Nepali','$2a$10$adUra6pUFKvcmRvnNfSHNeWWeavgRUEjAWgU5zjuRFxqANXU1HILS','Male','Suffix','Adhikari','Mr','2019-01-22 10:24:01','a@gmail.com','a@gmail.com'),(31,'Master','2019-01-22 10:37:17','1988-11-14','sa@snigdh@gmail.com.np','Sujan','Citizenship','65846','','Unmarried,','','Nepali','$2a$10$cqomKHjgzDCQl1tvWuVRLeGHHErGUZdOE/DUjxMhStoq196MKRH8m','Male','Suffix','Aryal','Mr','2019-01-22 10:42:06','a@gmail.com','a@gmail.com'),(32,'Bachelor','2019-01-22 10:44:20','1989-03-03','lt@snigdh@gmail..com.np','Labha','Citizenship','84316','','Unmarried,','','Nepali','$2a$10$pVpmEzYROeLepry3JCIw/.vfaKj1MaalUA9T.v48/ysDVBf4DotoS','Male','Suffix','Thapa','Mr','2019-01-22 10:44:20','a@gmail.com','a@gmail.com'),(33,'Bachelor','2019-01-22 10:57:41','1989-01-20','mb@snigdh@gmail.com.np','Manoj','Citizenship','209452','','Unmarried,','','Nepali','$2a$10$Wsp.akTxM0KLluiaHJR1FuKX9nLSmstBkrUxChvOArj9oDTFf91wa','Male','Suffix','Bajracharya','Mr','2019-01-22 10:57:41','a@gmail.com','a@gmail.com'),(34,'Bachelor','2019-01-22 11:10:03','1988-12-13','mb1@snigdh@gmail.com.np','Mina','Citizenship','63210','Shrestha','Married,','','Nepali','$2a$10$Dmxzi7HJ08/gCkgra6FVd.R9WmfQ1m53Te/QnxUS1Fsr9oOiR6FCO','Female','Suffix','Basnet','Mrs','2019-01-22 11:10:03','a@gmail.com','a@gmail.com'),(35,'Intermediate','2019-01-22 11:38:41','1988-11-23','kb@snigdh@gmail.com.np','Keshab','Citizenship','15783','','Married,','','Nepali','$2a$10$kZ.zUsjrt2kK/k3BVNXpfuOP1vFWEKhN8kLW1aTkC5SQhMriuzP0u','Male','Suffix','Baskota','Mr','2019-01-22 11:38:41','a@gmail.com','a@gmail.com'),(36,'Master','2019-01-22 11:50:56','1989-01-14','sb@snigdh@gmail.com.np','Sibha ','Citizenship','560021','','Unmarried,','','Nepali','$2a$10$w2yVFQdBRVwuFnKnPmf/DOOCF9WG5R1qolwFyLzBdbkQN2C1cq4Sq','Female','Suffix','Basnet','Miss','2019-01-22 11:50:56','a@gmail.com','a@gmail.com'),(37,'Bachelor','2019-01-22 11:58:14','1988-12-04','nb@snigdh@gmail.com.np','Narayan','Identification Type','65745','','Select Marital Status','','Nepali','$2a$10$YNzdvEooo3ZS2PzH4Kp/juH3QSdPhR8YPhCXegAAu2tpMS.wEv5nq','Male','Suffix','Bhattarai','Mr','2019-01-22 11:58:14','a@gmail.com','a@gmail.com'),(38,'Master','2019-01-22 12:21:28','1988-12-29','lb@snigdh@gmail.com.np','Luna','Citizenship','502790','','Married,','','Nepali','$2a$10$edXnSVyE74CzSsYg.b/PPuzkAHOTo2YZv0khyNg5Pad5zBdbnzIwi','Female','Suffix','Bhattarai','Mrs','2019-01-22 12:21:28','a@gmail.com','a@gmail.com'),(39,'Master','2019-01-22 12:34:05','1988-12-27','ab@snigdh@gmail.com.np','Amrit','Citizenship','652458','','Unmarried,','Kumar','Nepali','$2a$10$Goq1Lug1Yyb4ZSq0cQD0OeYenJ5cf6qhhGvUYQGng/N9aF4..84AK','Male','Suffix','Bista','Mr','2019-01-22 12:34:05','a@gmail.com','a@gmail.com');
/*!40000 ALTER TABLE `employee_personal_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:05
