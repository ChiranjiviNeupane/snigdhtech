-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dependent_information`
--

DROP TABLE IF EXISTS `dependent_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dependent_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `children1` varchar(255) DEFAULT NULL,
  `children2` varchar(255) DEFAULT NULL,
  `children3` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `spouse` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `employee_personal_information_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2sdi48nj6q8nuyyysn6mm6ocj` (`employee_personal_information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependent_information`
--

LOCK TABLES `dependent_information` WRITE;
/*!40000 ALTER TABLE `dependent_information` DISABLE KEYS */;
INSERT INTO `dependent_information` VALUES (1,'','','','RAM MANANDHAR','MIRA MANANDHAR','','2019-01-21 10:12:02',1,'a@gmail.com'),(4,'','','','Sanu Maharjan','Sama Maharjan','','2019-01-17 12:29:58',4,NULL),(5,'','','','Able Prasai','Saraita Prasai','','2019-01-17 12:35:06',5,NULL),(6,'','','','Hira Maharjan','Mina Maharjan','','2019-01-17 12:35:45',6,NULL),(7,'','','','Krishna Shrestha','Kalpana Shrestha','','2019-01-17 12:44:00',7,NULL),(8,'','','','','Asha Gurung','','2019-01-17 12:49:34',8,NULL),(9,'','','','Krishna Shrestha','Sapana Shrestha','','2019-01-17 12:50:45',9,NULL),(12,'Aditi Sherpa','','','','Shera Prasai','','2019-01-20 10:56:40',12,'a@gmail.com'),(14,'Aditi Sherpa','','','Prem Maharjan','Roji Maharjan','','2019-01-20 10:57:16',14,'a@gmail.com'),(15,'','','','Shyam Kumar Mukhiya','Champa Mukhiya','','2019-01-21 10:21:11',15,'a@gmail.com'),(16,'X','X','X','x','BISHNU SHRESTHA','X','2019-01-20 11:10:28',16,NULL),(17,'','','','x','x','','2019-01-20 11:23:04',17,NULL),(18,'','','','x','x','x','2019-01-20 11:33:28',18,NULL),(19,'','','','SUMAN THAPA','SANU DEVI THAPA','','2019-01-20 12:34:09',19,NULL),(20,'','','','','','','2019-01-21 10:11:15',20,NULL),(21,'','','','Hari Pd. Acharya','Santi Acharya','Laxmi Acharya','2019-01-21 10:14:49',21,NULL),(22,'','','','Birkha Yakthumba','Chameli Yakthumba','','2019-01-21 12:33:37',22,NULL),(23,'','','','Saurav Adhikari','Parbati Adhikari ','','2019-01-21 12:35:08',23,NULL),(24,'','','','Dev Timilsina','Kanchi Timilsina','Gauri Timilsina','2019-01-21 12:40:14',24,NULL),(25,'','','','Dipendra Adhikari','Santi Adhikari','x','2019-01-21 12:43:18',25,NULL),(27,'','','','Anjan Thapa','Karishma Thapa','','2019-01-22 10:03:07',27,NULL),(28,'','','','Bhemsen Thapa','Santi Thapa','','2019-01-22 10:12:58',28,NULL),(29,'','','','Bishnu Adhikari','Naina Adhikari','','2019-01-22 10:24:01',29,NULL),(31,'','','','Siva Pd. Aryal','Saru Aryal','x','2019-01-22 10:37:17',31,NULL),(32,'','','','Ram Jil Thapa','Kamali Thapa','','2019-01-22 10:44:20',32,NULL),(33,'','','','Kutan Bajracharya','Nutan Bajracharya','','2019-01-22 10:57:41',33,NULL),(34,'Kelav Shrestha','','','','Kumari Basnet','Jiten Shrestha','2019-01-22 11:13:10',34,'a@gmail.com'),(35,'SUSAN BASKOTA','ROMO BASKOTA','','DEVKUMAR BASKOTA','TARA BASKOTA','MIRA BASKOTA','2019-01-22 11:38:41',35,NULL),(36,'','','','Ramesh Basnet','Kumari Basnet','','2019-01-22 11:50:56',36,NULL),(37,'','','','Naran Bhattarai','Bimala Bhattarai','','2019-01-22 11:58:14',37,NULL),(38,'','','','RAM HARI BHATTARAI','SANTAKUMARI BHATTARAI','LAXMAN BHATTARAI','2019-01-22 12:21:28',38,NULL),(39,'','','','AMAR KUMAR BISTA','BIDYA BISTA','','2019-01-22 12:34:05',39,NULL);
/*!40000 ALTER TABLE `dependent_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:04
