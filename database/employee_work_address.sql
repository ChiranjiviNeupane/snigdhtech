-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `work_address`
--

DROP TABLE IF EXISTS `work_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `work_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `ward_no` varchar(255) DEFAULT NULL,
  `employee_personal_information_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq2m4mgxlmfd32tyrnjap9279p` (`employee_personal_information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_address`
--

LOCK TABLES `work_address` WRITE;
/*!40000 ALTER TABLE `work_address` DISABLE KEYS */;
INSERT INTO `work_address` VALUES (1,NULL,'Snigdh',NULL,NULL,'Nakku','2019-01-17 12:06:03',NULL,1,NULL),(4,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-17 12:29:58','13',4,NULL),(5,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-17 12:35:06','13',5,NULL),(6,'Lalitpur','SnigdhTech','Neapal','3','Nakhu','2019-01-17 12:35:45','13',6,NULL),(7,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-17 12:44:00','13',7,NULL),(8,'Lalitpur','SnigdhTech','Nepal','3','Nahku','2019-01-17 12:49:34','13',8,NULL),(9,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-17 12:50:45','13',9,NULL),(12,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 10:39:51','13',12,NULL),(14,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 10:48:13','13',14,NULL),(15,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 10:55:44','13',15,NULL),(16,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 11:10:28','13',16,NULL),(17,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 11:23:04','13',17,NULL),(18,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 11:33:28','13',18,NULL),(19,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-20 12:34:09','13',19,NULL),(20,'','','','sadfaaaaa','','2019-01-21 10:11:15','',20,NULL),(21,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-21 10:14:49','13',21,NULL),(22,'Lalitpur','','Nepal','3','Vaisipati','2019-01-21 12:33:37','13',22,NULL),(23,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-21 12:35:08','13',23,NULL),(24,'Lalitpur','','Nepal','3','Dholahiti','2019-01-21 12:40:14','15',24,NULL),(25,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-21 12:43:18','13',25,NULL),(27,'Lalitpur','','Nepal','3','Nakhipot','2019-01-22 10:03:07','14',27,NULL),(28,'Lalitpur','','Nepal','3','Talchhikhel','2019-01-22 10:12:58','14',28,NULL),(29,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 10:24:01','13',29,NULL),(31,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 10:37:17','13',31,NULL),(32,'Lalitpur','','Nepal','3','Tikhedewal','2019-01-22 10:44:20','14',32,NULL),(33,'Lalitpur','SnigdhTech','Nepal','3','Gabahal','2019-01-22 10:57:41','13',33,NULL),(34,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 11:10:03','13',34,NULL),(35,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 11:38:41','13',35,NULL),(36,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 11:50:56','13',36,NULL),(37,'Kathmandu','SnigdhTech','Nepal','3','Nakhu','2019-01-22 11:58:14','13',37,NULL),(38,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 12:21:28','13',38,NULL),(39,'Lalitpur','SnigdhTech','Nepal','3','Nakhu','2019-01-22 12:34:05','13',39,NULL);
/*!40000 ALTER TABLE `work_address` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:04
