-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `emergency_contact_information`
--

DROP TABLE IF EXISTS `emergency_contact_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `emergency_contact_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_number` varchar(255) DEFAULT NULL,
  `full_address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `relationship` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `employee_personal_information_id` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKka47wmryycb81irl68hfk7nq9` (`employee_personal_information_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emergency_contact_information`
--

LOCK TABLES `emergency_contact_information` WRITE;
/*!40000 ALTER TABLE `emergency_contact_information` DISABLE KEYS */;
INSERT INTO `emergency_contact_information` VALUES (1,'9814523416','THAMEL,KATHMANDU','RAM MANANDHAR','Father','2019-01-21 10:11:32',1,'a@gmail.com'),(4,'9860456484','chamati, Kathmandu',NULL,'Mother','2019-01-17 12:29:58',4,NULL),(5,'9841559765','NAKHIPOT-14, LALITPUR','ABLE PRASAI','Father','2019-01-21 10:15:32',5,'a@gmail.com'),(6,'9849526475','Swambu,Kathmandu',NULL,'Father','2019-01-17 12:35:45',6,NULL),(7,'9851145685','Balaju,Kathmandu',NULL,'Father','2019-01-17 12:44:00',7,NULL),(8,'061-465674','PARDI BIRAUTA-17, POKHARA','ASHA GURUNG','Mother','2019-01-21 10:13:48',8,'a@gmail.com'),(9,'9841981654','Thamel, Kathmandu','KRISHNA SHRESTHA','Father','2019-01-21 10:20:15',9,'a@gmail.com'),(12,'9849851354','KUPONDOL, LALITPUR','SHERA PRASAI','Mother','2019-01-21 10:16:54',12,'a@gmail.com'),(14,'9841756421','DHAPAKHEL, LALITPUR','PREM MAHARJAN','Father','2019-01-21 10:18:59',14,'a@gmail.com'),(15,'9846052146','JHAPA,BHARATPUR','SHYAM KUMAR MUKHIYA','Father','2019-01-21 10:18:09',15,'a@gmail.com'),(16,'9841755864','LUBU,LALITPUR','BISHNU SHRESTHA','Mother','2019-01-21 10:19:26',16,'a@gmail.com'),(17,'7','x',NULL,'x','2019-01-20 11:23:04',17,NULL),(18,'x','x',NULL,'x','2019-01-20 11:33:28',18,NULL),(19,'9851046526','TALCHHIKHEL,LALITPUR',NULL,'Father','2019-01-20 12:34:09',19,NULL),(20,'','',NULL,'','2019-01-21 10:11:15',20,NULL),(21,'9851025789','Hetauda, Makwanpur',NULL,'Father','2019-01-21 10:14:49',21,NULL),(22,'980852374','Vaisipati,Lalitpur',NULL,'Father','2019-01-21 12:33:37',22,NULL),(23,'9851025254','VAISIPATI,LALITPUR',NULL,'Father','2019-01-21 12:35:08',23,NULL),(24,'9808546134','Gotikhel,Lalitpur',NULL,'Father','2019-01-21 12:40:14',24,NULL),(25,'9851016327','TALCHHIKHEL,LALITPUR',NULL,'Father','2019-01-21 12:43:18',25,NULL),(27,'9851109657','Nakhipot-14,Lalitpur',NULL,'Father','2019-01-22 10:03:07',27,NULL),(28,'9841258545','Talchhikhel, Lalitpur',NULL,'Father','2019-01-22 10:12:58',28,NULL),(29,'9851139510','IMADOLE, LALITPUR','Bishnu Adhikari','Father','2019-01-22 10:26:27',29,'a@gmail.com'),(31,'9851045852','THAMEL, KATHMANDU',NULL,'Father','2019-01-22 10:37:17',31,NULL),(32,'9841250750','Tikhidewol.,Lalitpur',NULL,'Father','2019-01-22 10:44:20',32,NULL),(33,'9851098884','GABAHAL,LALITPUR',NULL,'Father','2019-01-22 10:57:41',33,NULL),(34,'9841042154','KOTESWOR,KATHMANDU','JITEN SHRESTHA','Husband','2019-01-22 11:12:57',34,'a@gmail.com'),(35,'9846325105','PHIDIM,PACHTHAR',NULL,'Father','2019-01-22 11:38:41',35,NULL),(36,'9851098664','TALCHHIKHEL,LALITPUR',NULL,'Father','2019-01-22 11:50:56',36,NULL),(37,'9846254878','MIRMEE,SANGJA',NULL,'Father','2019-01-22 11:58:14',37,NULL),(38,'9851056070','THAPATHALI,KATHMANDU',NULL,'Husband','2019-01-22 12:21:28',38,NULL),(39,'9851102487','DHOLAHITI,LALITPUR',NULL,'Father','2019-01-22 12:34:05',39,NULL);
/*!40000 ALTER TABLE `emergency_contact_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 10:29:02
