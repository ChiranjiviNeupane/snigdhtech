-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: employee
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `other_information`
--

DROP TABLE IF EXISTS `other_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `other_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `panNumber` varchar(45) DEFAULT NULL,
  `drivingLiscenceNumber` varchar(45) DEFAULT NULL,
  `insurancePolicyNumber` varchar(45) DEFAULT NULL,
  `otherProfessionalTrainingCourses` varchar(45) DEFAULT NULL,
  `pastOrganisations` varchar(45) DEFAULT NULL,
  `majorAlergyOrSickness` varchar(45) DEFAULT NULL,
  `preferences` varchar(45) DEFAULT NULL,
  `employee_personal_information_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_personal_information_id_UNIQUE` (`employee_personal_information_id`),
  KEY `fk_other_information_employee_personal_information1_idx` (`employee_personal_information_id`),
  CONSTRAINT `fk_other_information_employee_personal_information1` FOREIGN KEY (`employee_personal_information_id`) REFERENCES `employee_personal_information` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_information`
--

LOCK TABLES `other_information` WRITE;
/*!40000 ALTER TABLE `other_information` DISABLE KEYS */;
INSERT INTO `other_information` VALUES (1,'1213134878','898979797','131313132',NULL,'qwert','N/A',NULL,2);
/*!40000 ALTER TABLE `other_information` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-20 12:05:16
