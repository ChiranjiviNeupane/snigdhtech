

///////////////////////////////////Start of Requesting a travel //////////////////////////////////////////////////////////

$(document).ready(function () {

	$('.add-more-wrapper').multifield({
		section: '.add-more-section',
		btnAdd:'.add-more-add__btn',
	    btnRemove:'.add-more-remove__btn',
	    max: 5
	});

	
	
	$('.travel__itinerary-add').multifield({
		section: '.travel__itinerary-section',
		btnAdd:'.add-itinerary__btn',
	    btnRemove:'.remove-itinerary__btn',
	    max: 8
	    

	  
	});
	
	$('.travel__advance-add').multifield({
		section: '.travel__advance-section',
		btnAdd:'.add-advance__btn',
	    btnRemove:'.remove-advance__btn',
	    max: 5
	});
	
		
	main.ajax("hrm/attendance/myAttendanceStatus", "",function(response){
		console.log(response);
		$('#fullName').val(response.name);
		$('#department').val(response.department);
		$('#position').val(response.position);
		
		
		checkPlaceholderValue()
	},"GET",main.getToken());
	
	main.ajax( "/hrm/costCenter/findall","",function(response){   
		var select = document.getElementById("costCenter");
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			/*el.value = element.id;*/
			el.value = element.code + "--" + element.name;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	main.ajax( "/hrm/sourceOfFund/findAllListing","",function(response){   
		var select = document.getElementById("sourceOfFund");
		console.log(response);
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.code + "--" + element.name;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	main.ajax( "/hrm/project/findAllListing","",function(response){   
		var select = document.getElementById("project");
		console.log(response);
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.code + "--" + element.name;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	main.ajax( "/hrm/activity/findAllListing","",function(response){   
		var select = document.getElementById("activity");
		console.log(response);
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.code + "--" + element.name;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	
});

$(document).on('click', '.add-more-remove__btn', function () {
	console.log("delete");
	setTimeout(function () {
		calculateAmount();
		}, 500);
	
});


//for advanced cost
function calculateAmount(){
	var total=0;
	
	$('.amount').each(function() {
		
		
		if($(this).val()!=""){
	    	total=total + parseInt($(this).val());	
	    	console.log("clicked");
	    	console.log(total);
	    	
		}	
    });
	$('#totalAdvance').text(total);
}	




function requestTravel() {
	
		$("#travelForm").validate({
			rules: {
				purpose: "required",
				placeOfTravel: "required",
				dateFrom: "required",
				dateTo: "required",
				duration: "required"      	
	        	
	        },
	        messages: {
	        	purpose: "Please specify your Place Of Purpose",
	        	placeOfTravel: "Please specify your Place Of Travel",
	        	dateFrom: "Please specify Date",
	        	dateTo: "Please specify Date",
	        	duration: "Please specify Duration"
	        	

	        }
			
	    })
	    
	    
	    var i=0;
	    $('.itenary.R').each(function() {
	    	
	    	$(this).attr('name', 'itenary'+i);
	    	
	    	i=i+1; 
	    });
	    
	    var j=0;
	    $('.air.R').each(function() {
	    	console.log("esma ta aauxa");
	    	$(this).attr('name', 'air'+j);
	    	
	    	j=j+1; 
	    });
	    
	    var k=0;
	    $('.vehicle.R').each(function() {
	    	
	    	$(this).attr('name', 'vechicle'+k);
	    	
	    	k=k+1; 
	    });
	    
	    var l=0;
	    $('.advance.R').each(function() {
	    	
	    	$(this).attr('name', 'advance'+l);
	    	
	    	l=l+1; 
	    });

	    

	//for Air Ticket fields
	var airTicketArr1=[];
		
	        if($('#airTicketCheckbox').prop("checked") == true){
	        	console.log("hello ayo airticket");
	        	$('.air.R').each(function() {
	        		console.log("air wala");
	                $(this).rules('add', {
	                    required: true,                    
	                    messages: {
	                        required:  "This Field is Required*",                     
	                    }
	                });
	            });
	        	
	        	
	        	
	        	
	        	
	        	$('.air').each(function(){	
	        		console.log("hy");
	        		airTicketArr1.push($(this).val());
	        	    });
	        		  	
	        }
	          
	     
	var airTicketArr2=[];
	
	       var airLength = airTicketArr1.length;
	       for(var i = 0 ; i < airLength ; i += 8){
	    	   var obj = {};
	    	   obj["travellerFullName"] = airTicketArr1[i];
	    	   obj["flightFrom"] = airTicketArr1[i+1];
	    	   obj["flightTo"] = airTicketArr1[i+2];
	    	   obj["departureDate"] = airTicketArr1[i+3];
	    	   obj["departureTime"] = airTicketArr1[i+4];
	    	   obj["returnDate"] = airTicketArr1[i+5];
	    	   obj["returnTime"] = airTicketArr1[i+6];
	    	   obj["remarks"] = airTicketArr1[i+7];
	    	   airTicketArr2.push(obj);
	    	   	
	       }
	    	   
	    console.log(airTicketArr2);
	  //for Air Ticket fields ends here   
	
	
	  //for vehicle Ticket fields
	    var vehicleTicketArr1=[];
	    	
	            if($('#vehicleCheckbox').prop("checked") == true){
	            	console.log("hello ayo airticket");
	            	$('.vehicle.R').each(function() {
	            		console.log("vehicle wala");
	                    $(this).rules('add', {
	                        required: true,                    
	                        messages: {
	                            required:  "This Field is Required*",                     
	                        }
	                    });
	                });
	            	
	            	
	            	
	    	        	$('.vehicle').each(function(){	
	    	        		console.log("hy");
	    	        		vehicleTicketArr1.push($(this).val());
	    	        	    });
	            		  	
	            }
	              
	         
	    var vehicleTicketArr2=[];
	
	           var vehicleLength = vehicleTicketArr1.length;
	           for(var i = 0 ; i < vehicleLength ; i += 7){
	        	   var obj = {};
	        	   obj["travellerFullName"] = vehicleTicketArr1[i];
	        	   obj["travelFrom"] = vehicleTicketArr1[i+1];
	        	   obj["travelTo"] = vehicleTicketArr1[i+2];
	        	   obj["date"] = vehicleTicketArr1[i+3];
	        	   obj["time"] = vehicleTicketArr1[i+4];
	        	   obj["estimatedTime"] = vehicleTicketArr1[i+5];
	        	   obj["remarks"] = vehicleTicketArr1[i+6];
	        	   vehicleTicketArr2.push(obj);
	        	   	
	           }
	        	 
	        console.log("vehical data below");
	        console.log(vehicleTicketArr1);
	        console.log(vehicleTicketArr2);
	      //for vehicle Ticket fields ends here   
	
	
	
		
	//for itenary fields
	var itenaryArr1=[];
		
	        if($('#itenaryCheckbox').prop("checked") == true){
	        	console.log("hello ayo");
	        	$('.itenary.R').each(function() {
	        		console.log("itenary wala");
	                $(this).rules('add', {
	                    required: true,                    
	                    messages: {
	                        required:  "This Field is Required*",                     
	                    }
	                });
	            });
	        	
	        	
	        	
		        	$('.itenary').each(function(){	
		        		console.log("hy");
		        		itenaryArr1.push($(this).val());
		        	    });
	        		   	
	        }
	          
	     
	var itenaryArr2=[];
	
	       var itenaryLength = itenaryArr1.length;
	       for(var i = 0 ; i < itenaryLength ; i += 5){
	    	   var obj = {};
	    	   obj["fromDestination"] = itenaryArr1[i];
	    	   obj["toDestination"] = itenaryArr1[i+1];
	    	   obj["fromDate"] = itenaryArr1[i+2];
	    	   obj["toDate"] = itenaryArr1[i+3];
	    	   obj["remarks"] = itenaryArr1[i+4];
	    	   itenaryArr2.push(obj);
	    	   	
	       }
	    	   
	    
	  //for itenary fields ends here       
	
	       
   	//for budget Code fields
   	var budgetArr1=[];
       	
    	$('.Budget').each(function(){	
    		console.log("value of budget codes is "+$(this).val());
    		budgetArr1.push($(this).val());
    	    });	
   	    
   	          
   	     
   	var budgetArr2=[];
   	
   	       var budgetLength = budgetArr1.length;
   	       for(var i = 0 ; i < budgetLength ; i += 5){
   	    	   var obj = {};
   	    	   obj["costCenter"] = budgetArr1[i];
   	    	   obj["sourceOfFund"] = budgetArr1[i+1];
   	    	   obj["project"] = budgetArr1[i+2];
   	    	   obj["activity"] = budgetArr1[i+3];
   	    	   obj["analysis"] = budgetArr1[i+4];
   	    	   budgetArr2.push(obj);
   	    	   	
   	       }
   	    	   
   	    console.log("below is buget code array");
   	    console.log(budgetArr1);
   	  //for Budget Code fields ends here   
	       
	       
	       
	       
	       
	    
	  //for Advanced Cost fields
	    var advanceArr1=[];
	    	
	            if($('#advanceCost').prop("checked") == true){
	            	console.log("hello ayo advanced");
	            	$('.advance.R').each(function() {
	            		console.log("advance wala");
	                    $(this).rules('add', {
	                        required: true,                    
	                        messages: {
	                            required:  "This Field is Required*",                     
	                        }
	                    });
	                });
	            	
	            	
	            	
	    	        	$('.advance').each(function(){	
	    	        		console.log("hy");
	    	        		advanceArr1.push($(this).val());
	    	        	    });
	            		  	
	            }
	              
	           console.log(advanceArr1);
	           
	    var advanceArr2=[];
	    var totalAdvanceCost=0;
	
	           var advancedLength = advanceArr1.length;
	           for(var i = 0 ; i < advancedLength ; i += 4){
	        	   var obj = {};
	        	   obj["particular"] = advanceArr1[i];
	        	   obj["currency"] = advanceArr1[i+1];
	        	   obj["estimatedAmount"] = advanceArr1[i+2];
	        	   totalAdvanceCost=totalAdvanceCost + parseInt(advanceArr1[i+2]);
	        	   obj["remarks"] = advanceArr1[i+3];
	        	   advanceArr2.push(obj);
	        	   	
	           }
	        	   
	        console.log(advanceArr2);
	        console.log(totalAdvanceCost);
	       /* $('#totalAdvance').text(totalAdvanceCost);*/
	      //for Advanced Cost fields ends here    
	    
   
	    
	var data ={
			   type: $("input[name='checkBoxField']:checked").val(),
			   purpose: $('#purpose').val(),
			   placeOfTravel: $('#placeOfTravel').val(),
			   team: $('#teamMember').val(),
			   dateFrom: $('#dateFrom').val(),
			   dateTo: $('#dateTo').val(),
			   duration: $('#duration').val(),
			   totalAdvanceCost: totalAdvanceCost ,
			   totalAdvanceCostInWords: "Twenty Thousand Rupees only",
			   travelItenarys: itenaryArr2,
			   travelAirTickets: airTicketArr2,
			   travelVehicles: vehicleTicketArr2,
			   travelAdvances: advanceArr2,
			   budgetCodeSamples : budgetArr2,
			   budgetCodeComment: $('#budgetCodeComment').val(),
			   
			 }
	
	console.log("data is");
	console.log(data);
	if($("#travelForm").valid()){
		console.log(data);
		
		extraMain.ajax("/hrm/travel/request/save" ,JSON.stringify(data),function(response){	
			$('#myTravelTable').DataTable().ajax.reload();
			toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
		},
		function(response){
			toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
		},"POST",main.getToken());
		
		
	}else{
		alert("check Errors and try again");
	}

}
//fixing placeholder
function checkPlaceholderValue() {

	$("input").each(function() {

		if ($(this).val() == "") {
			$(this).removeClass('form-input-character-on');
		} else {

			$(this).addClass('form-input-character-on');
		}
	});
}

///////////////////////////////////End of Requesting a travel //////////////////////////////////////////////////////////

///////////////////////////////////Start of my travel////////////////////////////////////////////////////////////////////
jQuery(function($) {
	var roleTable = $('#myTravelTable')

			.DataTable(
					
					{
						"responsive" : true,

						"columnDefs" : [ {
							"targets" : [ 0 ],
							"visible" : false
						},

						],

						"order" : [ 0, "desc" ],
						/* "paging": false, */

						"ajax" : {
							"url" : "/hrm/travel/request/myTravel",
							"dataSrc" : "",
							"beforeSend" : function(request) {
								request.setRequestHeader(
										"Authorization", main.getToken());
							},
						},

						"columnDefs" : [
								{
									"targets" : [ 0 ],
									"visible" : false
								},
								{
									targets : 5,
									render : function(data, type, row) {
										var color = 'black';
										if (data == "Pending") {
											color = 'badge badge-pill badge-warning';
										}
										if (data == "Approved") {
											color = 'badge badge-pill badge-success';
										}
										if (data == "Rejected") {
											color = 'badge badge-pill badge-danger';
										}
										if (data == "Cancelled") {
											color = 'badge badge-pill badge-secondary';
										}
										return '<span class="' +  color + '">'
												+ data + '</span>';

									}
								} ],

						"columns" : [ {
								"data":"id"
						},
						{
								sortable : false,
								"render" : function(data, meta, full) {								
									return full.placeOfTravel
							}
						},
						{
							sortable : false,
							"render" : function(data, meta, full) {
								return full.dateFrom+" TO "+full.dateTo
								
							}
							
						},					
						{
							sortable : false,
							"render" : function(data, meta, full) {
								return full.duration
								
							}
						}, {
							sortable : false,
							"render" : function(data, meta, full) {
								return full.purpose
								
							}
							
						}, {
							"data":"overallStatus"
						},{
							sortable : false,
							"render" : function(data, meta, full, row) {
								if(full.requestStatus=="Cancelled" || full.requestStatus == "Rejected"){	
									return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item'"+
									"onClick='viewMyTravels("
								    + JSON.stringify(full)
								    + ")'>View</a></div></div>"
								}else{
									return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item' onClick='viewMyTravels("
									+ JSON.stringify(full)
									+ ")'>View</a><a class='dropdown-item' onClick='extendTravel("
									+ JSON.stringify(full)
									+ ")'>Extend Travel</a><a class='dropdown-item' onClick='cancelTravel("
									+ JSON.stringify(full)
									+ ")'>Cancel Travel</a></div></div>"
									
								}
								
							}
						} 
						]
					});
})

//get current date for checking extend leave
function getTodaysDate(){
	var d1 = new Date();	
	return d1;
}

function viewMyTravels(data){
	 $('#travelinfo_modal').modal('show'); 
	 getTravelDetails(data);
	 
} 

function extendTravel(response){
	$('#travelextend_modal').modal('show'); 
	getTravelDetails(response);
	
	
	
	
	$( "#extendButton" ).click(function() {
			
		//for extend itenary
		 var i=0;
		    $('.Eitenary.R').each(function() {
		    	
		    	$(this).attr('name', 'Eitenary'+i);		    	
		    	i=i+1; 
		    });
		    
		    
		    var j=0;
		    $('.Eadvance.R').each(function() {
		    	
		    	$(this).attr('name', 'Eadvance'+j);
		    	
		    	j=j+1; 
		    });
		
		console.log(i);
		
		
		

		$("#extendItenaryf").validate({		
			
	    })
		
	   
	    
	    var extendItenaryarr=[];
		
		if($('#extendItenaryCheckbox').prop("checked") == true){
        	console.log("extended itenary");
        	$('.Eitenary.R').each(function() {
        		console.log("b");
               $(this).rules('add', {
                    required: true,                    
                    messages: {
                        required:  "This Field is Required*",                     
                    }
                });
            });
        	
        	
		    	$('.Eitenary').each(function(){	
				console.log("hy");
				extendItenaryarr.push($(this).val());
			    });
			   	
		}
  

		var extendedItenaryArr2=[];
		
		var itenaryLength = extendItenaryarr.length;
		for(var i = 0 ; i < itenaryLength ; i += 5){
		   var obj = {};
		   obj["fromDestination"] = extendItenaryarr[i];
		   obj["toDestination"] = extendItenaryarr[i+1];
		   obj["fromDate"] = extendItenaryarr[i+2];
		   obj["toDate"] = extendItenaryarr[i+3];
		   obj["remarks"] = extendItenaryarr[i+4];
		   extendedItenaryArr2.push(obj);
		   	
		}
	//for extend advance
		
/*$("#extendadvancef").validate({		
			
	    })*/
		
		
var extendadvancearr=[];
		
		if($('#extendAdvanceCost').prop("checked") == true){
        	console.log("extended advance");
        	$('.Eadvance.R').each(function() {
        		console.log("b");
               $(this).rules('add', {
                    required: true,                    
                    messages: {
                        required:  "This Field is Required*",                     
                    }
                });
            });
        	
        	
		    	$('.Eadvance').each(function(){	
				console.log("hy");
				extendadvancearr.push($(this).val());
			    });
			   	
		}
  

		var extendadvancearr2=[];
		
		var totalExtendAdvanceCost=0;
		
        var advancedLength = extendadvancearr.length;
        for(var i = 0 ; i < advancedLength ; i += 4){
     	   var obj = {};
     	   obj["particular"] = extendadvancearr[i];
     	   obj["currency"] = extendadvancearr[i+1];
     	   obj["estimatedAmount"] = extendadvancearr[i+2];
     	   totalExtendAdvanceCost=totalExtendAdvanceCost + parseInt(extendadvancearr[i+2]);
     	   obj["remarks"] = extendadvancearr[i+3];
     	  extendadvancearr2.push(obj);
     	   	
        }
			
		
		data={
				extendTotalAdvance:totalExtendAdvanceCost,
				travelAdvances:extendadvancearr2,
				travelItenarys:extendedItenaryArr2,
				extendRemarks:$('#extendRemarks').val(),
				extendTill: $('#extendTillDate').val(),
		}
		
		console.log(data);
		if($("#extendItenaryf").valid()){
			alert("valid");
			extraMain.ajax("/hrm/travel/request/update/"+response.id ,JSON.stringify(data),function(response){	
				$('#myTravelTable').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"PUT",main.getToken());
			
			
		}else{
			alert("Check Errors");
		}
		
		
		
	});
	
	
}

function cancelTravel(response){
	
	$('#travelcancel_modal').modal('show'); 
	getTravelDetails(response);
	
	
	
	$( "#cancelButton" ).click(function() {
		console.log("cancel vayo");
		console.log(response);
		
		
		data ={
				overallStatus:"Cancelled",
				cancelRemarks: $('#cancelRemarks').val(),
		}
		extraMain.ajax("/hrm/travel/request/update/"+response.id ,JSON.stringify(data),function(response){	
			$('#myTravelTable').DataTable().ajax.reload();
			toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
		},
		function(response){
			toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
		},"PUT",main.getToken());
		
		
		});
	
	
	
	
	
}






function getTravelDetails(response){
	console.log(response);
	
	$('.travelDetails').empty();
	
	var generalRow = $('<div class="mt-4 table-responsive"><h5 class="text-center">Travel Basic Information</h5><div class="table-responsive"><table id="basicInfo" class="table table-bordered table-sm my-3">'+
			 			 '<tbody><tr><th scope="row">Travel Type: </th><td>'+response.type+'</td><th scope="row">Travel Request No: </th><td>'+response.id+'</td></tr>'+
		                 '<tr><th scope="row">Name Of Travel: </th><td>Daniel Prasai</td><th scope="row">Place Of Travel: </th><td>'+response.placeOfTravel+'</td></tr>'+
		                 '<tr><th scope="row">Travel Date From: </th><td>'+response.dateFrom+'</td><th scope="row">Travel Date To: </th><td>'+response.dateTo+'</td></tr>'+
		                 '<tr><th scope="row">Purpose of Travel </th><td colspan='+"3"+'>'+response.purpose+'</td></tr></tbody></table></div></div>');
	$('.travelDetails').append(generalRow);
	
	
	if(response.extendTill!=null){
		var extendRow = $('<tr><th scope="row">ExtendedTill: </th><td>'+response.extendTill+'</td><th scope="row">Extended On: </th><td>'+response.extendOn+'</td></tr> ');
		$('#basicInfo').append(extendRow);
	}
	
	if(response.cancelOn!=null && response.cancelResponseDate!=null){
		var cancel1Row = $('<tr><th scope="row">'+"Cancel-Request"+response.cancelResponse+" On"+'</th><td colspan='+"3"+'>'+response.cancelOn+'</td></tr>');
		$('#basicInfo').append(cancel1Row);
	}else if(response.cancelOn!=null && response.cancelResponseDate==null){
		var cancel2Row = $('<tr><th scope="row">'+"Canceled On"+'</th><td colspan='+"3"+'>'+response.cancelOn+'</td></tr>');
		$('#basicInfo').append(cancel2Row);
	}
	
	
	var budgetCodeTableRow = $('<div class="mt-5"><h5 class="text-center"> TRAVEL BUDGET CODE</h5><div class="table-responsive">'+
                			'<table id="budgetCodeTableData" class="table table-bordered table-sm my-3"><thead class="thead-light"><tr><th scope="col">Office</th><th scope="col">Source Of Fund</th>'+
                            '<th scope="col">Project</th><th scope="col">Activity</th><th scope="col">Analysis</th> </tr></thead><tbody></tbody></table></div></div>');
	
	$('.travelDetails').append(budgetCodeTableRow);
	
	(response.budgetCodeSamples).forEach(function(element){
		var budgetCodeRow = $('<tr><th scope="row">'+element.costCenter+'</th><td>'+element.sourceOfFund+'</td><td>'+element.project+'</td><td>'+element.activity+'</td><td>'+element.analysis+'</td></tr>');
		
		$('#budgetCodeTableData tbody').append(budgetCodeRow);
	
	});
	
	
	if((response.travelAirTickets).length!=0){
		var airTicketTableRow = $('<div class="mt-5"><h5 class="text-center">Air Ticket</h5><div class="table-responsive"><table id="airTicketTable" class="table table-bordered table-sm my-3"><thead class="text-center thead-light"><tr>'+
	                            '<th scope="col">Full Name</th><th scope="col" colspan="2">Sector Of Flight</th><th scope="col" colspan="4">Date and Time</th><th scope="col">Remarks</th></tr></thead>'+
	                            '<tbody><tr><th scope="col"></th><th scope="col">From</th><th scope="col">To</th><th scope="col">Departure Date</th>'+
	                            '<th scope="col">Departure Time</th><th scope="col">Return Date</th><th scope="col">Return Time</th><th scope="col"></th></tr></tbody></table></div></div>');
	        
		$('.travelDetails').append(airTicketTableRow);
		
		(response.travelAirTickets).forEach(function(element){
	        var airTicketRow = $('<tr><td scope="row">'+element.travellerFullName+'</td><td scope="row">'+element.flightFrom+'</td><td>'+element.flightTo+'</td>'+
	                			 '<td>'+element.departureDate+'</td><td>'+element.departureTime+'</td><td>'+element.returnDate+'</td><td>'+element.returnTime+'</td><td>'+element.remarks+'</td></tr>');
		
	        $('#airTicketTable tbody').append(airTicketRow);
		});
	}
	
	
	if((response.travelVehicles).length!=0){
		
		var vehicleTicketTableRow = $('<div class="mt-5"><h5 class="text-center">Vehicle Way</h5><div class="table-responsive"><table id="vehicleTicketTable" class="table table-bordered table-sm my-3"><thead class="text-center thead-light">'+
                         '<tr><th scope="col">Full Name</th><th scope="col" colspan="2">Route Of Travel</th><th scope="col" colspan="2">Date and Time</th><th scope="col">Est Time</th><th scope="col">Remarks</th></tr></thead>'+
                        '<tbody><tr><th scope="col"></th><th scope="col">From</th><th scope="col">To</th><th scope="col">Date</th><th scope="col">Time</th><th scope="col"></th><th scope="col"></th></tr></tbody></table></div></div>');
		
		$('.travelDetails').append(vehicleTicketTableRow);
           
		(response.travelVehicles).forEach(function(element){
            var vehicleTicketRow= $('<tr><td scope="row">'+element.travellerFullName+'</td><td scope="row">'+element.travelFrom+'</td><td>'+element.travelTo+'</td><td>'+element.date+'</td><td>'+element.time+'</td><td>'+element.estimatedTime+'</td><td>'+element.remarks+'</td></tr>');
           
            $('#vehicleTicketTable tbody').append(vehicleTicketRow);
		});
		
		
	}
	
	
	if((response.travelItenarys).length!=0){
	
	var itenaryTableRow = $('<div class="mt-5"><h5 class="text-center">Itinerary</h5><div class="table-responsive"><table id="itenaryTable" class="table table-bordered table-sm my-3"><thead class="text-center thead-light">'+
                     '<tr><th scope="col" colspan="2">Place</th><th scope="col" colspan="2">Date</th><th scope="col">Remarks</th></tr></thead>'+
                     '<tbody><tr><th scope="row">From</th><th>To</th><th>From</th><th>To</th><th></th><th></th></tr></tbody></table></div></div>');
	
	$('.travelDetails').append(itenaryTableRow);
	
	(response.travelItenarys).forEach(function(element){
         var itenaryRow = $('<tr><td scope="row">'+element.fromDestination+'</td><td>'+element.toDestination+'</td><td>'+element.fromDate+'</td><td>'+element.toDate+'</td><td>'+element.remarks+'</td></tr>');
         
	
         $('#itenaryTable tbody').append(itenaryRow);
	
		});
	}
	
	if(response.totalAdvanceCost!=0){
		var travelAdvanceRow = $('<div class="mt-5"><h5 class="text-center">TRAVEL ADVANCE</h5><div class="table-responsive"><table class="table table-bordered table-sm my-3"><tbody>'+
								'<tr><th scope="row">Advance</th><td>NPR</td><td>'+response.totalAdvanceCost+'</td><td>'+response.totalAdvanceCostInWords+'</td></tr></tbody></table></div></div>');
	         
		$('.travelDetails').append(travelAdvanceRow);
	}
	
	
	//Authorization
	var travelAuthorizationTableRow = $('<div class="mt-5"><h5 class="text-center">Travel Authorization</h5>'+
            '<div class="table-responsive"><table class=" authorizationTable table table-bordered table-sm my-3"><tbody></tbody></table></div></div>'); 
	
	$('.travelDetails').append(travelAuthorizationTableRow);
        
	
    var travelAuthorizationRequestRow =$('<tr><td scope="row">Requested By: </th><td>(Daniel Prasai)</td><td scope="row">Requested on</th><td>'+response.createdAt+'</td></tr>');
    
    $('.authorizationTable').append(travelAuthorizationRequestRow);


    if(response.recommendDate!=null){
	    var travelAuthorizationRecommendRow =$('<tr><td scope="row">Recommend By: </th><td>'+response.recommendBy+'</td><td scope="row">Recommended On</th><td>'+response.recommendDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationRecommendRow);
    }
    
    if(response.securityClearanceResponse!="Pending"){
	    var travelAuthorizationSecurityRow =$('<tr><td scope="row">'+"Security "+response.securityClearanceResponse+" By"+'</th><td>'+response.securityClearanceResponseBy+'</td><td scope="row">'+response.securityClearanceResponse+ " On"+'</th><td>'+response.securityClearanceResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationSecurityRow);
    }
    
    if(response.securityClearanceResponseRemarks==null || response.securityClearanceResponseRemarks==""){
	  }else{
		  var securityRemarksRow= $('<tr><td scope="row">Security Remarks: </th><td colspan='+"3"+'>'+ response.securityClearanceResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( securityRemarksRow);		  
	  }
    
    
    if(response.administratorResponse!="Pending"){
	    var travelAuthorizationAdminRow =$('<tr><td scope="row">'+"Administration "+response.administratorResponse+" By"+'</th><td>'+response.administratorResponseBy+'</td><td scope="row">'+response.administratorResponse+ " On"+'</th><td>'+response.administratorResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationAdminRow);
    }
    
    if(response.administratorResponseRemarks==null || response.administratorResponseRemarks==""){	  
	  }else{
		  var adminRemarksRow= $('<tr><td scope="row">Admin Remarks: </th><td colspan='+"3"+'>'+ response.administratorResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( adminRemarksRow);
	  }
    
    if(response.financeResponse!="Pending"){
	    var travelAuthorizationAFinanceRow =$('<tr><td scope="row">'+"Finance "+response.financeResponse+" By"+'</th><td>'+response.financeResponseBy+'</td><td scope="row">'+response.financeResponse+ " On"+'</th><td>'+response.administratorResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationAFinanceRow);
    }
    
    if(response.financeResponseRemarks==null || response.financeResponseRemarks==""){	  
	  }else{
		  var FinanceRemarksRow= $('<tr><td scope="row">Finance Remarks: </th><td colspan='+"3"+'>'+ response.financeResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( FinanceRemarksRow);
	  }
    
    if(response.lineManagerResponse!="Pending"){
	    var travelAuthorizationALineManagerRow =$('<tr><td scope="row">'+"LineManager "+response.lineManagerResponse+" By"+'</th><td>'+response.lineManagerResponseBy+'</td><td scope="row">'+response.lineManagerResponse+ " On"+'</th><td>'+response.lineManagerResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationALineManagerRow);
    }
    
    if(response.lineManagerResponseRemarks==null || response.lineManagerResponseRemarks==""){	
	  }else{
		  var lineManagerRemarksRow= $('<tr><td scope="row">LineManager Remarks: </th><td colspan='+"3"+'>'+ response.lineManagerResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( lineManagerRemarksRow);
	  }
    
    //extend authorization
    if(response.extendRemarks==null || response.extendRemarks==""){	
	  }else{
		  var extendRemarksRow= $('<tr><td scope="row">Traveller Extend Remarks: </th><td colspan='+"3"+'>'+ response.extendRemarks+'</th></tr>');
		  $('.authorizationTable').append( extendRemarksRow);
	  }
    
    
    if(response.securityClearanceExtendResponse==null || response.securityClearanceExtendResponse=="Pending"){    
    }else{
    	var travelAuthorizationExtendSecurityRow =$('<tr><td scope="row">'+"Extended Security"+response.securityClearanceExtendResponse+" By"+'</th><td>'+response.securityClearanceExtendResponseBy+'</td><td scope="row">'+response.securityClearanceExtendResponse+ " On"+'</th><td>'+response.securityClearanceExtendResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationExtendSecurityRow);
    }
    
    if(response.securityClearanceExtendResponseRemarks==null || response.securityClearanceExtendResponseRemarks==""){
	  }else{
		  var securityExtendRemarksRow= $('<tr><td scope="row">Extend Security Remarks: </th><td colspan='+"3"+'>'+ response.securityClearanceExtendResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( securityRemarksRow);		  
	  }
    
    //extend travel not for admin
    
    if(response.financeExtendResponse=="Pending" || response.financeExtendResponse==null){    
    }else{
    	var travelAuthorizationExtendFinanceRow =$('<tr><td scope="row">'+"Extend Finance "+response.financeExtendResponse+" By"+'</th><td>'+response.financeExtendResponseBy+'</td><td scope="row">'+response.financeExtendResponse+ " On"+'</th><td>'+response.financeExtendResponseDate+'</td></tr>');    
	    $('.authorizationTable').append(travelAuthorizationExtendFinanceRow);
    }
    
    if(response.financeExtendResponseRemarks==null || response.financeExtendResponseRemarks==""){	  
	  }else{
		  var ExtendFinanceRemarksRow= $('<tr><td scope="row">Extend Finance Remarks: </th><td colspan='+"3"+'>'+ response.financeExtendResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( ExtendFinanceRemarksRow);
	  }
    
    if(response.lineManagerExtendResponse=="Pending" || response.lineManagerExtendResponse==null){
	    
    }else{
    	var travelAuthorizationAExtendLineManagerRow =$('<tr><td scope="row">'+"Extend LineManager "+response.lineManagerExtendResponse+" By"+'</th><td>'+response.lineManagerExtendResponseBy+'</td><td scope="row">'+response.lineManagerExtendResponse+ " On"+'</th><td>'+response.lineManagerExtendResponseDate+'</td></tr>');	    
	    $('.authorizationTable').append(travelAuthorizationAExtendLineManagerRow);
    }
    
    if(response.lineManagerExtendResponseRemarks==null || response.lineManagerExtendResponseRemarks==""){	
	  }else{
		  var ExtendlineManagerRemarksRow= $('<tr><td scope="row">Extend LineManager Remarks: </th><td colspan='+"3"+'>'+ response.lineManagerExtendResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( ExtendlineManagerRemarksRow);
	  }
    
    //cancel Travel
    
    if(response.cancelRemarks==null || response.cancelRemarks==""){	
	  }else{
		  var cancelRemarksRow= $('<tr><td scope="row">Traveller Cancel Remarks: </th><td colspan='+"3"+'>'+ response.cancelRemarks+'</th></tr>');
		  $('.authorizationTable').append( cancelRemarksRow);
	  }
    
    if(response.cancelResponseDate!=null){
	    var travelAuthorizationCancelLineManagerRow =$('<tr><td scope="row">'+"cancel-request "+response.cancelResponse+" By"+'</th><td>'+response.cancelResponseBy+'</td><td scope="row">'+response.cancelResponseBy+ " On"+'</th><td>'+response.cancelResponseDate+'</td></tr>');
	    
	    $('.authorizationTable').append(travelAuthorizationCancelLineManagerRow);
    }
    
    if(response.cancelResponseRemarks==null || response.cancelResponseRemarks==""){	
	  }else{
		  var cancellineManagerRemarksRow= $('<tr><td scope="row">Cancel Line Manager Remarks: </th><td colspan='+"3"+'>'+ response.cancelResponseRemarks+'</th></tr>');
		  $('.authorizationTable').append( cancellineManagerRemarksRow);
	  }
    
    
}



//////////////////////////start of travel authorization////////////////////////

main.ajax("/hrm/travel/request/requestsToMe", "", function(response) {
	console.log("tala");
	console.log(response);
	console.log("mathi");	
	getTravelRequest(response)
}, "GET", main.getToken());


function getTravelRequest(response){
	
	var requestLength = response.length;
	console.log(requestLength);
	
	if(requestLength!=0){	
		response.forEach(function(response){
			var requestBody=$('<tr href="#" class="dashboard-card box-shadow-none requestRow" data-toggle="modal" data-target="#examplemodalcenter">'+                  
					'<td>'+response.nameOfTraveller+'</td><td>'+response.overallType+'</td><td>'+response.placeOfTravel+'</td><td>'+response.dateFrom+" To "+response.dateTo+'</td><td>'+response.purpose+'</td></tr>');
			
			$('#travelAuthorizationTable tbody').append(requestBody);
			
			$('.requestRow').click(function(){
				$('#travelauthorization_modal').modal('show');
				getTravelDetails(response);
				
					
				
				
				
			});
			
			$('#approveButton').click(function(){
							
				data ={
						typeAuth:response.overallType,
						responseAuth:"Approved",
						responseRemarks: $('#responseRemarks').val(),
				}
				
				extraMain.ajax("/hrm/travel/request/update/"+response.id ,JSON.stringify(data),function(response){	
					$('#myTravelTable').DataTable().ajax.reload();
					toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
				},
				function(response){
					toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
				},"PUT",main.getToken());
				
				alert("approved");
				console.log(response);
				
				
			});
			
			
			$('#rejectButton').click(function(){
				
				
				alert("rejected");
				console.log(response);
				
			});
			
			
			
			
		});	
	}else{
		//no pending travel request
	}
	
	
}



/////////////////////////////////////////////////////////////////////End of travel authorization//////////////////////////////////////////////////////////////





