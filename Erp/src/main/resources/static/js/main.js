$(document).ready(function () {
	  $("#header").load("Header.html"); 
	  $("#footer").load("Footer.html"); 
	  $("#sidebar").load("Sidebar.html");
	  
 
	  $('.dropdown').on('show.bs.dropdown', function (e) {
	        $(this).find('.dropdown-menu').slideDown(300);
	    });
	    $('.dropdown').on('hide.bs.dropdown', function (e) {
	        $(this).find('.dropdown-menu').slideUp(300);
	    });  
	    
	    fieldCorrectionValue();
	  
	});


function fieldCorrectionValue(){
	//Input Box active when Character is On
	  $('input').blur(function () {
	        // check if the input has any value (if we've typed into it)
	        if ($(this).val())
	            $(this).parent().addClass('form-input-character-on');
	        else
	            $(this).parent().removeClass('form-input-character-on');
	    });
	  
	  $('select').blur(function () {
	        // check if the input has any value (if we've typed into it)
	        if ($(this).val())
	            $(this).parent().addClass('form-input-character-on');
	        else
	            $(this).parent().removeClass('form-input-character-on');
	    });
	  $('textarea').blur(function () {
	        // check if the input has any value (if we've typed into it)
	        if ($(this).val())
	            $(this).parent().addClass('form-input-character-on');
	        else
	            $(this).parent().removeClass('form-input-character-on');
	    });
}


function access(){
	
    $.ajax({
		type : 'GET',
		headers : {
			// 'Accept':'application/json',
			'Content-Type' : 'application/json'
		},
		beforeSend : function(request) {
			request.setRequestHeader("Authorization", main.getToken());
		},
		url : "hrm/attendance/myAttendanceStatus",
		async : false,
		data :"",
		cache : false,
		success : function(data) {
			if(data.role[0].authority!="ROLE_ADMIN"){
				window.location.href="/access-denied";
			}
			
			
		},
	});

			/*if(role!="ROLE_ADMIN"){
				window.location.href="/access-denied";
			}*/
	
		
}


var extraMain = {
		
		ajax: function(url, params, succFunc,errorFunc, method, token, extra){
			$.ajax({
				type : method,
				headers : {
					// 'Accept':'application/json',
					'Content-Type' : 'application/json'
				},
				beforeSend : function(request) {
					request.setRequestHeader("Authorization", token);
					$(".overlay").show();
					$("body").addClass('overflow-y--hidden');
				},
				url : url,
				async : true,
				data : params, 
				cache : false,
				success : function(data) {
					if (succFunc != null) {
						succFunc(data, extra);
					}
				},
				complete: function (request) {
				    $(".overlay").hide();
				    $("body").removeClass('overflow-y--hidden');
				  },	
				  error: function(data) {
					  if (errorFunc != null) {
							errorFunc(data, extra);
						}
				    }
			});	
		}
		
		
}




var main = {	
		
		
		
		checkToken: function(){
			if (localStorage.getItem("Authorization") === null) {
				window.location.href = "/login";
			}
		},
		
		getToken: function(){
			var token = localStorage.getItem("Authorization");
			return token;
		},
		
		clearSessionStorage: function(){
			localStorage.clear();
			window.location.href = "/login";
			
		},
		
		checkStorage: function(){
			var date1 = new Date(localStorage.getItem("time"));
			var date2 = new Date();	
			if(date1 < date2){	
				localStorage.clear();
				window.location.href = "/login";
			}
		},
		
		ajax: function(url, params, succFunc, method, token, extra){
			$.ajax({
				type : method,
				headers : {
					// 'Accept':'application/json',
					'Content-Type' : 'application/json'
				},
				beforeSend : function(request) {
					request.setRequestHeader("Authorization", token);
				},
				url : url,
				async : true,
				data : params, 
				cache : false,
				success : function(data) {
					if (succFunc != null) {
						succFunc(data, extra);
					}
				},
				complete: function (request) {
				    $(".overlay").hide();
				    $("body").removeClass('overflow-y--hidden');
				  },			
			});	
		}
}








