	$(document).ready(function(){
	
	//get all linemanager for delegate in modal
	main.ajax("/hrm/findLineManager", "", function(response) {
		var select = document.getElementById("delegateTo");
		response.forEach(function(element) {
			var opt = element.name
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});
	},"GET", main.getToken());
	
	//get delegate To data
	main.ajax("/hrm/delegate/getDelegateToOther", "", function(response) {
		console.log(response);
		if(response.length > 0){
		
		response.forEach(function(response) {
			
		  var delegateToList= $('<tr class="dashboard-card"><td>'+response.delegatedTo+'</td><td>'+response.delegateFrom+'</td><td>'+response.delegateTill+'</td><td>'+
	            '<div class="dropdown cursor-pointer employee-list__dropdown mr-3 text-right">'+
	            '<a class="text-center" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
	            '<i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton">'+	            
	                   '<a class="dropdown-item" data-toggle="modal" data-target="#modal-holiday-edit">Edit</a>'+
	                   '<a class="dropdown-item" href="#">Remove</a>'+
	                '</div></div></td></tr>');
		  
		  $( "#delegateToTable tbody" ).append( delegateToList );
		
		});
		
	}else{
		
		
		 $("#delegateToTable").hide();
		 var delegateToList= $('<div class="dashboard-card w-100 py-3 px-4 mt-5">No Delegation</div>');
		 $( "#noRequests2" ).append( delegateToList );
	}
	                   
	},"GET", main.getToken());
	
	
	
	//get delegate By data
	main.ajax("/hrm/delegate/getDelegateByOther", "", function(response) {
		console.log(response);
		if(response.length > 0){
		
		response.forEach(function(response) {
			
		  var delegateToList= $('<tr class="dashboard-card"><td>'+response.delegatedBy+'</td><td>'+response.delegateFrom+'</td><td>'+response.delegateTill+'</td></tr>');
		  
		  $( "#delegateByTable tbody" ).append( delegateToList );
		
		});
		
	}else{
		
		
		 $("#delegateByTable").hide();
		 var delegateToList= $('<div class="dashboard-card w-100 py-3 px-4 mt-5">No Delegation</div>');
		 $( "#noRequests1" ).append( delegateToList );
	}
	                   
	},"GET", main.getToken());
	
	
	
});




function addDelegate(){
	
	data={
			delegateFrom: $('#dateFrom').val(),
			delegateTill: $('#dateTill').val(),
			delegateTo: $('#delegateTo').val()
	}
	
	main.ajax("hrm/delegate/saveDelegate", JSON.stringify(data), function(response) { 
			alert("delegated");
	    }, "POST", main.getToken());
}