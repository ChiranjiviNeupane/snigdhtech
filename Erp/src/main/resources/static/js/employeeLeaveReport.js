$(document).ready(function () {
	
$("#employeeNames,#department").hide();

$('input:radio[name="checkBoxField"]').change(function(){

		if($(this).val() == "byDepartment"){
			$('#employeeNames').hide();
			$('#department').show();
			
		}else if($(this).val() == "byEmployee"){
			$('#department').hide();
			$('#employeeNames').show();
		}
		    
	});

	
	
	main.ajax("/hrm/findLineManager", "", function(response) {
		var select = document.getElementById("employeeNames");
		response.forEach(function(element) {
			var opt = element.name
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});

	}, "GET", main.getToken());
	
	

	
	var now = new Date()
	var currentYear = now.getFullYear()
	var currentMonth = ("0" + (now.getMonth() + 1)).slice(-2)	
	var currentdate = ("0" + (now.getDate())).slice(-2)
	
	
	//for year
	var max = currentYear,
    min = 2015
    select = document.getElementById('year');

	for (var i = max; i>=min; i--){
	    var opt = document.createElement('option');
	    opt.value = i;
	    opt.innerHTML = i;
	    select.appendChild(opt);
	}
	
	//for month
	$('#year').change(function(){	
		$('#month option').prop('disabled', false);
		
		 if($(this).val()==currentYear){
			$('#month option').filter(function() {
			    return $(this).val() > currentMonth;
			}).prop('disabled', true); 
			 	
		}
		 
	}); 
	
	
	$('#month option').filter(function() {
	    return $(this).val() > currentMonth;
	}).prop('disabled', true); 
		 	
	
	
	$('#year').val(currentYear);
	$('#month').val(currentMonth);
	
	InitialReport();
	
});	



function InitialReport(){
	
	var now = new Date()
	var currentYear = now.getFullYear()
	var currentMonth = ("0" + (now.getMonth() + 1)).slice(-2)	
	var currentdate = ("0" + (now.getDate())).slice(-2)
	
	
	main.ajax("/hrm/leavereq/getMonthlyLeaveAllEmp/"+ currentYear + "/" + currentMonth,"", function(response) {
		console.log("vitra ayoo");
		console.log(response);
		createTable(response);
	}, "GET", main.getToken()); 
	
	
}
	



function generateReport(){

	var now = new Date()
	var currentYear = now.getFullYear()
	var currentMonth = ("0" + (now.getMonth() + 1)).slice(-2)	
	var currentdate = ("0" + (now.getDate())).slice(-2)
	
	
	var selectedYear=$('#year').val();
	var selectedMonth=$('#month').val();
	
	var dept=$('#department').val();
	var empName=$('#employeeNames').val();
	
	
	if($('#byDepartment').is(':checked')){
	
		/*/getMonthlyLeaveByDeptm/{department}/{year}/{month}*/
		
	main.ajax("/hrm/leavereq/getMonthlyLeaveByDeptm/"+dept+"/"+selectedYear+"/"+selectedMonth,"", function(response) {
		console.log(response);
		createTable(response);
	}, "GET", main.getToken());
	
	
		console.log("dept wala");
		
	}else if($('#byEmployee').is(':checked')){
		main.ajax("/hrm/leavereq/getMonthlyLeave/"+empName+"/"+selectedYear+"/"+selectedMonth,"", function(response) {
			console.log(response);
			createTable(response);
		}, "GET", main.getToken());
		
	}else{
		InitialReport();
	}
}




function createTable(data){
	
	jQuery(function($) {
		var roleTable = $('#table')

				.DataTable(
						
						{
							"responsive" : true,

							"columnDefs" : [ {
								"targets" : [ 0 ],
								"visible" : false
							},
							

							],

							"order" : [ 0, "desc" ],
							/* "paging": false, */

							dom : 'lBfrtip',
							buttons : [ 'copy', 'csv', 'excel', 'pdf',
									'print' ],
							"destroy" : true,
							data : data,

							"columnDefs" : [
									{
										"targets" : [ 0 ],
										"visible" : false
									},
									{
										targets : 6,
										render : function(data, type, row) {
											var color = 'black';
											if (data == "Pending") {
												color = 'badge badge-pill badge-warning';
											}
											if (data == "Approved") {
												color = 'badge badge-pill badge-success';
											}
											if (data == "Rejected") {
												color = 'badge badge-pill badge-danger';
											}
											if (data == "Cancelled") {
												color = 'badge badge-pill badge-secondary';
											}
											return '<span class="' +  color + '">'
													+ data + '</span>';

										}
									} ],

							"columns" : [ {
								"data" : "requestId"
							}, 
							{
								"data" : "name"
							}, 
							{
									data : null,
									"render" : function(data, type, row) {
									
									if(data.LeaveRequestType=="NormalLeave" && data.extendTill!="")	{
										return (data.LeaveRequestType + "(Extended:" + data.extendStatus + ")" )
									}else{
										return data.LeaveRequestType
									}
								}
							},
							{
								data : null,
								"render" : function(data, type, row) {
									if(data.LeaveRequestType == "NormalLeave"){							
										  	return (data.leaveFrom+" to "+data.LeaveTill);
										  }
									  else if(data.LeaveRequestType == "HalfDayLeave"){								
											  return data.leaveDate
										  }
									  else{
										  return data.leaveDate
									  }
									
								}
								
							},					
							{
								data : null,
								"render" : function(data, type, row) {
									if(data.LeaveRequestType == "NormalLeave"){								  	
										  	return (data.noOfDays+" Days");
										  }
									  else if(data.LeaveRequestType == "HalfDayLeave"){	  
										  return (data.halfDayType+" Half");
										  }
									  else{			  
										  
										 return (data.noOfHours +" Hours");
									  }
									
								}
							}, {
								"data" : "leaveType"
							}, {
								"data" : "requestStatus"
							},{
								data : null,
								"render" : function(data, type, row) {
									return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item'"+
									"onClick='viewMyLeaves("
								    + JSON.stringify(data)
								    + ")'>View</a></div></div>"
									
									
								}
							} 
							]
						});
	})
}

function viewMyLeaves(response){

	 $('#examplemodalcenter').modal('show'); 
		
	  console.log(response);	 
	  $("#fullname").text(response.name);
	  
	  $("#department1").text(response.department);
	  $("#position").text(response.position);
	  $("#leaveReason1").text(response.leaveReason);
	  $("#leaveType").text(response.leaveType);
	  
	  $("#extendTill").text(response.extendTill);
	  $("#extendDays").text(response.extendDays);
	  
	  
	 
	  $("#authorizationTableBody").empty();
	  
	  if(response.requestedBy!=""){
		  var requestRow= $('<tr class = "recommendData"><td scope="row">Requested By: </th><td class = "recommendBy">'+response.name+'</td><td scope="row">Requested On: </th><td class="recommendOn">'+response.createdDate+" "+response.createdTime+'</td></tr>');
		  $('#authorizationTableBody').append( requestRow);
	  }
   
      if(response.recommendedBy!=""){
		  var recommendRow= $('<tr class = "recommendData"><td scope="row">Recommended By: </th><td class = "recommendBy">'+response.recommendedBy+'</td><td scope="row">Recommended On: </th><td class="recommendOn">'+response.recommendedDate+'</td></tr>');
		  $('#authorizationTableBody').append( recommendRow);
	  }
      
      if(response.responseBy!=""){
		  var responseRow= $('<tr class = "recommendData"><td scope="row">'+response.requestStatus+" By:"+'</th><td class = "recommendBy">'+response.responseBy+'</td><td scope="row">'+response.requestStatus+" On"+'</th><td class="recommendOn">'+response.responseDate+" "+response.responseTime+'</td></tr>');
		  $('#authorizationTableBody').append( responseRow);
	  }
	  
	  if(response.remarks!=""){
		  var lmRemarksRow= $('<tr><td scope="row">Line Manager Remarks: </th><td scope="row">'+ response.remarks+'</th></tr>');
		  $('#authorizationTableBody').append( lmRemarksRow);
	  }
	  
	  if(response.extendTill!=""){
		  var extendRow= $('<tr class = "recommendData"><td scope="row">Extend By: </th><td class = "recommendBy">'+response.name+'</td><td scope="row">Extended On: </th><td class="recommendOn">'+response.extendedDate+'</td></tr>');
		  $('#authorizationTableBody').append( extendRow);
	  }
	  
	  if(response.extendRemarks!=null){
		  var extendremarksRow= $('<tr><td scope="row">Extend	 Remarks: </th><td scope="row">'+ response.extendRemarks+'</th></tr>');
		  $('#authorizationTableBody').append( extendremarksRow);
	  }
	  
	  if(response.extendedResponseDate!=null){
		  var extendResponseDate= $('<tr class = "recommendData"><td scope="row">Extend '+response.extendStatus+" By"+'</th><td class = "recommendBy">'+response.extendResponseBy+'</td><td scope="row">Extend '+response.extendStatus+" On"+'</th><td class="recommendOn">'+response.extendedResponseDate+'</td></tr>');
		  $('#authorizationTableBody').append( extendResponseDate);
	  }
	  if(response.extendResponseRemarks!=null){
		  var extendremarksRow= $('<tr><td scope="row">Line Manager Extend Remarks: </th><td scope="row">'+ response.extendResponseRemarks+'</th></tr>');
		  $('#authorizationTableBody').append( extendremarksRow);
	  }
	  
	  //cancel
	  
	  if(response.cancelDate!=null){
		  var extendRow= $('<tr class = "recommendData"><td scope="row">Cancel From: </th><td class = "recommendBy">'+response.cancelFrom+'</td><td scope="row">Cancelled On: </th><td class="recommendOn">'+response.cancelDate+'</td></tr>');
		  $('#authorizationTableBody').append( extendRow);
	  }
	  
	  if(response.cancelRemarks!=null){
		  var extendremarksRow= $('<tr><td scope="row">Cancel Remarks: </th><td scope="row">'+ response.cancelRemarks+'</th></tr>');
		  $('#authorizationTableBody').append( extendremarksRow);
	  }
	  
	  if(response.cancelResponseDate!=null){
		  var extendResponseDate= $('<tr class = "recommendData"><td scope="row">Cancel '+response.cancelStatus+" By"+'</th><td class = "recommendBy">'+response.cancelResponseBy+'</td><td scope="row">Cancel '+response.cancelStatus+" On"+' </th><td class="recommendOn">'+response.cancelResponseDate+'</td></tr>');
		  $('#authorizationTableBody').append( extendResponseDate);
	  }
	  if(response.cancelResponseRemarks!=null){
		  var extendremarksRow= $('<tr><td scope="row">Cancel Response Remarks: </th><td scope="row">'+ response.cancelResponseRemarks+'</th></tr>');
		  $('#authorizationTableBody').append( extendremarksRow);
	  }
	  
	  
	  
	  
	  
	  
	  if(response.LeaveRequestType == "NormalLeave"){
		  $("#LeaveRequestType").text(response.LeaveRequestType);
		  	$("#leaveDated").text(response.leaveFrom+" to "+response.LeaveTill);
		  	$("#noOfDays").text(response.noOfDays+" Days");
		  }
	  else if(response.LeaveRequestType == "HalfDayLeave"){
		  $("#LeaveRequestType").text(response.LeaveRequestType+"("+response.halfDayType+")");
			  $("#leaveDated").text(response.leaveDate);
			  $("#noOfDays").text(response.halfDayType+" Half");
		  }
	  else{
		  $("#LeaveRequestType").text(response.LeaveRequestType);
		  $("#leaveDated").text(response.leaveDate);
		  $("#noOfDays").text(response.leaveFrom+" to "+ response.LeaveTill);
	  }
	  
	  
	  $("#cmd").click(function() {
		    
		   /* html2canvas($('#report').get(0), {
		        onrendered: function(canvas) {
		          var img = canvas.toDataURL("image/png");
		          //var doc = new jsPDF();
		          doc.addImage(img, 'JPEG', 50, 50);
		          doc.save('test.pdf');
		        }
		      });*/
		    
		 
		 //testing
		 
		 var doc = new jsPDF('p', 'pt', 'a4', true);	
		 doc.setFont("arial", "bold");
		 doc.setFontSize(18);
		 doc.text(220, 40, "Leave Details");
		 
		 doc.setFont("arial","");
		 doc.setFontSize(10);
		 doc.text(400, 60,"Date:");
		 doc.text(430, 60,"2019-05-06");
		 
		
/*		 doc.setLineWidth(0.5);
		 doc.line(125, 250, 0, 75);
		 */
		 if(response.LeaveRequestType == "NormalLeave"){
			  	var requestType = response.LeaveRequestType;
			  	var dated = response.leaveFrom+" to "+response.LeaveTill;
			  	var noOfDays = response.noOfDays+" Days";
			  }
		  else if(response.LeaveRequestType == "HalfDayLeave"){
			  var requestType = response.LeaveRequestType+"("+response.halfDayType+")";
			  var dated = response.leaveDate;
			  var noOfDays = response.halfDayType+" Half";
			  }
		  else{
			  var requestType = response.LeaveRequestType;
			  var dated = response.leaveDate;
			  var noOfDays = response.leaveFrom+" - "+ response.LeaveTill;
		  }
		 var y = 100;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Full Name:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(40, y+20, response.name);
		 
		 y = y+60;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Position:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(40, y+20, response.position);
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, y, "Department:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(400, y+20, response.department);
		 
		 y = y+60;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Request Type:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(40, y+20, requestType);
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, y, "Leave Type:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(400, y+20, response.leaveType);
		  
		 y = y+60;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Leave Date:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(40, y+20, dated);
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, y, "Duration:");
		 doc.setFont("arial", "");
		 doc.setFontSize(13);
		 doc.text(400, y+20, noOfDays);
		 
		 if(response.LeaveRequestType == "NormalLeave"){
			 y = y+60;
			 doc.setFont("arial", "");
			 doc.setFontSize(14);
			 doc.text(40, y, "Extend Till:");
			 doc.setFont("arial", "");
			 doc.setFontSize(14);
			 doc.text(40, y+20, response.extendTill);
			 
			 doc.setFont("arial", "");
			 doc.setFontSize(14);
			 doc.text(400, y, "Extend Days:");
			 doc.setFont("arial", "");
			 doc.setFontSize(14);
			 doc.text(400, y+20, response.extendDays + "");
		 }
		 
		 y = y+60;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Reason For Leave:");
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y+20, response.leaveReason);
		 
		 y = y+60;
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y, "Status");
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, y+20, response.requestStatus);
		 
		 y = y+60;
		 doc.setFont("arial","");
		 doc.setFontSize(14);
		 doc.text(40, y,"Authorization:");
		 
		    var y = y+10;
		    // Or JavaScript:	
		    doc.autoTable({
		    	
		    	html: '#LeaveDetailAuthorization',
		    	textColor: 1000,
		    	theme: 'grid',
		    	startY: y,
		    	startX: 100,
		    		
		    });
		    doc.setLineWidth(0.5);
		    doc.setDrawColor(0, 0, 0);
			doc.rect(15, 18, y+35, 600);
			 
	 
		 doc.save('thisMotion.pdf');
		 
		  });

}