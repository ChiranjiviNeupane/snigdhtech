
$(document).ready( function () {
	main.ajax("/hrm/leavetype/findAllLeave", "", function(response) {
	}, "GET", main.getToken());
	
	
	
jQuery(function($) {
	var roleTable = $('#manageLeaveTypeTable')

			.DataTable(
					
					{
						"responsive" : true,
						"lengthChange": false,

		
						/* "paging": false, */

						"ajax" : {
							"url" : "/hrm/leavetype/findAllLeave",
							"dataSrc" : "",
							"beforeSend" : function(request) {
								request.setRequestHeader(
										"Authorization", main.getToken());
							},
						},

		

						"columns" : [ {
							"data" : "typeOfLeave"
						}, {
							"data" : "totalLeave"
						}, 
						{
							sortable : false,
							"render" : function(data, meta, full) {
								var name= JSON.stringify(full.typeOfLeave);
								return '<div class="dropdown cursor-pointer ovel-on-hover mr-3 text-center"><a class="" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton"><a class="dropdown-item" onClick="editLeaveType('
								+ full.id
								+ ')">Edit</a><a class="dropdown-item" onClick="extendLeave('
								+ full.id
								+ ')">Delete</a></div></div>'

							}
						} 
						]
					});
})
});

//onclick pop up modal
function addLeaveBtn() {
	$('#modalleaveadd').modal('show');
}

function addLeaveType() {
	var data ={
		    typeOfLeave:$('#leaveType').val(),
		    totalLeave:$('#leaveDays').val()
		}
	
	if($('#leaveType').val() && $('#leaveDays').val()){
		
		main.ajax("/hrm/leavetype/saveLeaveType", JSON.stringify(data), function(response) {
			alert("added");
			location.reload();
		}, "POST", main.getToken());
		
	}else{
		alert("please provide Leave Type and Days");
	}
	
}
function editLeaveType(id){
	$('#modalleaveedit').modal('show');
	main.ajax("/hrm/leavetype/findById/"+ id, "", function(response) {
		$('#leaveType1').val(response.typeOfLeave);
		$('#leaveDays1').val(response.totalLeave);
		
		$('#editLeaveTypeData').click(function(){
			  var name = $("#leaveType1").val();
			  var days = $("#leaveDays1").val();
			  var data = {
					  typeOfLeave : name,	
					  totalLeave : days,
			  }
			  
			  main.ajax( "hrm/leavetype/updateLeaveType/" + id,JSON.stringify(data),function(response){   
				  	alert("Updated")
			    	location.reload();
				},"PUT",main.getToken());
			});
		
		
		
	}, "GET", main.getToken());
}


