$(document).ready(function () {
	
	
	main.ajax("/hrm/findLineManager", "", function(response) {
		var select = document.getElementById("employeeNames");
		response.forEach(function(element) {
			var opt = element.name
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});

	}, "GET", main.getToken());
	
	

	
	var now = new Date()
	var currentYear = now.getFullYear()
	var currentMonth = ("0" + (now.getMonth() + 1)).slice(-2)	
	var currentdate = ("0" + (now.getDate())).slice(-2)
	
	
	//for year
	var max = currentYear,
    min = 2015
    select = document.getElementById('year');

	for (var i = max; i>=min; i--){
	    var opt = document.createElement('option');
	    opt.value = i;
	    opt.innerHTML = i;
	    select.appendChild(opt);
	}
	
	//for month
	$('#year').change(function(){	
		$('#month option').prop('disabled', false);
		
		 if($(this).val()==currentYear){
			$('#month option').filter(function() {
			    return $(this).val() > currentMonth;
			}).prop('disabled', true); 
			 	
		}
		 
	}); 
	
	var doc = new jsPDF();
	var specialElementHandlers = {
	    '#editor': function (element, renderer) {
	        return true;
	    }
	};

	$('#cmd').click(function () {   
	    doc.fromHTML($('#multipleTables').html(), 30, 55, {
	        'width': 170,
	            'elementHandlers': specialElementHandlers
	    });
	    doc.save('sample-file.pdf');
	});
	
	//button on click
	
	
	$("#generateReport").click(function(){
		var names= $('#employeeNames').val();		
        if ($('#year').val() && $('#month').val()){
        	
        	var selectedYear=$('#year').val();
        	var selectedMonth=$('#month').val();
        	
        	$('.table').DataTable().destroy();
        	$('div').remove('#monthlyReport');
        	
        	main.ajax("/hrm/attmonthlyreport/getMonthlyAttendanceAllEmp/"+selectedYear+"/"+selectedMonth,"", function(response) {
        		console.log(response);
        		createTables(response);
        	}, "GET", main.getToken()); 
           	
        }else{
        	alert("Select valid year and a month");
        }
    }); 
		
	
});

function changeEmpData(response){
$('#department').change(function(){
	var dept = $("#department").val();
	var select = document.getElementById("employeeNames");
	$("#employeeNames").empty();
		response.forEach(function(element) {
			
			//to be done
			if(jQuery.inArray(dept)){
			var opt = element.name
		    var el = document.createElement("option");
		    el.textContent = opt;
		    el.value = element.id;
		    select.appendChild(el);
			}
			
		});
	});
}



function createTables(response){
	
	/*var data =[];
	var id =[];
		
	response.forEach(function(element){
		var multiTable = $('<h6 class= "id">'+element.id+'</h6><br><h6>Date</h6><br><table class="table" class="display nowrap" width="100%"><thead><tr><th>Date</th><th>Checked In Time</th><th>Checked out Time</th><th>Type</th><th>Remarks</th></tr></thead></table><hr><br>');
		$('#multipleTables').append(  multiTable );*/
	/*	data.push(element.data);
		id.push(element.id);
	});*/
	
	
	
	
	
	
	response.forEach(function(element){
		var multiTable = $('<div id="monthlyReport"><h6 class= "id">'+element.name+'</h6><br><h6>Date</h6><br><table class="table" class="display nowrap" width="100%"><thead><tr><th>Date</th><th>Checked In Time</th><th>Checked out Time</th><th>Type</th><th>Remarks</th></tr></thead></table><hr><br></div>');
		$('#multipleTables').append(  multiTable );
	
	$('.table').DataTable({
	
		"pageLength": 50,
		retrieve: true,
		"responsive": true,
		"destroy" : true,
		 data : Object.values(element)[0],
		"columns" : [

				{
					data: null,
	                render: function ( data, type, row ) {
	                	
	                	if(data.date){
	                		return data.date;
	                	}else{
	                		return "";
	                	}
	                	
	                	
	                }
				},
				{
					data: null,
	                render: function ( data, type, row ) {
	                	if(data.checkIn){
	                		return data.checkIn;
	                	}else{	           
	                	return "";
	                	}
	                }
				},
				{
					data: null,
	                render: function ( data, type, row ) {
	                	if(data.checkOut){
	                		return data.checkOut;
	                	}else{	           
	                	return "";
	                	}
	                }
				},
				{
					data: null,
	                render: function ( data, type, row ) {  
	                	
	                	if(data.type){
	                		return data.type;
	                	}else{
	                		return "";
	                	}
	                	
	                	
	                }
				},
				{
					data: null,
	                render: function ( data, type, row ) { 
	                	if(data.remarks){
	                		return data.remarks;
	                	}else{
	                		return "";
	                	}
	                		
	              
	                	
	                }
				}
				

		]
		
	});
		
	});
	
}