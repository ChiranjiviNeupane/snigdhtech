	
//populating linemanager based on department selected
function getDataOnClick() {

			main.ajax("/hrm/findLineManager", "", function(response) {
				
				getLineManager(response)
			}, "GET", main.getToken());

			
			
			
			
			function getLineManager(response) { 
				var dept = $("#department").val();
				
				var select = document.getElementById("lineManager");
				$("#lineManager").empty();
				if (dept){
					response.forEach(function(element) {
						
						if(element.department == dept){
						var opt = element.name
					    var el = document.createElement("option");
					    el.textContent = opt;
					    el.value = element.id;
					    select.appendChild(el);
						}
						
					});
				}
				else{
					response.forEach(function(element) {
						
						var opt = element.name
					    var el = document.createElement("option");
					    el.textContent = opt;
					    el.value = element.id;
					    select.appendChild(el);
				
						
					});
				}

			}

		} 


$(document).ready(function () {
	
	//populating line manager list
	main.ajax("/hrm/findLineManager", "", function(response) {
		
		var select = document.getElementById("lineManager");
		response.forEach(function(element) {
			var opt = element.name
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});
	},"GET", main.getToken());
	
	
	
	
	
	
	//validation for adding new Emp
	$("#regForm").validate({
        rules: {
        	firstName: "required",
        	surName: "required",
        	pemail: "required",
        	password: "required",
        	role: "required",
        	department: "required",
        	lineManager: "required",
        	position: "required",
        	
        },
        messages: {
        	firstName: "Please Specify First Name",
        	surName: "Please Specify Last Name",
        	fatherName: "Please Specify father Name",
        	pemail: "Please Specify Email",
        	password: "Please Specify Password",
        	role: "Please Specify role",
        	department: "Please Specify Department",
            lineManager: "Please Specify Line Manager",
            position: "Please Specify Position",

        }
    })
	
	
	
	
	
	
	var mystepper = new Stepper($('.bs-stepper')[0]);
	var stepperCount = 0;
	    $(".bs-stepper-content .btn-prev").click(function () {
	    	stepperCount--
	    	
	        mystepper.previous();
	        
	    });
	    $(".bs-stepper-content .btn-next").click(function () {
	    	
	    	if (stepperCount == 0){
	    		if($("#regForm").valid()){		
		    		mystepper.next();
		    		stepperCount++ 
	    		}
	    	}else if(stepperCount == 2){
	    		if($("#regForm").valid()){
		    		stepperCount++ 		
		    		mystepper.next();
		    		}
	    		
	    	}else{
	    		stepperCount++
		    	mystepper.next();
	    	}
	    	
	       
	    });
	    
	    
	    
});

var addEmp = {
		
		addemployees: function() {
			
			 
			
			var nameValue = document.getElementById("regForm").value;	
			var employeedata= {
				academicQualification:academicqualification.value,
				dateOfBirth: dob.value,
				email: pemail.value,
				firstName:firstName.value ,
				identificationName: identificationName.value,
				identificationNumber: identificationNumber.value,
				maidenName: maidenName.value,
				maritalStatus: maritalStatus.value,
				middleName: middleName.value,
				nationality: nationality.value,
				password: password.value,
				sex: sex.value,
				suffix: suffix.value,
				surname: surname.value,
				title: title.value,
				companyInformations: [
				{
					contractEndDate: contractenddate.value,
					contractStartDate: contractstartdate.value,
					contractType: contracttype.value,
					department: department.value,
					level: level.value,
					lineManager: lineManager.value,
					office: office.value,
					position: position.value,
					section: section.value,
					step: step.value,
					unit: unit.value
				}
				],
				contactAddresses: [
				{

					city: cacity.value,
					country: cacountry.value,
					state: castate.value,
					street: castreet.value,
					wardNo: cawardno.value
				}
				],
				contactInformations: [
				{

					email: email.value,
					mobile: mobile.value,
					phone: phone.value,
					poBox: poBox.value
				}
				],
				dependentInformations: [
				{

					children1: children1.value,
					/*children2: children2.value,
					children3: children3.value,*/
					fatherName: fatherName.value,
					motherName: motherName.value,
					spouse: spouse.value
				}
				],
				emergencyContactInformations: [
				{

					contactNumber: contactnumber.value,
					fullAddress: fulladdress.value,
					name: name.value,
					relationship: relationship.value
				}
				],
				otherInformations: [
				{
					religion: religion.value,
					ethnicity: ethnicity.value,
					bloodGroup:	bloodGroup.value,
					drivingLiscenceNumber: drivingliscencenumber.value,
					insurancePolicyNumber: insurancepolicynumber.value,
					majorAlergyOrSickness: majoralergyorsickness.value,
					otherProfessionalTrainingCourses: otherprofessionaltrainingcourses.value,
					panNumber: pannumber.value,
					pastOrganisations: pastorganisations.value,
					preferences: preferences.value
				}
				],
				permanentHomeAddresses: [
				{

					city: pacity.value,
					country: pacountry.value,
					state: pastate.value,
					street: pastreet.value,
					wardNo: pawardno.value
				}
				],
				role: [role.value

				],
				workAddresses: [
				{
					city: wacity.value,
					company: wacompany.value,
					country: wacountry.value,
					state: wastate.value,
					street: wastreet.value,
					wardNo: wawardno.value
				}
				]
			}
			
			
			extraMain.ajax("/hrm/save" ,JSON.stringify(employeedata),function(response){	

				toastr.options.onHidden = function() { window.location.href = "/"; }
				toastr.options.onclick = function() { window.location.href = "/"; }
				toastr.success('Successful', 'Updated Employee Information', {timeOut: 4000}).css("width","900px","height","50px")		
				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 4000}).css("width","900px","height","50px")
			},"POST",main.getToken());
		
			
		}
		
}

// add new employee
