$( document ).ready(function() {
	
	 $("#cmd").click(function() {
		    
		   /* html2canvas($('#report').get(0), {
		        onrendered: function(canvas) {
		          var img = canvas.toDataURL("image/png");
		          //var doc = new jsPDF();
		          doc.addImage(img, 'JPEG', 50, 50);
		          doc.save('test.pdf');
		        }
		      });*/
		    
		 
		 //testing
		 
		 var doc = new jsPDF('p', 'pt', 'a4', true);	
		 
		 doc.setFont("arial", "bold");
		 doc.setFontSize(18);
		 doc.text(220, 40, "Leave Details");
		 
		 doc.setFont("arial","");
		 doc.setFontSize(10);
		 doc.text(400, 60,"Date:");
		 doc.text(430, 60,"2019-05-06");
		 
		
/*		 doc.setLineWidth(0.5);
		 doc.line(125, 250, 0, 75);
		 */
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 100, "Full Name:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 150, "Position:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, 150, "Department:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 200, "Request Type:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, 200, "Dated:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 250, "Leave Type:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, 250, "Duration:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 300, "Extend Till:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(400, 300, "Extend Days:");
		 
		 doc.setFont("arial", "");
		 doc.setFontSize(14);
		 doc.text(40, 350, "Reason For Leave:");
		 
		 doc.setFont("arial","");
		 doc.setFontSize(14);
		 doc.text(40, 500,"Authorization:");
		 
		    var ggg = 510;
		    // Or JavaScript:	
		    doc.autoTable({
		    	
		    	html: '#LeaveDetailAuthorization',
		    	textColor: 1000,
		    	theme: 'grid',
		    	startY: ggg,
		    	startX: 100,
		    		
		    });
		    doc.setLineWidth(0.5);
		    doc.setDrawColor(0, 0, 0);
			doc.rect(17, 20, ggg+40, 600);
			 
	 
		 doc.save('thisMotion.pdf');
		 
		  });
	
	 $('input').blur(function () {
	        // check if the input has any value (if we've typed into it)
	        if ($(this).val())
	            $(this).addClass('form-input-character-on');
	        else
	            $(this).removeClass('form-input-character-on');
	    });
	 
	 $('textarea').blur(function () {
	        // check if the input has any value (if we've typed into it)
	        if ($(this).val())
	            $(this).addClass('form-input-character-on');
	        else
	            $(this).removeClass('form-input-character-on');
	    });
	 
	 
	 // Display date on employee on Leave card
     var d = new Date($.now());
     var date = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate();
     $('#leaveDate').text(date); 
     
	 
	 $("#remaining-leave__btn").click(function(){
         if ($(this).text() == 'View More'){
        	 $(this).siblings($("i")).removeClass("fa-caret-down");
        	 $(this).siblings($("i")).addClass("fa-caret-up");
         }else if ($(this).text() == 'View Less'){
        	 $(this).siblings($("i")).removeClass("fa-caret-up");
        	 $(this).siblings($("i")).addClass("fa-caret-down");
         }
         $(this).text($(this).text() == 'View More' ? 'View Less' : 'View More');
     });

	//see all remaining card with leave Types and leave Total
	//get remaining leaves
	main.ajax("/hrm/leaverem/findMyRemainingLeave", "" , function(response){
		getleavetypes(response);
		$("#leaveType1").text(response[0].leaveType); 
		$("#leaveTypeValue1").text(response[0].totalLeave); 
		$("#leaveType2").text(response[1].leaveType); 
		$("#leaveTypeValue2").text(response[1].totalLeave); 
		$("#leaveType3").text(response[2].leaveType); 
		$("#leaveTypeValue3").text(response[2].totalLeave); 
		$("#leaveType4").text(response[3].leaveType); 
		$("#leaveTypeValue4").text(response[3].totalLeave); 
		
		 for(var i=4;i<response.length;i++){
			 var newdiv1 = $( '<div class="col-lg-3 col-md-6"><div class="dashboard-card dashboard-gender mb-2"><div class="w-75 mx-auto">'+
					'<div class=" "><div class="align-self-center text-center">'+
					'<span class="text-size-h1 text-weight-bold"id="leaveTypeValue3">'+response[i].totalLeave+'</span><div id="leaveType3">'+response[i].leaveType+'</div></div></div></div></div></div>');
			 $( "#addMoreRemainingLeave" ).append( newdiv1 );
		 }

	}, "GET", main.getToken());
	
	//employees on leave
	main.ajax("/hrm/leavereq/getEmployeeOnLeave", "" , function(response){
	
		employeeOnLeaveBoard(response);

	}, "GET", main.getToken());
	
	
//full day half day and hourly leave
	
	$("#leaveRequestContent").hide();

	$('input:radio[name="checkBoxField"]').change(function(){
		
		$("#leaveRequestContent").show();
		
		
		if($(this).val() == "oneDay" || $(this).val() == "firstHalf" || $(this).val() == "secondHalf"){
			$("#multiple").hide();
			$("#hourlyy").hide();		
			$("#single").show();
		}
		else if($(this).val() == "multipleDay"){
			$("#single").hide();
			$("#hourlyy").hide();			
			$("#multiple").show();
			
		}else if($(this).val() == "hourly"){
			$("#single").hide();
			$("#multiple").hide();		
			$("#hourlyy").show();

		}	    
	});
	
		
	 
	
	
	
	
leaveAuthorization();	

//getting line managers name for recommendation
main.ajax("/hrm/findLineManager", "", function(response) {
	var select = document.getElementById("recommendTo");
	response.forEach(function(element1) {
		var opt = element1.name
		var el = document.createElement("option");
		el.textContent = opt;
		el.value = element1.id;
		select.appendChild(el);
	});

}, "GET", main.getToken());
	
	
});


function leaveAuthorization(){
	$( "#leaveRequests tr" ).remove();
	//leave Requests
	main.ajax("/hrm/leavereq/getLeaveRequest", "", function(response) {
		getLeaveRequest(response);
		
	}, "GET", main.getToken());
		
}

function getLeaveRequest(response){

	if(response.length > 0){
		
	//counter for sn
	var snCounter=1;	
    
	response.forEach(function(element) {	
    	var tr = document.createElement("tr");
		tr.setAttribute('class', 'dashboard-card box-shadow-none');
		tr.setAttribute('href', '#');
		tr.setAttribute('data-toggle', 'modal');
		tr.setAttribute('data-target', '#leaveAuthorizationModal'); 
		
		console.log(element);
		//showing modal based on clicked data
		tr.onclick = function(){
			if(element.LeaveRequestType=="NormalLeave"){
				$('#recommendPart').show();
			}else{
				$('#recommendPart').hide();
			}
			getLeaveDetails(element);
		  
			  //for authorization
			  $('#accept').click(function(){
				  var remarks = $("#remarks").val();
				  var data = {
						  requestStatus : "Approved",
						  remarks : remarks,
				  }
				  
				  
				 /* main.ajax( "hrm/leavereq/updateLeaveRequest/" + element.requestId,JSON.stringify(data),function(response){   
				    	location.reload();
						},"PUT",main.getToken());*/
				 if(element.LeaveRequestType=="NormalLeave"){
					 
					  extraMain.ajax("/hrm/leavereq/updateLeaveRequest/" + element.requestId ,JSON.stringify(data),function(response){	
						  $('#table1').DataTable().ajax.reload();
						  leaveAuthorization();
						  
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					  
				 }else if(element.LeaveRequestType=="HalfDayLeave"){
					 	console.log("half wala accepted");
					 	
					 	extraMain.ajax("/hrm/halfdayleave/updateHalfDayLeave/" + element.requestId ,JSON.stringify(data),function(response){
					 		$('#table1').DataTable().ajax.reload();
					 		leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 	
					 
				 }else if(element.LeaveRequestType=="HourlyLeave"){
					 console.log("hourly wala accepted");
					 
					 extraMain.ajax("/hrm/hourlyleave/updateLeaveRequest/" + element.requestId ,JSON.stringify(data),function(response){	
						 	$('#table1').DataTable().ajax.reload();
						 	leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }else if(element.LeaveRequestType=="ExtendAuthorization"){
					 console.log("approve Extend authorization")
					  var data = {
						 extendStatus : "Approved",	
						 extendResponseRemarks : $('#remarks').val(),
					 	}
					 						
					 extraMain.ajax("/hrm/leavereq/updateExtendLeave/" + element.requestId ,JSON.stringify(data),function(response){
						 	$('#table1').DataTable().ajax.reload();
						 	leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }else if(element.LeaveRequestType=="CancelAuthorization"){
					 console.log("approve cancel authorization");
					 var data = {
							 cancelStatus : "Approved",
							 cancelResponseRemarks : $('#remarks').val(),
						 	}
					 extraMain.ajax("/hrm/leavereq/cancelLeaveAuthorization/" + element.requestId ,JSON.stringify(data),function(response){
						 	$('#table1').DataTable().ajax.reload();
						 	leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }
				  
				  
				});
			  
			  //for rejection 
			  $('#reject').click(function(){
				  var remarks = $("#remarks").val();
				  var data = {
						  requestStatus : "Rejected",
						  remarks : remarks,
				  }
				 
				  if(element.LeaveRequestType=="NormalLeave"){
						 
					  extraMain.ajax("/hrm/leavereq/updateLeaveRequest/" + element.requestId ,JSON.stringify(data),function(response){	
						  	$('#table1').DataTable().ajax.reload();
						  	leaveAuthorization();
						  	toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					  
				 }else if(element.LeaveRequestType=="HalfDayLeave"){
					 	console.log("half wala accepted");
					 	
					 	extraMain.ajax("/hrm/halfdayleave/updateHalfDayLeave/" + element.requestId ,JSON.stringify(data),function(response){
					 		$('#table1').DataTable().ajax.reload();
					 		leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 	
					 
				 }else if(element.LeaveRequestType=="HourlyLeave"){
					 console.log("hourly wala accepted");
					 
					 extraMain.ajax("/hrm/hourlyleave/updateLeaveRequest/" + element.requestId ,JSON.stringify(data),function(response){
						 $('#table1').DataTable().ajax.reload();
						 leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }else if(element.LeaveRequestType=="ExtendAuthorization"){
					 console.log("reject Extend authorization")
					 var data = {
						 extendStatus : "Rejected",
						 extendResponseRemarks : $('#remarks').val(),
						 	}
					 extraMain.ajax("/hrm/leavereq/updateExtendLeave/" + element.requestId ,JSON.stringify(data),function(response){
						 	$('#table1').DataTable().ajax.reload();
						 	leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }else if(element.LeaveRequestType=="CancelAuthorization"){
					 console.log("reject cancel authorization");
					 var data = {
							 cancelStatus : "Rejected",
							 cancelResponseRemarks : $('#remarks').val(),
							 
						 	}
					 extraMain.ajax("/hrm/leavereq/cancelLeaveAuthorization/" + element.requestId ,JSON.stringify(data),function(response){	
						 $('#table1').DataTable().ajax.reload();
						 leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
					 
				 }
				  
				  
				  
				});
			  
			  $('#recommend').click(function(){
				  var remarks = $("#remarks").val();
				  var linemanager = $("#recommendTo").val();
				  var data = {
						  requestTo : linemanager,	
						  remarks : remarks,
				  }
				  
				 /* main.ajax( "hrm/leavereq/updateLeaveRequest/" + element.requestId,JSON.stringify(data),function(response){   
					  alert("recommended")
				    	location.reload();
						},"PUT",main.getToken());*/
				  
				  extraMain.ajax("hrm/leavereq/updateLeaveRequest/" + element.requestId ,JSON.stringify(data),function(response){	
						 $('#table1').DataTable().ajax.reload();
						 leaveAuthorization();
							toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
							$('#examplemodalcenter').modal('hide');
						},
						function(response){
							toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
						},"PUT",main.getToken());
				  
				  
				});
			  
			}
		
		//S N.
		var td5= document.createElement("td");
		var t6 = document.createTextNode(snCounter); 
		td5.appendChild(t6); 
		tr.appendChild(td5);
		snCounter++;
		
		//first column
		var td1 = document.createElement("td");
		
		var a1 = document.createElement("a");
		var t1 = document.createTextNode(element.name); 
		a1.appendChild(t1); 
		td1.appendChild(a1);
		
		var div12 = document.createElement("div");
		div12.setAttribute('class', 'text-small text-color-light');
		var t2 = document.createTextNode(element.position); 
		div12.appendChild(t2); 
		td1.appendChild(div12);
		
		tr.appendChild(td1);
		
		
		//third column
		var td3= document.createElement("td");
		var t4 = document.createTextNode(element.LeaveRequestType); 
		td3.appendChild(t4); 			
		tr.appendChild(td3);
		
		//second column leave from to
		if(element.LeaveRequestType == "NormalLeave" || element.LeaveRequestType =="ExtendAuthorization" || element.LeaveRequestType =="CancelAuthorization"){
			var td2= document.createElement("td");
			var t3 = document.createTextNode(element.leaveFrom+ " to "+ element.LeaveTill); 
			td2.appendChild(t3); 
			tr.appendChild(td2);
		}else{
			var td2= document.createElement("td");
			var t3 = document.createTextNode(element.leaveDate ); 
			td2.appendChild(t3); 
			tr.appendChild(td2);
		}
		
		
		
		//fourth column
		var td6= document.createElement("td");
		var t7 = document.createTextNode(element.leaveReason); 
		td6.appendChild(t7); 
		tr.appendChild(td6);
		
		//fourth column
		if(element.LeaveRequestType == "NormalLeave" || element.LeaveRequestType =="ExtendAuthorization" || element.LeaveRequestType =="CancelAuthorization"){
			var td4= document.createElement("td");
			var t5 = document.createTextNode(element.noOfDays+" Days"); 
			td4.appendChild(t5); 
			tr.appendChild(td4);
		}else if (element.LeaveRequestType == "HourlyLeave"){
			var td4= document.createElement("td");
			var t5 = document.createTextNode(element.noOfHours+" Hours"); 
			td4.appendChild(t5); 
			tr.appendChild(td4);
		}else if (element.LeaveRequestType == "HalfDayLeave"){
			var td4= document.createElement("td");
			var t5 = document.createTextNode(element.halfDayType+" Half"); 
			td4.appendChild(t5); 
			tr.appendChild(td4);
		}
		
		
		/* var br = document.createElement("br");
		tr.appendChild(br); */
		
		document.getElementById('leaveRequests').appendChild(tr); 
    });	 
    
	}else{
		//for none pending leaves to line manager
		$("#requestLeaveTable").hide();
		
		var tr = document.createElement("tr");
		tr.setAttribute('class', 'dashboard-card box-shadow-none');
		
		var td= document.createElement("td");
		var t = document.createTextNode(" No Pending Leave Requests"); 
		td.appendChild(t); 
		tr.appendChild(td);
		
		document.getElementById('noRequests').appendChild(tr); 
		
		
		
	}
}



//get all leave Types
function getleavetypes(response){
	var select1 = document.getElementById("leaveType_s");
	var select2 = document.getElementById("leaveType_m");
	var select3 = document.getElementById("leaveType_h");
	
	response.forEach(function(element) {
		var opt = element.leaveType
	    var el = document.createElement("option");
	    el.textContent = opt;
	    el.value = opt;
	    select1.appendChild(el);		
	});
	response.forEach(function(element) {
		var opt = element.leaveType
	    var el = document.createElement("option");
	    el.textContent = opt;
	    el.value = opt;
	    select2.appendChild(el);		
	});
	response.forEach(function(element) {
		var opt = element.leaveType
	    var el = document.createElement("option");
	    el.textContent = opt;
	    el.value = opt;
	    select3.appendChild(el);		
	});
}








//submitting the leaverequest on click of request button for leave
function SubmitLeaveRequest(){
	
	//validation
	$("#singlef").validate({
	    rules: {
	    	leaveType_s: "required",
	    	singleDate_s: "required",
	    	leaveReason_s: "required",	
	    },
	    messages: {
	    	leaveType_s: "Please Specify Leave Type",
	    	singleDate_s: "Please Specify date",
	    	leaveReason_s: "Please Specify Leave Reason",
	    }
	})


	$("#multiplef").validate({
	    rules: {
	    	leaveType_m: "required",
	    	LeaveFromDate1: "required",
	    	LeaveToDate1: "required",
	    	leaveReason_m: "required"  	
	    },
	    messages: {
	    	leaveType_s: "Please Specify Leave Type",
	    	LeaveFromDate1: "Please Specify date",
	    	LeaveToDate1: "Please Specify date",
	    	leaveReason_s: "Please Specify Leave Reason"
	    }
	})
	$("#hourlyf").validate({
	    rules: {
	    	leaveType_h: "required",
	    	singleDate_h: "required",
	    	LeaveFromTime1: "required", 
	    	LeaveToTime1: "required",
	    	leaveReason_h: "required"
	    },
	    messages: {
	    	leaveType_h: "Please Specify Leave Type",
	    	singleDate_h: "Please Specify date",
	    	LeaveFromTime1: "Please Specify date",
	    	LeaveToTime1: "Please Specify date",
	    	leaveReason_h: "Please Specify Leave Reason"
	    }
	})
	
	
	
	/*var LeaveFrom = $('#LeaveFromDate1').val();
	var LeaveTo = $('#LeaveToDate1').val();*/
	var LeaveReason = $('#leaveReason_s').val();
	var LeaveType = $('#leaveType_s').val();
	var LeaveDate = $('#singleDate_s').val();
	
	
	if($('#oneDay').is(':checked')){
		
		
		if($("#singlef").valid()){	
			
			var data = {
					leaveFrom : LeaveDate,
					leaveTill : LeaveDate,
					leaveReason : LeaveReason,
					leaveType : {
						typeOfLeave : LeaveType,
					}		
			}	
			console.log(data);
			extraMain.ajax("/hrm/leavereq/makeLeaveRequest" ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"POST",main.getToken());
			
			
		}
		
	}else if($('#multipleDay').is(':checked')){
		if($("#multiplef").valid()){	

			var LeaveType = $('#leaveType_m').val();
			var LeaveFrom = $('#LeaveFromDate1').val();
			var LeaveTo = $('#LeaveToDate1').val();
			var LeaveReason = $('#leaveReason_m').val();
			
			var data = {
					leaveFrom : LeaveFrom,
					leaveTill : LeaveTo,
					leaveReason : LeaveReason,
					leaveType : {
						typeOfLeave : LeaveType,
					}		
			}
		
			extraMain.ajax("/hrm/leavereq/makeLeaveRequest" ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"POST",main.getToken());
		}
	}
	
	else if($('#firstHalf').is(':checked')){
		if($("#singlef").valid()){	
			var halfType = "first";
			
			var data = {
					leaveDate : LeaveDate,
					leaveReason : LeaveReason,
					halfDayType : halfType,
					leaveType : {
						typeOfLeave : LeaveType,
					}		
			}
			extraMain.ajax("/hrm/halfdayleave/halfDayLeaveRequest" ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"POST",main.getToken());
		}
		
		
	}else if($('#secondHalf').is(':checked')){
		if($("#singlef").valid()){	
			var halfType = "second";
			var data = {
					leaveDate : LeaveDate,
					leaveReason : LeaveReason,
					halfDayType : halfType,
					leaveType : {
						typeOfLeave : LeaveType,
					}		
			}
			
			extraMain.ajax("/hrm/halfdayleave/halfDayLeaveRequest" ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"POST",main.getToken());
		}
	}
	
	else if($('#hourly').is(':checked')){
		if($("#hourlyf").valid()){	
			var leaveType = $('#leaveType_h').val();
			var leaveDate = $('#singleDate_h').val();
			var hourFrom = $('#LeaveFromTime1').val()+":00"
			var hourTill = $('#LeaveToTime1').val()+":00"
			var leaveReason = $('#leaveReason_h').val();
			
			
			var data = {
					leaveDate : leaveDate,
					leaveReason : LeaveReason,
					hourFrom : hourFrom,
					hourTill : hourTill,
					leaveType : {
						typeOfLeave : LeaveType,
					}		
			}
			console.log(data);
			
			extraMain.ajax("/hrm/hourlyleave/saveLeaveRequest" ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"POST",main.getToken());
			
		}
	}		
}	

function employeeOnLeaveBoard(response){
	response.forEach(function(element) {	
		
		var li = document.createElement("li");
		li.setAttribute('class', 'border-bottom onhover-background-grey');
		
		var div1 = document.createElement("div");
		div1.setAttribute('class', 'font-weight-semi-bold');
		var t1 = document.createTextNode(element.name); 
		div1.appendChild(t1);
		li.appendChild(div1);
		
		var div2 = document.createElement("div");
		div2.setAttribute('class', 'text-small');
		var t2 = document.createTextNode(element.position); 
		div2.appendChild(t2);
		li.appendChild(div2);
		document.getElementById('employeeOnLeave').appendChild(li);
	
	});
}

//my leaves table
jQuery(function($) {
	var roleTable = $('#table')

			.DataTable(
					
					{
						"responsive" : true,

						"columnDefs" : [ {
							"targets" : [ 0 ],
							"visible" : false
						},

						],

						"order" : [ 0, "desc" ],
						/* "paging": false, */

						"ajax" : {
							"url" : "/hrm/leavereq/getMyLeaveRequest",
							"dataSrc" : "",
							"beforeSend" : function(request) {
								request.setRequestHeader(
										"Authorization", main.getToken());
							},
						},

						"columnDefs" : [
								{
									"targets" : [ 0 ],
									"visible" : false
								},
								{
									targets : 5,
									render : function(data, type, row) {
										var color = 'black';
										if (data == "Pending") {
											color = 'badge badge-pill badge-warning';
										}
										if (data == "Approved") {
											color = 'badge badge-pill badge-success';
										}
										if (data == "Rejected") {
											color = 'badge badge-pill badge-danger';
										}
										if (data == "Cancelled") {
											color = 'badge badge-pill badge-secondary';
										}
										return '<span class="' +  color + '">'
												+ data + '</span>';

									}
								} ],

						"columns" : [ {
							"data" : "requestId"
						}, 
						{
								sortable : false,
								"render" : function(data, meta, full) {
								if(full.LeaveRequestType=="NormalLeave" && full.extendTill!="")	{
									return (full.LeaveRequestType + "(Extended:" + full.extendStatus + ")" )
								}else{
									return full.LeaveRequestType
								}
							}
						},
						{
							sortable : false,
							"render" : function(data, meta, full) {
								if(full.LeaveRequestType == "NormalLeave"){							
									  	return (full.leaveFrom+" to "+full.LeaveTill);
									  }
								  else if(full.LeaveRequestType == "HalfDayLeave"){								
										  return full.leaveDate
									  }
								  else{
									  return full.leaveDate
								  }
								
							}
							
						},					
						{
							sortable : false,
							"render" : function(data, meta, full) {
								if(full.LeaveRequestType == "NormalLeave"){								  	
									  	return (full.noOfDays+" Days");
									  }
								  else if(full.LeaveRequestType == "HalfDayLeave"){	  
									  return (full.halfDayType+" Half");
									  }
								  else{			  
									  
									 return (full.noOfHours +" Hours");
								  }
								
							}
						}, {
							"data" : "leaveType"
						}, {
							"data" : "requestStatus"
						},{
							sortable : false,
							"render" : function(data, meta, full, row) {
								/*console.log(full.LeaveTill);*/
								if(full.LeaveRequestType=="NormalLeave"){
									var d3 = full.LeaveTill;
								}else{
									var d3=full.leaveDate;
								}
								console.log(d3);
								var d2= new Date(d3.replace(/-/g,'/'));	
								console.log(d2);
								if(full.LeaveRequestType != "NormalLeave" || full.extendTill!= "" || full.requestStatus == "Rejected" ||  d2 < getTodaysDate()){
									
									
									return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item'"+
									"onClick='viewMyLeaves("
								    + JSON.stringify(full)
								    + ")'>View</a></div></div>"
								}else{
									return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item' onClick='viewMyLeaves("
									+ JSON.stringify(full)
									+ ")'>View</a><a class='dropdown-item' onClick='extendLeave("
									+ JSON.stringify(full)
									+ ")'>Extend Leave</a><a class='dropdown-item' onClick='cancelLeave("
									+ JSON.stringify(full)
									+ ")'>Cancel Leave</a></div></div>"
									
								}
								
							}
						} 
						]
					});
})

//get current date for checking extend leave
function getTodaysDate(){
	var d1 = new Date();
	
	return d1;
}

function viewMyLeaves(data){
	console.log(data);	
	 $('#leaveViewModal').modal('show'); 
	 getLeaveDetails(data);

	 
} 

function getLeaveDetails(response){
	 		
		  console.log(response);	 
		  $(".fullname").text(response.name);
		  
		  $(".department").text(response.department);
		  $(".position").text(response.position);
		  $(".leaveReason1").text(response.leaveReason);
		  $(".leaveType").text(response.leaveType);
		  
		  $(".extendTill").text(response.extendTill);
		  $(".extendDays").text(response.extendDays);
		  
		  $(".requestedBy").text(response.name);
		  $(".requestedOn").text(response.createdAt);
		  
		  $("#authorizationTableBody").empty();
		  
		  if(response.requestedBy!=""){
			  var requestRow= $('<tr class = "recommendData"><td scope="row">Requested By: </th><td class = "recommendBy">'+response.name+'</td><td scope="row">Requested On: </th><td class="recommendOn">'+response.createdDate+" "+response.createdTime+'</td></tr>');
			  $('#authorizationTableBody').append( requestRow);
		  }
	   
	      if(response.recommendedBy!=""){
			  var recommendRow= $('<tr class = "recommendData"><td scope="row">Recommended By: </th><td class = "recommendBy">'+response.recommendedBy+'</td><td scope="row">Recommended On: </th><td class="recommendOn">'+response.recommendedDate+'</td></tr>');
			  $('#authorizationTableBody').append( recommendRow);
		  }
	      
	      if(response.responseBy!=""){
			  var responseRow= $('<tr class = "recommendData"><td scope="row">'+response.requestStatus+" By:"+'</th><td class = "recommendBy">'+response.responseBy+'</td><td scope="row">'+response.requestStatus+" On"+'</th><td class="recommendOn">'+response.responseDate+" "+response.responseTime+'</td></tr>');
			  $('#authorizationTableBody').append( responseRow);
		  }
		  
		  if(response.remarks!=""){
			  var lmRemarksRow= $('<tr><td scope="row">Line Manager Remarks: </th><td scope="row">'+ response.remarks+'</th></tr>');
			  $('#authorizationTableBody').append( lmRemarksRow);
		  }
		  
		  if(response.extendTill!=""){
			  var extendRow= $('<tr class = "recommendData"><td scope="row">Extend By: </th><td class = "recommendBy">'+response.name+'</td><td scope="row">Extended On: </th><td class="recommendOn">'+response.extendedDate+'</td></tr>');
			  $('#authorizationTableBody').append( extendRow);
		  }
		  
		  if(response.extendRemarks!=null){
			  var extendremarksRow= $('<tr><td scope="row">Extend	 Remarks: </th><td scope="row">'+ response.extendRemarks+'</th></tr>');
			  $('#authorizationTableBody').append( extendremarksRow);
		  }
		  
		  if(response.extendedResponseDate!=null){
			  var extendResponseDate= $('<tr class = "recommendData"><td scope="row">Extend '+response.extendStatus+" By"+'</th><td class = "recommendBy">'+response.extendResponseBy+'</td><td scope="row">Extend '+response.extendStatus+" On"+'</th><td class="recommendOn">'+response.extendedResponseDate+'</td></tr>');
			  $('#authorizationTableBody').append( extendResponseDate);
		  }
		  if(response.extendResponseRemarks!=null){
			  var extendremarksRow= $('<tr><td scope="row">Line Manager Extend Remarks: </th><td scope="row">'+ response.extendResponseRemarks+'</th></tr>');
			  $('#authorizationTableBody').append( extendremarksRow);
		  }
		  
		  //cancel
		  
		  if(response.cancelDate!=null){
			  var extendRow= $('<tr class = "recommendData"><td scope="row">Cancel From: </th><td class = "recommendBy">'+response.cancelFrom+'</td><td scope="row">Cancelled On: </th><td class="recommendOn">'+response.cancelDate+'</td></tr>');
			  $('#authorizationTableBody').append( extendRow);
		  }
		  
		  if(response.cancelRemarks!=null){
			  var extendremarksRow= $('<tr><td scope="row">Cancel Remarks: </th><td scope="row">'+ response.cancelRemarks+'</th></tr>');
			  $('#authorizationTableBody').append( extendremarksRow);
		  }
		  
		  if(response.cancelResponseDate!=null){
			  var extendResponseDate= $('<tr class = "recommendData"><td scope="row">Cancel '+response.cancelStatus+" By"+'</th><td class = "recommendBy">'+response.cancelResponseBy+'</td><td scope="row">Cancel '+response.cancelStatus+" On"+' </th><td class="recommendOn">'+response.cancelResponseDate+'</td></tr>');
			  $('#authorizationTableBody').append( extendResponseDate);
		  }
		  if(response.cancelResponseRemarks!=null){
			  var extendremarksRow= $('<tr><td scope="row">Cancel Response Remarks: </th><td scope="row">'+ response.cancelResponseRemarks+'</th></tr>');
			  $('#authorizationTableBody').append( extendremarksRow);
		  }
		  
		  
		  
		  
		  
		 
		  if(response.requestStatus =="Pending"){
			  console.log("no recommend by");
			  $(".responseData").hide();
		  }else{
			  $(".responseData").show();
			  $(".responseBy").text(response.response);
			  $(".responseOn").text(response.updatedOn);
		  }
		  
		  //to be done for extend and cancel date and by:
		  
		  
		  if(response.LeaveRequestType == "NormalLeave"){
			  $(".LeaveRequestType").text(response.LeaveRequestType);
			  	$(".leaveDated").text(response.leaveFrom+" to "+response.LeaveTill);
			  	$(".noOfDays").text(response.noOfDays+" Days");
			  }
		  else if(response.LeaveRequestType == "HalfDayLeave"){
			  $(".LeaveRequestType").text(response.LeaveRequestType+"("+response.halfDayType+")");
				  $(".leaveDated").text(response.leaveDate);
				  $(".noOfDays").text(response.halfDayType+" Half");
			  }
		  else{
			  $(".LeaveRequestType").text(response.LeaveRequestType);
			  $(".leaveDated").text(response.leaveDate);
			  $(".noOfDays").text(response.leaveFrom+" to "+ response.LeaveTill);
		  }
		  
		  
}

function extendLeave(data){
	$('#leaveExtendModal').modal('show');
	getLeaveDetails(data);

	  //for min date for extend leave
	  var mimDateExtend = data.LeaveTill;
	  $('#extendTillDate').attr('min',mimDateExtend); 
	  
	
	  
	extendLeaveRequest(data);
}


function extendLeaveRequest(response){
	
	$( "#extend" ).click(function() {

		//validation
		$("#extendForm").validate({
		    rules: {
		    	extendTillDate: "required",
		   
		    },
		    messages: {
		    	extendTillDate: "Please Specify Date Till To Extend",
		  
		    }
		})
		
		if($("#extendForm").valid()){	
			
			var extendTill = $('#extendTillDate').val();
			var remarks = $('#extendRemarks').val();
			console.log(extendTill);
			var data = {
					extendTill : extendTill,
					extendRemarks : remarks,
			}
			console.log(response);
			extraMain.ajax("/hrm/leavereq/extendLeaveRequest/" + response.requestId ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
				$('#examplemodalcenter').modal('hide');
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"PUT",main.getToken());
		
		}
	});
	
}

function cancelLeave(response){

	$('#leaveCancelModal').modal('show');
	
	getLeaveDetails(response);
	  //for min max date in cancel leave
	  var maxDateCancel = response.LeaveTill;
	  var minDateCancel = response.leaveFrom;
	  $('#cancelFromDate').attr('min',minDateCancel); 
	  $('#cancelFromDate').attr('max',maxDateCancel); 
	
	  $("#cancelFrom").hide();
	
	
	$('#partialCancel').click(function(){
		$("#cancelFrom").show();
	});
	
	$('#fullCancel').click(function(){
		$("#cancelFrom").hide();
	});
	
	
	$( "#cancelBtn" ).click(function() {
		
		if($('#partialCancel').is(':checked')){
			
			data={
					cancelFrom : $('#cancelFromDate').val(),
					cancelRemarks : $('#cancelRemarks').val()
			}
	
			extraMain.ajax("/hrm/leavereq/cancelLeaveRequestPartial/" + response.requestId ,JSON.stringify(data),function(response){
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
				$('#examplemodalcenter').modal('hide');
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"PUT",main.getToken());
			
		}
		if($('#fullCancel').is(':checked')){
			
			data={				
					cancelRemarks : $('#cancelRemarks').val()
			}
			
			extraMain.ajax("/hrm/leavereq/cancelLeaveRequest/" + response.requestId ,JSON.stringify(data),function(response){	
				$('#table').DataTable().ajax.reload();
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")	
				$('#examplemodalcenter').modal('hide');
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"PUT",main.getToken());
			
		}
	
	});
		
	
}






//for testing
$( document ).ready(function() {
	main.ajax("/hrm/leavereq/getAllLeaveResponses/", "",function(response){
	},"GET",main.getToken());
	
});





//my responses
jQuery(function($) {
	var roleTable = $('#table1')

			.DataTable(
					
					{
						"responsive" : true,
						"aaSorting": [],

						"ajax" : {
							"url" : "/hrm/leavereq/getAllLeaveResponses",
							"dataSrc" : "",
							"beforeSend" : function(request) {
								request.setRequestHeader(
										"Authorization", main.getToken());
							},
						},

						"columnDefs" : [
								{
									targets : 5,
									render : function(data, type, row) {
										var color = 'black';
										if (data == "Approved") {
											color = 'badge badge-pill badge-success';
										}
										if (data == "Rejected") {
											color = 'badge badge-pill badge-danger';
										}
										if (data == "Cancelled") {
											color = 'badge badge-pill badge-secondary';
										}
										return '<span class="' +  color + '">'
										+ data + '</span>';

									}
								} ],

						"columns" : [ 
						{
							"data" : "name"
						},
						
						
						{
							sortable : false,
							"render" : function(data, meta, full) {
							if(full.LeaveRequestType=="NormalLeave" && full.extendTill!="")	{
								return (full.LeaveRequestType + "(Extended:" + full.extendStatus + ")" )
							}else{
								return full.LeaveRequestType
							}
						}
					},
					{
						sortable : false,
						"render" : function(data, meta, full) {
							if(full.LeaveRequestType == "NormalLeave"){							
								  	return (full.leaveFrom+" to "+full.LeaveTill);
								  }
							  else if(full.LeaveRequestType == "HalfDayLeave"){								
									  return full.leaveDate
								  }
							  else{
								  return full.leaveDate
							  }
							
						}
						
					},					
					{
						sortable : false,
						"render" : function(data, meta, full) {
							if(full.LeaveRequestType == "NormalLeave"){								  	
								  	return (full.noOfDays+" Days");
								  }
							  else if(full.LeaveRequestType == "HalfDayLeave"){	  
								  return (full.halfDayType+" Half");
								  }
							  else{			  
								  						
								 return (full.noOfHours +" Hours");
							  }
							
						}
					},
						
						
						{
							"data" : "leaveType"
						}, 		
						{
							"data" : "requestStatus"
						},{
							sortable : false,
							"render" : function(data, meta, full) {
								return "<div onClick='viewMyLeaves("
								+ JSON.stringify(full)
								+ ")'> <small class='btn-link font-weight-bold'>View</small> </div>"
								
							/*sortable : false,
							"render" : function(data, meta, full) {
								return <div class="" 'onClick="viewMyLeaves('
								+ full.requestId
								+ ')"'>view</div>
*/
							}
						} 
						]
					});
})


/*return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'><a class='dropdown-item'"+
									"onClick='viewMyLeaves("
								    + JSON.stringify(full)
								    + ")'>View</a></div></div>"*/
