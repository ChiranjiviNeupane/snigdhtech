
$(document).ready(function() {
	main.ajax("/hrm/leavereq/getEmployeeLeaveDate","",function(response){
			
	}
		,"GET",main.getToken());
	
	
	 var calendar = $('#calendar').fullCalendar({
			
			fixedWeekCount:false,
			/*weekends:false,*/
			showNonCurrentDates: false,
 	    	header:{
 	     		left:'prev,next today',
 	     		center:'title',
 	     		right:'month,agendaWeek,agendaDay'
 	    	},
	 	   	eventSources: [ 
 	   		{
 		   		events: function(start, end, timezone, callback) {
 		    		main.ajax("/hrm/leavereq/getEmployeeLeaveDate","",function(response){
 		    			var events = [];
		        		response.forEach(function(element){
		        			events.push(element);
		        		});
		        		callback(events);
 		    		}
 		    		,"GET",main.getToken());
 		  		},
	 	  		color: 'green',   // an option!
	 	  		textColor: 'white' // an option! 
 	   		}/*,
 	   
 	  {
 		events: function(start, end, timezone, callback) {
 		    main.ajax("/hrm/leavereq/getEmployeeLeaveDate","",function(response){
 		    	var events = [];
		        response.forEach(function(element){
		        	events.push(element);
		        });
		        callback(events);
 		    }
 		    ,"GET",main.getToken());
 		},
 		color: 'green',   // an option!
 		textColor: 'white' // an option!
 	   }*/
 	 ] 
  });
});
