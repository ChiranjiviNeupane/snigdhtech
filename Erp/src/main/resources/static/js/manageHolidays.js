$(document).ready( function () {

jQuery(function($) {
	var roleTable = $('#holidayTable')

			.DataTable(
					
					{
						"responsive" : true,
						"lengthChange": false,
						
						/* "paging": false, */

						"ajax" : {
							"url" : "/hrm/holiday/getHoliday",
							"dataSrc" : "",
							"beforeSend" : function(request) {
								request.setRequestHeader(
										"Authorization", main.getToken());
							},
						},

		

						"columns" : [ {
							"data" : "dateFrom"
						}, {
							"data" : "dateTill"
						}, 
						{
							"data" : "nameOfHoliday"
						},
						{
							sortable : false,
							"render" : function(data, meta, full) {
								/*console.log(full);*/
								return "<div class='dropdown cursor-pointer employee-list__dropdown mr-3 text-center'><a class='' type='' id='dropdownmenubutton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-ellipsis-v'></i></a><div class='dropdown-menu employee-list__dropdown-menu' aria-labelledby='dropdownmenubutton'>"
								+ "<a class='dropdown-item' onClick='editHoliday("
								+ JSON.stringify(full)
								+ ")'>Edit</a><a class='dropdown-item' onClick='deleteHoliday("
								+ JSON.stringify(full)
								+ ")'>Delete</a></div></div>"

							}
						} 
						]
					});
})
});


/*edit Holiday*/
function editHoliday(data){
	
	$('#exampleModal').modal('show');
	
	$('#dateFromEdit').val(data.dateFrom);
	$('#dateTillEdit').val(data.dateTill);	
	$('#holidayNameEdit').val(data.nameOfHoliday);
	 
	fieldCorrectionValue();

	
	$('#editHoliday').click(function(){
		
		console.log(data);
		
		$("#workOnHoliday").validate({
		       rules: {
		    	   DateFromEdit: "required",
		    	   DateTillEdit: "required",
		    	   holidayNameEdit:"required",
		           
		           
		       },
		       messages: {
		           DateFromEdit: "Please Specify Date",
		    	   DateTillEdit: "Please Specify Date",
		    	   holidayNameEdit:"Please Specify Holiday Name",
		           

		       }
		   })
		
		data1 = {
			
			nameOfHoliday: $('#holidayNameEdit').val(),
		    dateFrom : $('#dateFromEdit').val(),
		    dateTill : $('#dateTillEdit').val()
		}
		
		if($('#workOnHoliday').valid()){
			
			
			
			extraMain.ajax("/hrm/holiday/updateHoliday/"+ data.id ,JSON.stringify(data1),function(response){	
				toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
				$('#exampleModal').modal('hide');
				$('#holidayTable').DataTable().ajax.reload();
				
			},
			function(response){
				toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
			},"PUT",main.getToken());
			
		}
		
		
		
	});
}

//Deleting a holiday

function deleteHoliday(data){
	$('#exampleModal1').modal('show');
data1 = {
			
			nameOfHoliday: $('#holidayNameEdit').val(),
		    dateFrom : $('#dateFromEdit').val(),
		    dateTill : $('#dateTillEdit').val()
		}
	console.log(data);


$('#deleteHoliday').click(function(){
	
		extraMain.ajax("/hrm/holiday/deleteHoliday/"+ data.id ,"",function(response){	
			toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")
			$('#exampleModal').modal('hide');
			$('#holidayTable').DataTable().ajax.reload();
			
		},
		function(response){
			toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
		},"DELETE",main.getToken());
	
	});
}



//onclick pop up modal
$( "#addHolidayBtn" ).click(function() {
	$('#modalholidayadd').modal('show');
});

//submitting and saving the holiday data 
$( "#addHoliday" ).click(function() {
	
	//validation
	$("#addHolidayf").validate({
	    rules: {
	    	holidayName: "required",
	    	holidayDateFrom: "required",
	    	holidayDateTill: "required",	
	    },
	    messages: {
	    	holidayName: "Please Specify Holiday Title",
	    	holidayDateFrom: "Please Specify date",
	    	holidayDateTill: "Please Specify date",
	    }
	})
	
	var data ={
		    nameOfHoliday: $('#holidayName').val(),
		    dateFrom : $('#holidayDateFrom').val(),
		    dateTill : $('#holidayDateTill').val()
		    	
		}
	
	if($('#addHolidayf').valid()){
		
		/*main.ajax("/hrm/holiday/saveHoliday", JSON.stringify(data), function(response) {
			alert("added");
			location.reload();
		}, "POST", main.getToken());*/
		
		extraMain.ajax("/hrm/holiday/saveHoliday" ,JSON.stringify(data),function(response){	
			toastr.success('Successful', response, {timeOut: 5000}).css("width","900px","height","50px")				
		},
		function(response){
			toastr.error('Unsucessful', response.responseText, {timeOut: 5000}).css("width","900px","height","50px")
		},"POST",main.getToken());
		
	}
	
	});