// getting dashboard parameter value
$(document).ready(function () {
	main.ajax("hrm/dashboard", "", function(response) {
		
		dashboardparam(response);
	}, "GET", main.getToken());
	
	function dashboardparam(response){
		
		
		
		
	//lopping through all new users and calling function to create dynamically html elements and value	
	response.users.new.forEach(function(element) {	
		document.getElementById('newEmp').appendChild(createDivForNewAndLeftEmp(element));
		
	});
	
	//lopping through all left users and calling function to create dynamically html elements and value	
	response.users.left.forEach(function(element) {	
		document.getElementById('leftEmp').appendChild(createDivForNewAndLeftEmp(element));
		
	});
	
	//function to create html elements for new and left users
	function createDivForNewAndLeftEmp(element){
		
		var a = document.createElement("a");
		a.setAttribute('class', 'onhover-background-grey d-block px-4 py-2');
		
		var div1 = document.createElement("div");
		a.appendChild(div1);
		
		var h6 = document.createElement("h6");
		var t1 = document.createTextNode(element.name); 
		h6.appendChild(t1);
		div1.appendChild(h6);
		
		var span1 = document.createElement("span");
		span1.setAttribute('class', 'float-right font-weight-light display-sm-none text-small-small');
		
		if(element.createdAt){
			
			var date = element.createdAt;
			var splittedDate = date.split('T');
			var t2 = document.createTextNode(splittedDate[0]);
		}
		else{
			var t2 = document.createTextNode(element.terminationDate);
		}
		
		span1.appendChild(t2);
		div1.appendChild(span1);
		
		var p1 = document.createElement("p");
		p1.setAttribute('class', 'font-weight-light text-small m-0');
		var t3 = document.createTextNode(element.department);
		p1.appendChild(t3);
		div1.appendChild(p1);
		
		return a;
		
		
	}
			
		
		$("#male").text(response.sex.male);
		$("#female").text(response.sex.female);
		$("#others").text(response.sex.other);
		$("#total").text(response.sex.male + response.sex.female + response.sex.other);

		$("#below20").text(response.ageGroup.below20);
		$("#twentyToThirty").text(response.ageGroup.twentyToThirty);
		$("#thirtyTofourty").text(response.ageGroup.thirtyTofourty);
		$("#fourtyTofifty").text(response.ageGroup.fourtyTofifty);
		$("#fiftyTosixty").text(response.ageGroup.fiftyTosixty);
		$("#sixtyPlus").text(response.ageGroup.sixtyPlus);
		$("#averageAge").text(response.ageGroup.average);
		
		
	    var chartAgeGroup = $("#chart-agegroup");
	    var myChart = new Chart(chartAgeGroup, {
	        type: 'line',
	        data: {
	            labels: [
	                "below 20",
	                "20-30",
	                "30-40",
	                "40-50",
	                "50-60",
	                "60 above"
	            ],
	            datasets: [{
	                data: [
	                	response.ageGroup.below20,
	                	response.ageGroup.twentyToThirty,
	                	response.ageGroup.thirtyTofourty,
	                	response.ageGroup.fourtyTofifty,
	                	response.ageGroup.fiftyTosixty,
	                	response.ageGroup.sixtyPlus
	                ],
	                backgroundColor: 'rgba(38, 128, 235, 0.2)',
	                borderColor: 'rgba(38, 128, 235, 0.9)',
	                borderWidth: 2
	            }]
	        },
	        options: {
	            legend: {
	                display: false,
	            }
	        }
	    });


    var chartDesignation = $("#chart-designation");
    var myChart = new Chart(chartDesignation, {
        type: 'bar',
        data: {
            labels: [
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I"
            ],
            datasets: [
                {
                    label: 'Designation',
                    data: [
                        response.level.A,
                        response.level.B,
                        response.level.C,
                        response.level.D,
                        response.level.E,
                        response.level.F,
                        response.level.G,
                        response.level.H,
                        response.level.I,
                    ],
                    backgroundColor: 'rgba(38, 128, 235, 0.6)',
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    barThickness: 16,
                }]
            },
            legend: {
                display: false,
            }
        }
    });
    var chartContractType = $("#chart-contracttype");
    var myChart = new Chart(chartContractType, {
        type: 'doughnut',
        data: {
            labels: [
                "Permanent",
                "ShortTerm",
                "Casual",
                "Fixed",
                "Trainee",
                "Internship"
            ],
            datasets: [
                {
                    label: [
                        "Red",
                        "Blue",
                        "Indigo",
                        "Green",
                        "Orange",
                        "Red",
                    ],
                    data: [
                        response.contract.permanent,
                        response.contract.shortTerm,
                        response.contract.casual,
                        response.contract.fixed,
                        response.contract.trainee,
                        response.contract.internship,
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.4)',
                        'rgba(75, 192, 192, 0.4)',
                        'rgba(0, 0, 0, 0.6)',
                        'rgba(255, 159, 32, 0.4)',
                        'rgba(66, 159, 64, 0.4)',
                        'rgba(255, 350, 64, 0.4)',
                    ],
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: .8
                }
            ]
        },
        options: {
            maintainAspectRatio: true,
            legend: {
              position: 'bottom',
              display: true,
            }
        }
    });
    var chartDepartment = $("#chart-department");
    var myChart = new Chart(chartDepartment, {
        type: 'polarArea',
        data: {
            labels: [
                "Administration",
                "Communication",
                "Finance",
                "Human Resource",
                "Program",
            ],
            datasets: [
                {
                    data: [
                        response.department.administration,
                        response.department.communication,
                        response.department.finance,
                        response.department.humanResource,
                        response.department.program,
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(75, 192, 192, 0.5)',
                        'rgba(0, 0, 0, 0.6)',
                        'rgba(255, 159, 32, 0.5)',
                        'rgba(66, 159, 64, 0.5)',
                    ],
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: .4
                }
            ]
        },
        options: {
            legend: {
                // position: 'top'
                display: true,
            }
        }
    });
    var chartProvince = $("#chart-province");
    var myChart = new Chart(chartProvince, {
        type: 'pie',
        data: {
            labels: [
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
            ],
            datasets: [
                {
                    data: [
                        response.state.one,
                        response.state.two,
                        response.state.three,
                        response.state.four,
                        response.state.five,
                        response.state.six,
                        response.state.seven, 
                    ],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(75, 192, 192, 0.5)',            
                        'rgba(255, 159, 32, 0.5)',
                        'rgba(0, 0, 0, 0.6)',
                        'rgba(66, 159, 64, 0.5)',
                        'rgba(255, 350, 64, 0.5)',
                    ],
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: .4
                }
            ]
        },
        options: {
            legend: {
                position: 'top',
                display: true,
            }
        }
    });
    var chartReligion = $("#chart-religion");
    var myChart = new Chart(chartReligion, {
        type: 'horizontalBar',
        data: {
            labels: [
                "Buddhist",
                "Christian",
                "Hindu",
                "Islam",
                "Jain",
                "Sikh",
                "Other",
                "Secular",
            ],
            datasets: [
                {
                    label: 'Religion',
                    data: [
                        response.religion.buddhist,
                        response.religion.christian,
                        response.religion.hindu,
                        response.religion.islam,
                        response.religion.jain,
                        response.religion.sikh,
                        response.religion.other,
                        response.religion.secular,
                    ],
                    backgroundColor: 'rgba(38, 128, 235, 0.6)',
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: .4
                }
            ]
        },
        options: {
            legend: {
                display: false,
            },
            scales: {
                yAxes: [{
                    barThickness: 16,
                }]
            }
        }
    });
    var chartEthnicity = $("#chart-ethnicity");
    var myChart = new Chart(chartEthnicity, {
        type: 'radar',
        data: {
            labels: [
                "Adibasi",
                "Dalit",
                "Janjati",
                "Khas Arya",
                "Madhesi",
                "Muslim",
                "Tharu"
            ],
            datasets: [
                {
                    data: [
                        response.ethnicity.adibasi,
                        response.ethnicity.dalit,
                        response.ethnicity.janjati,
                        response.ethnicity.khasarya,
                        response.ethnicity.madhesi,
                        response.ethnicity.muslim,
                        response.ethnicity.tharu,
                    ],
                    backgroundColor: [
                        'rgba(38, 128, 235, 0.6)'
                    ],
                    borderColor: [
                        '#FFFFFF'
                    ],
                    borderWidth: .8
                }
            ]
        },
        options: {
            legend: {
                display: false,
            }
        }
    });
	}
});

