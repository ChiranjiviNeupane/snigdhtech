
//adding new cost center from modal
$( "#addCostCenter" ).click(function() {
	data={
		code : $('#costCenterCode').val(),
		name : $('#costCenterName').val()
	}
	
	main.ajax( "hrm/costCenter/save",JSON.stringify(data),function(response){   
		alert( "Cost Center added" );
    	location.reload();
	},"POST",main.getToken());
	
	console.log(data);
	
	
  
});
//end adding cost center

//adding source of fund from modal
$( "#addSourceOfFund" ).click(function() {
	var id = $('#sourceOfFundCostCenter').val()
	data={
		code : $('#sourceOfFundCode').val(),
		name : $('#sourceOfFundName').val(),
		
	}
	
	main.ajax( "hrm/sourceOfFund/"+id+"/save",JSON.stringify(data),function(response){   
		alert( "Sof added" );
    	location.reload();
	},"POST",main.getToken());
	
	console.log(data);
	
	
  
});

//end adding sof

//adding project from modal
$( "#addProject" ).click(function() {
	var id = $('#projectSourceOfFund').val()
	data={
		code : $('#projectCode').val(),
		name : $('#projectName').val(),
		
	}
	
	main.ajax( "hrm/sourceOfFund/"+id+"/save",JSON.stringify(data),function(response){   
		alert( "project added" );
    	location.reload();
	},"POST",main.getToken());
	
	console.log(data);
	
	
  
});

//end adding project

//adding activity from modal
$( "#addActivity" ).click(function() {
	var id = $('#activityProject').val()
	data={
		code : $('#activityCode').val(),
		name : $('#activityName').val(),
		
	}
	
	main.ajax( "hrm/activity/"+id+"/save",JSON.stringify(data),function(response){   
		alert( " new activity added" );
    	location.reload();
	},"POST",main.getToken());
	
	console.log(data);
	
	
  
});

//end adding project




$(document).ready(function (){
	console.log("hello");
	
	main.ajax( "/hrm/costCenter/findall","",function(response){   
		var select = document.getElementById("sourceOfFundCostCenter");
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	main.ajax( "/hrm/sourceOfFund/findAllListing","",function(response){   
		var select = document.getElementById("projectSourceOfFund");
		console.log(response);
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	main.ajax( "/hrm/project/findAllListing","",function(response){   
		var select = document.getElementById("activityProject");
		console.log(response);
		response.forEach(function(element) {
			var opt = element.code + "  " + element.name;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = element.id;
			select.appendChild(el);
		});
		
	},"GET",main.getToken());
	
	
	//for Cost Center
	$('#costCenterTable').DataTable(
		
		{
			"responsive": true,
			"ajax" : {
				"url" : "/hrm/costCenter/findall",
				"dataSrc" : "",
				"beforeSend" : function(request) {
					request.setRequestHeader(
							"Authorization", main
									.getToken());
				},
			},
			"columnDefs": [
			    { 
			    	"searchable": false, 
			      	"targets": -1 
			      }
			  ],
							
			"columns" : [
					{
						"render" : function(data, type,
								full, meta) {
							
							if(full.code){
								return full.code
							}else{
								return ""
							}
						}
					},
					{
						"render" : function(data, type,
								full, meta) {
							if(full.name){
								return full.name
							}else{
								return ""
							}
						}
					},
					
					{
						sortable : false,
						"render" : function(data, type,
								full, meta) {
							
							return '<div class="dropdown cursor-pointer employee-list__dropdown mr-3 text-center"><a class="" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton"><a class="dropdown-item onhover-background-grey" href="/employoee-info?getId='
									+ full.id
									+ '">View</a><a class="dropdown-item onhover-background-grey" href="/update-employoee?getId='
									+ full.id
									+ '">Edit</a><a href="#" class="dropdown-item onhover-background-grey" onClick="deleteHrm('
									+ full.id 
									+ ')">Terminate</a></div></div>'

						}
					}
			]
		
	});
	
	
	//for Source Of Fund
	$('#sourceOfFund').DataTable(
		
		{
			"responsive": true,
			"ajax" : {
				"url" : "/hrm/sourceOfFund/findAllListing",
				"dataSrc" : "",
				"beforeSend" : function(request) {
					request.setRequestHeader(
							"Authorization", main
									.getToken());
				},
			},
			"columnDefs": [
			    { 
			    	"searchable": false, 
			      	"targets": -1 
			      }
			  ],
							
			"columns" : [
					{
						"render" : function(data, type,
								full, meta) {
							if(full.code){
								return full.code
							}else{
								return ""
							}
						}
					},
					{	
						"render" : function(data, type,
								full, meta) {
							if(full.name){
								return full.name
							}else{
								return ""
							}
						}
					},
					{
						"render" : function(data, type,
								full, meta) {
							if(full.costCenter){
								return full.costCenter
							}else{
								return ""
							}
						}
					},
					
					{
						sortable : false,
						"render" : function(data, type,
								full, meta) {
							
							return '<div class="dropdown cursor-pointer employee-list__dropdown mr-3 text-center"><a class="" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton"><a class="dropdown-item onhover-background-grey" href="/employoee-info?getId='
									+ full.id
									+ '">View</a><a class="dropdown-item onhover-background-grey" href="/update-employoee?getId='
									+ full.id
									+ '">Edit</a><a href="#" class="dropdown-item onhover-background-grey" onClick="deleteHrm('
									+ full.id 
									+ ')">Terminate</a></div></div>'

						}
					}
			]
		
	});
	
	//for Project
	$('#project').DataTable(
		
		{
			"responsive": true,
			"ajax" : {
				"url" : "/hrm/project/findAllListing",
				"dataSrc" : "",
				"beforeSend" : function(request) {
					request.setRequestHeader(
							"Authorization", main
									.getToken());
				},
			},
			"columnDefs": [
			    { 
			    	"searchable": false, 
			      	"targets": -1 
			      }
			  ],
							
			"columns" : [
					{
						"render" : function(data, type,
								full, meta) {
							console.log(full);
							if(full.code){
								return full.code
							}else{
								return ""
							}
						}
					},
					{	
						"render" : function(data, type,
								full, meta) {
							if(full.name){
								return full.name
							}else{
								return ""
							}
						}
					},
					{
						"render" : function(data, type,
								full, meta) {
							if(full.sourceOfFund){
								return full.sourceOfFund
							}else{
								return ""
							}
						}
					},
					
					{
						sortable : false,
						"render" : function(data, type,
								full, meta) {
							
							return '<div class="dropdown cursor-pointer employee-list__dropdown mr-3 text-center"><a class="" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton"><a class="dropdown-item onhover-background-grey" href="/employoee-info?getId='
									+ full.id
									+ '">View</a><a class="dropdown-item onhover-background-grey" href="/update-employoee?getId='
									+ full.id
									+ '">Edit</a><a href="#" class="dropdown-item onhover-background-grey" onClick="deleteHrm('
									+ full.id 
									+ ')">Terminate</a></div></div>'

						}
					}
			]
		
	});

	
	//for Activity
	$('#activity').DataTable(
		
		{
			"responsive": true,
			"ajax" : {
				"url" : "/hrm/activity/findAllListing",
				"dataSrc" : "",
				"beforeSend" : function(request) {
					request.setRequestHeader(
							"Authorization", main
									.getToken());
				},
			},
			"columnDefs": [
			    { 
			    	"searchable": false, 
			      	"targets": -1 
			      }
			  ],
							
			"columns" : [
					{
						"render" : function(data, type,
								full, meta) {
							console.log(full);
							if(full.code){
								return full.code
							}else{
								return ""
							}
						}
					},
					{	
						"render" : function(data, type,
								full, meta) {
							if(full.name){
								return full.name
							}else{
								return ""
							}
						}
					},
					{
						"render" : function(data, type,
								full, meta) {
							if(full.project){
								return full.project
							}else{
								return ""
							}
						}
					},
					
					{
						sortable : false,
						"render" : function(data, type,
								full, meta) {
							
							return '<div class="dropdown cursor-pointer employee-list__dropdown mr-3 text-center"><a class="" type="" id="dropdownmenubutton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-ellipsis-v"></i></a><div class="dropdown-menu employee-list__dropdown-menu" aria-labelledby="dropdownmenubutton"><a class="dropdown-item onhover-background-grey" href="/employoee-info?getId='
									+ full.id
									+ '">View</a><a class="dropdown-item onhover-background-grey" href="/update-employoee?getId='
									+ full.id
									+ '">Edit</a><a href="#" class="dropdown-item onhover-background-grey" onClick="deleteHrm('
									+ full.id 
									+ ')">Terminate</a></div></div>'

						}
					}
			]
		
	});
	
	//end of activity
	
	
	
});

