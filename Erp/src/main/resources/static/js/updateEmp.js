
//check if field is empty so that manupulate placeholder
function checkPlaceholderValue() {

	$("input").each(function() {

		if ($(this).val() == "") {
			$(this).parent().removeClass('form-input-character-on');
		} else {

			$(this).parent().addClass('form-input-character-on');
		}
	});
}
var updateEmployee = {
		

	updateData : function(url, data) {
		extraMain.ajax(url, JSON.stringify(data), function(response) {
			if(response=="Fail -> Email is already in use!"){
				toastr.error('E-mail has been already used for another user.', 'E-mail error!', {timeOut: 5000}).css("width","900px","height","50px")		
			}
			else{
				toastr.success('Information has been Updated', 'Updated Employee Information', {timeOut: 5000}).css("width","900px","height","50px")		
			}
				
			checkPlaceholderValue()	
		},function(response){
			toastr.success('Unsuccessful', 'Somthing Went Wrong', {timeOut: 5000}).css("width","900px","height","50px")
		}, "PUT", main.getToken());
	},

	//Personal data get in the form and update from the form
	employeeGet : function() {
		main.ajax("/hrm/findone/" + id, "", function(response) {
			$("#title").val(response.title);
			$("#firstName").val(response.firstName);
			$("#middleName").val(response.middleName);
			$("#surName").val(response.surname);
			$("#email").val(response.email);
			$("#dob").val(response.dateOfBirth);
			$("#maidenName").val(response.maidenName);
			$("#nationality").val(response.nationality);
			$("#suffix").val(response.suffix);
			$("#identificationName").val(response.identificationName);
			$("#identificationNumber").val(response.identificationNumber);
			$("#sex").val(response.sex);
			$("#maritalStatus").val(response.maritalStatus);
			$("#academicQualification").val(response.academicQualification);

			//getting role
			var arguments = response.roles[0].type.split('_');
			var role = arguments[1];
			$("#role").val(role.toLowerCase());
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
		}, "GET", main.getToken());
	},

	employeeUpdate : function() {
		var nameValue = document.getElementById("updateEmployeeInfoForm").value;
		var updatedemployeedata = {
			academicQualification : academicQualification.value,
			dateOfBirth : dob.value,
			email : email.value,
			firstName : firstName.value,
			identificationName : identificationName.value,
			identificationNumber : identificationNumber.value,
			maidenName : maidenName.value,
			maritalStatus : maritalStatus.value,
			middleName : middleName.value,
			nationality : nationality.value,
			sex : sex.value,
			suffix : suffix.value,
			surname : surName.value,
			title : title.value
			
			
			
		}
		this.updateData("/hrm/update/" + id, updatedemployeedata);
	},

	//Company data of employee get in the form and update from the form
	employeeCompanyGet : function() {
		
		
		//for populating line managers
		main.ajax("/hrm/findLineManager", "", function(response) {

			var select = document.getElementById("lineManager");
			response.forEach(function(element) {
				var opt = element.name
				var el = document.createElement("option");
				el.textContent = opt;
				el.value = element.id;
				select.appendChild(el);
			});

		}, "GET", main.getToken());
		
		
		
		
		
		
		main.ajax("/hrm/compinfo/findone/" + id, "", function(response) {
			
		
			$("#lineManager").val(response.lineManagerId);
			$("#position").val(response.position);
			$("#level").val(response.level);
			$("#step").val(response.step);
			$("#contractType").val(response.contractType);
			$("#contractStartDate").val(response.contractStartDate);
			$("#contractEndDate").val(response.contractEndDate);
			$("#office").val(response.office);
			$("#department").val(response.department);
			$("#section").val(response.section);
			$("#unit").val(response.unit);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeCompanyUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeCompanyInfoForm").value;
		var updatedemployeedata = {
			contractEndDate : contractEndDate.value,
			contractStartDate : contractStartDate.value,
			contractType : contractType.value,
			department : department.value,
			level : level.value,
			lineManager : lineManager.value,
			office : office.value,
			position : position.value,
			section : section.value,
			step : step.value,
			unit : unit.value
		}
		this.updateData("/hrm/compinfo/update/" + id, updatedemployeedata);
	},

	//Contact Address of employee data get in the form and update from the form
	employeeContactAddressGet : function() {
		main.ajax("/hrm/contactaddress/findone/" + id, "", function(response) {
			$("#CAcountry").val(response.country);
			$("#CAstate").val(response.state);
			$("#CAcity").val(response.city);
			$("#CAstreet").val(response.street);
			$("#CAwardNo").val(response.wardNo);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeContactAddressUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeContactAddressForm").value;
		var updatedemployeedata = {
			city : CAcity.value,
			country : CAcountry.value,
			state : CAstate.value,
			street : CAstreet.value,
			wardNo : CAwardNo.value
		}
		this
				.updateData("/hrm/contactaddress/update/" + id,
						updatedemployeedata);
	},

	//Permanent Address of employee data get in the form and update from the form
	employeePermanentAddressGet : function() {
		main.ajax("/hrm/permhomeaddr/findone/" + id, "", function(response) {
			$("#PAcountry").val(response.country);
			$("#PAstate").val(response.state);
			$("#PAcity").val(response.city);
			$("#PAstreet").val(response.street);
			$("#PAwardNo").val(response.wardNo);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeePermanentAddressUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeePermanentAddressForm").value;
		var updatedemployeedata = {

			city : PAcity.value,
			country : PAcountry.value,
			state : PAstate.value,
			street : PAstreet.value,
			wardNo : PAwardNo.value
		}
		this.updateData("/hrm/permhomeaddr/update/" + id, updatedemployeedata);
	},

	//Work Address of employee data get in the form and update from the form
	employeeWorkAddressGet : function() {
		main.ajax("/hrm/workaddress/findone/" + id, "", function(response) {
			$("#WAcountry").val(response.country);
			$("#WAstate").val(response.state);
			$("#WAcity").val(response.city);
			$("#WAstreet").val(response.street);
			$("#WAwardNo").val(response.wardNo);
			$("#WAcompany").val(response.company);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeWorkAddressUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeWorkAddressForm").value;
		var updatedemployeedata = {
			city : WAcity.value,
			country : WAcountry.value,
			state : WAstate.value,
			street : WAstreet.value,
			wardNo : WAwardNo.value,
			company : WAcompany.value
		}
		this.updateData("/hrm/workaddress/update/" + id, updatedemployeedata);
	},

	//Contact Information of employee data get in the form and update from the form
	employeeContactInformationGet : function() {
		main.ajax("/hrm/contactinfo/findone/" + id, "", function(response) {
			$("#phone").val(response.phone);
			$("#mobile").val(response.mobile);
			$("#secondaryEmail").val(response.secondaryEmail);
			$("#poBox").val(response.poBox);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeContactInformationUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeContactInformationForm").value;
		var updatedemployeedata = {
			secondaryEmail : secondaryEmail.value,
			mobile : mobile.value,
			phone : phone.value,
			poBox : poBox.value
		}
		this.updateData("/hrm/contactinfo/update/" + id, updatedemployeedata);
	},

	//Emergency Contact of employee data get in the form and update from the form
	employeeEmergencyContactGet : function() {
		main.ajax("/hrm/emgcont/findone/" + id, "", function(response) {
			$("#fullName").val(response.name);
			$("#relationship").val(response.relationship);
			$("#contactNumber").val(response.contactNumber);
			$("#fullAddress").val(response.fullAddress);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeEmergencyContactInformationUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeEmergencyContactInformationForm").value;
		var updatedemployeedata = {
			contactNumber : contactNumber.value,
			fullAddress : fullAddress.value,
			name : fullName.value,
			relationship : relationship.value
		}
		this.updateData("/hrm/emgcont/update/" + id, updatedemployeedata);
	},

	//Dependent Informtion of employee data get in the form and update from the form
	employeeDependentInformationGet : function() {
		main.ajax("/hrm/dependentinfo/findone/" + id, "", function(response) {
			$("#fatherName").val(response.fatherName);
			$("#motherName").val(response.motherName);
			$("#spouse").val(response.spouse);
			$("#children1").val(response.children1);
			$("#children2").val(response.children2);
			$("#children3").val(response.children3);
			
			//check if field is empty so that manupulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeDependentInformationUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeDependentInformationForm").value;
		var updatedemployeedata = {
			children1 : children1.value,
			fatherName : fatherName.value,
			motherName : motherName.value,
			spouse : spouse.value
		}
		this.updateData("/hrm/dependentinfo/update/" + id, updatedemployeedata);
	},

	//Other Information of employee data get in the form and update from the form
	employeeOtherInformationGet : function() {
		main.ajax("/hrm/otherinfo/findone/" + id, "", function(response) {
			$("#religion").val(response.religion);
			$("#ethnicity").val(response.ethnicity);
			$("#bloodGroup").val(response.bloodGroup);
			$("#panNumber").val(response.panNumber);
			$("#drivingLiscenceNumber").val(response.drivingLiscenceNumber);
			$("#insurancePolicyNumber").val(response.insurancePolicyNumber);
			$("#otherProfessionalTrainingCourses").val(
					response.otherProfessionalTrainingCourses);
			$("#pastOrganisations").val(response.pastOrganisations);
			$("#majorAlergyOrSickness").val(response.majorAlergyOrSickness);
			$("#preferences").val(response.preferences);
			
			//check if field is empty so that manipulate placeholder
			checkPlaceholderValue();
			
		}, "GET", main.getToken());
	},

	employeeOtherInformationUpdate : function() {
		var nameValue = document
				.getElementById("updateEmployeeOtherInformationForm").value;

		var updatedemployeedata = {
			religion : religion.value,
			ethnicity : ethnicity.value,
			bloodGroup : bloodGroup.value,
			drivingLiscenceNumber : drivingLiscenceNumber.value,
			insurancePolicyNumber : insurancePolicyNumber.value,
			majorAlergyOrSickness : majorAlergyOrSickness.value,
			otherProfessionalTrainingCourses : otherProfessionalTrainingCourses.value,
			panNumber : panNumber.value,
			pastOrganisations : pastOrganisations.value,
			preferences : preferences.value
		}
		this.updateData("/hrm/otherinfo/update/" + id, updatedemployeedata);
	}
	
	/*employeeRoleUpdate : function() {
		var updatedemployeedata = {
			role : [$('#role').val()]		
		}
		this.updateData("/hrm/changeRole/" + id, updatedemployeedata);
	}*/
}

function updateRole(){
	var updatedemployeedata = {
			role : [$('#role').val()]		
		}
	updateEmployee.updateData("/hrm/changeRole/" + id, updatedemployeedata);
	
}
