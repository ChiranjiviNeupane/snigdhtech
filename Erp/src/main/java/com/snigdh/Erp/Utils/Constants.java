package com.snigdh.Erp.Utils;

public class Constants {

	public static final String BASE_URL = "/hrm";

	public static final String COMP_INFO = BASE_URL + "/compinfo";

	public static final String CONT_ADDR = BASE_URL + "/contactaddress";

	public static final String CONT_INFO = BASE_URL + "/contactinfo";

	public static final String DEPND_INFO = BASE_URL + "/dependentinfo";

	public static final String EMG_CONT = BASE_URL + "/emgcont";

	public static final String OTHER_INFO = BASE_URL + "/otherinfo";

	public static final String PERM_HOME_ADDR = BASE_URL + "/permhomeaddr";

	public static final String WORK_ADDR = BASE_URL + "/workaddress";

	public static final String TRAVEL = BASE_URL + "/travel";

	public static final String MULMEDIA = BASE_URL + "/multimedia";

	public static final String ATTENDANCE = BASE_URL + "/attendance";

	public static final String LEAVEREQUEST = BASE_URL + "/leavereq";

	public static final String LEAVEREMAINING = BASE_URL + "/leaverem";

	public static final String LEAVETYPE = BASE_URL + "/leavetype";

	public static final String HOLIDAY = BASE_URL + "/holiday";

	public static final String HALFDAYLEAVE = BASE_URL + "/halfdayleave";
	
	public static final String HOURLYLEAVE = BASE_URL + "/hourlyleave";
	
	public static final String ATTMONREP = BASE_URL + "/attmonthlyreport";
	
	public static final String DELEGATE = BASE_URL + "/delegate";
	
	public static final String LIEUREQUEST = BASE_URL + "/lieurequest";

	public static final String COST_CENTER = BASE_URL + "/costCenter";
	
	public static final String SOURCE_OF_FUND= BASE_URL + "/sourceOfFund";
	
	public static final String PROJECT = BASE_URL + "/project";
	
	public static final String ACTIVITY = BASE_URL + "/activity";
	
	public static final String NOTIFICATION = BASE_URL + "/notification";
	
	public static final String AUTHORIZATION = BASE_URL + "/authorization";

}