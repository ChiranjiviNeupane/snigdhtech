package com.snigdh.Erp.MvcConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin(origins = "*")
public class UrlController {

	@GetMapping("/access-denied")
	public String accessdenied() {
		return "error_accessDenied";
	}
	
	@GetMapping("/login")
	public String login() {
		return "signin";
	}

	@GetMapping("/")
	public String home() {
		return "Dashboard";
	}
	
	@GetMapping("/add-employee")
	public String addemployee() {
		return "dashboard-add-employee";
	}

	@GetMapping("/attendance")
	public String attendance() {
		return "Attendance";
	}

	@GetMapping("/employoee-info")
	public String employeeinfo() {
		return "dashboard-view-employee";
	}

	@GetMapping("/update-employoee")
	public String updateemployee() {
		return "dashboard-edit-employee";
	}

	@GetMapping("/manage-employoee")
	public String manageemployee() {
		return "dashboard-manage-employee";
	}

	@GetMapping("/myattendance")
	public String myattendance() {
		return "hrmisMyattendance";
	}

	@GetMapping("/allAttendance")
	public String allAttendance() {
		return "allAttendance";
	}

	@GetMapping("/travel")
	public String travelrequest() {
		return "travel";
	}

	@GetMapping("/myProfile")
	public String myprofile() {
		return "MyProfile";
	}

	@GetMapping("/editMyProfile")
	public String editmyprofile() {
		return "UpdateMyProfile";
	}

	@GetMapping("/Leave")
	public String leave() {
		return "dashboard-leave";
	}

	@GetMapping("/TakeLeave")
	public String takeleave() {
		return "TakeLeave";
	}

	@GetMapping("/LeaveRequests")
	public String requestedleave() {
		return "LeaveRequests";
	}

	@GetMapping("/allLeave")
	public String allleave() {
		return "EmployeeLeaves";
	}

	@GetMapping("/changePassword")
	public String changepassword() {
		return "change-password";
	}

	@GetMapping("/calender")
	public String calender() {
		return "calender";
	}

	@GetMapping("/monthlyReport")
	public String monthlyReport() {
		return "LM-monthly-report-employee-list";
	}

	@GetMapping("/monthlyReportDetail")
	public String monthlyReportDetail() {
		return "LM-monthly-report-employee-detail";
	}

	@GetMapping("/hrMonthlyReport")
	public String hrMonthlyReportDetail() {
		return "HR-monthly-report";
	}

	@GetMapping("/manage-holidays")
	public String manageholidays() {
		return "manage-holiday";
	}

	@GetMapping("/manage-leave")
	public String manageleave() {
		return "manage-leave";
	}

	@GetMapping("/manage-budgetCode")
	public String managebudgetCode() {
		return "manage-budget-code";
	}

	@GetMapping("/list-of-holidays")
	public String listOfHolidays() {
		return "list-of-holidays";
	}
	@GetMapping("/delegate")
	public String delegate() {
		return "delegate";
	}
	@GetMapping("/work-on-holidays")
	public String workOnHolidays() {
		return "work-on-holidays";
	}
	@GetMapping("/manage-authorization")
	public String manageAuthorization() {
		return "manage-authorization";
	}

}
