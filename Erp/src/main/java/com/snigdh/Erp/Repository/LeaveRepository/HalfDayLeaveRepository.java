package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;

@Repository
public interface HalfDayLeaveRepository extends JpaRepository<HalfDayLeave, Integer> {

	List<HalfDayLeave> findByEmployeePersonalInformation_IdAndLeaveDate(int id, String leaveFrom);

	List<HalfDayLeave> findByEmployeePersonalInformation_IdAndRequestTo(int id, String status);

	@Query("select u.id, u.onLeaveStatus, u.leaveType.typeOfLeave, u.halfDayType, u.leaveReason, u.leaveDate, u.requestTo, u.requestStatus, u.responseDate, u.responseTime, u.createdDate, u.createdTime, u.recommendedBy, u.remarks, u.responseBy, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, c.department, c.position from HalfDayLeave u INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where u.requestTo=:id and u.requestStatus=:status order by u.id desc")
	List<Object[]> findByRequestAndStatus(@Param("id") int id, @Param("status") String status);

	@Query("select u.id, u.leaveType.typeOfLeave, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname,u.halfDayType,u.leaveDate,u.leaveReason,u.recommendedBy, u.onLeaveStatus, u.requestStatus,u.requestTo, c.department, c.position, u.createdTime, u.responseBy, u.responseTime from HalfDayLeave u INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where u.employeePersonalInformation.id=:id order by u.id desc")
	List<Object[]> findMyLeaveStatus(@Param("id") int id);

	@Query("select u.id, u.onLeaveStatus, u.leaveType.typeOfLeave, u.halfDayType, u.leaveReason, u.leaveDate, u.requestTo, u.requestStatus, u.responseDate, u.responseTime, u.createdDate, u.createdTime, u.recommendedBy, u.remarks, u.responseBy, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, c.department, c.position from HalfDayLeave u INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where u.employeePersonalInformation.id=:id order by u.id desc")
	List<Object[]> findEmpById(@Param("id") int id);

	@Query("select COALESCE(u.leaveDate),COALESCE(u.halfDayType) from HalfDayLeave u where u.employeePersonalInformation.id=:id and u.leaveDate like CONCAT(:date,'%') and u.requestStatus='Approved'")
	List<Object[]> getMonthlyLeave(@Param("id") Integer id, @Param("date") String date);

	@Query("select u.id, u.onLeaveStatus, u.leaveType.typeOfLeave, u.halfDayType, u.leaveReason, u.leaveDate, u.requestTo, u.requestStatus, u.responseDate, u.responseTime, u.createdDate, u.createdTime, u.recommendedBy, u.remarks, u.responseBy, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, c.department, c.position from HalfDayLeave u "
			+ "INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where u.requestTo=:id and u.requestStatus=:status1 or u.requestStatus=:status2 or u.requestStatus=:status3 order by u.id desc")
	List<Object[]> findByRequestTo(@Param("id") int id, @Param("status1") String status1,
			@Param("status2") String status2, @Param("status3") String status3);

	@Query("select u.id, u.leaveType.typeOfLeave, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname,u.halfDayType,u.leaveDate,u.leaveReason,u.recommendedBy, u.onLeaveStatus, u.requestStatus,u.requestTo, c.department, c.position from HalfDayLeave u INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id "
			+ "where u.id=:id order by u.id desc")
	List<Object[]> findRequestById(Integer id); 
	
	@Query("select u.id, u.onLeaveStatus, u.leaveType.typeOfLeave, u.halfDayType, u.leaveReason, u.leaveDate, u.requestTo, u.requestStatus, u.responseDate, u.responseTime, u.createdDate, u.createdTime, u.recommendedBy, u.remarks, u.responseBy, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, c.department, c.position from HalfDayLeave u INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where u.employeePersonalInformation.id=:id and u.leaveDate like CONCAT(:date,'%') and u.requestStatus='Approved'")
	List<Object[]> getAllMonthlyLeaveDate(@Param("id") Integer id, @Param("date") String date);
}
