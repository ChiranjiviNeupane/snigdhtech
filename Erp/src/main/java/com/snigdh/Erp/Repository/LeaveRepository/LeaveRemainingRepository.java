package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;

@Repository
public interface LeaveRemainingRepository extends JpaRepository<LeaveRemaining, Integer>{
	
	@Query("select u.leaveType.typeOfLeave,u.remainingLeave from LeaveRemaining u where u.employeePersonalInformation.id=:id")
	public List<Object[]> findEmployeeLeaveRemaining(@Param("id") int id);

	List<LeaveRemaining> findByEmployeePersonalInformation_Id(int empId);
	
	LeaveRemaining findByLeaveType_IdAndEmployeePersonalInformation_Id(int typeId, int empId);
		
	LeaveRemaining findByLeaveType_TypeOfLeaveAndEmployeePersonalInformation_Id(String typeId, int empId);
	 
	List<LeaveRemaining> findByLeaveType_TypeOfLeave(String typeId);
	
}
