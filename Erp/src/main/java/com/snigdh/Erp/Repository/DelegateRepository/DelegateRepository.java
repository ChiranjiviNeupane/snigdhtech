package com.snigdh.Erp.Repository.DelegateRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.Delegate.Delegate;

@Repository
public interface DelegateRepository extends JpaRepository<Delegate, Integer>{

	List<Delegate> findByDelegateToAndOnDelegateStatus(Integer id,boolean status);
	
	Optional<Delegate> findByEmployeePersonalInformation_IdAndOnDelegateStatus(Integer id,boolean status);
	
	List<Delegate> findByEmployeePersonalInformation_Id(Integer id);
	
	List<Delegate> findByOnDelegateStatusAndRequestStatus(boolean isOnLeave, String requestStatus);
	
}
     