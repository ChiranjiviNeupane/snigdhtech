package com.snigdh.Erp.Repository.AttendanceRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.AttendanceModel.LieuRequest;

@Repository
public interface LieuRequestRepository  extends JpaRepository<LieuRequest, Integer> {
	/*@Query("select s.id, s.leaveType.typeOfLeave, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.noOfDays, s.leaveFrom, s.leaveTill, s.leaveReason, s.recommendedBy, s.onLeaveStatus, s.requestStatus, s.extendTill, s.extendDays, s.extendStatus, s.remarks, c.department,c.position from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where s.requestTo=:id and s.requestStatus=:status order by s.id desc")
	List<Object[]> findByRequesAndStatus(@Param("id") int id,@Param("status")String status);*/
	
	Optional<LieuRequest> findByDateAndEmployeePersonalInformation_Id(String date, Integer id);

}
