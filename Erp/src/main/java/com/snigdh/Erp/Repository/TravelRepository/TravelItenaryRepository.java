package com.snigdh.Erp.Repository.TravelRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.TravelModel.TravelItenary;

public interface TravelItenaryRepository extends JpaRepository<TravelItenary, Integer> {

}
