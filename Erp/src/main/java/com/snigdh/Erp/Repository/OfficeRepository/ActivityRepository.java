package com.snigdh.Erp.Repository.OfficeRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.snigdh.Erp.Model.Office.Activity;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
	
	@Query("select a.id,a.code,a.name from Activity a join a.project p where p.id = :id") 
	public List<Object[]> getListActivityDetails(Integer id);

	@Query("select a.project.name,a.id,a.code,a.name from Activity a") 
	public List<Object[]> getListActivity();

}
