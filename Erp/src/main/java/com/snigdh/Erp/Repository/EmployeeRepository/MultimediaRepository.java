package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.Multimedia;

public interface MultimediaRepository extends JpaRepository<Multimedia, Integer> {
	Optional<Multimedia> findByEmployeePersonalInformation_Id(int id);

}
