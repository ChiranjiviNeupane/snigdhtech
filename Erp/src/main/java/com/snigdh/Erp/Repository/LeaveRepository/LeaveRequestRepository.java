package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;

@Repository
public interface LeaveRequestRepository extends JpaRepository<LeaveRequest, Integer>{
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.requestTo=:id and s.requestStatus=:status order by s.id desc")
	List<Object[]> findByRequestAndStatus(@Param("id") int id,@Param("status")String status);
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where s.requestTo=:id and s.extendStatus=:status order by s.id desc")
	List<Object[]> findByRequestAndExtendStatus(@Param("id") int id,@Param("status")String status);
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where s.requestTo=:id and s.cancelStatus=:status order by s.id desc")
	List<Object[]> findByRequestAndCancelStatus(@Param("id") int id,@Param("status")String status);
		
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.requestTo=:id and s.requestStatus=:status1 or s.requestStatus=:status2 or s.requestStatus=:status3 order by s.id desc")
	List<Object[]> findByRequestTo(@Param("id") int id, @Param("status1") String status1, @Param("status2") String status2, @Param("status3") String status3);
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.recommendedBy=:id")
	List<Object[]> findByRecommend(@Param("id") String id);
	
	List<LeaveRequest> findByRequestStatusAndLeaveType_IdAndEmployeePersonalInformation_Id(String status,Integer leaveType, Integer id);
	
	List<LeaveRequest> findByOnLeaveStatusAndRequestStatus(boolean isOnLeave, String requestStatus);
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where s.employeePersonalInformation.id=:id order by s.id desc")
	List<Object[]> findEmpById(Integer id); 
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id "
			+ "where s.id=:id order by s.id desc")
	List<Object[]> findRequestById(Integer id); 
	
	@Query("select u.id, u.leaveType.typeOfLeave, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, u.noOfDays, u.leaveFrom, u.leaveTill, u.leaveReason, u.recommendedBy, u.onLeaveStatus, u.requestStatus, u.extendTill, u.extendDays, u.extendStatus, u.remarks,u.requestTo,c.position from LeaveRequest u "
			+ "INNER JOIN EmployeePersonalInformation e on u.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where u.onLeaveStatus=:status")
	List<Object[]> findEmployeeOnLeave(@Param("status")boolean status); 
	
	List<LeaveRequest> findByEmployeePersonalInformation_IdAndLeaveFrom(int id, String leaveFrom);
	
	List<LeaveRequest> findByEmployeePersonalInformation_Id(int id);
	
	@Query("select COALESCE(u.leaveFrom),COALESCE(u.leaveTill) from LeaveRequest u where u.employeePersonalInformation.id=:id and u.leaveTill like CONCAT(:date,'%') and u.leaveFrom like CONCAT(:date,'%') and u.requestStatus='Approved'")
	public List<Object[]> findEmployeeOnLeaveDate(@Param("id") Integer id,@Param("date") String date);
	

	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.noOfDays, s.leaveReason, s.leaveFrom, s.leaveTill, s.requestTo, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, s.requestStatus, s.recommendedBy, s.recommendedDate, s.createdDate, s.createdTime, s.extendStatus, s.extendTill, s.extendDays, s.remarks, s.cancelFrom, s.cancelStatus, s.cancelDate, s.cancelRemarks, s.extendedDate, s.extendResponseDate, s.responseBy, s.responseDate, s.responseTime, s.extendRemarks, s.extendResponseRemarks, c.department, c.position, s.cancelResponseDate, s.cancelResponseBy, s.cancelResponseRemarks, s.extendResponseBy from LeaveRequest s INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.employeePersonalInformation.id=:id and (s.requestStatus='Approved' and s.leaveTill like CONCAT(:date,'%') or s.leaveFrom like CONCAT(:date,'%'))")
	public List<Object[]> getAllMonthlyLeaveDate(@Param("id") Integer id,@Param("date") String date);
}
