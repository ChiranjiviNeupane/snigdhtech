package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.PermanentHomeAddress;

public interface PermanentHomeAddressRepository extends JpaRepository<PermanentHomeAddress, Integer> {
	Optional<PermanentHomeAddress> findByEmployeePersonalInformation_Id(int id);
	
	long countByStateAndEmployeePersonalInformation_presentStatus(String state, boolean activeStatus);
}
