package com.snigdh.Erp.Repository.EmployeeRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.snigdh.Erp.Model.EmployeeModel.Authorization;

public interface AuthorizationRepository extends JpaRepository<Authorization, Integer> {
	
	@Query("select  c.employeeId from Authorization c where c.position = :position")
	public int getEmpId(@Param("position") String position);

}
