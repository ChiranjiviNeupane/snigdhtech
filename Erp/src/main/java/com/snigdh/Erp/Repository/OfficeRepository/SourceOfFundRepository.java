package com.snigdh.Erp.Repository.OfficeRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.snigdh.Erp.Model.Office.SourceOfFund;

public interface SourceOfFundRepository extends JpaRepository<SourceOfFund, Integer>{

	SourceOfFund findOneDetailById(int id);
	
	@Query("select s.id,s.code,s.name from SourceOfFund s join s.costCenter c where c.id = :id") 
	public List<Object[]> getListSourceFundDetails(Integer id);
	
	@Query("select s.costCenter.name,s.id,s.code,s.name from SourceOfFund s") 
	public List<Object[]> getListSourceFund();

}
