package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.OtherInformation;

public interface OtherInformationRepository extends JpaRepository<OtherInformation, Integer> {
	Optional<OtherInformation> findByEmployeePersonalInformation_Id(int id);
	
	long countByReligionAndEmployeePersonalInformation_presentStatus(String religion, boolean activeStatus);
	long countByEthnicityAndEmployeePersonalInformation_presentStatus(String ethnicity, boolean activeStatus);
}
