package com.snigdh.Erp.Repository.NotificationRepository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.snigdh.Erp.Model.Notification.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Integer>{
	
	/*@Query("select n.id, n.leaveRequest.leaveRequestId,  n.notificationStatus, n.employeePersonalInformation.id, n.halfDayLeave.halfDayLeaveId, n.hourlyLeave.hourlyLeaveId, n.travel.travelId from Notification n where n.createdAt BETWEEN NOW() - INTERVAL 15 DAY AND NOW()")
	public List<Object[]> findEmployeeLeaveRemaining(@Param("id") int id); */
	
	@Query("select n.employeePersonalInformation.id from Notification n where n.createdAt >= :endDate")
	public List<Object[]> findNotification(@Param("endDate") LocalDate endDate); 

}
