package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.EmergencyContactInformation;

public interface EmergencyContactInformationRepository extends JpaRepository<EmergencyContactInformation, Integer> {
	Optional<EmergencyContactInformation> findByEmployeePersonalInformation_Id(int id);

}
