package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.ContactInformation;

public interface ContactInformationRepository extends JpaRepository<ContactInformation, Integer> {
	Optional<ContactInformation> findByEmployeePersonalInformation_Id(int id);


}
