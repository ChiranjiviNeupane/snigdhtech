package com.snigdh.Erp.Repository.OfficeRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.snigdh.Erp.Model.Office.CostCenter;

public interface CostCenterRepository extends JpaRepository<CostCenter, Integer> {

	CostCenter findOneDetailById(int id);
	
	@Query("select id,code,name from CostCenter") 
	public List<Object[]> getListCostCenterDetails();

}
