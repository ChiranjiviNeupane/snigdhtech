package com.snigdh.Erp.Repository.AttendanceRepository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.AttendanceModel.Attendance;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {

	List<Attendance> findByEmployeePersonalInformation_Id(Integer id);

	@Query("select u.checkInTime,u.checkOutTime from Attendance u where u.employeePersonalInformation.id=:id order by u.id desc")
	public List<Object[]> findSpecificEmployeeAttendance(@Param("id") int id);

	@Query("select u.status from Attendance u where u.employeePersonalInformation.id=:id order by u.checkInTime desc")
	public List<Object> findSpecificEmployeeAttendanceStatus(@Param("id") int id,Pageable page);

	@Query("select a.employeePersonalInformation.firstName,a.employeePersonalInformation.middleName,a.employeePersonalInformation.surname,a.checkInTime,a.checkOutTime from Attendance a where a.employeePersonalInformation.presentStatus=1")
	public List<Object[]> findAllEmployeeAttendance();
	
	@Query("select COALESCE(u.checkInTime), COALESCE(checkOutTime) from Attendance u where u.employeePersonalInformation.id=:id and u.checkInTime like CONCAT(:date,'%')")
	public List<Object[]> findEmployeeCheckInDate(@Param("id") Integer id,@Param("date") String date);
	
	List<Attendance> findByStatus(boolean b);
}
