package com.snigdh.Erp.Repository.TravelRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.TravelModel.Travel;

@Repository
public interface TravelRepository extends JpaRepository<Travel, Integer> {

	@Query("select u.firstName,u.middleName,u.surname,c.position from Travel t JOIN t.employeePersonalInformation "
			+ " u JOIN u.companyInformations c where t.onTravel=1")
	public List<Object[]> onTravelList();

	public List<Travel> findByEmployeePersonalInformation_Id(int id);

	public List<Travel> findByOnTravel(boolean status);

	@Query("select u from Travel u where u.overallStatus='Approved' and (u.dateFrom like CONCAT(:from,'%') or u.dateTo like CONCAT(:to,'%'))")
	List<Travel> findListForReportWithDate(@Param("from") String from, @Param("to") String to);

	@Query("select u from Travel u where u.employeePersonalInformation=:id and u.overallStatus='Approved' and (u.dateFrom like CONCAT(:from,'%') or u.dateTo like CONCAT(:to,'%'))")
	public List<Travel> findListForReportWithDateAndEmp(@Param("from") String from, @Param("to") String to,
			@Param("id") EmployeePersonalInformation empId);

	@Query("select t from Travel t join t.employeePersonalInformation e join e.companyInformations c"
			+ " where t.overallStatus='Approved' and (t.dateFrom like CONCAT(:fromdate,'%') or t.dateTo like CONCAT(:todate,'%')) and c.department=:department")
	public List<Travel> findListForReportWithDateAndDepart(@Param("fromdate") String fromdate, @Param("todate") String todate,@Param("department") String department);
}
