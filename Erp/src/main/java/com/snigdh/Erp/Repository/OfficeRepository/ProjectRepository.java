package com.snigdh.Erp.Repository.OfficeRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.snigdh.Erp.Model.Office.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
	
	Project findOneDetailById(Integer id); 
	
	@Query("select p.id,p.code,p.name from Project p join p.sourceOfFund s where s.id = :id") 
	public List<Object[]> getListProjectDetails(Integer id);
	
	@Query("select p.sourceOfFund.name,p.id,p.code,p.name from Project p") 
	public List<Object[]> getListProject();

}
