package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.WorkAddress;

public interface WorkAddressRepository extends JpaRepository<WorkAddress, Integer> {
	Optional<WorkAddress> findByEmployeePersonalInformation_Id(int id);

}
