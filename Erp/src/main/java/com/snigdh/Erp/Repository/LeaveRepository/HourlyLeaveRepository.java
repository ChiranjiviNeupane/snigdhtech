package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;

@Repository
public interface HourlyLeaveRepository extends JpaRepository<HourlyLeave, Integer> {

	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.hourFrom, s.hourTill, s.numberOfHours, s.leaveReason, s.leaveDate, s.requestTo,  s.requestStatus, s.createdDate, s.createdTime, s.responseDate, s.responseTime, s.responseBy, s.recommendedBy, s.remarks, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, c.department, c.position from HourlyLeave s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.requestTo=:id and s.requestStatus=:status1 or s.requestStatus=:status2 or s.requestStatus=:status3 order by s.id desc")
	List<Object[]> findByRequestTo(@Param("id") int id, @Param("status1") String status1,
			@Param("status2") String status2, @Param("status3") String status3);

	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.hourFrom, s.hourTill, s.numberOfHours, s.leaveReason, s.leaveDate, s.requestTo,  s.requestStatus, s.createdDate, s.createdTime, s.responseDate, s.responseTime, s.responseBy, s.recommendedBy, s.remarks, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, c.department, c.position from HourlyLeave s INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.employeePersonalInformation.id=:id order by s.id desc")
	List<Object[]> findEmpById(Integer id);

	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.hourFrom, s.hourTill, s.numberOfHours, s.leaveReason, s.leaveDate, s.requestTo,  s.requestStatus, s.createdDate, s.createdTime, s.responseDate, s.responseTime, s.responseBy, s.recommendedBy, s.remarks, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, c.department, c.position from HourlyLeave s "
			+ "INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id  where s.requestTo=:id and s.requestStatus=:status order by s.id desc")
	List<Object[]> findByRequestAndStatus(@Param("id") int id, @Param("status") String status);

	@Query("select COALESCE(u.leaveDate),COALESCE(u.hourFrom),COALESCE(u.hourTill) from HourlyLeave u where u.employeePersonalInformation.id=:id and u.leaveDate like CONCAT(:date,'%') and u.requestStatus='Approved'")
	List<Object[]> getMonthlyLeave(@Param("id") Integer id, @Param("date") String date);
	
	List<HourlyLeave> findByEmployeePersonalInformation_IdAndLeaveDate(int id,String leaveFrom);
	
	@Query("select u.id, u.leaveType.typeOfLeave, u.employeePersonalInformation.firstName, u.employeePersonalInformation.middleName, u.employeePersonalInformation.surname, u.numberOfHours, u.hourFrom, u.hourTill, u.leaveReason, u.recommendedBy, u.onLeaveStatus, u.requestStatus, u.remarks,u.requestTo, u.createdDate, u.leaveDate from HourlyLeave u"
			+ " where u.id=:id order by u.id desc")
	List<Object[]> findRequestById(Integer id); 
	
	@Query("select s.id, s.onLeaveStatus, s.leaveType.typeOfLeave, s.hourFrom, s.hourTill, s.numberOfHours, s.leaveReason, s.leaveDate, s.requestTo,  s.requestStatus, s.createdDate, s.createdTime, s.responseDate, s.responseTime, s.responseBy, s.recommendedBy, s.remarks, s.employeePersonalInformation.firstName, s.employeePersonalInformation.middleName, s.employeePersonalInformation.surname, c.department, c.position from HourlyLeave s INNER JOIN EmployeePersonalInformation e on s.employeePersonalInformation.id = e.id INNER JOIN CompanyInformation c on e.id = c.employeePersonalInformation.id where s.employeePersonalInformation.id=:id and s.leaveDate like CONCAT(:date,'%') and s.requestStatus='Approved'")
	List<Object[]> getAllMonthlyLeaveDate(@Param("id") Integer id, @Param("date") String date);
}
