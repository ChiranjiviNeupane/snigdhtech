package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

public interface EmployeePersonalInformationRepository extends JpaRepository<EmployeePersonalInformation, Integer> {

	Optional<EmployeePersonalInformation> findByEmailAndPresentStatus(String mail, boolean status);

	Boolean existsByEmail(String email);
	
	EmployeePersonalInformation findIdByEmailAndPresentStatus(String email, boolean status);
	
	long countBySexAndPresentStatus(String sex, boolean activeStatus);

	@Query("select u.firstName,u.middleName,u.surname,u.createdAt,c.department from EmployeePersonalInformation"
			+ " u JOIN u.companyInformations c where u.id = c.employeePersonalInformation AND u.presentStatus=1 order by u.createdAt desc")
	public List<Object[]> findNewEmployee();

	@Query("select u.firstName,u.middleName,u.surname,u.updatedAt,c.department,o.terminationDate from EmployeePersonalInformation"
			+ " u JOIN u.companyInformations c JOIN u.otherInformations o where u.id = c.employeePersonalInformation AND u.id = o.employeePersonalInformation AND u.presentStatus=0 order by o.terminationDate desc")
	public List<Object[]> findRecentLeftEmployee();

	@Query("select u.id,u.firstName,u.middleName,u.surname,c.department from EmployeePersonalInformation"
			+ " u JOIN u.companyInformations c where u.id = c.employeePersonalInformation AND u.presentStatus=1")
	public List<Object[]> findListOfEmployeeNameIdDepart();

	@Query("select u.id,u.firstName,u.middleName,u.surname,u.email,c.office,c.department,c.position,c.lineManager,o.terminationDate from EmployeePersonalInformation"
			+ " u JOIN u.companyInformations c JOIN u.otherInformations o where u.id = c.employeePersonalInformation AND u.id = o.employeePersonalInformation AND u.presentStatus=:status")
	public List<Object[]> findEmployeeDetailWithCustomDetails(@Param("status") boolean status);

	public EmployeePersonalInformation findDetailById(int id);
	
	@Query("select e.id, e.firstName, e.middleName, e.surname, e.email, c.office, c.department, c.position from EmployeePersonalInformation e JOIN e.companyInformations c where c.lineManager=:id order by e.id") 
	public List<Object[]> getMyEmployeesData(@Param("id") String id);
	
	@Query("select e.id from EmployeePersonalInformation e where e.presentStatus = 1 order by e.id")
	public List<Integer> getAllIds();
	
	@Query("select e.id,e.firstName, e.middleName, e.surname, e.email, c.office, c.department, c.position from EmployeePersonalInformation e JOIN e.companyInformations c where e.presentStatus = 1 order by e.id")
	public List<Object[]> getAllEmployeeDatas();
	
	@Query("select c.employeePersonalInformation.id from EmployeePersonalInformation e JOIN e.companyInformations c where c.department=:department")
	public List<Integer> getIdsByDepartment(@Param("department") String department);
	
	@Query("select  c.department, c.position from EmployeePersonalInformation e JOIN e.companyInformations c where e.id = :id")
	public List<Object[]> getCompanyAndDepart(@Param("id") int id);
	
}