package com.snigdh.Erp.Repository.TravelRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.TravelModel.TravelAdvance;

public interface TravelAdvanceRepository extends JpaRepository<TravelAdvance, Integer> {

}
