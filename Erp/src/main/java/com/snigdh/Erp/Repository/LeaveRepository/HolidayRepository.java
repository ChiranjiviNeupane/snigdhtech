package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.snigdh.Erp.Model.LeaveModel.Holiday;

public interface HolidayRepository extends JpaRepository<Holiday, Integer>{
	
	@Query("select h.dateFrom, h.dateTill from Holiday h order by h.dateFrom desc")
	List<Object[]> getHolidayDates();
	
	@Query("select h.dateFrom, h.dateTill from Holiday h where h.dateFrom like CONCAT(:dateFrom,'%') or h.dateTill like CONCAT(:dateTill,'%')")
	public List<String> getMonthlyHoliday(@Param("dateFrom") String dateFrom,@Param("dateTill") String dateTill);
	
	Optional<Holiday> findByDateFrom(String date); 

}
