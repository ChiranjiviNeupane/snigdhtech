package com.snigdh.Erp.Repository.TravelRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.snigdh.Erp.Model.TravelModel.TravelBudgetCode;

public interface TravelBudgetCodeRepository extends JpaRepository<TravelBudgetCode, Integer>{
	
	@Query(value="select c.name,c.id,s.name as source_name,s.id as source_id from budget_code_sample b  inner join cost_center c on b.cost_center_id = c.id inner join source_of_fund s on b.source_of_fund_id = s.id", nativeQuery =true)
	public Object getCostCenter();
}
//select c.name,c.id from cost_center c inner join budget_code_sample b on b.cost_center_id = c.id