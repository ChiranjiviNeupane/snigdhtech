package com.snigdh.Erp.Repository.EmployeeRepository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.Role;
import com.snigdh.Erp.Model.EmployeeModel.RoleName;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer>{
	 Optional<Role> findByType(RoleName roleName);
}
