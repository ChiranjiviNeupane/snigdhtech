package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.ContactAddress;

public interface ContactAddressRepository extends JpaRepository<ContactAddress, Integer> {
	Optional<ContactAddress> findByEmployeePersonalInformation_Id(int id);

}
