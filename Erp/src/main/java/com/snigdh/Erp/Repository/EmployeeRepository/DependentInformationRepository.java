package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.EmployeeModel.DependentInformation;

public interface DependentInformationRepository extends JpaRepository<DependentInformation, Integer> {
	Optional<DependentInformation> findByEmployeePersonalInformation_Id(int id);


}
