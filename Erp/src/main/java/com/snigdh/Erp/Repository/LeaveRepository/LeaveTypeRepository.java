package com.snigdh.Erp.Repository.LeaveRepository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snigdh.Erp.Model.LeaveModel.LeaveType;

public interface LeaveTypeRepository extends JpaRepository<LeaveType, Integer>{	

	LeaveType findByTypeOfLeave(String typeLeave);
}
