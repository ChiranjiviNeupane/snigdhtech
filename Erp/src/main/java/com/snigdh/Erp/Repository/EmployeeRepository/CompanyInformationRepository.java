package com.snigdh.Erp.Repository.EmployeeRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;

public interface CompanyInformationRepository extends JpaRepository<CompanyInformation, Integer> {
	
	Optional<CompanyInformation> findByEmployeePersonalInformation_Id(int id);
	
	long countByLevelAndEmployeePersonalInformation_presentStatus(String level, boolean status);

	long countByDepartmentAndEmployeePersonalInformation_presentStatus(String depart, boolean status);

	long countByContractTypeAndEmployeePersonalInformation_presentStatus(String contract, boolean status);
	
	CompanyInformation findDetailByEmployeePersonalInformation_Id(int id);
	
	@Query("select c.lineManager from CompanyInformation c inner JOIN c.employeePersonalInformation e where e.id=:id")
	public String getLineManager(@Param("id") int id );
	
	@Query("select c.employeePersonalInformation from CompanyInformation c where c.department=:depart")
	public List<Object[]> getIdFromDepart(@Param("depart") String depart );
}
