package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.PermanentHomeAddress;
import com.snigdh.Erp.Repository.EmployeeRepository.PermanentHomeAddressRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class PermanentHomeAddressService {

	private PermanentHomeAddressRepository homeAddressRepository;
	
	@Autowired
	UserService genService;

	@Autowired
	public PermanentHomeAddressService(PermanentHomeAddressRepository homeAddressRepository) {
		super();
		this.homeAddressRepository = homeAddressRepository;
	}

	public List<PermanentHomeAddress> findAll() {
		return homeAddressRepository.findAll();
	}

	public void save(PermanentHomeAddress address) {
		address.setUpdatedAt(new Date());
		address.setUpdatedBy(genService.getUserEmail());
		homeAddressRepository.save(address);
	}

	public PermanentHomeAddress retrieve(Integer id) {
		Optional<PermanentHomeAddress> homeAddress = homeAddressRepository.findByEmployeePersonalInformation_Id(id);
		return homeAddress.get();
	}

	public void delete(PermanentHomeAddress id) {
		homeAddressRepository.delete(id);
	}

	public PermanentHomeAddress update(PermanentHomeAddress permanentHomeAddress, Integer id) {
		return homeAddressRepository.findByEmployeePersonalInformation_Id(id)
				.map(homeaddress ->
				{
					homeaddress.setCountry(permanentHomeAddress.getCountry());
					homeaddress.setState(permanentHomeAddress.getState());
					homeaddress.setCity(permanentHomeAddress.getCity());
					homeaddress.setStreet(permanentHomeAddress.getStreet());
					homeaddress.setWardNo(permanentHomeAddress.getWardNo());
					homeaddress.setUpdatedAt(new Date());
					homeaddress.setUpdatedBy(genService.getUserEmail());
										
					return homeAddressRepository.save(homeaddress);
				})
				.orElseGet(() ->
				{
					permanentHomeAddress.setId(id);
					return homeAddressRepository.save(permanentHomeAddress);
				});
	}

}
