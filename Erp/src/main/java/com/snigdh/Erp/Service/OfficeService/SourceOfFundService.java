package com.snigdh.Erp.Service.OfficeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Office.CostCenter;
import com.snigdh.Erp.Model.Office.SourceOfFund;
import com.snigdh.Erp.Repository.OfficeRepository.CostCenterRepository;
import com.snigdh.Erp.Repository.OfficeRepository.SourceOfFundRepository;

@Service
public class SourceOfFundService {
	
	@Autowired
	public SourceOfFundRepository fundRepo;
	
	@Autowired
	public CostCenterRepository costCenterRepo;
	
	public SourceOfFund save(SourceOfFund fund,Integer id) {
		CostCenter cost = costCenterRepo.findOneDetailById(id);
		fund.setCostCenter(cost);
		return fundRepo.save(fund);
	}
	
	public SourceOfFund retrieve(Integer id) {
		Optional<SourceOfFund> fund = fundRepo.findById(id);
		return fund.get();
	}
	
	public List<SourceOfFund> findAll() {
		return fundRepo.findAll();
	}
	
	public SourceOfFund updateValue( int costCenter,int id,SourceOfFund fund) {	
		CostCenter cost = costCenterRepo.findOneDetailById(costCenter);
		return fundRepo.findById(id).map(fundKey -> {
			fundKey.setCostCenter(cost);
			fundKey.setCode(fund.getCode());
			fundKey.setName(fund.getName());	
			return fundRepo.save(fundKey);
		})

				.orElseGet(() -> {
					fund.setId(id);
					return fundRepo.save(fund);
				});
	}
	
	public void deleteValue(SourceOfFund id) {
		fundRepo.delete(id);
	}

	public List<Object> findForTravelRequest(Integer id) {
		List<Object[]> sourceOfFundList = fundRepo.getListSourceFundDetails(id);
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] fundList : sourceOfFundList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("name", fundList[0]);
			entityMap.put("id", fundList[1]);
			entityMap.put("code", fundList[2]);
			entityMap.put("name", fundList[3]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}
	
	public List<Object> findForListing() {
		List<Object[]> sourceOfFundList = fundRepo.getListSourceFund();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] fundList : sourceOfFundList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("costCenter", fundList[0]);
			entityMap.put("id", fundList[1]);
			entityMap.put("code", fundList[2]);
			entityMap.put("name", fundList[3]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}

}
