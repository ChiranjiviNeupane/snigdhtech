package com.snigdh.Erp.Service.AttendanceService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.AttendanceModel.Attendance;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.AttendanceRepository.AttendanceRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.LeaveRemainingService;

@Service("AttendanceService")
public class AttendanceService {
		
	@Autowired
	EmployeePersonalInformationRepository empRepository;

	@Autowired
	private AttendanceRepository repository;

	@Autowired
	private UserService generalService;
	
	@Autowired
	private UtilityService utl;
	
	@Autowired
	LeaveRemainingService leaveRemainingService;
	
	@Autowired
	LieuRequestService lieuService;

	public AttendanceService(AttendanceRepository repository) {
		this.repository = repository;
	}

	public String dateAndTime() {
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar calendar = Calendar.getInstance();
		String setDateAndTeime = dateFormat.format(new Date()) +" "+ timeFormat.format(calendar.getTime());
		return setDateAndTeime;
	}

	public void saveValue(Attendance attendanceObj) {
		int id = generalService.getUserId();
		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		List<Attendance> att = repository.findByEmployeePersonalInformation_Id(id);
		for (Attendance var : att) {
			if (var.getStatus() == true) {
				return;
			}
		}
		attendanceObj.setStatus(true);
		attendanceObj.setCheckInLocation(attendanceObj.getCheckInLocation());
		String dateAndTime = dateAndTime();
		attendanceObj.setCheckInTime(dateAndTime);
		attendanceObj.setEmployeePersonalInformation(employee.get());
		repository.save(attendanceObj);
		boolean bool = utl.checkIfHolidayWeekend(utl.splitDateFromTime(dateAndTime));
		if(bool == true) {
			if(lieuService.getLieuHoliday(id, utl.splitDateFromTime(dateAndTime)) == true) {
				leaveRemainingService.saveLieuLeave(id,"Lieu");
			}			
		}
	}

	public void updateValue(Attendance attendance) {
		
		int id = generalService.getUserId();
		List<Attendance> att = repository.findByEmployeePersonalInformation_Id(id);
		for (Attendance var : att) {
			if (var.getStatus() == true) {
				var.setCheckOutLocation(attendance.getCheckOutLocation());
				var.setStatus(false);
				var.setCheckOutTime(dateAndTime());
				repository.save(var);
			}
		}
	}

	public Object myAttendance() {
		int id = generalService.getUserId();
		List<Object[]> att = repository.findSpecificEmployeeAttendance(id);
		List<Object> attendanceArrayWithMap = new ArrayList<Object>();
		for (Object[] recent : att) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("checkIn", recent[0]);
			recentUserJson.put("checkOut", recent[1]);
			attendanceArrayWithMap.add(recentUserJson);
		}
		
		return attendanceArrayWithMap;
	}
	
	public Object allAttendance() {
		List<Object[]> att = repository.findAllEmployeeAttendance();
		List<Object> attendanceArrayWithMap = new ArrayList<Object>();
		for (Object[] recent : att) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("name",utl.mergeName(recent[0].toString(),recent[1].toString(), recent[2].toString()));
			recentUserJson.put("checkIn", recent[3]);
			recentUserJson.put("checkOut", recent[4]);
			attendanceArrayWithMap.add(recentUserJson);
		}

		return attendanceArrayWithMap;
	}
	
	public Object attendanceStatusWithRole() {
		int id = generalService.getUserId();
		Map<String, Object> status = new HashMap<String, Object>();
		status.put("role", generalService.getUserRole());
		status.put("id", id);
		status.put("status",repository.findSpecificEmployeeAttendanceStatus(id, PageRequest.of(0,1)));
		status.put("name",utl.mergeName(generalService.getUserFirstName(),generalService.getUserMiddleName(),generalService.getSurName()));
		List<Object[]> att = empRepository.getCompanyAndDepart(id);
		for (Object[] recent : att) {	
			status.put("department", recent[0]);
			status.put("position", recent[1]);
		}
		return status;
	}
	
	public void attendanceAutoCheckOut() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		List<Attendance> att = repository.findByStatus(true);
		for (Attendance recent : att) {
			recent.setCheckOutTime(formatter.format(date)+" "+"17:30:00");
			recent.setStatus(false);
			repository.save(recent);
		}
	}
}
