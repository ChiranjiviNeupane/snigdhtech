package com.snigdh.Erp.Service.OfficeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Office.Activity;
import com.snigdh.Erp.Model.Office.Project;
import com.snigdh.Erp.Repository.OfficeRepository.ActivityRepository;
import com.snigdh.Erp.Repository.OfficeRepository.ProjectRepository;

@Service
public class ActivityService {
	@Autowired
	public ActivityRepository activityRepo;
	
	@Autowired
	public ProjectRepository projectRepo;
	
	public Activity save(Activity activity,Integer id) {	
		Project project1 = projectRepo.findOneDetailById(id);
		activity.setProject(project1);
		return activityRepo.save(activity);
	}
	
	public Activity retrieve(Integer id) {
		Optional<Activity> activity = activityRepo.findById(id);
		return activity.get();
	}
	
	public List<Activity> findAll() {
		return activityRepo.findAll();
	}
	
	public Activity updateValue(Activity activity, Integer id,Integer projectId) {
		Project project = projectRepo.findOneDetailById(projectId);
		return activityRepo.findById(id).map(activityKey -> {
			activityKey.setProject(project);
			activityKey.setCode(activity.getCode());
			activityKey.setName(activity.getName());		
			return activityRepo.save(activityKey);
		})

				.orElseGet(() -> {
					activity.setId(id);
					return activityRepo.save(activity);
				});
	}
	
	public List<Object> findForTravelRequest(Integer id) {
		List<Object[]> activityList = activityRepo.getListActivityDetails(id);
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] aList : activityList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("id", aList[0]);
			entityMap.put("code", aList[1]);
			entityMap.put("name", aList[2]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}
	
	public void deleteValue(Activity id) {
		activityRepo.delete(id);
	}

	public List<Object> findForListing() {
		List<Object[]> activityList = activityRepo.getListActivity();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] aList : activityList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("project", aList[0]);
			entityMap.put("id", aList[1]);
			entityMap.put("code", aList[2]);
			entityMap.put("name", aList[3]); 
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}
}
