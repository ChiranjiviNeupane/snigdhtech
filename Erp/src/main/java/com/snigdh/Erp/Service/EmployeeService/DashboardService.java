package com.snigdh.Erp.Service.EmployeeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Repository.EmployeeRepository.CompanyInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.OtherInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.PermanentHomeAddressRepository;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class DashboardService {

	@Autowired
	EmployeePersonalInformationRepository empRepo;

	@Autowired
	OtherInformationRepository otherRepo;

	@Autowired
	PermanentHomeAddressRepository permanentRepo;

	@Autowired
	CompanyInformationRepository compRepo;
	@Autowired
	UtilityService utl;

	public Object getDashBoard() {
		Map<String, HashMap<String, Object>> dashboard = new HashMap<String, HashMap<String, Object>>();
		dashboard.put("sex", (HashMap<String, Object>) sex());
		dashboard.put("religion", (HashMap<String, Object>) religion());
		dashboard.put("state", (HashMap<String, Object>) state());
		dashboard.put("contract", (HashMap<String, Object>) contract());
		dashboard.put("level", (HashMap<String, Object>) level());
		dashboard.put("department", (HashMap<String, Object>) department());
		dashboard.put("ageGroup", (HashMap<String, Object>) age());
		dashboard.put("ethnicity", (HashMap<String, Object>) ethnicity());
		dashboard.put("users", (HashMap<String, Object>) recentUser());
		return dashboard;
	}

	// methods to store object in getDashBoard
	public Map<String, Object> religion() {
		HashMap<String, Object> religion = new HashMap<String, Object>();
		religion.put("hindu", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Hinduism", true));
		religion.put("buddhist",otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Buddhism", true));
		religion.put("islam", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Islam", true));
		religion.put("jain", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Jainism", true));
		religion.put("christian",otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Christianity", true));
		religion.put("other", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Other", true));
		religion.put("sikh", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Sikhism", true));
		religion.put("secular", otherRepo.countByReligionAndEmployeePersonalInformation_presentStatus("Secular", true));
		return religion;
	}

	public Map<String, Object> sex() {
		HashMap<String, Object> sex = new HashMap<String, Object>();
		sex.put("male", empRepo.countBySexAndPresentStatus("Male", true));
		sex.put("female", empRepo.countBySexAndPresentStatus("Female", true));
		sex.put("other", empRepo.countBySexAndPresentStatus("Others", true));
		return sex;
	}

	public Map<String, Object> state() {
		HashMap<String, Object> state = new HashMap<String, Object>();
		state.put("one", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("1", true));
		state.put("two", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("2", true));
		state.put("three", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("3", true));
		state.put("four", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("4", true));
		state.put("five", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("5", true));
		state.put("six", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("6", true));
		state.put("seven", permanentRepo.countByStateAndEmployeePersonalInformation_presentStatus("7", true));
		return state;
	}

	public Map<String, Object> level() {
		HashMap<String, Object> level = new HashMap<String, Object>();
		level.put("A", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("A", true));
		level.put("B", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("B", true));
		level.put("C", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("C", true));
		level.put("D", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("D", true));
		level.put("E", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("E", true));
		level.put("F", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("F", true));
		level.put("G", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("G", true));
		level.put("H", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("H", true));
		level.put("I", compRepo.countByLevelAndEmployeePersonalInformation_presentStatus("I", true));
		return level;
	}

	public Map<String, Object> contract() {
		HashMap<String, Object> contract = new HashMap<String, Object>();
		contract.put("permanent",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Permanent", true));
		contract.put("fixed",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Fixed Term", true));
		contract.put("shortTerm",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Short Term", true));
		contract.put("casual",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Casual", true));
		contract.put("trainee",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Trainee", true));
		contract.put("internship",
				compRepo.countByContractTypeAndEmployeePersonalInformation_presentStatus("Internship", true));
		return contract;
	}

	public Map<String, Object> department() {
		HashMap<String, Object> depart = new HashMap<String, Object>();
		depart.put("program", compRepo.countByDepartmentAndEmployeePersonalInformation_presentStatus("Program", true));
		depart.put("finance", compRepo.countByDepartmentAndEmployeePersonalInformation_presentStatus("Finance", true));
		depart.put("humanResource",
				compRepo.countByDepartmentAndEmployeePersonalInformation_presentStatus("Human Resource", true));
		depart.put("administration",
				compRepo.countByDepartmentAndEmployeePersonalInformation_presentStatus("Administration", true));
		depart.put("communication",
				compRepo.countByDepartmentAndEmployeePersonalInformation_presentStatus("Communication", true));
		return depart;
	}

	public Map<String, Object> ethnicity() {
		HashMap<String, Object> ethnicity = new HashMap<String, Object>();
		ethnicity.put("dalit", otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Dalit", true));
		ethnicity.put("madhesi",
				otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Madhesi", true));
		ethnicity.put("tharu", otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Tharu", true));
		ethnicity.put("muslim", otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Muslim", true));
		ethnicity.put("khasarya",
				otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Khasarya", true));
		ethnicity.put("adibasi",
				otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Adibasi", true));
		ethnicity.put("janjati",
				otherRepo.countByEthnicityAndEmployeePersonalInformation_presentStatus("Janjati", true));
		return ethnicity;
	}

	public Map<String, Object> age() {
		int countBelow20 = 0;
		int count20To30 = 0;
		int count30To40 = 0;
		int count40To50 = 0;
		int count50To60 = 0;
		int count60Plus = 0;
		int totalAge = 0;
		int unknown=0;
		HashMap<String, Object> age = new HashMap<String, Object>();

		List<com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation> list = empRepo.findAll();
		for (com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation model : list) {
					int convertedAge = utl.convertBirthDateToAgeInYear(model.getDateOfBirth(), new Date());
					totalAge = totalAge + convertedAge;
					if (convertedAge > 0 && convertedAge < 20) {
						countBelow20 = countBelow20 + 1;
					} else if (convertedAge >= 20 && convertedAge < 30) {
						count20To30 = count20To30 + 1;
					} else if (convertedAge >= 30 && convertedAge < 40) {
						count30To40 = count30To40 + 1;
					} else if (convertedAge >= 40 && convertedAge < 50) {
						count40To50 = count40To50 + 1;
					} else if (convertedAge >= 50 && convertedAge < 60) {
						count50To60 = count50To60 + 1;
					} else if (convertedAge >= 60) {
						count60Plus = count60Plus + 1;
					} else if (convertedAge == 0) {
					unknown = unknown + 1;
			}
		}
		long averageAge = totalAge / empRepo.count();
		age.put("average", averageAge);
		age.put("below20", countBelow20);
		age.put("twentyToThirty", count20To30);
		age.put("thirtyTofourty", count30To40);
		age.put("fourtyTofifty", count40To50);
		age.put("fiftyTosixty", count50To60);
		age.put("sixtyPlus", count60Plus);
		age.put("unknown", unknown);
		return age;
	}

	public Map<String, Object> recentUser() {
		// recent enrollment
		List<Object[]> recentUser = empRepo.findNewEmployee();
		List<Object> recentArrayWithMap = new ArrayList<Object>();
		Date  date = new Date();
		for (Object[] recent : recentUser) {
			//System.out.println(utl.convertDateToNoOfDay(date, (Date) recent[3]));
			if((date.getTime() - ((Date) recent[3]).getTime()) / (1000 * 60 * 60 * 24) <= 60) { //calculated the date and converted to day and compared with 60
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("name",utl.mergeName(recent[0].toString(),recent[1].toString(), recent[2].toString()));
			recentUserJson.put("createdAt", recent[3]);
			recentUserJson.put("department", recent[4]);
			recentArrayWithMap.add(recentUserJson);
			}
		}
		// recent left
		List<Object[]> recentLeftUser = empRepo.findRecentLeftEmployee();
		List<Object> recentLeftArrayWithMap = new ArrayList<Object>();
		for (Object[] recentLeft : recentLeftUser) {
			if(utl.daysBetweenDates(date, (String)recentLeft[5]) <= 60) {
			HashMap<String, Object> recentLeftUserJson = new HashMap<String, Object>();
			recentLeftUserJson.put("name",utl.mergeName(recentLeft[0].toString(),recentLeft[1].toString(), recentLeft[2].toString()));
			recentLeftUserJson.put("terminationDate", recentLeft[5]);
			recentLeftUserJson.put("department", recentLeft[4]); 
			recentLeftArrayWithMap.add(recentLeftUserJson);
			}
		}

		HashMap<String, Object> recent = new HashMap<String, Object>();
		recent.put("new", recentArrayWithMap);
		recent.put("left", recentLeftArrayWithMap);
		return recent;
	}
	// end of parsing data to getDashBoard
}
