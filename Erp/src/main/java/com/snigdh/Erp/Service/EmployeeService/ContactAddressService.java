package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.ContactAddress;
import com.snigdh.Erp.Repository.EmployeeRepository.ContactAddressRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class ContactAddressService {

	private ContactAddressRepository addressRepository;

	@Autowired
	UserService genService;
	
	@Autowired
	public ContactAddressService(ContactAddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}

	public List<ContactAddress> findAll() {
		return addressRepository.findAll();
	}

	public void saveValue(ContactAddress contactAddressObj) {
		contactAddressObj.setUpdatedAt(new Date());
		contactAddressObj.setUpdatedBy(genService.getUserEmail());
		addressRepository.save(contactAddressObj);
	}

	public ContactAddress retrieve(Integer id) {
		Optional<ContactAddress> contactAddress = addressRepository.findByEmployeePersonalInformation_Id(id);
		return contactAddress.get();
	}

	public void deleteContactAddress(ContactAddress id) {
		addressRepository.delete(id);
	}

	
	public ContactAddress updateContactAddress(ContactAddress contactAddress,Integer id) {
		return addressRepository.findByEmployeePersonalInformation_Id(id)
				.map(contactAddr ->
				{
						contactAddr.setCity(contactAddress.getCity());
						contactAddr.setCountry(contactAddress.getCountry());
						contactAddr.setState(contactAddress.getState());
						contactAddr.setStreet(contactAddress.getStreet());
						contactAddr.setWardNo(contactAddress.getWardNo());
						contactAddr.setUpdatedAt(new Date());
						contactAddr.setUpdatedBy(genService.getUserEmail());
						
						return addressRepository.save(contactAddr);
				})
				
				.orElseGet(() ->
				{
					contactAddress.setId(id);
					return addressRepository.save(contactAddress);
				});

	}

}
