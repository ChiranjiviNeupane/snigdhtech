package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.EmergencyContactInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.EmergencyContactInformationRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class EmergencyContactInformationService {

	private EmergencyContactInformationRepository informationRepository;

	@Autowired
	UserService genService;
	
	@Autowired
	public EmergencyContactInformationService(EmergencyContactInformationRepository informationRepository) {
		super();
		this.informationRepository = informationRepository;
	}

	public List<EmergencyContactInformation> findAll() {
		return informationRepository.findAll();
	}

	public void save(EmergencyContactInformation emergencyContactInformation) {
		emergencyContactInformation.setUpdatedAt(new Date());
		emergencyContactInformation.setUpdatedBy(genService.getUserEmail());
		informationRepository.save(emergencyContactInformation);
	}

	public EmergencyContactInformation retrieve(Integer id) {
		Optional<EmergencyContactInformation> emgContInfo = informationRepository
				.findByEmployeePersonalInformation_Id(id);
		return emgContInfo.get();
	}

	public void delete(EmergencyContactInformation id) {
		informationRepository.delete(id);
	}

	public EmergencyContactInformation update(EmergencyContactInformation emergencyContactInformation, Integer id) {
		return informationRepository.findByEmployeePersonalInformation_Id(id).map(emgContInfo -> {
			emgContInfo.setName(emergencyContactInformation.getName());
			emgContInfo.setRelationship(emergencyContactInformation.getRelationship());
			emgContInfo.setContactNumber(emergencyContactInformation.getContactNumber());
			emgContInfo.setFullAddress(emergencyContactInformation.getFullAddress());
			emgContInfo.setUpdatedAt(new Date());
			emgContInfo.setUpdatedBy(genService.getUserEmail());


			return informationRepository.save(emgContInfo);
		})

				.orElseGet(() -> {
					emergencyContactInformation.setId(id);
					return informationRepository.save(emergencyContactInformation);
				});
	}

}
