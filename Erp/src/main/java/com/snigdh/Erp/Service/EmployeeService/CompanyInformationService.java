package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.CompanyInformationRepository;
import com.snigdh.Erp.Service.UserService;

@Service("CompanyInformationService")
public class CompanyInformationService {

	private CompanyInformationRepository companyInformationRepository;
	
	@Autowired
	UserService genService;

	@Autowired
	public CompanyInformationService(CompanyInformationRepository companyInformationRepository) {
		super();
		this.companyInformationRepository = companyInformationRepository;
	}

	public List<CompanyInformation> findAll() {
		return companyInformationRepository.findAll();
	}

	public HashMap<String, Object> retrieve(Integer id) {
		
		return companyInformationRepository.findByEmployeePersonalInformation_Id(id).map(companyInfo -> {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("lineManagerId", companyInfo.getLineManager());
			entityMap.put("position", companyInfo.getPosition());
			entityMap.put("level", companyInfo.getLevel());
			entityMap.put("step", companyInfo.getStep());
			entityMap.put("contractType", companyInfo.getContractType());
			entityMap.put("contractStartDate", companyInfo.getContractStartDate());
			entityMap.put("contractEndDate", companyInfo.getContractEndDate());
			entityMap.put("office", companyInfo.getOffice());
			entityMap.put("department", companyInfo.getDepartment());
			entityMap.put("section", companyInfo.getSection());
			entityMap.put("unit", companyInfo.getUnit());
			
			
			
			return entityMap;
		})

				.orElseThrow(() -> {
					Error er = new Error();
					return er;
				});
	}
	
	
	

	public void saveValue(CompanyInformation compInfoObj) {
		compInfoObj.setUpdatedAt(new Date());
		compInfoObj.setUpdatedBy(genService.getUserEmail());
		companyInformationRepository.save(compInfoObj);
	}

	public void deleteValue(CompanyInformation id) {
		companyInformationRepository.delete(id);
	}

	public CompanyInformation updateValue(CompanyInformation companyInformation, Integer id) {
		return companyInformationRepository.findByEmployeePersonalInformation_Id(id).map(companyInfo -> {
			companyInfo.setLineManager(companyInformation.getLineManager());
			companyInfo.setPosition(companyInformation.getPosition());
			companyInfo.setLevel(companyInformation.getLevel());
			companyInfo.setStep(companyInformation.getStep());
			companyInfo.setContractType(companyInformation.getContractType());
			companyInfo.setContractStartDate(companyInformation.getContractStartDate());
			companyInfo.setContractEndDate(companyInformation.getContractEndDate());
			companyInfo.setOffice(companyInformation.getOffice());
			companyInfo.setDepartment(companyInformation.getDepartment());
			companyInfo.setSection(companyInformation.getSection());
			companyInfo.setUnit(companyInformation.getUnit());
			companyInfo.setUpdatedAt(new Date());
			companyInfo.setUpdatedBy(genService.getUserEmail());
			

			return companyInformationRepository.save(companyInfo);
		})

				.orElseGet(() -> {
					companyInformation.setId(id);
					return companyInformationRepository.save(companyInformation);
				});
	}

}
