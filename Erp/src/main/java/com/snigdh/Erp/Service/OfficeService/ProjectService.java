package com.snigdh.Erp.Service.OfficeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Office.Project;
import com.snigdh.Erp.Model.Office.SourceOfFund;
import com.snigdh.Erp.Repository.OfficeRepository.ProjectRepository;
import com.snigdh.Erp.Repository.OfficeRepository.SourceOfFundRepository;

@Service
public class ProjectService {

	@Autowired
	public ProjectRepository projectRepo;
	
	@Autowired
	public SourceOfFundRepository sourceFundRepo;
	
	public Project save(Project project,Integer id) {	
		SourceOfFund source= sourceFundRepo.findOneDetailById(id);
		project.setSourceOfFund(source);
		return projectRepo.save(project);
	} 
	
	public Project retrieve(Integer id) {
		Optional<Project> project = projectRepo.findById(id);
		return project.get();
	}
	
	public List<Project> findAll() {
		return projectRepo.findAll();
	}
	
	public Project updateValue(Project project, Integer id,Integer sourceFund) {
		SourceOfFund source = sourceFundRepo.findOneDetailById(sourceFund);
		return projectRepo.findById(id).map(projectKey -> {
			projectKey.setSourceOfFund(source);
			projectKey.setCode(project.getCode());
			projectKey.setName(project.getName());		
			return projectRepo.save(projectKey);
		})

				.orElseGet(() -> {
					project.setId(id);
					return projectRepo.save(project);
				});
	}
	
	public List<Object> findForTravelRequest(Integer id) {
		List<Object[]> ProjectList = projectRepo.getListProjectDetails(id);
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] pList : ProjectList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("id", pList[0]);
			entityMap.put("code", pList[1]);
			entityMap.put("name", pList[2]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}
	
	public List<Object> findForListing() {
		List<Object[]> ProjectList = projectRepo.getListProject();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] pList : ProjectList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("sourceOfFund",pList[0]);
			entityMap.put("id", pList[1]);
			entityMap.put("code", pList[2]);
			entityMap.put("name", pList[3]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}
	
	public void deleteValue(Project id) {
		projectRepo.delete(id);
	}
}
