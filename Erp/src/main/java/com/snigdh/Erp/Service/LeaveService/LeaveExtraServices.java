package com.snigdh.Erp.Service.LeaveService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HalfDayLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HourlyLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class LeaveExtraServices {

	@Autowired
	UtilityService utilityService;

	@Autowired
	LeaveRequestRepository leaveRequestRepository;

	@Autowired
	HourlyLeaveService hourlyLeaveService;

	@Autowired
	HalfDayLeaveService halfDayLeaveService;

	@Autowired
	HalfDayLeaveRepository halfDayLeaveRepository;

	@Autowired
	private HourlyLeaveRepository hourlyLeaveRepository;

	@Autowired
	EmployeePersonalInformationRepository empRepository;

	// Line Manager Get All Leave responses

	public Object getAllLeaveResponses(Integer id) {
		return getAllLeaveRequest(id);
	}

	public Object getAllLeaveRequest(Integer id) {
		List<Object[]> leaverequest = leaveRequestRepository.findByRequestTo(id, "Approved", "Rejected", "Cancelled");
		List<Object[]> leaveRecommend = leaveRequestRepository.findByRecommend(String.valueOf(id));
		List<Object> arrayWithMap = new ArrayList<Object>();
		List<Object[]> halfDayrequest = halfDayLeaveRepository.findByRequestTo(id, "Approved", "Rejected", "Cancelled");
		List<Object[]> hourlyrequest = hourlyLeaveRepository.findByRequestTo(id, "Approved", "Rejected", "Cancelled");

		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Object[] recent : leaverequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("noOfDays", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveFrom", recent[5]);
			recentUserJson.put("LeaveTill", recent[6]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[7])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			String name = utilityService.mergeName(String.valueOf(recent[8]), String.valueOf(recent[9]),
					String.valueOf(recent[10]));
			recentUserJson.put("name", name);
			recentUserJson.put("requestStatus", recent[11]);
			String recommendBy = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				recommendBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				recommendBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", recommendBy);
			recentUserJson.put("recommendedDate", recent[13]);
			recentUserJson.put("createdDate", recent[14]);
			recentUserJson.put("createdTime", recent[15]);
			recentUserJson.put("extendStatus", recent[16]);
			recentUserJson.put("extendTill", recent[17]);
			recentUserJson.put("extendDays", recent[18]);
			recentUserJson.put("remarks", recent[19]);
			recentUserJson.put("cancelFrom", recent[20]);
			recentUserJson.put("cancelStatus", recent[21]);
			recentUserJson.put("cancelDate", recent[22]);
			recentUserJson.put("cancelRemarks", recent[23]);
			recentUserJson.put("extendedDate", recent[24]);
			recentUserJson.put("extendedResponseDate", recent[25]);
			String responseBy = "";
			if (String.valueOf(recent[26]).equals("") || String.valueOf(recent[26]).equals(null)
					|| String.valueOf(recent[26]).isEmpty()) {
				responseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[26])));
				responseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("responseBy", responseBy);
			recentUserJson.put("responseDate", recent[27]);
			recentUserJson.put("responseTime", recent[28]);
			recentUserJson.put("extendRemarks", recent[29]);
			recentUserJson.put("extendResponseRemarks", recent[30]);
			recentUserJson.put("department", recent[31]);
			recentUserJson.put("position", recent[32]);
			recentUserJson.put("cancelResponseDate", recent[33]);
			String cancelResponseBy = "";
			if (String.valueOf(recent[34]).equals("") || String.valueOf(recent[34]).equals(null)
					|| String.valueOf(recent[34]).isEmpty()) {
				cancelResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[34])));
				cancelResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("cancelResponseBy", cancelResponseBy);
			recentUserJson.put("cancelResponseRemarks", recent[35]);
			String extendResponseBy = "";
			if (String.valueOf(recent[36]).equals("") || String.valueOf(recent[36]).equals(null)
					|| String.valueOf(recent[36]).isEmpty()) {
				extendResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[36])));
				extendResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("extendResponseBy", extendResponseBy);
			recentUserJson.put("LeaveRequestType", "NormalLeave");
			data_list.add(recentUserJson);
		}

		for (Object[] recent : leaveRecommend) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("noOfDays", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveFrom", recent[5]);
			recentUserJson.put("LeaveTill", recent[6]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[7])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			String name = utilityService.mergeName(String.valueOf(recent[8]), String.valueOf(recent[9]),
					String.valueOf(recent[10]));
			recentUserJson.put("name", name);
			recentUserJson.put("requestStatus", recent[11]);
			String recommendBy = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				recommendBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				recommendBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", recommendBy);
			recentUserJson.put("recommendedDate", recent[13]);
			recentUserJson.put("createdDate", recent[14]);
			recentUserJson.put("createdTime", recent[15]);
			recentUserJson.put("extendStatus", recent[16]);
			recentUserJson.put("extendTill", recent[17]);
			recentUserJson.put("extendDays", recent[18]);
			recentUserJson.put("remarks", recent[19]);
			recentUserJson.put("cancelFrom", recent[20]);
			recentUserJson.put("cancelStatus", recent[21]);
			recentUserJson.put("cancelDate", recent[22]);
			recentUserJson.put("cancelRemarks", recent[23]);
			recentUserJson.put("extendedDate", recent[24]);
			recentUserJson.put("extendedResponseDate", recent[25]);
			String responseBy = "";
			if (String.valueOf(recent[26]).equals("") || String.valueOf(recent[26]).equals(null)
					|| String.valueOf(recent[26]).isEmpty()) {
				responseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[26])));
				responseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("responseBy", responseBy);
			recentUserJson.put("responseDate", recent[27]);
			recentUserJson.put("responseTime", recent[28]);
			recentUserJson.put("extendRemarks", recent[29]);
			recentUserJson.put("extendResponseRemarks", recent[30]);
			recentUserJson.put("department", recent[31]);
			recentUserJson.put("position", recent[32]);
			recentUserJson.put("cancelResponseDate", recent[33]);
			String cancelResponseBy = "";
			if (String.valueOf(recent[34]).equals("") || String.valueOf(recent[34]).equals(null)
					|| String.valueOf(recent[34]).isEmpty()) {
				cancelResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[34])));
				cancelResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("cancelResponseBy", cancelResponseBy);
			recentUserJson.put("cancelResponseRemarks", recent[35]);
			String extendResponseBy = "";
			if (String.valueOf(recent[36]).equals("") || String.valueOf(recent[36]).equals(null)
					|| String.valueOf(recent[36]).isEmpty()) {
				extendResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[36])));
				extendResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("extendResponseBy", extendResponseBy);
			recentUserJson.put("LeaveRequestType", "NormalLeave");
			data_list.add(recentUserJson);
		}

		for (Object[] recent : halfDayrequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("halfDayType", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveDate", recent[5]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[6])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			recentUserJson.put("requestStatus", recent[7]);
			recentUserJson.put("responseDate", recent[8]);
			recentUserJson.put("responseTime", recent[9]);
			recentUserJson.put("createdDate", recent[10]);
			recentUserJson.put("createdTime", recent[11]);
			String RecomendName = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				RecomendName = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("remarks", recent[13]);
			recentUserJson.put("responseBy", recent[14]);
			String name = utilityService.mergeName(String.valueOf(recent[15]), String.valueOf(recent[16]),
					String.valueOf(recent[17]));
			recentUserJson.put("name", name);
			recentUserJson.put("department", recent[18]);
			recentUserJson.put("position", recent[19]);
			recentUserJson.put("LeaveRequestType", "HalfDayLeave");
			data_list.add(recentUserJson);
		}

		for (Object[] recent : hourlyrequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("leaveFrom", recent[3]);
			recentUserJson.put("LeaveTill", recent[4]);
			recentUserJson.put("noOfHours", recent[5]);
			recentUserJson.put("leaveReason", recent[6]);
			recentUserJson.put("leaveDate", recent[7]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[8])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			recentUserJson.put("requestStatus", recent[9]);
			recentUserJson.put("createdDate", recent[10]);
			recentUserJson.put("createdTime", recent[11]);
			recentUserJson.put("responseDate", recent[12]);
			recentUserJson.put("responseTime", recent[13]);
			recentUserJson.put("responseBy", recent[14]);
			String RecomendName = "";
			if (String.valueOf(recent[15]).equals("") || String.valueOf(recent[15]).equals(null)
					|| String.valueOf(recent[15]).isEmpty()) {
				RecomendName = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[15])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("remarks", recent[16]);
			String name = utilityService.mergeName(String.valueOf(recent[17]), String.valueOf(recent[18]),
					String.valueOf(recent[19]));
			recentUserJson.put("name", name);
			recentUserJson.put("department", recent[20]);
			recentUserJson.put("position", recent[21]);
			recentUserJson.put("LeaveRequestType", "HourlyLeave");
			data_list.add(recentUserJson);
		}

		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("responseDate") && null != o1.get("responseDate")) {
					return o2.get("responseDate").toString().compareTo(o1.get("responseDate").toString());
				} else if (null != o1.get("responseDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}
}
