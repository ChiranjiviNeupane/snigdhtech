package com.snigdh.Erp.Service.LeaveService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Repository.DelegateRepository.DelegateRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HalfDayLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HourlyLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRemainingRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.NotificationService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class HourlyLeaveService {

	@Autowired
	private UserService generalService;

	@Autowired
	MailService mailService;

	@Autowired
	DelegateRepository delegateRepository;

	@Autowired
	LeaveRemainingService leaveRemainingService;

	@Autowired
	LeaveRemainingRepository leaveRemainingRepository;

	@Autowired
	UtilityService utilityService;

	@Autowired
	LeaveTypeRepository leaveTypeRepo;

	@Autowired
	private EmployeePersonalInformationRepository empRepository;

	@Autowired
	private HourlyLeaveRepository leaveRepository;

	@Autowired
	LeaveRequestRepository leaveRequestRepository;

	@Autowired
	private HolidayService holidayService;

	@Autowired
	HalfDayLeaveRepository halfDayLeaveRepository;

	@Autowired
	NotificationService notificationService;

	/*
	 * public HourlyLeaveService(EmployeePersonalInformationRepository
	 * empRepository, HourlyLeaveRepository leaveRepository) { super();
	 * this.empRepository = empRepository; this.leaveRepository = leaveRepository; }
	 */

	public ResponseEntity<String> saveLeaveRequest(HourlyLeave hourlyLeave, Integer id) {
		int lineId = 0;
		int delegateStatus = 0;
		String lineManager = "";
		String lineManagerEmail = "";
		String delegateName = "";
		String delegateManagerEmail = "";
		String message = "";
		String subject = "LeaveRequest";
		List<LeaveRemaining> remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		if (remId == null) {
			leaveRemainingService.saveRemainingLeave(id);
			remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		}
		List<LeaveRequest> requestList = leaveRequestRepository.findByEmployeePersonalInformation_Id(id);
		for (LeaveRequest var : requestList) {
			if ((utilityService.convertToLocalDate(hourlyLeave.getLeaveDate())
					.isAfter(utilityService.convertToLocalDate(var.getLeaveFrom()))
					&& utilityService.convertToLocalDate(hourlyLeave.getLeaveDate())
							.isBefore(utilityService.convertToLocalDate(var.getLeaveTill())))
					|| utilityService.convertToLocalDate(hourlyLeave.getLeaveDate())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveFrom()))
					|| utilityService.convertToLocalDate(hourlyLeave.getLeaveDate())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveTill()))) {
				return new ResponseEntity<String>("Leave already requested in given date", HttpStatus.BAD_REQUEST);
			}
		}

		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		List<String> holiday = holidayService.getHolidayDates();
		List<HalfDayLeave> halfLeaveRequestList = halfDayLeaveRepository
				.findByEmployeePersonalInformation_IdAndLeaveDate(id, hourlyLeave.getLeaveDate());
		if (!(halfLeaveRequestList.isEmpty())) {
			return new ResponseEntity<String>("Leave already requested in given date", HttpStatus.BAD_REQUEST);
		}

		LeaveType leaveType = leaveTypeRepo.findByTypeOfLeave(hourlyLeave.getLeaveType().getTypeOfLeave());
		hourlyLeave.setLeaveType(leaveType);
		hourlyLeave.setResponseDate("");
		hourlyLeave.setEmployeePersonalInformation(employee.get());
		hourlyLeave.setLeaveDate(hourlyLeave.getLeaveDate());
		hourlyLeave.setLeaveReason(hourlyLeave.getLeaveReason());
		hourlyLeave.setHourFrom(hourlyLeave.getHourFrom());
		hourlyLeave.setHourTill(hourlyLeave.getHourTill());
		hourlyLeave.setNumberOfHours(
				utilityService.differenceInTime(hourlyLeave.getHourFrom(), hourlyLeave.getHourTill()));
		hourlyLeave.setRecommendedBy("");
		hourlyLeave.setRemarks("");
		hourlyLeave.setRemarks(hourlyLeave.getRemarks());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		hourlyLeave.setCreatedDate(simpleDateFormat.format(new Date()));

		for (CompanyInformation var : employee.get().getCompanyInformations()) {
			Optional<EmployeePersonalInformation> emp = empRepository.findById(Integer.valueOf(var.getLineManager()));
			lineManager = String.valueOf(emp.get().getEmail());
			lineId = emp.get().getId();
			lineManagerEmail = utilityService.mergeName(emp.get().getFirstName(), emp.get().getMiddleName(),
					emp.get().getSurname());

			Optional<Delegate> delegateOpt = delegateRepository
					.findByEmployeePersonalInformation_IdAndOnDelegateStatus(lineId, true);
			if (delegateOpt.isPresent()) {
				delegateStatus = 1;
				delegateManagerEmail = delegateOpt.get().getEmployeePersonalInformation().getEmail();
				delegateName = utilityService.mergeName(
						delegateOpt.get().getEmployeePersonalInformation().getFirstName(),
						delegateOpt.get().getEmployeePersonalInformation().getMiddleName(),
						delegateOpt.get().getEmployeePersonalInformation().getSurname());
				message = "HourlyLeave Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName())
						+ " has been delegated.";
				mailService.sendMail(delegateManagerEmail, subject, message);
			}
			hourlyLeave.setRequestTo(lineId);
		}
		int typeId = leaveType.getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId, id);
		if (rem == null) {
			leaveRemainingService.saveRemainingLeave(id);
			rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId, id);
		}
		if (rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Annual")
				|| rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Sick")) {
			leaveRepository.save(hourlyLeave);
		} else if (rem.getRemainingLeave() >= 1.0) {
			leaveRepository.save(hourlyLeave);
		} else {
			return new ResponseEntity<String>("Remaining day is " + rem.getRemainingLeave(), HttpStatus.BAD_REQUEST);
		}
		message = "HourlyLeave Request from " + utilityService.mergeName(generalService.getUserFirstName(),
				generalService.getUserMiddleName(), generalService.getSurName());
		mailService.sendMail(lineManager, subject, message);
		message = "HourlyLeave Request to " + lineManagerEmail;
		mailService.sendMail(employee.get().getEmail(), subject, message);
		notificationService.saveHourlyLeaveNotification(id, hourlyLeave.getLeaveDate());
		return ResponseEntity.ok().body("Leave Request successfully!");
	}

	public ResponseEntity<String> updateLeaveRequest(HourlyLeave hourlyLeave, Integer id) {
		Optional<HourlyLeave> hourlyOptional = leaveRepository.findById(id);
		hourlyOptional.get().setRequestStatus(hourlyLeave.getRequestStatus());
		hourlyOptional.get().setResponseDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		hourlyOptional.get().setResponseTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
		Optional<EmployeePersonalInformation> employee = empRepository.findById(generalService.getUserId());
		hourlyOptional.get().setResponseBy(utilityService.mergeName(employee.get().getFirstName(),
				employee.get().getMiddleName(), employee.get().getSurname()));
		if (hourlyLeave.getRequestStatus().equals("Approved")) {
			leaveRemainingService.updateHourlyLeave(id);
		}

		int lineId = 0;
		int delegateStatus = 0;
		String lineManagerEmail = "";
		String name = "";
		String delegateManagerEmail = "";
		String delegateName = "";
		String message2 = "";

		Optional<Delegate> delegateOpt = delegateRepository
				.findByEmployeePersonalInformation_IdAndOnDelegateStatus(lineId, true);
		if (delegateOpt.isPresent()) {
			delegateStatus = 1;
			delegateManagerEmail = delegateOpt.get().getEmployeePersonalInformation().getEmail();
			delegateName = utilityService.mergeName(delegateOpt.get().getEmployeePersonalInformation().getFirstName(),
					delegateOpt.get().getEmployeePersonalInformation().getMiddleName(),
					delegateOpt.get().getEmployeePersonalInformation().getSurname());
		}
		try {
			String subject = "Leave Request";
			if (delegateStatus == 1) {
				message2 = "Leave Request Updated";
				mailService.sendMail(delegateManagerEmail, subject, message2);
			}
			name = utilityService.mergeName(hourlyOptional.get().getEmployeePersonalInformation().getFirstName(),
					hourlyOptional.get().getEmployeePersonalInformation().getMiddleName(),
					hourlyOptional.get().getEmployeePersonalInformation().getSurname());
			String message = "Leave Request from " + name + " has been Updated by "
					+ utilityService.mergeName(generalService.getUserFirstName(), generalService.getUserMiddleName(),
							generalService.getSurName())
					+ ".";

			mailService.sendMail(hourlyOptional.get().getEmployeePersonalInformation().getEmail(), subject, message);
			mailService.sendMail(generalService.getUserEmail(), subject, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		leaveRepository.save(hourlyOptional.get());
		return ResponseEntity.ok().body("Leave Updated successfully!");
	}

	public void cancelLeaveRequest(Integer id) {
		Optional<HourlyLeave> leavereq = leaveRepository.findById(id);
		if (leavereq.get().getRequestStatus().equals("Pending")) {
			leavereq.get().setRequestStatus("Cancelled");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String date = simpleDateFormat.format(new Date());
			leavereq.get().setResponseDate(date);
			leaveRepository.save(leavereq.get());
		}
		if (leavereq.get().getRequestStatus().equals("Approved")) {
			leavereq.get().setRequestStatus("Cancelled");
			leaveRemainingService.cancelHourlyLeave(leavereq.get().getId());
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String date = simpleDateFormat.format(new Date());
			leavereq.get().setResponseDate(date);
			leaveRepository.save(leavereq.get());
		}
	}

	// called from attendance service
	public List<Object[]> getMonthlyHoliday(int id, String year, String month) {
		String date = (year + "-" + month);
		return leaveRepository.getMonthlyLeave(id, date);
	}

	// save notification for leave request
	public List<HourlyLeave> saveHourlyLeaveNotification(int id, String date) {
		return leaveRepository.findByEmployeePersonalInformation_IdAndLeaveDate(id, date);
	}

	public Object findRequestById(Integer id) {
		List<Object[]> request = leaveRepository.findRequestById(id);
		List<Object> arrayWithMap = new ArrayList<Object>();

		for (Object[] recent : request) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("leaveType", recent[1]);
			String name = utilityService.mergeName(String.valueOf(recent[2]), String.valueOf(recent[3]),
					String.valueOf(recent[4]));
			recentUserJson.put("name", name);
			recentUserJson.put("noOfHours", recent[5]);
			recentUserJson.put("leaveFrom", recent[6]);
			recentUserJson.put("LeaveTill", recent[7]);
			recentUserJson.put("leaveReason", recent[8]);
			String RecomendName = "";
			if (String.valueOf(recent[9]).equals("") || String.valueOf(recent[9]).equals(null)
					|| String.valueOf(recent[9]).isEmpty()) {
				RecomendName = "";

			} else {
				Optional<EmployeePersonalInformation> employee = empRepository
						.findById(Integer.valueOf(String.valueOf(recent[9])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("onLeaveStatus", recent[10]);
			recentUserJson.put("requestStatus", recent[11]);
			recentUserJson.put("remarks", recent[12]);
			recentUserJson.put("requestTo", recent[13]);
			// recentUserJson.put("position", recent[15]);
			recentUserJson.put("leaveDate", recent[15]);
			recentUserJson.put("LeaveRequestType", "HourlyLeave");
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}

	// Get Method For Leave Request line manager
	public ArrayList<HashMap<String, Object>> findByRequestAndStatus(int id, String status) {
		return getObjectData(leaveRepository.findByRequestAndStatus(id, status));
	}

	// get all my leave requests
	public ArrayList<HashMap<String, Object>> getMyLeaveRequest(int id) {
		return getObjectData(leaveRepository.findEmpById(id));
	}

	// find by id year month
	public ArrayList<HashMap<String, Object>> getAllMonthlyLeaveDate(int id, String date) {
		return getObjectData(leaveRepository.getAllMonthlyLeaveDate(id, date));
	}

	public ArrayList<HashMap<String, Object>> getObjectData(List<Object[]> leaverequest) {
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Object[] recent : leaverequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("leaveFrom", recent[3]);
			recentUserJson.put("LeaveTill", recent[4]);
			recentUserJson.put("noOfHours", recent[5]);
			recentUserJson.put("leaveReason", recent[6]);
			recentUserJson.put("leaveDate", recent[7]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[8])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			recentUserJson.put("requestStatus", recent[9]);
			recentUserJson.put("createdDate", recent[10]);
			recentUserJson.put("createdTime", recent[11]);
			recentUserJson.put("responseDate", recent[12]);
			recentUserJson.put("responseTime", recent[13]);
			recentUserJson.put("responseBy", recent[14]);
			String RecomendName = "";
			if (String.valueOf(recent[15]).equals("") || String.valueOf(recent[15]).equals(null)
					|| String.valueOf(recent[15]).isEmpty()) {
				RecomendName = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[15])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("remarks", recent[16]);
			String name = utilityService.mergeName(String.valueOf(recent[17]), String.valueOf(recent[18]),
					String.valueOf(recent[19]));
			recentUserJson.put("name", name);
			recentUserJson.put("department", recent[20]);
			recentUserJson.put("position", recent[21]);
			recentUserJson.put("LeaveRequestType", "HourlyLeave");
			data_list.add(recentUserJson);
		}
		return data_list;
	}

}
