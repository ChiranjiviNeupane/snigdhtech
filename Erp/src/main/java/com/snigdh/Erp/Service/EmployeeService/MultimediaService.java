package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.Multimedia;
import com.snigdh.Erp.Repository.EmployeeRepository.MultimediaRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class MultimediaService {
	
 private MultimediaRepository mulRepository;
	
	@Autowired
	UserService genService;

	@Autowired
	public MultimediaService(MultimediaRepository mulRepository) {
		super();
		this.mulRepository = mulRepository;
	}

	public List<Multimedia> findAll() {
		return mulRepository.findAll();
	}

	public Multimedia retrieve(Integer id) {
		Optional<Multimedia> mulInfo = mulRepository.findByEmployeePersonalInformation_Id(id);
		return mulInfo.get();
	}

	public void saveValue(Multimedia mulInfo) {
		mulInfo.setUpdatedAt(new Date());
		mulInfo.setUpdatedBy(genService.getUserEmail());
		mulRepository.save(mulInfo);
	}

	public void deleteValue(Multimedia id) {
		mulRepository.delete(id);
	}

	public Multimedia updateValue(Multimedia multimediaInformation, Integer id) {
		return mulRepository.findByEmployeePersonalInformation_Id(id).map(mulInfo -> {
			mulInfo.setPath(multimediaInformation.getPath());
			mulInfo.setType(multimediaInformation.getType());
			mulInfo.setUpdatedAt(new Date());
			mulInfo.setUpdatedBy(genService.getUserEmail());
			

			return mulRepository.save(mulInfo);
		})

				.orElseGet(() -> {
					multimediaInformation.setId(id);
					return mulRepository.save(multimediaInformation);
				});
	}

}
