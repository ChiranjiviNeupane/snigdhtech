package com.snigdh.Erp.Service.AttendanceService;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.snigdh.Erp.Model.AttendanceModel.LieuRequest;
import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.AttendanceRepository.LieuRequestRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.HolidayService;

@Service
public class LieuRequestService {
	
	@Autowired
	private UserService generalService;

	@Autowired
	MailService mailService;
	
	@Autowired
	LieuRequestRepository lieuRequestRepository;
	
	@Autowired
	EmployeePersonalInformationRepository empRepository;
	
	@Autowired
	private HolidayService holidayService;
	
	@Autowired
	UtilityService utilityService;

	public ResponseEntity<String> saveRequest(LieuRequest lieuRequest) {
		int id = generalService.getUserId();
		String lineManagerName = "", lineManagerEmail = "", subject = "Request To Work On Holiday", message = "";
		List<String> holiday = holidayService.getHolidayDates();
		DayOfWeek dayOfWeek = utilityService.convertToLocalDate(lieuRequest.getDate()).getDayOfWeek();
		if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY || holiday.contains(lieuRequest.getDate()))) {
			Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
			lieuRequest.setEmployeePersonalInformation(employee.get());
			lieuRequest.setDate(lieuRequest.getDate());
			lieuRequest.setCancelRequest("");
			for (CompanyInformation var : employee.get().getCompanyInformations()) {
				Optional<EmployeePersonalInformation> emp = empRepository.findById(Integer.valueOf(var.getLineManager()));
				lieuRequest.setRequestTo(emp.get().getId());
				lineManagerEmail = emp.get().getEmail();
				lineManagerName = utilityService.mergeName(emp.get().getFirstName(), emp.get().getMiddleName(), emp.get().getSurname());
			}
			message = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(), employee.get().getSurname()) + " has requested to work on "+lieuRequest.getDate()+". Please respond correspondingly."; 
			mailService.sendMail(lineManagerEmail, subject, message);
			message = "Your request is submitted to "+ lineManagerName+".";
			mailService.sendMail(employee.get().getEmail(), subject, message);
			lieuRequestRepository.save(lieuRequest);
			return ResponseEntity.ok().body("Leave Request successfully!");
		}else {
			return new ResponseEntity<String>("Leave Cannot Be Taken On Working Day", HttpStatus.BAD_REQUEST);
		}		
	}
	
	public void cancelRequest(Integer id) {
		String lineManagerName = "", lineManagerEmail = "", subject = "Request To Work On Holiday", message = "";
		Optional<LieuRequest> lieuRequest = lieuRequestRepository.findById(id);
		if(lieuRequest.get().getRequestStatus().equals("Cancelled")) {
			return;
		}
		lieuRequest.get().setRequestStatus("Cancelled");
		lieuRequestRepository.save(lieuRequest.get());
		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		for (CompanyInformation var : employee.get().getCompanyInformations()) {
			Optional<EmployeePersonalInformation> emp = empRepository.findById(Integer.valueOf(var.getLineManager()));
			lineManagerEmail = emp.get().getEmail();
			lineManagerName = utilityService.mergeName(emp.get().getFirstName(), emp.get().getMiddleName(), emp.get().getSurname());
		}
		message = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(), employee.get().getSurname()) + " has cancelled his request."; 
		mailService.sendMail(lineManagerEmail, subject, message);
		message = "Your request has been cancelled.";
		mailService.sendMail(employee.get().getEmail(), subject, message);
	}
	
	public void updateRequest(LieuRequest lieuRequest, Integer id) {
		Optional<LieuRequest> lieuRequestOpt = lieuRequestRepository.findById(id);
		lieuRequestOpt.get().setRequestStatus(lieuRequest.getRequestStatus());
		lieuRequestRepository.save(lieuRequestOpt.get()); 
	}
	
	//Called From Attendance Service saveValue
	public boolean getLieuHoliday(Integer id, String date) {
		Optional<LieuRequest> lieuRequest = lieuRequestRepository.findByDateAndEmployeePersonalInformation_Id(date, id);
		if(lieuRequest.get().getRequestStatus().equals("Approved")) {
			return true;
		}
		return false;
	}
}
