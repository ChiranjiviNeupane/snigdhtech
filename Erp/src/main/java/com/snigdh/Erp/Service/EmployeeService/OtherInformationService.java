package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.OtherInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.OtherInformationRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class OtherInformationService {

	private OtherInformationRepository informationRepository;

	@Autowired
	UserService genService;
	
	@Autowired
	public OtherInformationService(OtherInformationRepository informationRepository) {
		super();
		this.informationRepository = informationRepository;
	}

	public List<OtherInformation> findAll() {
		return informationRepository.findAll();
	}

	public void save(OtherInformation otherInformation) {
		otherInformation.setUpdatedAt(new Date());
		otherInformation.setUpdatedBy(genService.getUserEmail());
		informationRepository.save(otherInformation);
	}

	public OtherInformation retrieve(Integer id) {
		Optional<OtherInformation> otherInfo = informationRepository.findByEmployeePersonalInformation_Id(id);
		return otherInfo.get();
	}

	public void delete(OtherInformation id) {
		informationRepository.delete(id);
	}

	public OtherInformation update(OtherInformation otherInformation, Integer id) {
		return informationRepository.findByEmployeePersonalInformation_Id(id).map(otherInfo -> {
			otherInfo.setPanNumber(otherInformation.getPanNumber());
			otherInfo.setDrivingLiscenceNumber(otherInformation.getDrivingLiscenceNumber());
			otherInfo.setInsurancePolicyNumber(otherInformation.getInsurancePolicyNumber());
			otherInfo.setOtherProfessionalTrainingCourses(otherInformation.getOtherProfessionalTrainingCourses());
			otherInfo.setPastOrganisations(otherInformation.getPastOrganisations());
			otherInfo.setMajorAlergyOrSickness(otherInformation.getMajorAlergyOrSickness());
			otherInfo.setPreferences(otherInformation.getPreferences());
			otherInfo.setBloodGroup(otherInformation.getBloodGroup());
			otherInfo.setEthnicity(otherInformation.getEthnicity());
			otherInfo.setReligion(otherInformation.getReligion());
			otherInfo.setUpdatedAt(new Date());
			otherInfo.setUpdatedBy(genService.getUserEmail());
			otherInfo.setTerminationDate(otherInformation.getTerminationDate());

			return informationRepository.save(otherInfo);
		}).orElseGet(() -> {
			otherInformation.setId(id);
			return informationRepository.save(otherInformation);
		});
	}
}
