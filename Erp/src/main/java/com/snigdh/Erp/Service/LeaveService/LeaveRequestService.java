package com.snigdh.Erp.Service.LeaveService;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.Holiday;
import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Repository.DelegateRepository.DelegateRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HolidayRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRemainingRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.NotificationService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class LeaveRequestService {

	@Autowired
	DelegateRepository delegateRepository;

	@Autowired
	private HolidayRepository holidayRepository;

	@Autowired
	private HolidayService holidayService;

	@Autowired
	private UserService generalService;

	@Autowired
	MailService mailService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	LeaveRemainingRepository leaveRemainingRepository;

	@Autowired
	LeaveTypeRepository leaveTypeRepo;

	@Autowired
	UtilityService utilityService;

	@Autowired
	EmployeePersonalInformationRepository empRepository;

	@Autowired
	LeaveRemainingService leaveRemainingService;

	@Autowired
	LeaveRequestRepository requestRepository;

	@Autowired
	HourlyLeaveService hourlyLeaveService;

	@Autowired
	HalfDayLeaveService halfDayLeaveService;

	// Line Manager Get Pending Leave Requests
	// Get Method For Leave Request line manager
	public Object getLeaveRequest(int id, String status) {
		// List<Object[]> leaverequest = requestRepository.findByRequestAndStatus(id,
		// status);
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		List<Object> arrayWithMap = new ArrayList<Object>();

		data_list.addAll(findByRequestAndStatus(id, status));
		data_list.addAll(findByRequestAndExtendStatus(id, status));
		data_list.addAll(findByRequestAndCancelStatus(id, status));
		data_list.addAll(halfDayLeaveService.findByRequestAndStatus(id, status));
		data_list.addAll(hourlyLeaveService.findByRequestAndStatus(id, status));
		data_list.addAll(getFromDelegates(id));
		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("createdDate") && null != o1.get("createdDate")) {
					return o2.get("createdDate").toString().compareTo(o1.get("createdDate").toString());
				} else if (null != o1.get("createdDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}

	// employee gets all his requested leave
	public Object getMyLeaveRequest(int id) {
		List<Object> arrayWithMap = new ArrayList<Object>();
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();

		data_list.addAll(findEmpById(id));
		data_list.addAll(hourlyLeaveService.getMyLeaveRequest(id));
		data_list.addAll(halfDayLeaveService.getMyHalfDayLeave(id));

		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("createdDate") && null != o1.get("createdDate")) {
					return o2.get("createdDate").toString().compareTo(o1.get("createdDate").toString());
				} else if (null != o1.get("createdDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}

	public Object getEmployeeOnLeave() {
		List<Object[]> request = requestRepository.findEmployeeOnLeave(true);
		List<Object> arrayWithMap = new ArrayList<Object>();

		for (Object[] recent : request) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			String name = utilityService.mergeName(String.valueOf(recent[2]), String.valueOf(recent[3]),
					String.valueOf(recent[4]));
			recentUserJson.put("name", name);
			recentUserJson.put("position", recent[17]);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}

	public Object getEmployeeLeaveDate(Integer id) {
		List<LeaveRequest> leaveList = requestRepository.findByEmployeePersonalInformation_Id(id);
		List<Object> arrayWithMap = new ArrayList<Object>();
		ArrayList<String> dates = new ArrayList<String>();
		List<Object[]> holiday = holidayRepository.getHolidayDates();

		for (LeaveRequest var : leaveList) {
			String dateFrom = var.getLeaveFrom();
			String dateTill = var.getLeaveTill();
			// String dateExtend = var.getExtendTill();

			if (var.getRequestStatus().equals("Approved")) {

				while (dateFrom.compareTo(dateTill) != 1) {

					if ((utilityService.convertToLocalDate(dateFrom).getDayOfWeek() == DayOfWeek.SATURDAY)
							|| (utilityService.convertToLocalDate(dateFrom).getDayOfWeek() == DayOfWeek.FRIDAY)
							|| holiday.contains(dateFrom)) {

					} else {
						if (holiday.contains(dateFrom)) {
						} else {
							dates.add(dateFrom);
						}
					}
					dateFrom = utilityService.getNextDate(dateFrom);
				}
			}
		}

		for (int i = 0; i < dates.size(); i++) {
			HashMap<String, String> arraymap = new HashMap<String, String>();
			arraymap.put("title", "On leave");
			arraymap.put("start", dates.get(i));
			arrayWithMap.add(arraymap);
		}
		return arrayWithMap;
	}

	public Object findRequestById(Integer id) {
		return getObjectData(requestRepository.findRequestById(id));
	}

	// Save Leave Request
	public ResponseEntity<String> saveLeaveRequest(LeaveRequest leavereq, Integer id) {

		int lineId = 0;
		int delegateStatus = 0;
		String lineManagerEmail = "";
		String lineManagerName = "";
		String delegateManagerEmail = "";
		String delegateName = "";
		String message2 = "";
		String msg = "Request saved successfully";
		List<LeaveRemaining> remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		if (remId == null) {
			leaveRemainingService.saveRemainingLeave(id);
			remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		}
		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		List<String> holiday = holidayService.getHolidayDates();
		// to check if leave is already in given date
		List<LeaveRequest> requestList = requestRepository.findByEmployeePersonalInformation_Id(id);
		for (LeaveRequest var : requestList) {
			if ((utilityService.convertToLocalDate(leavereq.getLeaveFrom())
					.isAfter(utilityService.convertToLocalDate(var.getLeaveFrom()))
					&& utilityService.convertToLocalDate(leavereq.getLeaveFrom())
							.isBefore(utilityService.convertToLocalDate(var.getLeaveTill())))
					|| utilityService.convertToLocalDate(leavereq.getLeaveFrom())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveFrom()))
					|| utilityService.convertToLocalDate(leavereq.getLeaveTill())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveTill()))) {
				return new ResponseEntity<String>("Leave already requested in given date", HttpStatus.BAD_REQUEST);
			}
		}
		LeaveType leaveType = leaveTypeRepo.findByTypeOfLeave(leavereq.getLeaveType().getTypeOfLeave());
		String date1 = leavereq.getLeaveFrom();
		DayOfWeek dayOfWeek = utilityService.convertToLocalDate(date1).getDayOfWeek();

		if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY
				|| holiday.contains(leavereq.getLeaveFrom()))) {
			return new ResponseEntity<String>("Leave Cannot Be Taken On Weekends Or Holiday", HttpStatus.BAD_REQUEST);
		}
		String date2 = leavereq.getLeaveTill();
		double day = utilityService.getNumberOfDay(utilityService.convertToLocalDate(date1),
				utilityService.convertToLocalDate(date2));
		if ((utilityService.convertToLocalDate(date1).isBefore(utilityService.convertToLocalDate(date2))
				|| utilityService.convertToLocalDate(date1).isEqual(utilityService.convertToLocalDate(date2)))) {
			leavereq.setCancelStatus("");
			leavereq.setNoOfDays(day);
			leavereq.setExtendStatus("");
			leavereq.setExtendTill("");
			leavereq.setResponseDate("");
			leavereq.setResponseTime("");
			leavereq.setRemarks("");
			leavereq.setRecommendedBy("");
			leavereq.setCancelResponseBy("");
			leavereq.setCancelFrom("");
			leavereq.setResponseBy("");
			leavereq.setExtendResponseBy("");
			leavereq.setLeaveFrom(leavereq.getLeaveFrom());
			leavereq.setLeaveTill(leavereq.getLeaveTill());
			leavereq.setEmployeePersonalInformation(employee.get());
			leavereq.setLeaveType(leaveType);
			leavereq.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			leavereq.setCreatedTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			// company information for line manager
			for (CompanyInformation var : employee.get().getCompanyInformations()) {

				Optional<EmployeePersonalInformation> emp = empRepository
						.findById(Integer.valueOf(var.getLineManager()));
				lineManagerEmail = String.valueOf(emp.get().getEmail());
				lineManagerName = utilityService.mergeName(emp.get().getFirstName(), emp.get().getMiddleName(),
						emp.get().getSuffix());
				lineId = emp.get().getId();
				leavereq.setRequestTo(lineId);
			}
			Optional<Delegate> delegateOpt = delegateRepository
					.findByEmployeePersonalInformation_IdAndOnDelegateStatus(lineId, true);
			if (delegateOpt.isPresent()) {
				delegateStatus = 1;
				delegateManagerEmail = delegateOpt.get().getEmployeePersonalInformation().getEmail();
				delegateName = utilityService.mergeName(
						delegateOpt.get().getEmployeePersonalInformation().getFirstName(),
						delegateOpt.get().getEmployeePersonalInformation().getMiddleName(),
						delegateOpt.get().getEmployeePersonalInformation().getSurname());
			}
			int typeId = leaveType.getId();
			LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
					id);
			if (rem == null) {
				leaveRemainingService.saveRemainingLeave(id);
				rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId, id);
			}
			if (rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Annual")
					|| rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Sick")) {
				requestRepository.save(leavereq);
			} else if (rem.getRemainingLeave() >= day) {
				requestRepository.save(leavereq);
			} else {
				msg = "Remaining day is " + rem.getRemainingLeave();
				return new ResponseEntity<String>(msg, HttpStatus.BAD_REQUEST);
			}
			try {
				String subject = "Leave Request";
				if (delegateStatus == 1) {
					message2 = "Leave Request from "
							+ utilityService.mergeName(generalService.getUserFirstName(),
									generalService.getUserMiddleName(), generalService.getSurName())
							+ " requested to " + lineManagerName + " has been delegate to " + delegateName;
					mailService.sendMail(delegateManagerEmail, subject, message2);
				}
				String message = "Leave Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName())
						+ " has been requested to " + lineManagerName + ".";

				mailService.sendMail(lineManagerEmail, subject, message);
				mailService.sendMail(generalService.getUserEmail(), subject, message);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} else {
			return new ResponseEntity<String>("LeaveTill greater than Leave From", HttpStatus.BAD_REQUEST);
		}
		notificationService.saveLeaveNotification(id, leavereq.getLeaveFrom());
		return ResponseEntity.ok().body("Leave Request successfully!");
	}

	// Line Manager Get All Leave responses
	public Object getAllLeaveResponses(Integer id) {
		List<Object> arrayWithMap = new ArrayList<Object>();

		// arrayWithMap.add(getAllLeaveRequest(id));
		// arrayWithMap.add(hourlyLeaveService.getAllLeaveRequest(id));

		return getAllLeaveRequest(id);
	}

	// Updates Leave Request
	public ResponseEntity<String> updateLeaveRequest(LeaveRequest leaverequest, Integer id) {
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		if (leaverequest.getRequestTo() == null) {
			leavereq.get().setRequestStatus(leaverequest.getRequestStatus());
			leavereq.get().setRemarks(leaverequest.getRemarks());
			leavereq.get().setResponseBy(String.valueOf(generalService.getUserId()));
			leavereq.get().setResponseDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			leavereq.get().setResponseTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			if ((leaverequest.getRequestStatus()).equals("Approved")) {
				leaveRemainingService.updateLeaveRem(id);
			}
			Optional<EmployeePersonalInformation> employee = empRepository.findById(generalService.getUserId());
			try {
				Optional<LeaveRequest> lr = requestRepository.findById(id);
				employee = empRepository.findById(lr.get().getEmployeePersonalInformation().getId());
				String subject = "Leave Request " + leaverequest.getRequestStatus();
				String message = "Leave Request of" + utilityService.mergeName(employee.get().getFirstName(),
						employee.get().getMiddleName(), employee.get().getSurname()) + "has been "
						+ leaverequest.getRequestStatus();
				mailService.sendMail(employee.get().getEmail(), subject, message);
				mailService.sendMail(generalService.getUserEmail(), subject, message);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} else {
			leavereq.get().setRecommendedBy(String.valueOf(leavereq.get().getRequestTo()));
			leavereq.get().setRequestTo(leaverequest.getRequestTo());
			leavereq.get().setRecommendedDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			leavereq.get().setRemarks(leaverequest.getRemarks());
		}
		try {
			Optional<LeaveRequest> lr = requestRepository.findById(id);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(lr.get().getEmployeePersonalInformation().getId());
			String subject = "Leave Request Forwarded";
			String message = "Leave Request of" + utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()) + "has been recommended by "
					+ generalService.getUserEmail();
			lr = requestRepository.findById(leaverequest.getRequestTo());
			Optional<EmployeePersonalInformation> line = empRepository
					.findById(lr.get().getEmployeePersonalInformation().getId());
			mailService.sendMail(line.get().getEmail(), subject, message);
			mailService.sendMail(generalService.getUserEmail(), subject, message);
			mailService.sendMail(employee.get().getEmail(), subject, "Your request has been forwarded");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		requestRepository.save(leavereq.get());
		return ResponseEntity.ok().body("Leave Updated successfully!");
	}

	// Cancel Leave Authorization
	public void cancelLeaveAuthorization(LeaveRequest leaverequest, Integer id) {
		String subject = "Cancel Leave Request";
		String messageManager = "";
		String messageUser = "";
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		Optional<EmployeePersonalInformation> emp = empRepository.findById(leavereq.get().getRequestTo());
		if ((leaverequest.getCancelStatus()).equals("Approved")) {
			if (leavereq.get().getCancelFrom().isEmpty()) {
				leaveRemainingService.cancelLeaveRem(leavereq.get().getId());
				leavereq.get().setCancelResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				Optional<EmployeePersonalInformation> employee = empRepository.findById(leavereq.get().getRequestTo());
				leavereq.get().setCancelResponseBy(String.valueOf(generalService.getUserId()));
				leavereq.get().setCancelStatus("Approved");
				leavereq.get().setRequestStatus("Cancelled");
				leavereq.get().setCancelResponseRemarks(leaverequest.getCancelResponseRemarks());
				requestRepository.save(leavereq.get());
			} else {
				if (leavereq.get().isOnLeaveStatus() == true) {
					String cancelFrom = leavereq.get().getCancelFrom();
					double day = utilityService.getNumberOfDay(utilityService.convertToLocalDate(cancelFrom),
							utilityService.convertToLocalDate(leavereq.get().getLeaveTill()));
					leaveRemainingService.cancelLeave(id, day);
				} else {
					leaveRemainingService.cancelLeaveRem(leavereq.get().getId());
				}
				leavereq.get().setCancelResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				leavereq.get().setCancelStatus("Approved");
				Optional<EmployeePersonalInformation> employee = empRepository.findById(leavereq.get().getRequestTo());
				leavereq.get().setCancelResponseBy(String.valueOf(generalService.getUserId()));
				leavereq.get().setRequestStatus("Cancelled");
				leavereq.get().setCancelResponseRemarks(leaverequest.getCancelResponseRemarks());
				requestRepository.save(leavereq.get());
			}
			messageUser = "Your Leave Cancel Request has been Approved";
			messageManager = "Leave Cancel Request from " + utilityService.mergeName(generalService.getUserFirstName(),
					generalService.getUserMiddleName(), generalService.getSurName()) + " has been Approved";
		} else {
			leavereq.get().setCancelStatus(leaverequest.getCancelStatus());
			requestRepository.save(leavereq.get());
			messageUser = "Your Leave Cancel Request has been Rejected";
			messageManager = "Leave Cancel Request from " + utilityService.mergeName(generalService.getUserFirstName(),
					generalService.getUserMiddleName(), generalService.getSurName()) + " has been Rejected";
		}
		try {
			mailService.sendMail(emp.get().getEmail(), subject, messageManager);
			mailService.sendMail(generalService.getUserEmail(), subject, messageUser);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// Cancel Leave Request Normal
	public ResponseEntity<String> cancelLeaveRequest(LeaveRequest leaveRequest, Integer id) {
		String subject = "Cancel Leave Request";
		String messageManager = "";
		String messageUser = "";
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = objSDF.format(new Date());
		String leaveFrom = leavereq.get().getLeaveFrom();
		if (leaveFrom.compareTo(currentDate) > 0) {
			if (leavereq.get().getRequestStatus().equals("Pending")) {
					leavereq.get().setRequestStatus("Cancelled");
					leavereq.get().setCancelStatus("Approved");
					leavereq.get().setCancelFrom(leavereq.get().getLeaveFrom());
					leavereq.get().setCancelRemarks(leaveRequest.getCancelRemarks());
					leavereq.get().setCancelDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					requestRepository.save(leavereq.get());
			

				messageUser = "Your Leave Request has been cancelled";
				messageManager = "Leave Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName())
						+ " has been cancelled";

			}
			if (leavereq.get().getRequestStatus().equals("Approved")) {
				leavereq.get().setCancelStatus("Pending");
				leavereq.get().setCancelFrom(leavereq.get().getLeaveFrom());
				leavereq.get().setCancelRemarks(leaveRequest.getCancelRemarks());
				messageUser = "Your Leave Cancel Request Status is Sent To Your Respective Line Manager";
				messageManager = "Leave Cancel Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName());
				
				leavereq.get().setCancelDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				requestRepository.save(leavereq.get());
				return ResponseEntity.ok().body("Leave Updated successfully!");
			}
			try {
				Optional<EmployeePersonalInformation> emp = empRepository.findById(leavereq.get().getRequestTo());
				mailService.sendMail(emp.get().getEmail(), subject, messageManager);
				mailService.sendMail(generalService.getUserEmail(), subject, messageUser);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return ResponseEntity.ok().body(messageUser);
		} else {
			messageUser = "lekhna baki xa";
			return new ResponseEntity<String>(messageUser, HttpStatus.BAD_REQUEST);
		}
	}
	
	// Cancel Leave Request Normal
	public ResponseEntity<String> cancelLeaveRequestPartial(LeaveRequest leaveRequest, Integer id) {
		String subject = "Cancel Leave Request";
		String messageManager = "";
		String messageUser = "";
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		String currentDate = objSDF.format(new Date());
		String leaveFrom = leavereq.get().getLeaveFrom();
		if (leaveFrom.compareTo(currentDate) > 0) {
			if (leavereq.get().getRequestStatus().equals("Pending")) {
				
					leavereq.get().setRequestStatus("Cancelled");
					leavereq.get().setCancelStatus("Approved");
					leavereq.get().setCancelFrom(leaveRequest.getCancelFrom());
					leavereq.get().setCancelRemarks(leaveRequest.getCancelRemarks());
					leavereq.get().setCancelDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					requestRepository.save(leavereq.get());
			
				messageUser = "Your Leave Request has been cancelled";
				messageManager = "Leave Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName())
						+ " has been cancelled";

			}
			if (leavereq.get().getRequestStatus().equals("Approved")) {
				leavereq.get().setCancelStatus("Pending");
				if (leaveRequest == null) {
					leavereq.get().setCancelFrom("");
				} else {
					leavereq.get().setCancelFrom(leaveRequest.getCancelFrom());
				}
				messageUser = "Your Leave Cancel Request Status is Sent To Your Respective Line Manager";
				messageManager = "Leave Cancel Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName());
				
				leavereq.get().setCancelDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				requestRepository.save(leavereq.get());
				return ResponseEntity.ok().body("Leave Updated successfully!");
			}
			try {
				Optional<EmployeePersonalInformation> emp = empRepository.findById(leavereq.get().getRequestTo());
				mailService.sendMail(emp.get().getEmail(), subject, messageManager);
				mailService.sendMail(generalService.getUserEmail(), subject, messageUser);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return ResponseEntity.ok().body(messageUser);
		} else {
			messageUser = "lekhna baki xa";
			return new ResponseEntity<String>(messageUser, HttpStatus.BAD_REQUEST);
		}
	}

	// Extended Leave Request
	public ResponseEntity<String> extendedLeaveRequest(LeaveRequest leaveRequest, Integer id) {
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		if (leavereq.get().getExtendStatus().equals("Rejected")) {
			return new ResponseEntity<String>("Leave already Rejected", HttpStatus.BAD_REQUEST);
		}
		if (utilityService.convertToLocalDate(leavereq.get().getLeaveTill())
				.isAfter(utilityService.convertToLocalDate(leaveRequest.getExtendTill()))) {
			return new ResponseEntity<String>("Cannot extend Before Last Date", HttpStatus.BAD_REQUEST);
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = simpleDateFormat.format(new Date());

		if (utilityService.convertToLocalDate(leavereq.get().getLeaveTill())
				.isBefore(utilityService.convertToLocalDate(date))) {
			return new ResponseEntity<String>("Leave Date Already Expired", HttpStatus.BAD_REQUEST);
		}
		// SimpleDateFormat dateTill2 = new SimpleDateFormat("yyyy-MM-dd");
		String date1 = leavereq.get().getLeaveTill();
		String date2 = leaveRequest.getExtendTill();
		double day = utilityService.getNumberOfDay(utilityService.convertToLocalDate(date1),
				utilityService.convertToLocalDate(date2));
		double noOfday = day - 1;

		if (leavereq.get().getRequestStatus().equals("Pending")) {
			leavereq.get().setNoOfDays(leavereq.get().getNoOfDays() + noOfday);

			leavereq.get().setLeaveTill(leaveRequest.getExtendTill());
		} else {
			leavereq.get().setExtendDays(noOfday);
			leavereq.get().setExtendTill(leaveRequest.getExtendTill());
			leavereq.get().setExtendRemarks(leaveRequest.getExtendRemarks());
			leavereq.get().setExtendStatus("Pending");
			leavereq.get().setExtendedDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		}
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(
				leavereq.get().getLeaveType().getId(), leavereq.get().getEmployeePersonalInformation().getId());
		if (rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Annual")
				|| rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Sick")) {
			requestRepository.save(leavereq.get());
		} else if (rem.getRemainingLeave() >= day) {
			requestRepository.save(leavereq.get());
		} else {
			return new ResponseEntity<String>("Remaining day is " + rem.getRemainingLeave(), HttpStatus.BAD_REQUEST);
		}
		requestRepository.save(leavereq.get());

		return ResponseEntity.ok().body("Leave Extended successfully!");
	}

	public void updateExtendLeave(LeaveRequest leaveRequest, Integer id) {
		Optional<LeaveRequest> leavereq = requestRepository.findById(id);
		leavereq.get().setExtendStatus(leaveRequest.getExtendStatus());
		leavereq.get().setExtendResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		leavereq.get().setExtendResponseRemarks(leaveRequest.getExtendResponseRemarks());
		leavereq.get().setExtendResponseBy(String.valueOf(generalService.getUserId()));
		requestRepository.save(leavereq.get());
	}

	// called from attendance service
	public List<Object[]> getMonthlyLeave(int id, String year, String month) {
		String date = (year + "-" + month);
		return requestRepository.findEmployeeOnLeaveDate(id, date);
	}

	// Called from SecheularTask -> checkOnLeaveTo
	public void checkOnLeaveTo(/* String date1,String date2 */) {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		List<LeaveRequest> requestAll = requestRepository.findByOnLeaveStatusAndRequestStatus(true, "Approved");
		for (LeaveRequest var : requestAll) {
			String date1 = var.getLeaveTill();
			String date2 = objSDF.format(new Date());
			if (date1.compareTo(date2) >= 0) {
				var.setOnLeaveStatus(false);
				requestRepository.save(var);
			}
		}
	}

	// Called from SecheularTask -> checkOnLeaveFrom
	public void checkOnLeaveFrom(/* String date1,String date2 */) {
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		List<LeaveRequest> requestAll = requestRepository.findByOnLeaveStatusAndRequestStatus(false, "Approved");
		for (LeaveRequest var : requestAll) {
			String date1 = var.getLeaveFrom();
			String date2 = objSDF.format(new Date());
			if (date1.compareTo(date2) >= 0) {
				var.setOnLeaveStatus(true);
				requestRepository.save(var);
			}
		}
	}

	public ArrayList<HashMap<String, Object>> getFromDelegates(int id) {
		List<Delegate> dg = delegateRepository.findByDelegateToAndOnDelegateStatus(Integer.valueOf(id), true);
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Delegate var : dg) {
			data_list.addAll(findByRequestAndStatus(var.getEmployeePersonalInformation().getId(), "Pending"));
			data_list.addAll(findByRequestAndExtendStatus(var.getEmployeePersonalInformation().getId(), "Pending"));
			data_list.addAll(findByRequestAndCancelStatus(var.getEmployeePersonalInformation().getId(), "Pending"));
			data_list.addAll(halfDayLeaveService.findByRequestAndStatus(var.getEmployeePersonalInformation().getId(),
					"Pending"));
			data_list.addAll(
					hourlyLeaveService.findByRequestAndStatus(var.getEmployeePersonalInformation().getId(), "Pending"));
		}
		return data_list;
	}

	// find by id year month
	public Object getAllMonthlyLeaveDate(int id, String year, String month) {
		String date = (year + "-" + month);
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		List<Object> arrayWithMap = new ArrayList<Object>();
		data_list.addAll(getAllMonthlyLeaveDate(id, date));
		data_list.addAll(halfDayLeaveService.getAllMonthlyLeaveDate(id, date));
		data_list.addAll(hourlyLeaveService.getAllMonthlyLeaveDate(id, date));

		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("createdDate") && null != o1.get("createdDate")) {
					return o2.get("createdDate").toString().compareTo(o1.get("createdDate").toString());
				} else if (null != o1.get("createdDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}

	public Object getMonthlyLeaveAllEmp(String year, String month) {
		List<Integer> a = empRepository.getAllIds();
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		List<Object> arrayWithMap = new ArrayList<Object>();
		for (Integer id : a) {
			String date = (year + "-" + month);
			data_list.addAll(getAllMonthlyLeaveDate(id, date));
			data_list.addAll(halfDayLeaveService.getAllMonthlyLeaveDate(id, date));
			data_list.addAll(hourlyLeaveService.getAllMonthlyLeaveDate(id, date));
		}
		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("createdDate") && null != o1.get("createdDate")) {
					return o2.get("createdDate").toString().compareTo(o1.get("createdDate").toString());
				} else if (null != o1.get("createdDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}

	public Object getMonthlyLeaveByDeptm(String department, String year, String month) {
		String[] deptList = department.split("-");
		String date = (year + "-" + month);
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		List<Object> arrayWithMap = new ArrayList<Object>();
		for (String var : deptList) {
			List<Integer> list = empRepository.getIdsByDepartment(var);
			for (Integer id : list) {
				data_list.addAll(getAllMonthlyLeaveDate(id, date));
				data_list.addAll(halfDayLeaveService.getAllMonthlyLeaveDate(id, date));
				data_list.addAll(hourlyLeaveService.getAllMonthlyLeaveDate(id, date));
			}
		}

		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("createdDate") && null != o1.get("createdDate")) {
					return o2.get("createdDate").toString().compareTo(o1.get("createdDate").toString());
				} else if (null != o1.get("createdDate")) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		for (HashMap<String, Object> data : data_list) {
			arrayWithMap.add(data);
		}
		return arrayWithMap;
	}

	// called from getAllLeaveResponses function
	public Object getAllLeaveRequest(Integer id) {
		return getAllLeaveRequest(id);
	}

	public ArrayList<HashMap<String, Object>> getAllLeaveRequest(int id) {
		return getObjectData(requestRepository.findByRequestTo(id, "Approved", "Rejected", "Cancelled"));
	}

	public ArrayList<HashMap<String, Object>> findByRequestAndStatus(int id, String status) {
		return getObjectData(requestRepository.findByRequestAndStatus(id, status));
	}

	public ArrayList<HashMap<String, Object>> getAllMonthlyLeaveDate(int id, String date) {
		return getMyObjectData(requestRepository.getAllMonthlyLeaveDate(id, date));
	}

	public ArrayList<HashMap<String, Object>> findByRequestAndExtendStatus(int id, String status) {
		return getObjectData(requestRepository.findByRequestAndExtendStatus(id, status));
	}

	public ArrayList<HashMap<String, Object>> findByRequestAndCancelStatus(int id, String status) {
		return getObjectData(requestRepository.findByRequestAndCancelStatus(id, status));
	}

	public ArrayList<HashMap<String, Object>> findEmpById(int id) {
		return getMyObjectData(requestRepository.findEmpById(id));
	}

	public ArrayList<HashMap<String, Object>> getObjectData(List<Object[]> leaverequest) {
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Object[] recent : leaverequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("noOfDays", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveFrom", recent[5]);
			recentUserJson.put("LeaveTill", recent[6]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[7])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			String name = utilityService.mergeName(String.valueOf(recent[8]), String.valueOf(recent[9]),
					String.valueOf(recent[10]));
			recentUserJson.put("name", name);
			recentUserJson.put("requestStatus", recent[11]);
			String recommendBy = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				recommendBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				recommendBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", recommendBy);
			recentUserJson.put("recommendedDate", recent[13]);
			recentUserJson.put("createdDate", recent[14]);
			recentUserJson.put("createdTime", recent[15]);
			recentUserJson.put("extendStatus", recent[16]);
			recentUserJson.put("extendTill", recent[17]);
			recentUserJson.put("extendDays", recent[18]);
			recentUserJson.put("remarks", recent[19]);
			recentUserJson.put("cancelFrom", recent[20]);
			recentUserJson.put("cancelStatus", recent[21]);
			recentUserJson.put("cancelDate", recent[22]);
			recentUserJson.put("cancelRemarks", recent[23]);
			recentUserJson.put("extendedDate", recent[24]);
			recentUserJson.put("extendedResponseDate", recent[25]);
			String responseBy = "";
			if (String.valueOf(recent[26]).equals("") || String.valueOf(recent[26]).equals(null)
					|| String.valueOf(recent[26]).isEmpty()) {
				responseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[26])));
				responseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("responseBy", responseBy);
			recentUserJson.put("responseDate", recent[27]);
			recentUserJson.put("responseTime", recent[28]);
			recentUserJson.put("extendRemarks", recent[29]);
			recentUserJson.put("extendResponseRemarks", recent[30]);
			recentUserJson.put("department", recent[31]);
			recentUserJson.put("position", recent[32]);
			recentUserJson.put("cancelResponseDate", recent[33]);
			String cancelResponseBy = "";
			if (String.valueOf(recent[34]).equals("") || String.valueOf(recent[34]).equals(null)
					|| String.valueOf(recent[34]).isEmpty()) {
				cancelResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[34])));
				cancelResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("cancelResponseBy", cancelResponseBy);
			recentUserJson.put("cancelResponseRemarks", recent[35]);
			String extendResponseBy = "";
			if (String.valueOf(recent[36]).equals("") || String.valueOf(recent[36]).equals(null)
					|| String.valueOf(recent[36]).isEmpty()) {
				extendResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[36])));
				extendResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("extendResponseBy", extendResponseBy);
			if (String.valueOf(recent[21]).equals("") || String.valueOf(recent[21]).equals(null)
					|| String.valueOf(recent[21]).isEmpty()) {
				if (String.valueOf(recent[17]).equals("") || String.valueOf(recent[17]).equals(null)
						|| String.valueOf(recent[17]).isEmpty()) {
					recentUserJson.put("LeaveRequestType", "NormalLeave");
				} else {
					recentUserJson.put("LeaveRequestType", "ExtendAuthorization");
				}
			} else {
				recentUserJson.put("LeaveRequestType", "CancelAuthorization");
			}
			data_list.add(recentUserJson);
		}
		return data_list;
	}

	public ArrayList<HashMap<String, Object>> getMyObjectData(List<Object[]> leaverequest) {
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Object[] recent : leaverequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("noOfDays", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveFrom", recent[5]);
			recentUserJson.put("LeaveTill", recent[6]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[7])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			String name = utilityService.mergeName(String.valueOf(recent[8]), String.valueOf(recent[9]),
					String.valueOf(recent[10]));
			recentUserJson.put("name", name);
			recentUserJson.put("requestStatus", recent[11]);
			String recommendBy = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				recommendBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				recommendBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", recommendBy);
			recentUserJson.put("recommendedDate", recent[13]);
			recentUserJson.put("createdDate", recent[14]);
			recentUserJson.put("createdTime", recent[15]);
			recentUserJson.put("extendStatus", recent[16]);
			recentUserJson.put("extendTill", recent[17]);
			recentUserJson.put("extendDays", recent[18]);
			recentUserJson.put("remarks", recent[19]);
			recentUserJson.put("cancelFrom", recent[20]);
			recentUserJson.put("cancelStatus", recent[21]);
			recentUserJson.put("cancelDate", recent[22]);
			recentUserJson.put("cancelRemarks", recent[23]);
			recentUserJson.put("extendedDate", recent[24]);
			recentUserJson.put("extendedResponseDate", recent[25]);
			String responseBy = "";
			if (String.valueOf(recent[26]).equals("") || String.valueOf(recent[26]).equals(null)
					|| String.valueOf(recent[26]).isEmpty()) {
				responseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[26])));
				responseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("responseBy", responseBy);
			recentUserJson.put("responseDate", recent[27]);
			recentUserJson.put("responseTime", recent[28]);
			recentUserJson.put("extendRemarks", recent[29]);
			recentUserJson.put("extendResponseRemarks", recent[30]);
			recentUserJson.put("department", recent[31]);
			recentUserJson.put("position", recent[32]);
			recentUserJson.put("cancelResponseDate", recent[33]);
			String cancelResponseBy = "";
			if (String.valueOf(recent[34]).equals("") || String.valueOf(recent[34]).equals(null)
					|| String.valueOf(recent[34]).isEmpty()) {
				cancelResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[34])));
				cancelResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("cancelResponseBy", cancelResponseBy);
			recentUserJson.put("cancelResponseRemarks", recent[35]);
			String extendResponseBy = "";
			if (String.valueOf(recent[36]).equals("") || String.valueOf(recent[36]).equals(null)
					|| String.valueOf(recent[36]).isEmpty()) {
				extendResponseBy = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[36])));
				extendResponseBy = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("extendResponseBy", extendResponseBy);
			recentUserJson.put("LeaveRequestType", "NormalLeave");
			data_list.add(recentUserJson);
		}
		return data_list;
	}
}
