package com.snigdh.Erp.Service.MonthlyReportService;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Repository.AttendanceRepository.AttendanceRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.HalfDayLeaveService;
import com.snigdh.Erp.Service.LeaveService.HolidayService;
import com.snigdh.Erp.Service.LeaveService.HourlyLeaveService;
import com.snigdh.Erp.Service.LeaveService.LeaveRemainingService;
import com.snigdh.Erp.Service.LeaveService.LeaveRequestService;

@Service("AttendanceMonthlyReportService")
public class AttendanceMonthlyReportService {

	@Autowired
	EmployeePersonalInformationRepository empRepository;

	@Autowired
	HourlyLeaveService hourlyService;

	@Autowired
	HolidayService holidayService;

	@Autowired
	LeaveRequestService leaveService;

	@Autowired
	HalfDayLeaveService halfDayService;

	@Autowired
	LeaveRemainingService lrs;

	@Autowired
	private UtilityService utl;

	@Autowired
	private AttendanceRepository repository;

	/*
	 * public Object getMonthlyAttendance(int id, String year, String month) {
	 * String date = (year + "-" + month); LocalDate startDate =
	 * utl.convertToLocalDate(utl.getStartDate(Integer.valueOf(month),
	 * Integer.valueOf(year))); LocalDate endDate =
	 * utl.convertToLocalDate(utl.getEndDate(Integer.valueOf(month),
	 * Integer.valueOf(year))); List<String> dateAfterTrim = new ArrayList<>();
	 * List<String> checkInDate = repository.findEmployeeCheckInDate(id, date);
	 * List<Object> arrayWithMap = new ArrayList<Object>(); HashMap<String, Object>
	 * recentUser = new HashMap<String, Object>(); for (String var : checkInDate) {
	 * var = utl.splitDateFromTime(var); dateAfterTrim.add(var); }
	 * 
	 * for (String var : dateAfterTrim) { recentUser.put(var, "Present"); }
	 * 
	 * List<Object[]> leavedates = leaveService.getMonthlyLeave(id, year, month);
	 * for (Object[] var : leavedates) { String start = var[0].toString(); String
	 * end = var[1].toString(); while (!(start.equals(end))) {
	 * recentUser.put(start.toString(), "OnLeave"); start = utl.getNextDate(start);
	 * } recentUser.put(start.toString(), "OnLeave"); }
	 * 
	 * List<String> halfDayLeave = halfDayService.getMonthlyLeave(id, year, month);
	 * for (String var : halfDayLeave) { recentUser.put(var, "HalfDayLeave"); }
	 * 
	 * List<String> hourlyLeave = hourlyService.getMonthlyHoliday(id, year, month);
	 * for (String var : hourlyLeave) { recentUser.put(var, "HourlyLeave"); }
	 * 
	 * List<String> holidayDates = holidayService.getMonthlyHoliday(year, month);
	 * for (String var : holidayDates) { recentUser.put(var, "Holiday"); }
	 * 
	 * while (!startDate.isEqual(endDate)) { DayOfWeek dayOfWeek =
	 * startDate.getDayOfWeek(); if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek
	 * == DayOfWeek.FRIDAY)) { recentUser.put(String.valueOf(startDate), "Weekend");
	 * } String nextDate = utl.getNextDate(String.valueOf(startDate)); startDate =
	 * utl.convertToLocalDate(nextDate); }
	 * 
	 * Map<String, Object> map = new TreeMap<>(recentUser);
	 * 
	 * for (Map.Entry<String, Object> entry : map.entrySet()) {
	 * arrayWithMap.add(entry); } return arrayWithMap; }
	 */

	public Object getMonthlyAttendance(int id, String year, String month) {

		String currentDate = (new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

		String strDate = "";
		String date = (year + "-" + month);
		String checkIn = "";
		String checkOut = "";
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		LocalDate startDate = utl.convertToLocalDate(utl.getStartDate(Integer.valueOf(month), Integer.valueOf(year)));
		LocalDate endDate = utl.convertToLocalDate(utl.getEndDate(Integer.valueOf(month), Integer.valueOf(year)));
		String startD = startDate.toString();
		String endD = endDate.toString();

		List<Object[]> checkInDate = repository.findEmployeeCheckInDate(id, date);
		for (Object[] var : checkInDate) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			strDate = utl.splitDateFromTime(var[0].toString());
			String remarks = "";
			checkIn = utl.splitTimeFromDate(var[0].toString());
			if (var[1] == null) {
				checkOut = "";
			} else {
				checkOut = utl.splitTimeFromDate(var[1].toString());
			}

			recentUser.put("date", strDate);
			if (checkIn.isEmpty()) {
				recentUser.put("checkIn", "");
			} else {
				recentUser.put("checkIn", checkIn);
			}
			if (checkOut.isEmpty() || checkOut == null) {
				recentUser.put("checkOut", "");
			} else {
				recentUser.put("checkOut", checkOut);
			}
			recentUser.put("type", "Present");
			if (checkIn.compareTo("08:45:00") < 0) {
				remarks = "CheckIn : Early, ";
			} else if (checkIn.compareTo("09:15:00") > 0) {
				remarks = "CheckIn : Late, ";
			} else {
				remarks = "CheckIn : On Time, ";
			}
			if(checkOut.equals(null) || checkOut.isEmpty() || checkOut == "") {
				remarks = remarks + "";
			}else {
				if (checkOut.compareTo("16:45:00") < 0) {
					remarks = remarks + "CheckOut : Early, ";
				} else if (checkOut.compareTo("17:15:00") > 0) {
					remarks = remarks + "CheckOut : Late, ";
				} else {
					remarks = remarks + "CheckOut : On Time, ";
				}
			}			
			recentUser.put("remarks", remarks);
			data_list.add(recentUser);
			if (currentDate.equals(strDate)) {
				break;
			}
		}

		List<Object[]> leavedates = leaveService.getMonthlyLeave(id, year, month);
		if (leavedates.size() > 0) {
			for (Object[] var : leavedates) {
				HashMap<String, Object> recentUser = new HashMap<String, Object>();
				String start = var[0].toString();
				String end = var[1].toString();
				while (!(start.equals(end))) {
					recentUser.put("date", start.toString());
					recentUser.put("type", "On Leave");
					data_list.add(recentUser);
					start = utl.getNextDate(start);
					if (currentDate.equals(start.toString())) {
						break;
					}
				}
				recentUser.put("date", start.toString());
				recentUser.put("type", "On Leave");
				data_list.add(recentUser);
			}
		}

		List<Object[]> halfDayLeave = halfDayService.getMonthlyLeave(id, year, month);
		if (halfDayLeave.size() > 0) {
			for (Object[] var : halfDayLeave) {
				HashMap<String, Object> recentUser = new HashMap<String, Object>();
				recentUser.put("date", var[0]);
				recentUser.put("type", var[1] + " Half Leave");
				data_list.add(recentUser);
				if (currentDate.equals(var[0])) {
					break;
				}
			}
		}

		List<Object[]> hourlyLeave = hourlyService.getMonthlyHoliday(id, year, month);
		if (hourlyLeave.size() > 0) {
			for (Object[] var : hourlyLeave) {
				HashMap<String, Object> recentUser = new HashMap<String, Object>();
				recentUser.put("date", var[0]);
				recentUser.put("hourFrom", var[1]);
				recentUser.put("hourTill", var[2]);
				recentUser.put("type", "Hourly Leave");
				data_list.add(recentUser);
				if (currentDate.equals(var[0])) {
					break;
				}
			}
		}

		List<String> holidayDates = holidayService.getMonthlyHoliday(year, month);
		for (String var : holidayDates) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			recentUser.put("date", var);
			recentUser.put("type", "Holiday");
			data_list.add(recentUser);
			if (currentDate.equals(var)) {
				break;
			}
		}

		// myCollection.add(el);
		while (!startDate.isEqual(endDate)) {
			DayOfWeek dayOfWeek = startDate.getDayOfWeek();
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY)) {
				recentUser.put("date", String.valueOf(startDate));
				recentUser.put("type", "Weekend");
				data_list.add(recentUser);
			}
			String nextDate = utl.getNextDate(String.valueOf(startDate));
			startDate = utl.convertToLocalDate(nextDate);
			if (currentDate.equals(String.valueOf(startDate))) {
				break;
			}
		}

		while (!startD.equalsIgnoreCase(endD)) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			recentUser.put("date", startD);
			recentUser.put("type", "Absent");
			data_list.add(recentUser);
			startD = utl.getNextDate(startD);
			if (currentDate.equals(startD)) {
				break;
			}
		}
		HashMap<String, Object> recentUser = new HashMap<String, Object>();
		recentUser.put("date", startD);
		recentUser.put("type", "Absent");
		data_list.add(recentUser);

		data_list.sort(new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				if (null != o1.get("date") && null != o1.get("date")) {
					return o1.get("date").toString().compareTo(o2.get("date").toString());
				} else if (null != o1.get("date")) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		ArrayList<HashMap<String, Object>> uniqueList = new ArrayList();
		for (HashMap<String, Object> prod : data_list) {
			HashMap<String, Object> foundProd = null;
			for (HashMap<String, Object> uniqueProd : uniqueList) {
				if (uniqueProd.get("date").equals(prod.get("date"))) {
					foundProd = uniqueProd;
					break;
				}
			}
			if (foundProd == null) {
				uniqueList.add(prod);
			}
		}
		return uniqueList;
	}
	
	
	
	public Object getMonthlyAttendMultipleId(String ids, String year, String month) {
		String[] idList = ids.split("-");
		List<Object> result = new ArrayList<Object>();
		for (String e : idList) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			recentUser.put("id", e);
			recentUser.put("data", getMonthlyAttendance(Integer.valueOf(e), year, month));
			result.add(recentUser);
		}
		return result;
	}

	public Object getMonthlyAttendanceAllEmp(String year, String month) {
		List<Integer> a = empRepository.getAllIds();
		List<Object> arrayWithMap = new ArrayList<Object>();
		for (Integer e : a) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			recentUser.put("id", e);
			recentUser.put("data", getMonthlyAttendance(e, year, month));
			arrayWithMap.add(recentUser);
		}
		return arrayWithMap;
	}
	
	public Object getMonthlyAttendanceAllEm(String year, String month) {
		String name = "";
		List<Object[]> obj = empRepository.getAllEmployeeDatas();
		List<Object> arrayWithMap = new ArrayList<Object>();
		for (Object[] recent : obj) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			name = utl.mergeName(String.valueOf(recent[1]), String.valueOf(recent[2]), String.valueOf(recent[3]));	
			recentUser.put("id", recent[0]);
			recentUser.put("name", name.toString());
			recentUser.put("email",recent[4].toString() );
			recentUser.put("office", recent[5].toString());
			recentUser.put("department", recent[6].toString());
			recentUser.put("position", recent[7].toString());
			recentUser.put("data", getMonthlyAttendance(Integer.valueOf(recent[0].toString()), year, month));
			arrayWithMap.add(recentUser);
			}		
		return arrayWithMap;
	}

	public Object getMonthlyAttendanceByDept(String department, String year, String month) {
		List<Integer> list = empRepository.getIdsByDepartment(department);
		List<Object> arrayWithMap = new ArrayList<Object>();
		for (Integer id : list) {
			HashMap<String, Object> recentUser = new HashMap<String, Object>();
			recentUser.put("id", id);
			recentUser.put("data", getMonthlyAttendance(id, year, month));
			arrayWithMap.add(recentUser);
		}
		return arrayWithMap;
	}

	public Object getMonthlyAttendanceByDeptm(String department, String year, String month) {
		String[] deptList = department.split("-");
		List<Object> result = new ArrayList<Object>();
		for (String var : deptList) {
			List<Integer> list = empRepository.getIdsByDepartment(var);
			List<Object> arrayWithMap = new ArrayList<Object>();
			for (Integer id : list) {
				HashMap<String, Object> recentUser = new HashMap<String, Object>();
				recentUser.put("id", id);
				recentUser.put("data", getMonthlyAttendance(id, year, month));
				arrayWithMap.add(recentUser);
			}
			HashMap<String, Object> ru = new HashMap<String, Object>();
			ru.put("Department", var);
			System.out.println(var);
			ru.put("values", arrayWithMap);
			result.add(ru);
		}
		return result;
	}

}
