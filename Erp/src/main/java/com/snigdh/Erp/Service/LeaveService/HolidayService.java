package com.snigdh.Erp.Service.LeaveService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.LeaveModel.Holiday;
import com.snigdh.Erp.Repository.LeaveRepository.HolidayRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class HolidayService {

	@Autowired
	private UserService generalService;

	@Autowired
	HolidayRepository holidayRepository;
	
	@Autowired
	UtilityService utilityService;

	public String saveHoliday(Holiday holiday) {
		String msg = "";
		Optional<Holiday> holidayOptional = holidayRepository.findByDateFrom(holiday.getDateFrom());
		if(holidayOptional.isPresent()) {
			msg = "Holiday Already Exists.";
		}
		else {
			holiday.setNameOfHoliday(holiday.getNameOfHoliday());
			holiday.setCreatedAt(new Date());
			holiday.setUpdatedAt(new Date());
			holiday.setDateFrom(holiday.getDateFrom());
			holiday.setDateTill(holiday.getDateTill());
			holiday.setCreatedBy(generalService.getUserEmail());
			holiday.setUpdatedBy(generalService.getUserEmail());
			holidayRepository.save(holiday);
			msg = "Holiday Successfully Saved";
		}
		return msg;
	}

	public void updateHoliday(Holiday holiday, int id) {
		holidayRepository.findById(id).map(mapper -> {
			mapper.setNameOfHoliday(holiday.getNameOfHoliday());
			mapper.setDateFrom(holiday.getDateFrom());
			mapper.setDateTill(holiday.getDateTill());
			mapper.setUpdatedBy(generalService.getUserEmail());
			mapper.setUpdatedAt(new Date());
			return holidayRepository.save(mapper);
		});
	}

	public List<Holiday> getHoliday() {
		return holidayRepository.findAll();
	}

	public void deleteHoliday(int id) {
		Optional<Holiday> holiday = holidayRepository.findById(id);
		holidayRepository.delete(holiday.get());
	}
	
	// called from attendance service
	public List<String> getMonthlyHoliday(String year, String month) {
		String date = (year + "-" + month);
		return holidayRepository.getMonthlyHoliday(date,date);
	} 
	
	public List<String> getHolidayDates(){
		List<Holiday> holiday = holidayRepository.findAll();
		int loop;String dateC;
		List<String> holidayList = new ArrayList();
		for (Holiday var : holiday) {
			loop = 0;
			dateC = var.getDateFrom();
			while (loop == 0) {
				if (utilityService.convertToLocalDate(dateC).isBefore(utilityService.convertToLocalDate(var.getDateTill()))
						|| utilityService.convertToLocalDate(dateC).isEqual(utilityService.convertToLocalDate(var.getDateTill()))) {
					holidayList.add(dateC);
					if(utilityService.convertToLocalDate(dateC).isEqual(utilityService.convertToLocalDate(var.getDateTill()))) {
						loop = 1;
					}
				}
				dateC = utilityService.getNextDate(dateC);
			}
		}
		
		return holidayList;
	}
	
	// If given date exists in holiday or Weekend
		public boolean checkIfHolidayWeekend(String dates) {
			List<String> holidayList = getHolidayDates();
			for(String var: holidayList) {
				if(var.equals(dates)) {					
					return true;
				}
			}
			return false;
		}
}
