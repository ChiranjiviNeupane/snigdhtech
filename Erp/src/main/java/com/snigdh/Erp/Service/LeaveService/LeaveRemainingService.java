package com.snigdh.Erp.Service.LeaveService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HalfDayLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HourlyLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRemainingRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;

@Service
public class LeaveRemainingService {
	
	@Autowired
	private HourlyLeaveRepository hourlyLeaveRepository;
	
	@Autowired
	HourlyLeaveRepository hourlyLeaveRepo;

	@Autowired
	EmployeePersonalInformationRepository empRepo;

	@Autowired
	EmployeePersonalInformationRepository repo;

	@Autowired
	LeaveTypeRepository leaveTypeRepo;

	@Autowired
	LeaveRemainingRepository leaveRemainingRepository;

	@Autowired
	LeaveRequestRepository request;

	@Autowired
	HalfDayLeaveRepository halfDayLeaveRepository;

	public LeaveRemainingService(HourlyLeaveRepository hourlyLeaveRepo, EmployeePersonalInformationRepository empRepo,
			EmployeePersonalInformationRepository repo, LeaveTypeRepository leaveTypeRepo,
			LeaveRemainingRepository leaveRemainingRepository, LeaveRequestRepository request,
			HalfDayLeaveRepository halfDayLeaveRepository) {
		super();
		this.hourlyLeaveRepo = hourlyLeaveRepo;
		this.empRepo = empRepo;
		this.repo = repo;
		this.leaveTypeRepo = leaveTypeRepo;
		this.leaveRemainingRepository = leaveRemainingRepository;
		this.request = request;
		this.halfDayLeaveRepository = halfDayLeaveRepository;
	}
	
	public List<Object> findAllRemainingLeave() {
		List<Object> arrayWithMap = new ArrayList<Object>();
		List<EmployeePersonalInformation> emp = empRepo.findAll();
		for (EmployeePersonalInformation var : emp) {			
			/*List<LeaveRemaining> remaining = leaveRemainingRepository.findByEmployeePersonalInformation_Id(var.getId());
			if (remaining.isEmpty()) {
				saveRemainingLeave(var.getId());
			}*/
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("employeeId", var.getId());
			recentUserJson.put("Remaining Leave", findRemainingLeaveById(var.getId()));
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}

	public Object findRemainingLeaveById(Integer eid) {
		List<LeaveRemaining> remaining = leaveRemainingRepository.findByEmployeePersonalInformation_Id(eid);
		if (remaining.isEmpty()) {
			saveLeaveBalance(eid);
		}else {
			List<LeaveType> lt = leaveTypeRepo.findAll();
			for(LeaveType var : lt) {
				LeaveRemaining lr = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(var.getId(), eid);
				if(lr == null) {
					saveNewlyAddedLeave(var, eid);
				}
			}
		}
		List<Object[]> leaveRemaining =  leaveRemainingRepository.findEmployeeLeaveRemaining(eid);
		List<Object> arrayWithMap = new ArrayList<Object>();

		for (Object[] recent : leaveRemaining) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("leaveType", recent[0]);
			recentUserJson.put("totalLeave", recent[1]);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}

	public void saveRemainingLeave(Integer id) {
		List<LeaveType> lt = leaveTypeRepo.findAll();
		List<LeaveRemaining> remaining = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		Optional<EmployeePersonalInformation> optional = repo.findById(id);

		if (remaining.isEmpty()) {
			for (LeaveType var : lt) {
				LeaveRemaining leaveRemaining = new LeaveRemaining();
				leaveRemaining.setEmployeePersonalInformation(optional.get());
				leaveRemaining.setLeaveType(var);
				leaveRemaining.setRemainingLeave(Integer.valueOf(var.getTotalLeave()));
				leaveRemainingRepository.save(leaveRemaining);
			}
		}
	}
	
	public void saveLeaveBalance(Integer id) {
		List<LeaveType> lt = leaveTypeRepo.findAll();
		List<LeaveRemaining> remaining = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		Optional<EmployeePersonalInformation> optional = repo.findById(id);

		if (remaining.isEmpty()) {
			for (LeaveType var : lt) {
				LeaveRemaining leaveRemaining = new LeaveRemaining();
				leaveRemaining.setEmployeePersonalInformation(optional.get());
				leaveRemaining.setLeaveType(var);
				if(var.getTypeOfLeave().equalsIgnoreCase("Annual")) {
					leaveRemaining.setRemainingLeave(0);
				}else if(var.getTypeOfLeave().equalsIgnoreCase("Sick")) {
					leaveRemaining.setRemainingLeave(0);
				}
				else {
					leaveRemaining.setRemainingLeave(Integer.valueOf(var.getTotalLeave()));
				}			
				leaveRemainingRepository.save(leaveRemaining);
			}
		}
	}
	
	public void saveLieuLeave(Integer id, String type) {
		LeaveRemaining lr = leaveRemainingRepository.findByLeaveType_TypeOfLeaveAndEmployeePersonalInformation_Id(type, id);
		lr.setRemainingLeave(lr.getRemainingLeave() + 1.0);
		leaveRemainingRepository.save(lr);
	}

	public void cancelLeaveRem(Integer id) {
		Optional<LeaveRequest> leavereq = request.findById(id);
		int typeId = leavereq.get().getLeaveType().getId();
		double leaveTaken = leavereq.get().getNoOfDays();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
				empId);
		double remL = rem.getRemainingLeave();
		double t = remL + leaveTaken;
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}

	public void updateLeaveRem(Integer id) {
		Optional<LeaveRequest> leavereq = request.findById(id);
		double leaveTaken;
		int typeId = leavereq.get().getLeaveType().getId();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		leaveTaken = leavereq.get().getNoOfDays();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
				empId);
		double remL = rem.getRemainingLeave();
		double t = remL - leaveTaken;
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}

	public void updateHalfLeave(Integer id) {
		Optional<HalfDayLeave> leavereq = halfDayLeaveRepository.findById(id);
		int typeId = leavereq.get().getLeaveType().getId();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
				empId);
		double remL = rem.getRemainingLeave();
		double t = remL - 0.5;
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}
	
	public void updateHourlyLeave(Integer id) {
		Optional<HourlyLeave> leavereq = hourlyLeaveRepo.findById(id);
		int typeId = leavereq.get().getLeaveType().getId();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
				empId);
		double remL = rem.getRemainingLeave();
		double hours = leavereq.get().getNumberOfHours();
		double t = remL - (1.0/8.0*hours);
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}
	
	public void cancelLeave(Integer id,Double day) {
		Optional<LeaveRequest> leavereq = request.findById(id);
		int typeId = leavereq.get().getLeaveType().getId();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
				empId);
		double remL = rem.getRemainingLeave();
		double t = remL + day;
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}
	
	public void cancelHourlyLeave(Integer id) {
		Optional<HourlyLeave> leavereq = hourlyLeaveRepository.findById(id);
		int typeId = leavereq.get().getLeaveType().getId();
		double leaveHours = leavereq.get().getNumberOfHours();
		int empId = leavereq.get().getEmployeePersonalInformation().getId();
		LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId, empId);
		double remL = rem.getRemainingLeave();
		double t = remL + (1.0/8.0*leaveHours);
		rem.setRemainingLeave(t);
		leaveRemainingRepository.save(rem);
	}
	
	//called from Schedular Task -> increaseAnnualLeave()
	public void increaseAnnualLeave() {
		LeaveType lt = leaveTypeRepo.findByTypeOfLeave("Annual");
		double days = Double.valueOf(lt.getTotalLeave());
		List<LeaveRemaining> rem = leaveRemainingRepository.findByLeaveType_TypeOfLeave("annual");
		for (LeaveRemaining var : rem) {
			double add = days / 12.0 + var.getRemainingLeave();
			var.setRemainingLeave(add);
			leaveRemainingRepository.save(var);
		}
	}
	
	public void saveNewlyAddedLeave(LeaveType id,Integer eid) {
		Optional<EmployeePersonalInformation> emp = empRepo.findById(eid);
		LeaveRemaining leaveRemaining = new LeaveRemaining();
		leaveRemaining.setLeaveType(id);
		leaveRemaining.setEmployeePersonalInformation(emp.get());
		leaveRemaining.setRemainingLeave(Double.valueOf(id.getTotalLeave()));
		leaveRemainingRepository.save(leaveRemaining);
	}
	
	public void increaseLeaveAnually() {
		List<LeaveRemaining> leaveRemaining = leaveRemainingRepository.findAll();
		List<LeaveType> lt = leaveTypeRepo.findAll();
		for(LeaveRemaining var: leaveRemaining) {
			var.setCarryOver(var.getRemainingLeave());
			var.setRemainingLeave(var.getCarryOver() + Double.valueOf(var.getLeaveType().getTotalLeave()));
			if(var.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Annual")) {
				var.setRemainingLeave(var.getCarryOver());
			}
			if(var.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Sick")) {
				var.setRemainingLeave(var.getCarryOver());
			}
			if(var.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Study")) {
				var.setRemainingLeave(var.getCarryOver());
			}
			leaveRemainingRepository.save(var);
		}		
	}	
}
