package com.snigdh.Erp.Service.TravelService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.TravelModel.Travel;
import com.snigdh.Erp.Repository.EmployeeRepository.AuthorizationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.CompanyInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.TravelRepository.TravelRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class TravelService {
	@Autowired
	private TravelRepository travelRepo;

	@Autowired
	private EmployeePersonalInformationRepository empRepo;

	@Autowired
	private CompanyInformationRepository compRepo;

	@Autowired
	private AuthorizationRepository authRepo;

	@Autowired
	UtilityService utService;

	@Autowired
	UserService genService;

	@Autowired
	MailService mailService;

	@Autowired
	private UserService userService;

	public ResponseEntity<String> save(Travel travel) {
		List<Travel> myTravelrequests = travelRepo.findByEmployeePersonalInformation_Id(genService.getUserId());
		for (Travel myTravelrequest : myTravelrequests) {
			if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(), myTravelrequest.getDateTo(),
					travel.getDateFrom()) == true) {
				return new ResponseEntity<String>("Your travel request for first date; already exist on travel list",
						HttpStatus.BAD_REQUEST);
			} else if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(),
					myTravelrequest.getDateTo(), travel.getDateTo()) == true) {
				return new ResponseEntity<String>("Your travel request for last date; already exist on travel list",
						HttpStatus.BAD_REQUEST);
			}

			if (myTravelrequest.getExtendTill() != null) {
				if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(),
						myTravelrequest.getExtendTill(), travel.getDateFrom()) == true) {
					return new ResponseEntity<String>(
							"Your travel request for first date; already exist on travel extend list",
							HttpStatus.BAD_REQUEST);
				} else if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(),
						myTravelrequest.getExtendTill(), travel.getDateTo()) == true) {
					return new ResponseEntity<String>(
							"Your travel request for last date; already exist on travel extend list",
							HttpStatus.BAD_REQUEST);
				}
			}

		}
		String lineManager = compRepo.getLineManager(userService.getUserId());
		int lineManagerId = Integer.parseInt(lineManager);
		return empRepo.findById(userService.getUserId()).map(emp -> {
			travel.setEmployeePersonalInformation(emp);
			travel.setRequestToLineManager(lineManagerId);
			travel.setRequestToSecurityClearance(authRepo.getEmpId("Security Officer"));
			travel.setOverallStatus("Pending");
			travel.setLineManagerResponse("Pending");
			travel.setSecurityClearanceResponse("Pending");
			if (travel.getTravelAirTickets() != null || travel.getTravelVehicles() != null) {
				travel.setRequestToAdministrator(authRepo.getEmpId("Admin Officer"));
				travel.setAdministratorResponse("Pending");
			}
			if (travel.getTravelAdvances() != null) {
				travel.setRequestToFinance(authRepo.getEmpId("Finance Officer"));
				travel.setFinanceResponse("Pending");
			}
			travel.setOnTravel(false);
			travel.setRecommendStatus(false);
			travel.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
			try {
				mailService.sendMail(Integer.toString(authRepo.getEmpId("Security Officer")), "Travel Request",
						"New travel requested has been performed by "
								+ utService.mergeName(genService.getUserFirstName(), genService.getUserMiddleName(),
										genService.getSurName()));
			} catch (Exception e) {
				System.out.println(e);
			}
			travelRepo.save(travel);
			return ResponseEntity.ok().body("Travel request success!");
		}).orElseGet(() -> {
			return new ResponseEntity<String>("User not found", HttpStatus.BAD_REQUEST);
		});
	}

	public List<Travel> findAll() {
		try {

		} catch (Exception e) {
			System.out.println(e);
		}
		return travelRepo.findAll();
	}

	public void delete(Travel id) {
		travelRepo.delete(id);
	}

	public Travel retrieve(Integer id) {
		Optional<Travel> compInfo = travelRepo.findById(id);
		return compInfo.get();
	}

	public ResponseEntity<String> update(Travel travel, Integer id) {
		return travelRepo.findById(id).map(travelInfo -> {
			if (travel.getTypeAuth() != null && travel.getResponseAuth() != null) {
				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Security Authorization")) {
					travelInfo.setSecurityClearanceResponse(travel.getResponseAuth());
					travelInfo.setSecurityClearanceResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setSecurityClearanceResponseDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setSecurityClearanceResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setOverallStatus("Rejected");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been approved from security check by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
							if (travelInfo.getRequestToAdministrator() != 0) {
								mailService.sendMail(Integer.toString(travelInfo.getRequestToAdministrator()),
										"Travel Request",
										"New travel request by " + utService.mergeName(
												travelInfo.getEmployeePersonalInformation().getFirstName(),
												travelInfo.getEmployeePersonalInformation().getMiddleName(),
												travelInfo.getEmployeePersonalInformation().getSurname()));
							}
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Administrator Authorization")) {
					travelInfo.setAdministratorResponse(travel.getResponseAuth());
					travelInfo.setAdministratorResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setAdministratorResponseDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setAdministratorResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setOverallStatus("Rejected");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been approved from admin; by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
							if (travelInfo.getRequestToFinance() != 0) {
								mailService.sendMail(Integer.toString(travelInfo.getRequestToFinance()),
										"Travel Request",
										"New travel request by " + utService.mergeName(
												travelInfo.getEmployeePersonalInformation().getFirstName(),
												travelInfo.getEmployeePersonalInformation().getMiddleName(),
												travelInfo.getEmployeePersonalInformation().getSurname()));
							}
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Finance Authorization")) {
					travelInfo.setFinanceResponse(travel.getResponseAuth());
					travelInfo.setFinanceExtendResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setFinanceResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setFinanceResponseRemarks(travel.getResponseRemarks());
					travelInfo.setFinanceResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setOverallStatus("Rejected");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been approved from finance; by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
							if (travelInfo.getRequestToLineManager() != 0) {
								mailService.sendMail(Integer.toString(travelInfo.getRequestToLineManager()),
										"Travel Request",
										"New travel request by " + utService.mergeName(
												travelInfo.getEmployeePersonalInformation().getFirstName(),
												travelInfo.getEmployeePersonalInformation().getMiddleName(),
												travelInfo.getEmployeePersonalInformation().getSurname()));
							}
						} catch (Exception e) {
							System.out.println(e);
						}

					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Line Manager Authorization")) {
					travelInfo
							.setLineManagerResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setLineManagerResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setLineManagerResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Approved")) {
						travelInfo.setOverallStatus("Approved");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						travelInfo.setLineManagerResponse("Approved");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been approved and verified by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setOverallStatus("Rejected");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						travelInfo.setLineManagerResponse("Rejected");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel requested has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(),
						"Security Extend Authorization")) {
					travelInfo.setSecurityClearanceExtendResponse(travel.getResponseAuth());
					travelInfo.setSecurityClearanceExtendResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setSecurityClearanceExtendResponseDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setSecurityClearanceExtendResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setExtendOverallStatus("Rejected");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your extend travel request has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel extend request has been approved from security check by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
							if (travelInfo.getRequestToFinance() != 0) {
								mailService.sendMail(Integer.toString(travelInfo.getRequestToFinance()),
										"Travel Extend Request",
										"New travel extend request by " + utService.mergeName(
												travelInfo.getEmployeePersonalInformation().getFirstName(),
												travelInfo.getEmployeePersonalInformation().getMiddleName(),
												travelInfo.getEmployeePersonalInformation().getSurname()));
							}
						} catch (Exception e) {
							System.out.println(e);
						}

					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Finance Extend Authorization")) {
					travelInfo.setFinanceExtendResponse(travel.getResponseAuth());
					travelInfo.setFinanceExtendResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setFinanceExtendResponseDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setFinanceExtendResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setExtendOverallStatus("Rejected");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your extend travel request has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel extend request has been approved from finance check by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
							if (travelInfo.getRequestToLineManager() != 0) {
								mailService.sendMail(Integer.toString(travelInfo.getRequestToLineManager()),
										"Travel Extend Request",
										"New travel extend request by " + utService.mergeName(
												travelInfo.getEmployeePersonalInformation().getFirstName(),
												travelInfo.getEmployeePersonalInformation().getMiddleName(),
												travelInfo.getEmployeePersonalInformation().getSurname()));
							}

						} catch (Exception e) {
							System.out.println(e);
						}

					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Line Manager Extend Response")) {
					travelInfo.setLineManagerExtendResponse(travel.getResponseAuth());
					travelInfo.setLineManagerExtendResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setLineManagerExtendResponseDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setLineManagerExtendResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Approved")) {
						travelInfo.setExtendOverallStatus("Approved");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your travel extend request has been approved and verified by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setExtendOverallStatus("Rejected");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your extend travel request has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}

				if (utService.compare2StringsAlsoWithNullValues(travel.getTypeAuth(), "Recommended Authorization")) {
					travelInfo.setRecommendResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					travelInfo.setRecommendResponseBy(utService.mergeName(genService.getUserFirstName(),
							genService.getUserMiddleName(), genService.getSurName()));
					travelInfo.setRecommendResponseRemarks(travel.getResponseRemarks());
					if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Approved")) {
						travelInfo.setOverallStatus("Approved");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						travelInfo.setRecommendResponse("Approved");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your request has been approved and verified by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					} else if (utService.compare2StringsAlsoWithNullValues(travel.getResponseAuth(), "Rejected")) {
						travelInfo.setOverallStatus("Rejected");
						travelInfo.setOverallStatusChangedDate(
								new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						travelInfo.setRecommendResponse("Rejected");
						try {
							mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
									"Travel Request Notification",
									"Your request has been rejected by "
											+ utService.mergeName(genService.getUserFirstName(),
													genService.getUserMiddleName(), genService.getSurName()));
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
			}

			if (travel.isRecommendStatus() == true) {
				String ceoId = Integer.toString(authRepo.getEmpId("CEO"));
				travelInfo.setRecommendTo(ceoId);
				travelInfo.setRecommendBy(utService.mergeName(genService.getUserFirstName(),
						genService.getUserMiddleName(), genService.getSurName()));
				travelInfo.setRecommendDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				travelInfo.setRecommendStatus(true);
				travelInfo.setRecommendResponse("Pending");
				travelInfo.setLineManagerResponseRemarks(travel.getLineManagerResponseRemarks());
				try {
					mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
							"Travel Request Notification",
							"Your request has been recommended to " + utService.mergeName(genService.getUserFirstName(),
									genService.getUserMiddleName(), genService.getSurName()));
					mailService.sendMail(travelInfo.getRecommendTo(), "Travel Request recommendation",
							"New travel request has been recommended to you.");
				} catch (Exception e) {
					System.out.println(e);
				}
			}

			if (travel.getExtendTill() != null) {
				List<Travel> myTravelrequests = travelRepo.findByEmployeePersonalInformation_Id(genService.getUserId());
				for (Travel myTravelrequest : myTravelrequests) {
					if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(),
							myTravelrequest.getDateTo(), travel.getExtendTill()) == true) {
						return new ResponseEntity<String>(
								"Your travel request for extend date; already exist on travel list",
								HttpStatus.BAD_REQUEST);
					}

					if (myTravelrequest.getExtendTill() != null) {

						if (utService.checkIfDateLiesInBetween2dates(myTravelrequest.getDateFrom(),
								myTravelrequest.getExtendTill(), travel.getExtendTill()) == true) {
							return new ResponseEntity<String>(
									"Your travel request for extend date; already exist on travel extend list",
									HttpStatus.BAD_REQUEST);
						}
					}
				}
				travelInfo.setSecurityClearanceExtendResponse("Pending");
				travelInfo.setLineManagerExtendResponse("Pending");
				travelInfo.setExtendOverallStatus("Pending");
				travelInfo.setExtendTotalAdvance(travel.getExtendTotalAdvance());
				travelInfo.setExtendTill(travel.getExtendTill());
				travelInfo.setExtendRemarks(travel.getExtendRemarks());
				travelInfo.setExtendOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

				if (travel.getTravelAdvances() != null && travel.getTravelItenarys() != null) {
					travelInfo.addTravelAdvance(travel.getTravelAdvances());
					travelInfo.addTravelItenary(travel.getTravelItenarys());
					travelInfo.setFinanceExtendResponse("Pending");
				}

				else if (travel.getTravelAdvances() != null && travel.getTravelItenarys() == null) {
					travelInfo.addTravelAdvance(travel.getTravelAdvances());
					travelInfo.setFinanceExtendResponse("Pending");
				}

				else if (travel.getTravelAdvances() == null && travel.getTravelItenarys() != null) {
					travelInfo.addTravelItenary(travel.getTravelItenarys());
				}
			}

			if (travel.getOverallStatus() != null) {
				if (utService.compare2StringsAlsoWithNullValues(travel.getOverallStatus(), "Cancelled")) {
					if ((travelInfo.getSecurityClearanceResponse() != null && (utService
							.compare2StringsAlsoWithNullValues(travelInfo.getSecurityClearanceResponse(), "Approved")
							|| utService.compare2StringsAlsoWithNullValues(travelInfo.getSecurityClearanceResponse(),
									"Rejected")))
							|| (travelInfo.getAdministratorResponse() != null
									&& (utService.compare2StringsAlsoWithNullValues(
											travelInfo.getAdministratorResponse(), "Approved")
											|| utService.compare2StringsAlsoWithNullValues(
													travelInfo.getAdministratorResponse(), "Rejected")))
							|| (travelInfo.getFinanceResponse() != null && ((utService
									.compare2StringsAlsoWithNullValues(travelInfo.getFinanceResponse(), "Approved")
									|| utService.compare2StringsAlsoWithNullValues(travelInfo.getFinanceResponse(),
											"Rejected"))))
							|| (travelInfo.getLineManagerResponse() != null && ((utService
									.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(), "Approved")
									|| utService.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(),
											"Rejected"))))) {
						travelInfo.setCancelResponse("Pending");
						travelInfo.setCancelRemarks(travel.getCancelRemarks());
						travelInfo
								.setCancelResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						try {
							mailService.sendMail(Integer.toString(travelInfo.getRequestToLineManager()),
									"Travel Request Notification",
									utService.mergeName(genService.getUserFirstName(), genService.getUserMiddleName(),
											genService.getSurName())
											+ "has asked for cancellation of his travel request.");
						} catch (Exception e) {
							System.out.println(e);
						}

					}
					if (utService.compare2StringsAlsoWithNullValues(travelInfo.getSecurityClearanceResponse(),
							"Pending")) {
						if (travelInfo.getAdministratorResponse() == null && travelInfo.getFinanceResponse() == null
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(),
										"Pending")) {
							travelInfo.setOverallStatus("Cancelled");
							travelInfo.setCancelOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							travelInfo.setCancelRemarks(travel.getCancelRemarks());
							travelInfo.setOverallStatusChangedDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							try {
								mailService.sendMail(
										Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
										"Travel Request Notification", "Your request has been cancelled");
							} catch (Exception e) {
								System.out.println(e);
							}
						} else if (travelInfo.getAdministratorResponse() == null
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getFinanceResponse(),
										"Pending")
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(),
										"Pending")) {
							travelInfo.setOverallStatus("Cancelled");
							travelInfo.setCancelOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							travelInfo.setCancelRemarks(travel.getCancelRemarks());
							travelInfo.setOverallStatusChangedDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							try {
								mailService.sendMail(
										Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
										"Travel Request Notification", "Your request has been cancelled");
							} catch (Exception e) {
								System.out.println(e);
							}
						} else if (utService.compare2StringsAlsoWithNullValues(travelInfo.getAdministratorResponse(),
								"Pending") && travelInfo.getFinanceResponse() == null
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(),
										"Pending")) {
							travelInfo.setOverallStatus("Cancelled");
							travelInfo.setCancelRemarks(travel.getCancelRemarks());
							travelInfo.setCancelOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							travelInfo.setOverallStatusChangedDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							try {
								mailService.sendMail(
										Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
										"Travel Request Notification", "Your request has been cancelled");
							} catch (Exception e) {
								System.out.println(e);
							}
						} else if (utService.compare2StringsAlsoWithNullValues(travelInfo.getAdministratorResponse(),
								"Pending")
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getFinanceResponse(),
										"Pending")
								&& utService.compare2StringsAlsoWithNullValues(travelInfo.getLineManagerResponse(),
										"Pending")) {
							travelInfo.setOverallStatus("Cancelled");
							travelInfo.setCancelRemarks(travel.getCancelRemarks());
							travelInfo.setCancelOn(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							travelInfo.setOverallStatusChangedDate(
									new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							try {
								mailService.sendMail(
										Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
										"Travel Request Notification", "Your request has been cancelled");
							} catch (Exception e) {
								System.out.println(e);
							}
						}

					}

				}
			}

			if (travel.getCancelResponse() != null) {
				travelInfo.setCancelResponse(travel.getCancelResponse());
				travelInfo.setCancelResponseBy(utService.mergeName(genService.getUserFirstName(),
						genService.getUserMiddleName(), genService.getSurName()));
				travelInfo.setCancelResponseDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				travelInfo.setCancelResponseRemarks(travel.getCancelResponseRemarks());
				if (utService.compare2StringsAlsoWithNullValues(travel.getCancelResponse(), "Approved")) {
					travelInfo.setOverallStatus("Cancelled");
					travelInfo.setOverallStatusChangedDate(
							new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					try {
						mailService.sendMail(Integer.toString(travelInfo.getEmployeePersonalInformation().getId()),
								"Travel Request Notification",
								"Your request has been cancelled and approved by "
										+ utService.mergeName(genService.getUserFirstName(),
												genService.getUserMiddleName(), genService.getSurName()));
					} catch (Exception e) {
						System.out.println(e);
					}
				}
			}

			travelRepo.save(travelInfo);
			return ResponseEntity.ok().body("Success!");
		}).orElseGet(() -> {
			travel.setId(id);
			travelRepo.save(travel);
			return ResponseEntity.ok().body("Travel request added!");
		});
	}

	public Object onTravel() {
		List<Object[]> list = travelRepo.onTravelList();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] empDetail : list) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("name",
					utService.mergeName(empDetail[0].toString(), empDetail[1].toString(), empDetail[2].toString()));
			entityMap.put("position", empDetail[3]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}

	public void checkForOnTravel() {
		List<Travel> list = travelRepo.findByOnTravel(false);
		for (Travel traveList : list) {
			if (traveList.getExtendTill() == null
					&& utService.compare2StringsAlsoWithNullValues(traveList.getOverallStatus(), "Approved")) {
				if (utService.checkIfDateLiesInBetween2dates(traveList.getDateFrom(), traveList.getDateTo(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date()))) {
					traveList.setOnTravel(true);
					travelRepo.save(traveList);
				}
			} else if (utService.compare2StringsAlsoWithNullValues(traveList.getExtendOverallStatus(), "Approved")) {
				if (utService.checkIfDateLiesInBetween2dates(traveList.getDateFrom(), traveList.getExtendTill(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date()))) {
					traveList.setOnTravel(true);
					travelRepo.save(traveList);
				}
			}
		}
	}

	public void revokeOnTravelStatus() {
		List<Travel> list = travelRepo.findByOnTravel(true);
		for (Travel traveList : list) {
			if (traveList.getExtendTill() == null) {
				if (utService.checkIfDateLiesInBetween2dates(traveList.getDateFrom(), traveList.getDateTo(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date())) == false) {
					traveList.setOnTravel(false);
					travelRepo.save(traveList);
				}
			} else {
				if (utService.checkIfDateLiesInBetween2dates(traveList.getDateFrom(), traveList.getExtendTill(),
						new SimpleDateFormat("yyyy-MM-dd").format(new Date())) == false) {
					traveList.setOnTravel(false);
					travelRepo.save(traveList);
				}
			}
		}
	}

	public List<Object> findMyTravel() {
		List<Travel> travel = travelRepo.findByEmployeePersonalInformation_Id(genService.getUserId());
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			array.add(checkForRequestAndResponse(travelList, "My Requests"));
		}

		return array;
	}

	public List<Object> findMyResponses() {
		List<Travel> travel = travelRepo.findAll();
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			if ((travelList.getRequestToSecurityClearance() == genService.getUserId()) && ((utService
					.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(), "Approved"))
					|| (utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(),
							"Rejected")))) {
				if ((utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceExtendResponse(),
						"Approved"))
						|| (utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceExtendResponse(),
								"Rejected"))) {
					array.add(checkForRequestAndResponse(travelList, "Security Extend Response"));
				} else {
					array.add(checkForRequestAndResponse(travelList, "Security Response"));
				}

			}

			if ((travelList.getRequestToAdministrator() == genService.getUserId())
					&& ((utService.compare2StringsAlsoWithNullValues(travelList.getAdministratorResponse(), "Approved"))
							|| (utService.compare2StringsAlsoWithNullValues(travelList.getAdministratorResponse(),
									"Rejected")))) {
				array.add(checkForRequestAndResponse(travelList, "Administrator Response"));
			}

			if ((travelList.getRequestToFinance() == genService.getUserId()) && ((utService
					.compare2StringsAlsoWithNullValues(travelList.getFinanceResponse(), "Approved"))
					|| (utService.compare2StringsAlsoWithNullValues(travelList.getFinanceResponse(), "Rejected")))) {
				if ((utService.compare2StringsAlsoWithNullValues(travelList.getFinanceExtendResponse(), "Approved"))
						|| (utService.compare2StringsAlsoWithNullValues(travelList.getFinanceExtendResponse(),
								"Rejected"))) {
					array.add(checkForRequestAndResponse(travelList, "Finance Extend Response"));
				} else {
					array.add(checkForRequestAndResponse(travelList, "Finance Response"));
				}

			}

			if ((travelList.getRequestToLineManager() == genService.getUserId())
					&& ((utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerResponse(), "Approved"))
							|| (utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerResponse(),
									"Rejected")))) {
				if ((utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerExtendResponse(), "Approved"))
						|| (utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerExtendResponse(),
								"Rejected"))) {
					array.add(checkForRequestAndResponse(travelList, "Line Manager Extend Response"));
				} else {
					array.add(checkForRequestAndResponse(travelList, "Line Manager Response"));
				}

			}

			if ((travelList.getRequestToLineManager() == genService.getUserId())
					&& ((utService.compare2StringsAlsoWithNullValues(travelList.getCancelResponse(), "Approved"))
							|| (utService.compare2StringsAlsoWithNullValues(travelList.getAdministratorResponse(),
									"Rejected")))) {
				array.add(checkForRequestAndResponse(travelList, "Cancel Response"));
			}

			int recommendId = Integer.parseInt(travelList.getRecommendTo());
			if ((recommendId == genService.getUserId()) && ((utService
					.compare2StringsAlsoWithNullValues(travelList.getRecommendResponse(), "Approved"))
					|| (utService.compare2StringsAlsoWithNullValues(travelList.getRecommendResponse(), "Rejected")))) {
				array.add(checkForRequestAndResponse(travelList, "Recommended Response"));
			}

		}

		return array;
	}

	public List<Object> requestsToMe() {
		List<Travel> travel = travelRepo.findAll();
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			if (travelList.isRecommendStatus() == false) {
				if (travelList.getRequestToSecurityClearance() == genService.getUserId() && utService
						.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(), "Pending")) {
					array.add(checkForRequestAndResponse(travelList, "Security Authorization"));
				}

				if (travelList.getRequestToAdministrator() == genService.getUserId() && utService
						.compare2StringsAlsoWithNullValues(travelList.getAdministratorResponse(), "Pending")) {
					if (utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(),
							"Approved")) {
						array.add(checkForRequestAndResponse(travelList, "Administrator Authorization"));
					}
				}

				if (travelList.getRequestToFinance() == genService.getUserId()
						&& utService.compare2StringsAlsoWithNullValues(travelList.getFinanceResponse(), "Pending")) {
					if (travelList.getRequestToAdministrator() != 0 && utService
							.compare2StringsAlsoWithNullValues(travelList.getAdministratorResponse(), "Approved")) {
						array.add(checkForRequestAndResponse(travelList, "Finance Authorization"));
					} else if (travelList.getRequestToAdministrator() == 0) {
						if (utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(),
								"Approved")) {
							array.add(checkForRequestAndResponse(travelList, "Finance Authorization"));
						}
					}
				}

				if (travelList.getRequestToLineManager() == genService.getUserId()) {
					if (utService.compare2StringsAlsoWithNullValues(travelList.getCancelResponse(), "Pending")) {
						array.add(checkForRequestAndResponse(travelList, "Cancel Authorization"));
					} else if ((utService.compare2StringsAlsoWithNullValues(travelList.getCancelResponse(), null)
							|| (utService.compare2StringsAlsoWithNullValues(travelList.getCancelResponse(),
									"Rejected")))
							&& utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerResponse(),
									"Pending")) {
						if (travelList.getRequestToFinance() != 0 && utService
								.compare2StringsAlsoWithNullValues(travelList.getFinanceResponse(), "Approved")) {
							array.add(checkForRequestAndResponse(travelList, "Line Manager Authorization"));
						} else if (travelList.getRequestToFinance() == 0) {
							if (travelList.getRequestToAdministrator() != 0
									&& utService.compare2StringsAlsoWithNullValues(
											travelList.getAdministratorResponse(), "Approved")) {
								array.add(checkForRequestAndResponse(travelList, "Line Manager Authorization"));
							} else if (travelList.getRequestToAdministrator() == 0) {
								if (utService.compare2StringsAlsoWithNullValues(
										travelList.getSecurityClearanceResponse(), "Approved")) {
									array.add(checkForRequestAndResponse(travelList, "Line Manager Authorization"));
								}
							}
						}
					}

				}
				if (travelList.getExtendTill() != null) {
					if (travelList.getRequestToSecurityClearance() == genService.getUserId()
							&& utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceResponse(),
									"Approved")
							&& utService.compare2StringsAlsoWithNullValues(
									travelList.getSecurityClearanceExtendResponse(), "Pending")) {
						array.add(checkForRequestAndResponse(travelList, "Security Extend Authorization"));
					}

					if (travelList.getRequestToFinance() == genService.getUserId()
							&& utService.compare2StringsAlsoWithNullValues(travelList.getFinanceResponse(), "Approved")
							&& utService.compare2StringsAlsoWithNullValues(travelList.getFinanceExtendResponse(),
									"Pending")) {
						if (utService.compare2StringsAlsoWithNullValues(travelList.getSecurityClearanceExtendResponse(),
								"Approved")) {
							array.add(checkForRequestAndResponse(travelList, "Finance Extend Authorization"));
						}

					}

					if (travelList.getRequestToLineManager() == genService.getUserId()
							&& utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerResponse(),
									"Approved")
							&& utService.compare2StringsAlsoWithNullValues(travelList.getLineManagerExtendResponse(),
									"Pending")) {

						if (travelList.getRequestToFinance() != 0 && utService
								.compare2StringsAlsoWithNullValues(travelList.getFinanceExtendResponse(), "Approved")) {
							array.add(checkForRequestAndResponse(travelList, "Line Manager Extend Authorization"));
						} else if (travelList.getRequestToFinance() == 0) {
							if (utService.compare2StringsAlsoWithNullValues(
									travelList.getSecurityClearanceExtendResponse(), "Approved")) {
								array.add(checkForRequestAndResponse(travelList, "Line Manager Extend Authorization"));
							}
						}
					}

				}
			} else if (travelList.isRecommendStatus() == true) {
				int recommendId = Integer.parseInt(travelList.getRecommendTo());
				if (genService.getUserId() == recommendId
						&& utService.compare2StringsAlsoWithNullValues(travelList.getRecommendResponse(), "Pending")) {
					array.add(checkForRequestAndResponse(travelList, "Recommended Authorization"));
				}

			}
		}
		return array;
	}

	public HashMap<String, Object> checkForRequestAndResponse(Travel travel, String type) {
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("id", travel.getId());
		hash.put("purpose", travel.getPurpose());
		hash.put("placeOfTravel", travel.getPlaceOfTravel());
		hash.put("type", travel.getType());
		hash.put("team", travel.getTeam());
		hash.put("dateFrom", travel.getDateFrom());
		hash.put("dateTo", travel.getDateTo());
		hash.put("onTravel", travel.isOnTravel());
		hash.put("duration", travel.getDuration());
		hash.put("totalAdvanceCost", travel.getTotalAdvanceCost());
		hash.put("totalAdvanceCostInWords", travel.getTotalAdvanceCostInWords());
		hash.put("budgetCodeComment", travel.getBudgetCodeComment());
		hash.put("createdAt", travel.getCreatedAt());
		hash.put("overallStatus", travel.getOverallStatus());
		hash.put("overallStatusChangedDate", travel.getOverallStatusChangedDate());
		hash.put("requestToLineManager", travel.getRequestToLineManager());
		hash.put("requestToSecurityClearance", travel.getRequestToSecurityClearance());
		hash.put("requestToAdministrator", travel.getRequestToAdministrator());
		hash.put("requestToFinance", travel.getRequestToFinance());
		hash.put("lineManagerResponse", travel.getLineManagerResponse());
		hash.put("lineManagerResponseDate", travel.getLineManagerResponseDate());
		hash.put("lineManagerResponseBy", travel.getLineManagerResponseBy());
		hash.put("lineManagerResponseRemarks", travel.getLineManagerResponseRemarks());
		hash.put("administratorResponse", travel.getAdministratorResponse());
		hash.put("administratorResponseDate", travel.getAdministratorResponseDate());
		hash.put("administratorResponseBy", travel.getAdministratorResponseBy());
		hash.put("administratorResponseRemarks", travel.getAdministratorResponseRemarks());
		hash.put("financeResponse", travel.getFinanceResponse());
		hash.put("financeResponseDate", travel.getFinanceResponseDate());
		hash.put("financeResponseBy", travel.getFinanceResponseBy());
		hash.put("financeResponseRemarks", travel.getFinanceResponseRemarks());
		hash.put("securityClearanceResponse", travel.getSecurityClearanceResponse());
		hash.put("securityClearanceResponseDate", travel.getSecurityClearanceResponseDate());
		hash.put("securityClearanceResponseBy", travel.getSecurityClearanceResponseBy());
		hash.put("securityClearanceResponseRemarks", travel.getSecurityClearanceResponseRemarks());
		hash.put("extendTill", travel.getExtendTill());
		hash.put("extendOn", travel.getExtendOn());
		hash.put("lineManagerExtendResponse", travel.getLineManagerExtendResponse());
		hash.put("lineManagerExtendResponseDate", travel.getLineManagerExtendResponseDate());
		hash.put("lineManagerExtendResponseBy", travel.getLineManagerExtendResponseBy());
		hash.put("lineManagerExtendResponseRemarks", travel.getLineManagerExtendResponseRemarks());
		hash.put("financeExtendResponse", travel.getFinanceExtendResponse());
		hash.put("financeExtendResponseDate", travel.getFinanceExtendResponseDate());
		hash.put("financeExtendResponseBy", travel.getFinanceExtendResponseBy());
		hash.put("financeExtendResponseRemarks", travel.getFinanceExtendResponseRemarks());
		hash.put("securityClearanceExtendResponse", travel.getSecurityClearanceExtendResponse());
		hash.put("securityClearanceExtendResponseDate", travel.getSecurityClearanceExtendResponseDate());
		hash.put("securityClearanceExtendResponseBy", travel.getSecurityClearanceExtendResponseBy());
		hash.put("securityClearanceExtendResponseRemarks", travel.getSecurityClearanceExtendResponseRemarks());
		hash.put("recommendStatus", travel.isRecommendStatus());
		hash.put("recommendBy", travel.getRecommendBy());
		hash.put("recommendTo", travel.getRecommendTo());
		hash.put("recommendDate", travel.getRecommendDate());
		hash.put("recommendResponse", travel.getRecommendResponse());
		hash.put("recommendResponseBy", travel.getRecommendResponseBy());
		hash.put("recommendResponseDate", travel.getRecommendResponseDate());
		hash.put("recommendResponseRemarks", travel.getRecommendResponseRemarks());
		hash.put("cancelResponse", travel.getCancelResponse());
		hash.put("cancelOn", travel.getCancelOn());
		hash.put("cancelResponseBy", travel.getCancelResponseBy());
		hash.put("cancelResponseDate", travel.getCancelResponseDate());
		hash.put("cancelResponseRemarks", travel.getCancelResponseRemarks());
		hash.put("travelAdvances", travel.getTravelAdvances());
		hash.put("budgetCodeSamples", travel.getBudgetCodeSamples());
		hash.put("travelItenarys", travel.getTravelItenarys());
		hash.put("travelAirTickets", travel.getTravelAirTickets());
		hash.put("travelVehicles", travel.getTravelVehicles());
		hash.put("overallType", type);
		hash.put("nameOfTraveller",
				utService.mergeName(travel.getEmployeePersonalInformation().getFirstName(),
						travel.getEmployeePersonalInformation().getMiddleName(),
						travel.getEmployeePersonalInformation().getSurname()));
		hash.put("recommendBy", travel.getRecommendBy());
		hash.put("extendOverallStatus", travel.getExtendOverallStatus());
		hash.put("extendTotalAdvance", travel.getExtendTotalAdvance());
		return hash;
	}

	public List<Object> reportByDate(String from, String to) {
		List<Travel> travel = travelRepo.findListForReportWithDate(from, to);
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			array.add(checkForRequestAndResponse(travelList, "Date Filter"));
		}
		return array;
	}

	public List<Object> reportByDateAndEmp(String from, String to, EmployeePersonalInformation empId) {
		List<Travel> travel = travelRepo.findListForReportWithDateAndEmp(from, to, empId);
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			array.add(checkForRequestAndResponse(travelList, "Date and Employee Id Filter"));
		}
		return array;
	}

	public List<Object> reportByDateAndDepart(String from, String to, String depart) {
		List<Travel> travel = travelRepo.findListForReportWithDateAndDepart(from, to, depart);
		List<Object> array = new ArrayList<Object>();
		for (Travel travelList : travel) {
			System.out.println(travelList);
			array.add(checkForRequestAndResponse(travelList, "Date and Depart Filter"));
		}
		return array;
	}

}
