package com.snigdh.Erp.Service.OfficeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Office.CostCenter;
import com.snigdh.Erp.Repository.OfficeRepository.CostCenterRepository;

@Service
public class CostCenterService {
	
	@Autowired
	public CostCenterRepository costCenterRepo;
	
	public CostCenter save(CostCenter costCenter) {	
		return costCenterRepo.save(costCenter);
	}
	
	public CostCenter retrieve(Integer id) {
		Optional<CostCenter> compInfo = costCenterRepo.findById(id);
		return compInfo.get();
	}
	
	public List<CostCenter> findAll() {
		return costCenterRepo.findAll();
	}
	
	public CostCenter updateValue(CostCenter costCenter, Integer id) {
		return costCenterRepo.findById(id).map(costCenterKey -> {
			costCenterKey.setCode(costCenter.getCode());
			costCenterKey.setName(costCenter.getName());		
			return costCenterRepo.save(costCenterKey);
		})

				.orElseGet(() -> {
					costCenter.setId(id);
					return costCenterRepo.save(costCenter);
				});
	}
	
	public void deleteValue(CostCenter id) {
		costCenterRepo.delete(id);
	}

	public List<Object> findForTravelRequest() {
		List<Object[]> costCenterList = costCenterRepo.getListCostCenterDetails();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] costList : costCenterList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("id", costList[0]);
			entityMap.put("code", costList[1]);
			entityMap.put("name", costList[2]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}


}
