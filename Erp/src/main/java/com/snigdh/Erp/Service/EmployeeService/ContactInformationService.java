package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.ContactInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.ContactInformationRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class ContactInformationService {

	private ContactInformationRepository informationRepository;

	@Autowired
	UserService genService;
	
	@Autowired
	public ContactInformationService(ContactInformationRepository informationRepository) {
		super();
		this.informationRepository = informationRepository;
	}

	public List<ContactInformation> findAll() {
		return informationRepository.findAll();
	}

	public void save(ContactInformation contactInfoObj) {
		contactInfoObj.setUpdatedAt(new Date());
		contactInfoObj.setUpdatedBy(genService.getUserEmail());
		informationRepository.save(contactInfoObj);
	}

	public ContactInformation retrieve(Integer id) {
		Optional<ContactInformation> contactInfo = informationRepository.findByEmployeePersonalInformation_Id(id);
		return contactInfo.get();
	}

	public void delete(ContactInformation id) {
		informationRepository.delete(id);
	}

	public ContactInformation updateValue(ContactInformation contactInformation, Integer id) {
		return informationRepository.findByEmployeePersonalInformation_Id(id).map(contactInfo -> {
			contactInfo.setPhone(contactInformation.getPhone());
			contactInfo.setMobile(contactInformation.getMobile());
			contactInfo.setSecondaryEmail(contactInformation.getSecondaryEmail());
			contactInfo.setPoBox(contactInformation.getPoBox());

			contactInfo.setUpdatedAt(new Date());
			contactInfo.setUpdatedBy(genService.getUserEmail());
			

			return informationRepository.save(contactInfo);
		}).orElseGet(() -> {

			contactInformation.setId(id);
			return informationRepository.save(contactInformation);
		});

	}
}
