package com.snigdh.Erp.Service.LeaveService;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Repository.DelegateRepository.DelegateRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.HalfDayLeaveRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRemainingRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.NotificationService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class HalfDayLeaveService {

	@Autowired
	MailService mailService;

	@Autowired
	private UserService generalService;

	@Autowired
	private HolidayService holidayService;

	@Autowired
	DelegateRepository delegateRepository;

	@Autowired
	LeaveRemainingService leaveRemainingService;

	@Autowired
	LeaveRemainingRepository leaveRemainingRepository;

	@Autowired
	LeaveTypeRepository leaveTypeRepo;

	@Autowired
	UtilityService utilityService;

	@Autowired
	EmployeePersonalInformationRepository empRepository;

	@Autowired
	HalfDayLeaveRepository halfDayLeaveRepository;

	@Autowired
	LeaveRequestRepository leaveRequestRepository;

	@Autowired
	NotificationService notificationService;

	// Save Leave Request
	public ResponseEntity<String> saveLeaveRequest(HalfDayLeave halfLeaveRequest, Integer id) {
		int lineId = 0;
		int delegateStatus = 0;
		String lineManagerEmail = "";
		String lineManagerName = "";
		String delegateManagerEmail = "";
		String delegateName = "";
		String message2 = "";
		List<LeaveRemaining> remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		if (remId == null) {
			leaveRemainingService.saveRemainingLeave(id);
			remId = leaveRemainingRepository.findByEmployeePersonalInformation_Id(id);
		}

		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		List<String> holiday = holidayService.getHolidayDates();
		List<HalfDayLeave> halfLeaveRequestList = halfDayLeaveRepository
				.findByEmployeePersonalInformation_IdAndLeaveDate(id, halfLeaveRequest.getLeaveDate());
		String msg = "Request saved successfully";

		List<LeaveRequest> requestList = leaveRequestRepository.findByEmployeePersonalInformation_Id(id);
		for (LeaveRequest var : requestList) {
			if ((utilityService.convertToLocalDate(halfLeaveRequest.getLeaveDate())
					.isAfter(utilityService.convertToLocalDate(var.getLeaveFrom()))
					&& utilityService.convertToLocalDate(halfLeaveRequest.getLeaveDate())
							.isBefore(utilityService.convertToLocalDate(var.getLeaveTill())))
					|| utilityService.convertToLocalDate(halfLeaveRequest.getLeaveDate())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveFrom()))
					|| utilityService.convertToLocalDate(halfLeaveRequest.getLeaveDate())
							.isEqual(utilityService.convertToLocalDate(var.getLeaveTill()))) {
				return new ResponseEntity<String>("Leave already requested in given date", HttpStatus.BAD_REQUEST);
			}
		}
		LeaveType leaveType = leaveTypeRepo.findByTypeOfLeave(halfLeaveRequest.getLeaveType().getTypeOfLeave());
		String date1 = halfLeaveRequest.getLeaveDate();
		DayOfWeek dayOfWeek = utilityService.convertToLocalDate(date1).getDayOfWeek();
		if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY
				|| holiday.contains(halfLeaveRequest.getLeaveDate()))) {
			return new ResponseEntity<String>("Leave Cannot Be Taken On Weekends Or Holiday", HttpStatus.BAD_REQUEST);
		}

		if (halfLeaveRequestList.isEmpty()) {
			halfLeaveRequest.setEmployeePersonalInformation(employee.get());
			halfLeaveRequest.setLeaveType(leaveType);
			halfLeaveRequest.setLeaveDate(halfLeaveRequest.getLeaveDate());
			halfLeaveRequest.setLeaveReason(halfLeaveRequest.getLeaveReason());
			halfLeaveRequest.setHalfDayType(halfLeaveRequest.getHalfDayType());
			halfLeaveRequest.setResponseDate("");
			halfLeaveRequest.setRecommendedBy("");
			halfLeaveRequest.setCreatedTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
			halfLeaveRequest.setCreatedDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			// halfLeaveRequest.setCreatedAt(simpleDateFormat.format(new Date()));

			// get lineManagerId
			for (CompanyInformation var : employee.get().getCompanyInformations()) {
				Optional<EmployeePersonalInformation> emp = empRepository
						.findById(Integer.valueOf(var.getLineManager()));
				lineId = emp.get().getId();
				lineManagerEmail = String.valueOf(emp.get().getEmail());
				halfLeaveRequest.setRequestTo(lineId);
			}

			int typeId = leaveType.getId();
			LeaveRemaining rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId,
					id);

			Optional<Delegate> delegateOpt = delegateRepository
					.findByEmployeePersonalInformation_IdAndOnDelegateStatus(lineId, true);
			if (delegateOpt.isPresent()) {
				delegateStatus = 1;
				delegateManagerEmail = delegateOpt.get().getEmployeePersonalInformation().getEmail();
				delegateName = utilityService.mergeName(
						delegateOpt.get().getEmployeePersonalInformation().getFirstName(),
						delegateOpt.get().getEmployeePersonalInformation().getMiddleName(),
						delegateOpt.get().getEmployeePersonalInformation().getSurname());
			}
			if (rem == null) {
				leaveRemainingService.saveRemainingLeave(id);
				rem = leaveRemainingRepository.findByLeaveType_IdAndEmployeePersonalInformation_Id(typeId, id);
			}
			if (rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Annual")
					|| rem.getLeaveType().getTypeOfLeave().equalsIgnoreCase("Sick")) {
				halfDayLeaveRepository.save(halfLeaveRequest);
			} else if (rem.getRemainingLeave() >= 0.5) {
				halfDayLeaveRepository.save(halfLeaveRequest);
			} else {
				msg = "Remaining day is " + rem.getRemainingLeave();
				return new ResponseEntity<String>(msg, HttpStatus.BAD_REQUEST);
			}
			try {
				String subject = "Leave Request";
				if (delegateStatus == 1) {
					message2 = "Leave Request from "
							+ utilityService.mergeName(generalService.getUserFirstName(),
									generalService.getUserMiddleName(), generalService.getSurName())
							+ " requested to " + lineManagerName + " has been delegate to " + delegateName;
					mailService.sendMail(delegateManagerEmail, subject, message2);
				}
				String message = "Leave Request from "
						+ utilityService.mergeName(generalService.getUserFirstName(),
								generalService.getUserMiddleName(), generalService.getSurName())
						+ " has been requested to " + lineManagerName + ".";

				mailService.sendMail(lineManagerEmail, subject, message);
				mailService.sendMail(generalService.getUserEmail(), subject, message);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} else {
			return new ResponseEntity<String>("Leave already requested in given date", HttpStatus.BAD_REQUEST);
		}
		notificationService.saveHalfLeaveNotification(id, halfLeaveRequest.getLeaveDate());
		return ResponseEntity.ok().body("Leave Request successfully!");
	}

	// Updates Leave Request
	public ResponseEntity<String> updateLeaveRequest(HalfDayLeave leaverequest, Integer id) {
		Optional<HalfDayLeave> halfDayOptional = halfDayLeaveRepository.findById(id);
		halfDayOptional.get().setRequestStatus(leaverequest.getRequestStatus());
		halfDayOptional.get().setRemarks(leaverequest.getRemarks());
		Optional<EmployeePersonalInformation> employee = empRepository.findById(generalService.getUserId());
		halfDayOptional.get().setResponseTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
		halfDayOptional.get().setResponseDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		halfDayOptional.get().setResponseBy(utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(), employee.get().getSurname()));
		halfDayLeaveRepository.save(halfDayOptional.get());

		int lineId = 0;
		int delegateStatus = 0;

		String name = "";
		String delegateManagerEmail = "";
		String delegateName = "";
		String message2 = "";
		if ((leaverequest.getRequestStatus()).equals("Approved")) {
			leaveRemainingService.updateHalfLeave(id);
		}
		Optional<Delegate> delegateOpt = delegateRepository
				.findByEmployeePersonalInformation_IdAndOnDelegateStatus(lineId, true);
		if (delegateOpt.isPresent()) {
			delegateStatus = 1;
			delegateManagerEmail = delegateOpt.get().getEmployeePersonalInformation().getEmail();
			delegateName = utilityService.mergeName(delegateOpt.get().getEmployeePersonalInformation().getFirstName(),
					delegateOpt.get().getEmployeePersonalInformation().getMiddleName(),
					delegateOpt.get().getEmployeePersonalInformation().getSurname());
		}
		try {
			String subject = "Leave Request";
			if (delegateStatus == 1) {
				message2 = "Leave Request Updated";
				mailService.sendMail(delegateManagerEmail, subject, message2);
			}
			name = utilityService.mergeName(halfDayOptional.get().getEmployeePersonalInformation().getFirstName(),
					halfDayOptional.get().getEmployeePersonalInformation().getMiddleName(),
					halfDayOptional.get().getEmployeePersonalInformation().getSurname());
			String message = "Leave Request from " + name + " has been Updated by "
					+ utilityService.mergeName(generalService.getUserFirstName(), generalService.getUserMiddleName(),
							generalService.getSurName())
					+ ".";

			mailService.sendMail(halfDayOptional.get().getEmployeePersonalInformation().getEmail(), subject, message);
			mailService.sendMail(generalService.getUserEmail(), subject, message);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		halfDayLeaveRepository.save(halfDayOptional.get());
		return ResponseEntity.ok().body("Leave Updated successfully!");
	}

	public List<HalfDayLeave> getLeave(Integer id) {
		List<HalfDayLeave> hl = halfDayLeaveRepository.findByEmployeePersonalInformation_IdAndRequestTo(id, "Pending");
		return hl;
	}

	// called from attendance service
	public List<Object[]> getMonthlyLeave(int id, String year, String month) {
		String date = (year + "-" + month);
		return halfDayLeaveRepository.getMonthlyLeave(id, date);
	}

	// save notification for leave request
	public List<HalfDayLeave> saveHalfLeaveNotification(int id, String date) {
		return halfDayLeaveRepository.findByEmployeePersonalInformation_IdAndLeaveDate(id, date);
	}

	public Object findRequestById(Integer id) {
		List<Object[]> request = halfDayLeaveRepository.findRequestById(id);
		List<Object> arrayWithMap = new ArrayList<Object>();

		for (Object[] recent : request) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("leaveType", recent[1]);
			String name = utilityService.mergeName(String.valueOf(recent[2]), String.valueOf(recent[3]),
					String.valueOf(recent[4]));
			recentUserJson.put("name", name);
			recentUserJson.put("halfDayType", recent[5]);
			recentUserJson.put("leaveDate", recent[6]);
			recentUserJson.put("leaveReason", recent[7]);
			String RecomendName = "";
			if (String.valueOf(recent[8]).equals("") || String.valueOf(recent[8]).equals(null)
					|| String.valueOf(recent[8]).isEmpty()) {
				RecomendName = "";
			} else {
				Optional<EmployeePersonalInformation> employee = empRepository
						.findById(Integer.valueOf(String.valueOf(recent[8])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("onLeaveStatus", recent[9]);
			recentUserJson.put("requestStatus", recent[10]);
			recentUserJson.put("requestTo", recent[11]);
			recentUserJson.put("LeaveRequestType", "HalfDayLeave");
			recentUserJson.put("department", recent[11]);
			recentUserJson.put("position", recent[12]);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}

	// Get Method For Leave Request
	public ArrayList<HashMap<String, Object>> findByRequestAndStatus(int id, String status) {
		return getObjectData(halfDayLeaveRepository.findByRequestAndStatus(id, status));
	}

	// Get Method for leave request employee
	public ArrayList<HashMap<String, Object>> getMyHalfDayLeave(int id) {
		return getObjectData(halfDayLeaveRepository.findEmpById(id));
	}

	// find by id year month
	public ArrayList<HashMap<String, Object>> getAllMonthlyLeaveDate(int id, String date) {
		return getObjectData(halfDayLeaveRepository.getAllMonthlyLeaveDate(id, date));
	}

	public ArrayList<HashMap<String, Object>> getObjectData(List<Object[]> leaverequest) {
		ArrayList<HashMap<String, Object>> data_list = new ArrayList<HashMap<String, Object>>();
		for (Object[] recent : leaverequest) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("requestId", recent[0]);
			recentUserJson.put("onLeaveStatus", recent[1]);
			recentUserJson.put("leaveType", recent[2]);
			recentUserJson.put("halfDayType", recent[3]);
			recentUserJson.put("leaveReason", recent[4]);
			recentUserJson.put("leaveDate", recent[5]);
			Optional<EmployeePersonalInformation> employee = empRepository
					.findById(Integer.valueOf(String.valueOf(recent[6])));
			recentUserJson.put("requestTo", utilityService.mergeName(employee.get().getFirstName(),
					employee.get().getMiddleName(), employee.get().getSurname()));
			recentUserJson.put("requestStatus", recent[7]);
			recentUserJson.put("responseDate", recent[8]);
			recentUserJson.put("responseTime", recent[9]);
			recentUserJson.put("createdDate", recent[10]);
			recentUserJson.put("createdTime", recent[11]);
			String RecomendName = "";
			if (String.valueOf(recent[12]).equals("") || String.valueOf(recent[12]).equals(null)
					|| String.valueOf(recent[12]).isEmpty()) {
				RecomendName = "";
			} else {
				employee = empRepository.findById(Integer.valueOf(String.valueOf(recent[12])));
				RecomendName = utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(),
						employee.get().getSurname());
			}
			recentUserJson.put("recommendedBy", RecomendName);
			recentUserJson.put("remarks", recent[13]);
			recentUserJson.put("responseBy", recent[14]);
			String name = utilityService.mergeName(String.valueOf(recent[15]), String.valueOf(recent[16]),
					String.valueOf(recent[17]));
			recentUserJson.put("name", name);
			recentUserJson.put("department", recent[18]);
			recentUserJson.put("position", recent[19]);
			recentUserJson.put("LeaveRequestType", "HalfDayLeave");
			data_list.add(recentUserJson);
		}
		return data_list;
	}
}
