package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.WorkAddress;
import com.snigdh.Erp.Repository.EmployeeRepository.WorkAddressRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class WorkAddressService {

	private WorkAddressRepository workAddressRepository;
	
	@Autowired
	UserService genService;

	@Autowired
	public WorkAddressService(WorkAddressRepository workAddressRepository) {
		super();
		this.workAddressRepository = workAddressRepository;
	}

	public List<WorkAddress> findAll() {
		return workAddressRepository.findAll();
	}

	public void save(WorkAddress workAddress) {
		workAddress.setUpdatedAt(new Date());
		workAddress.setUpdatedBy(genService.getUserEmail());
		workAddressRepository.save(workAddress);
	}

	public WorkAddress retrieve(Integer id) {
		Optional<WorkAddress> workAddress = workAddressRepository.findByEmployeePersonalInformation_Id(id);
		return workAddress.get();
	}

	public void delete(WorkAddress id) {
		workAddressRepository.delete(id);
	}

	public WorkAddress update(WorkAddress workAddress, Integer id) {
		return workAddressRepository.findByEmployeePersonalInformation_Id(id)
				.map( workAddr -> {
					workAddr.setCompany(workAddress.getCompany());
					workAddr.setCountry(workAddress.getCountry());
					workAddr.setState(workAddress.getState());
					workAddr.setCity(workAddress.getCity());
					workAddr.setStreet(workAddress.getStreet());
					workAddr.setWardNo(workAddress.getWardNo());
					workAddr.setUpdatedAt(new Date());
					workAddr.setUpdatedBy(genService.getUserEmail());
					return workAddressRepository.save(workAddr);				
				})
				.orElseGet(() ->{
					workAddress.setId(id);
					return workAddressRepository.save(workAddress);
				});
	}

}
