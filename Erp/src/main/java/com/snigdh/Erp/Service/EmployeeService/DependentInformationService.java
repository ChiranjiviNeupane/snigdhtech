package com.snigdh.Erp.Service.EmployeeService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.DependentInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.DependentInformationRepository;
import com.snigdh.Erp.Service.UserService;

@Service
public class DependentInformationService {

	private DependentInformationRepository informationRepository;
	
	@Autowired
	UserService genService;

	@Autowired
	public DependentInformationService(DependentInformationRepository informationRepository) {
		super();
		this.informationRepository = informationRepository;
	}

	public List<DependentInformation> findAll() {
		return informationRepository.findAll();
	}

	public void save(DependentInformation dependentInformation) {
		dependentInformation.setUpdatedAt(new Date());
		dependentInformation.setUpdatedBy(genService.getUserEmail());
		informationRepository.save(dependentInformation);
	}

	public DependentInformation retrieve(Integer id) {
		Optional<DependentInformation> dependentInfo = informationRepository.findByEmployeePersonalInformation_Id(id);
		return dependentInfo.get();
	}

	public void delete(DependentInformation id) {
		informationRepository.delete(id);
	}

	public DependentInformation updateValue(DependentInformation dependentInformation, Integer id) {
		return informationRepository.findByEmployeePersonalInformation_Id(id).map(dependentInfo -> {
			dependentInfo.setFatherName(dependentInformation.getFatherName());
			dependentInfo.setMotherName(dependentInformation.getMotherName());
			dependentInfo.setSpouse(dependentInformation.getSpouse());
			dependentInfo.setChildren1(dependentInformation.getChildren1());
			dependentInfo.setChildren2(dependentInformation.getChildren2());
			dependentInfo.setChildren3(dependentInformation.getChildren3());

			dependentInfo.setUpdatedAt(new Date());
			dependentInfo.setUpdatedBy(genService.getUserEmail());
			
			return informationRepository.save(dependentInfo);
		}).orElseGet(() -> {
			dependentInformation.setId(id);
			return informationRepository.save(dependentInformation);

		});
	}

}
