package com.snigdh.Erp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailService {

	@Autowired
	private JavaMailSender javaMail;

	@Async
	public void sendMail(String sendTo, String subject, String message) throws MailException {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(sendTo);
		msg.setSubject(subject);
		msg.setText(message);
		javaMail.send(msg);
	}

	@Async
	public void sendMail(String sendTo, String sendTo1, String subject, String message) throws MailException {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(sendTo, sendTo1);
		msg.setSubject(subject);
		msg.setText(message);
		javaMail.send(msg);
	}

}
