package com.snigdh.Erp.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Repository.LeaveRepository.HolidayRepository;
import com.snigdh.Erp.Service.LeaveService.HolidayService;

@Service
public class UtilityService {

	@Autowired
	HolidayRepository holidayRepository;

	@Autowired
	HolidayService holidayService;

	public Integer convertBirthDateToAgeInYear(String testDateString, Date date) {
		if (testDateString.isEmpty()) {
			return 0;
		} else {
			LocalDate localDate = LocalDate.parse(testDateString);
			LocalDate conversionDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Period period = Period.between(localDate, conversionDate);
			int years = convertPeriodToYear(period);
			return years;
		}
	}

	// convert and return date on yyyyMMdd format when there are two string dates
	public String convertDateToNoOfDay(String testDate1, String testDate2) {
		LocalDate localDate1 = LocalDate.parse(testDate1);
		LocalDate localDate2 = LocalDate.parse(testDate2);
		Period period = Period.between(localDate1, localDate2);
		String numberOfDay = convertPeriodToYearMonthAndDay(period);
		return numberOfDay;
	}

	// convert and return date on yyyyMMdd when there are two dates of
	// java.util.date
	public String convertDateToNoOfDay(Date date, Date date2) {
		LocalDate localDate1 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate localDate2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Period period = Period.between(localDate1, localDate2);
		String numberOfDay = convertPeriodToYearMonthAndDay(period);
		return numberOfDay;
	}

	// Only Year
	public Integer convertPeriodToYear(Period p) {
		return p.getYears();
	}

	// Year and Month
	public String convertPeriodToYearAndMonth(Period p) {
		return (p.getYears() + " Years " + p.getMonths() + " Months ");
	}

	// Year Month and Day
	public String convertPeriodToYearMonthAndDay(Period p) {
		return (p.getYears() + " Years " + p.getMonths() + " Months " + p.getDays() + " Days ");
	}

	// Convert string to date
	public LocalDate convertToLocalDate(String dateStr) {
		String DATE_FORMAT_INPUT = "yyyy-MM-dd";
		return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(DATE_FORMAT_INPUT));
	}

	public boolean checkIfDateLiesInBetween2dates(String firstDate, String secondDate, String toBeComparedDate) {
		LocalDate previousFrom = convertToLocalDate(firstDate);
		LocalDate previousTo = convertToLocalDate(secondDate);
		LocalDate currentFrom = convertToLocalDate(toBeComparedDate);
		return (previousFrom.compareTo(currentFrom) * currentFrom.compareTo(previousTo) >= 0);
	}

	// If given date exists in holiday or Weekend
	public boolean checkIfHolidayWeekend(String date) {
		DayOfWeek dayOfWeek = convertToLocalDate(date).getDayOfWeek();
		if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY)) {// excludes friday and saturday
			return true;
		}
		if (holidayService.checkIfHolidayWeekend(date) == true) {
			return true;
		}
		return false;
	}

	// Difference between two dates
	public Double getNumberOfDay(LocalDate startDate, LocalDate endDate) {
		LocalDate d1 = startDate;
		int noOfHoliday = 0;
		int count = 0;
		noOfHoliday = countHoliday(startDate, endDate);
		while (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
			DayOfWeek dayOfWeek = startDate.getDayOfWeek();
			if ((dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.FRIDAY)) {// excludes friday and saturday
				count++;
			}
			startDate = startDate.plusDays(1);
		}
		return (double) d1.until(endDate, ChronoUnit.DAYS) + 1 - count - noOfHoliday;
	}

	// count the days between two date where first is java.util.date and second is
	// string
	public Long daysBetweenDates(Date date, String testDateString) {
		LocalDate localDate = LocalDate.parse(testDateString);
		LocalDate conversionDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		long dateDifference = ChronoUnit.DAYS.between(localDate, conversionDate);
		return dateDifference;
	}

	/*
	 * // count holiday private int countHoliday(LocalDate startDate, LocalDate
	 * endDate) { List<Holiday> holiday = holidayRepository.findAll(); int count =
	 * 0; for (Holiday var : holiday) { LocalDate s =
	 * convertToLocalDate(var.getDate()); if (s.compareTo(startDate) >= 0 &&
	 * s.compareTo(endDate) <= 0) { count++; } } return count; }
	 */

	private int countHoliday(LocalDate startDate, LocalDate endDate) {
		List<String> holidayList = holidayService.getHolidayDates();
		int count = 0;
		for (String var : holidayList) {
			LocalDate s = convertToLocalDate(var);
			if (s.compareTo(startDate) >= 0 && s.compareTo(endDate) <= 0) {
				count++;
			}
		}
		return count;
	}

	// merging first middle, and last name in one
	public String mergeName(String firstName, String middleName, String lastName) {
		String beforeRemovingSpaces = firstName + " " + middleName + " " + lastName.toString();
		String afterRemovingSpaces = beforeRemovingSpaces.trim().replaceAll(" +", " ");
		return afterRemovingSpaces;
	}

	// find next day date
	public String getNextDate(String currentDate) {

		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = objSDF.parse(currentDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return objSDF.format(calendar.getTime());
	}

	// difference betweem two time in hours
	public double differenceInTime(String time1, String time2) {
		Date date1 = null, date2 = null;
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		try {
			date1 = format.parse(time1);
			date2 = format.parse(time2);
		} catch (ParseException e) {
			System.out.println(e);
		}
		double difference = date2.getTime() - date1.getTime(); 
		double diffHours = difference / (60 * 60 * 1000) % 24;
		return diffHours;
	}

	// get Start of month
	public String getStartDate(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		// passing month-1 because 0-->jan, 1-->feb... 11-->dec
		calendar.set(year, month - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
		Date date = calendar.getTime();
		DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
		return DATE_FORMAT.format(date);
	}

	// get End of month
	public String getEndDate(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		// passing month-1 because 0-->jan, 1-->feb... 11-->dec
		calendar.set(year, month - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date date = calendar.getTime();
		DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
		return DATE_FORMAT.format(date);
	}

	// Split date from time
	public String splitDateFromTime(String value) {
		DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		Date d = null;
		try {
			DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			d = f.parse(value);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date.format(d);
	}

	// Split time from date
	public String splitTimeFromDate(String value) {
		DateFormat time = new SimpleDateFormat("HH:mm:ss");
		Date d = null;
		try {
			DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			d = f.parse(value);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time.format(d);
	}

	public boolean compare2StringsAlsoWithNullValues(String str1, String str2) {
		return (str1 == null ? str2 == null : str1.equals(str2));
	}

}
