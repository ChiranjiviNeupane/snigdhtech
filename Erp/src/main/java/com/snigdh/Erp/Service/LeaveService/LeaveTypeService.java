package com.snigdh.Erp.Service.LeaveService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;

@Service
public class LeaveTypeService {

	@Autowired
	LeaveTypeRepository leaveTypeRepository;

	public String saveLeaveType(LeaveType leaveType) {
		String msg = "";
		LeaveType leaveTypeObj = leaveTypeRepository.findByTypeOfLeave(leaveType.getTypeOfLeave());
		if(leaveTypeObj == null) {
			leaveTypeRepository.save(leaveType);
			msg = "New Leave Type Added.";
		}else {
			msg = "Leave Type Already Exists.";
		}
		return msg;
	}
	
	public Optional<LeaveType> findById(Integer id) {
		return leaveTypeRepository.findById(id);
	}
	

	public void updateLeaveType(LeaveType leaveType, Integer id) {
		Optional<LeaveType> leaveTypeObj = leaveTypeRepository.findById(id);
		leaveTypeObj.get().setTypeOfLeave(leaveType.getTypeOfLeave());
		leaveTypeObj.get().setTotalLeave(leaveType.getTotalLeave());
		leaveTypeRepository.save(leaveTypeObj.get());
	}

	public void deleteLeaveType(LeaveType id) {
		leaveTypeRepository.delete(id);
	}

	public LeaveType findByleaveType(LeaveType leaveType) {
		return (leaveTypeRepository.findByTypeOfLeave(leaveType.getTypeOfLeave()));
	}

	public List<LeaveType> findall() {
		return leaveTypeRepository.findAll();
	}

}
