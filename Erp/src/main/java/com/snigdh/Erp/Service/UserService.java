package com.snigdh.Erp.Service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.config.services.jwt.UserPrinciple;

@Service
public class UserService {

	public String getUserEmail() {
		UserPrinciple details = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return details.getEmail();
	}

	public String getUserFirstName() {
		UserPrinciple details = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return details.getFirstName();
	}

	public String getUserMiddleName() {
		UserPrinciple details = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return details.getMiddleName();
	}

	public String getSurName() {
		UserPrinciple details = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return details.getLastName();
	}

	public int getUserId() {
		UserPrinciple detail = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return detail.getId();
	}

	public Collection<? extends GrantedAuthority> getUserRole() {
		UserPrinciple detail = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return detail.getAuthorities();
	}
}
