package com.snigdh.Erp.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.Notification.Notification;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.NotificationRepository.NotificationRepository;
import com.snigdh.Erp.Service.LeaveService.HalfDayLeaveService;
import com.snigdh.Erp.Service.LeaveService.HourlyLeaveService;

@Service
public class NotificationService {
	
	@Autowired
	UtilityService utilityService;
	
	@Autowired
	LeaveRequestRepository leaveRequestRepository;
	
	@Autowired
	HalfDayLeaveService halfDayLeaveService;
	
	@Autowired
	EmployeePersonalInformationRepository empRepository;
	
	@Autowired
	NotificationRepository notificationRepository;
	
	@Autowired
	HourlyLeaveService hourlyLeaveService;
	
	public void saveLeaveNotification(int id,String leaveFrom) {
		List<LeaveRequest> leaveRequest = leaveRequestRepository.findByEmployeePersonalInformation_IdAndLeaveFrom(id, leaveFrom);
		Optional<EmployeePersonalInformation> optEmp = empRepository.findById(id);
		Notification notification = new Notification();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for(LeaveRequest var: leaveRequest) {
			notification.setEmployeePersonalInformation(optEmp.get());
			notification.setOnNotificationStatus(true);
			notification.setLeaveRequest(var);
			notification.setCreatedAt(utilityService.convertToLocalDate(simpleDateFormat.format(new Date()))); 
			notificationRepository.save(notification);
		}
	}
	
	public void saveHalfLeaveNotification(int id,String date) {
		List<HalfDayLeave> leaveRequest = halfDayLeaveService.saveHalfLeaveNotification(id, date);
		Optional<EmployeePersonalInformation> optEmp = empRepository.findById(id);
		Notification notification = new Notification();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for(HalfDayLeave var: leaveRequest) {
			notification.setEmployeePersonalInformation(optEmp.get());
			notification.setOnNotificationStatus(true);
			notification.setHalfDayLeave(var);
			notification.setCreatedAt(utilityService.convertToLocalDate(simpleDateFormat.format(new Date())));
			notificationRepository.save(notification);
		}
	}
	
	public void saveHourlyLeaveNotification(int id,String date) {
		List<HourlyLeave> leaveRequest = hourlyLeaveService.saveHourlyLeaveNotification(id, date);
		Optional<EmployeePersonalInformation> optEmp = empRepository.findById(id);
		Notification notification = new Notification();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for(HourlyLeave var: leaveRequest) {
			notification.setEmployeePersonalInformation(optEmp.get());
			notification.setOnNotificationStatus(true);
			notification.setHourlyLeave(var);
			notification.setCreatedAt(utilityService.convertToLocalDate(simpleDateFormat.format(new Date())));
			notificationRepository.save(notification);
		}
	}
	
	public void getNotification() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = simpleDateFormat.format(new Date());
		LocalDate endDate = LocalDate.now().minusDays(10);
		List<Object[]> notificationList = notificationRepository.findNotification(endDate);
		for(Object var : notificationList) {
			System.out.println(var);
		}
	}

}
