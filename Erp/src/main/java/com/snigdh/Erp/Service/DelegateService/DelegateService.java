package com.snigdh.Erp.Service.DelegateService;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.DelegateRepository.DelegateRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service("DelegateService")
public class DelegateService {
	
	@Autowired
	MailService mailService;
	
	@Autowired
	DelegateRepository delegateRepository;
	
	@Autowired
	EmployeePersonalInformationRepository empRepository;
	
	@Autowired
	UtilityService utilityService;
	
	@Autowired
	private UserService generalService;
	
	public String saveDelegation(Delegate delegation) {
		int id = generalService.getUserId();
		Optional<EmployeePersonalInformation> employee = empRepository.findById(id);
		LocalDate date1 = utilityService.convertToLocalDate(delegation.getDelegateFrom());
		LocalDate date2,date3 = null;
		delegation.setEmployeePersonalInformation(employee.get());
		delegation.setDelegateFrom(delegation.getDelegateFrom());
		delegation.setDelegateTill(delegation.getDelegateTill());
		delegation.setDelegateTo(delegation.getDelegateTo());
		List<Delegate> delegate = delegateRepository.findByEmployeePersonalInformation_Id(id);
		for(Delegate obj : delegate) {
			date2 = utilityService.convertToLocalDate(obj.getDelegateFrom());
			date3 = utilityService.convertToLocalDate(obj.getDelegateTill());
			if((date1.equals(date2) || date1.isAfter(date2)) && (date1.isEqual(date3) || date1.isBefore(date3))) {
				return "Delegate Already Exists On The Given Date";
			}
		}
		Optional<EmployeePersonalInformation> delegateTo = empRepository.findById(delegation.getDelegateTo());
		delegateRepository.save(delegation);
		mailService.sendMail(employee.get().getEmail(), "Delegate", "You have delegated to "+delegateTo.get().getEmail());
		mailService.sendMail(delegateTo.get().getEmail(), "Delegate", "You have been delegated by "+employee.get().getEmail());
		return "Delegate Successful";
	}
	
	public Object getDelegateToOther() {
		int id = generalService.getUserId();
		List<Object> arrayWithMap = new ArrayList<Object>();
		List<Delegate> delegate = delegateRepository.findByEmployeePersonalInformation_Id(id);
		for(Delegate obj : delegate) {
			HashMap<String, String> recentUserJson = new HashMap<String, String>();
			recentUserJson.put("delegateId", String.valueOf(obj.getId()));
			recentUserJson.put("delegateFrom", obj.getDelegateFrom());
			recentUserJson.put("delegateTill", obj.getDelegateTill());
			Optional<EmployeePersonalInformation> employee = empRepository.findById(obj.getDelegateTo());
			String name = utilityService.mergeName(employee.get().getFirstName(),employee.get().getMiddleName(),employee.get().getSurname());
			recentUserJson.put("delegatedTo", name);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}
	
	public Object getDelegateByOther() {
		int id = generalService.getUserId();
		List<Object> arrayWithMap = new ArrayList<Object>();
		List<Delegate> delegate = delegateRepository.findByDelegateToAndOnDelegateStatus(id, true);
		for(Delegate obj : delegate) {			
			HashMap<String, String> recentUserJson = new HashMap<String, String>();
			recentUserJson.put("delegateId", String.valueOf(obj.getId()));
			recentUserJson.put("delegateFrom", obj.getDelegateFrom());
			recentUserJson.put("delegateTill", obj.getDelegateTill());
			String name = utilityService.mergeName(obj.getEmployeePersonalInformation().getFirstName(),obj.getEmployeePersonalInformation().getMiddleName(),obj.getEmployeePersonalInformation().getSurname());
			recentUserJson.put("delegatedBy", name);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;
	}
	
	public String CancelDelegate(Integer id) {
		Optional<Delegate> delegate = delegateRepository.findById(id);
		String currentDate = (new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		String delegateTill = delegate.get().getDelegateTill();
	
		if(utilityService.convertToLocalDate(currentDate).isAfter(utilityService.convertToLocalDate(delegateTill))) {
			return "Cannot Cancelled After Delegation Date";
		}
		delegate.get().setRequestStatus("Cancelled");
		delegateRepository.save(delegate.get());
		return "Delegate Cancelled Successfully";
	}
	
	public String extendDelegate(Delegate delegate, Integer id) {
		Optional<Delegate> delegateOptional = delegateRepository.findById(id);
		String currentDate = (new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		String delegateTill = delegateOptional.get().getDelegateTill();
		if(utilityService.convertToLocalDate(currentDate).isAfter(utilityService.convertToLocalDate(delegateTill))) {
			return "Cannot Extend After Delegation Date Is Over";
		}
		if(utilityService.convertToLocalDate(delegate.getDelegateTill()).isAfter(utilityService.convertToLocalDate(delegateTill))) {
			delegateOptional.get().setDelegateTill(delegate.getDelegateTill());
			delegateRepository.save(delegateOptional.get());
			return "Delegate Extended Successfully";
		}else {
			return "Extended Date Is Before Delegate Date";
		}		
	}
	
	public void checkDelegateStatusFrom() {
		String currentDate = (new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		List<Delegate> delegates = delegateRepository.findByOnDelegateStatusAndRequestStatus(false, "Approved");
		for(Delegate var : delegates) {
			String date1 = var.getDelegateFrom();
			if (date1.compareTo(currentDate) >= 0) {
				var.setOnDelegateStatus(true);
				delegateRepository.save(var);
			}
		}
	}
	
	public void checkDelegateStatusTill() {
		String currentDate = (new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		List<Delegate> delegates = delegateRepository.findByOnDelegateStatusAndRequestStatus(true, "Approved");
		for(Delegate var : delegates) {
			String date1 = var.getDelegateFrom();
			if (date1.compareTo(currentDate) >= 0) {
				var.setOnDelegateStatus(true);
				delegateRepository.save(var);
			}
		}
	}
		
}
