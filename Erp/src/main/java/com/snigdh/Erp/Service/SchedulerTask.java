package com.snigdh.Erp.Service;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Service.AttendanceService.AttendanceService;
import com.snigdh.Erp.Service.DelegateService.DelegateService;
import com.snigdh.Erp.Service.LeaveService.LeaveRemainingService;
import com.snigdh.Erp.Service.LeaveService.LeaveRequestService;
import com.snigdh.Erp.Service.TravelService.TravelService;

@Service
public class SchedulerTask {

	@Autowired
	LeaveRemainingService remainingService;
	
	@Autowired
	LeaveRequestService leaveRequestService;
	
	@Autowired
	DelegateService delegateService;
	
	@Autowired
	AttendanceService attendanceService;
	
	@Autowired
	TravelService travelService;

	@Scheduled(cron = "* 40 11 * * ?")
	public void checkOnLeaveFrom(/* String date1,String date2 */) throws ParseException {
		leaveRequestService.checkOnLeaveFrom();
	}

	@Scheduled(cron = "* 56 23 * * ?")
	public void checkOnLeaveTo(/* String date1,String date2 */) throws ParseException {
		leaveRequestService.checkOnLeaveTo();
	}
	
	@Scheduled(cron = "0 0 0 1 * ?") // runs on the first day of each month
	public void increaseAnnualLeave() throws ParseException {
		remainingService.increaseAnnualLeave();
	}
	
	@Scheduled(cron = "0 0 0 1 1 ?") // it will run 1st January every year
	public void increaseLeaveAnually() throws ParseException{
		remainingService.increaseLeaveAnually();
	}
	
	@Scheduled(cron = "0 0 1 * * ?")
	public void checkDelegateStatusFrom() throws ParseException{
		delegateService.checkDelegateStatusFrom();
	}
	
	@Scheduled(cron = "* 56 23 * * ?")
	public void checkDelegateStatusTill() throws ParseException{
		delegateService.checkDelegateStatusTill();
	}
		
	@Scheduled(cron = "* 22 10 * * ?")
	public void checkOutStatusAndChangeIfEmpty(){
		attendanceService.attendanceAutoCheckOut();
	}
	
	@Scheduled(cron = "* 01 00 * * ?")
	public void changeTravelStatusTrue(){
		travelService.checkForOnTravel();
		travelService.revokeOnTravelStatus();
	}

}
