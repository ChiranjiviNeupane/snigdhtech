package com.snigdh.Erp.Service.TravelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.TravelModel.TravelItenary;
import com.snigdh.Erp.Repository.TravelRepository.TravelItenaryRepository;

@Service
public class TravelItenerayService {

	@Autowired
	private TravelItenaryRepository travelItinaryRepo;

	public void save(TravelItenary travel) {
		travelItinaryRepo.save(travel);
	}

	public List<TravelItenary> findAll() {
		return travelItinaryRepo.findAll();
	}

	public void delete(TravelItenary id) {
		travelItinaryRepo.delete(id);
	}

	/*
	 * public Travel retrieve(Integer id) { Optional<Travel> compInfo =
	 * travelRepo.findByEmployeePersonalInformation_Id(id); return compInfo.get();
	 * return ""; }
	 */

	public TravelItenary update(TravelItenary travel, Integer id) {
		/*
		 * return travelRepo.findByEmployeePersonalInformation_Id(id).map(otherInfo -> {
		 * otherInfo.setPanNumber(otherInformation.getPanNumber());
		 * otherInfo.setDrivingLiscenceNumber(otherInformation.getDrivingLiscenceNumber(
		 * ));
		 * otherInfo.setInsurancePolicyNumber(otherInformation.getInsurancePolicyNumber(
		 * )); otherInfo.setOtherProfessionalTrainingCourses(otherInformation.
		 * getOtherProfessionalTrainingCourses());
		 * otherInfo.setPastOrganisations(otherInformation.getPastOrganisations());
		 * otherInfo.setMajorAlergyOrSickness(otherInformation.getMajorAlergyOrSickness(
		 * )); otherInfo.setPreferences(otherInformation.getPreferences());
		 * otherInfo.setBloodGroup(otherInformation.getBloodGroup());
		 * otherInfo.setEthnicity(otherInformation.getEthnicity());
		 * otherInfo.setReligion(otherInformation.getReligion());
		 * otherInfo.setUpdatedAt(new Date());
		 * otherInfo.setUpdatedBy(genService.getUserEmail());
		 * 
		 * return informationRepository.save(otherInfo); }).orElseGet(() -> {
		 * otherInformation.setId(id); return
		 * informationRepository.save(otherInformation); });
		 */
		return travel;
	}
}
