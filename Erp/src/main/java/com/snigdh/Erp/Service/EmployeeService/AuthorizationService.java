package com.snigdh.Erp.Service.EmployeeService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.Authorization;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.AuthorizationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service
public class AuthorizationService {

	@Autowired
	AuthorizationRepository authorizationRepository;
	
	@Autowired
	private UserService generalService;
	
	@Autowired
	EmployeePersonalInformationRepository empRepository;
	
	@Autowired
	UtilityService utilityService;

	public void saveValues(Authorization authorization) {
		Optional<EmployeePersonalInformation> employee = empRepository.findById(generalService.getUserId());
		authorization.setName(authorization.getName());
		authorization.setPosition(authorization.getPosition());
		authorization.setEmployeeId(authorization.getEmployeeId());
		authorization.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		authorization.setCreatedBy(utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(), employee.get().getSurname()));
		authorizationRepository.save(authorization);
	}
	
	public void updateValues(Authorization authorization, Integer id) {
		Optional<EmployeePersonalInformation> employee = empRepository.findById(generalService.getUserId());
		Optional<Authorization> optionalAuthorization = authorizationRepository.findById(id);
		optionalAuthorization.get().setName(authorization.getName());
		optionalAuthorization.get().setEmployeeId(authorization.getEmployeeId());
		optionalAuthorization.get().setUpdatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		optionalAuthorization.get().setUpdatedBy(utilityService.mergeName(employee.get().getFirstName(), employee.get().getMiddleName(), employee.get().getSurname()));
		authorizationRepository.save(optionalAuthorization.get());
	}
	
	public Object getValues() {
		return authorizationRepository.findAll();
	}
	
}
