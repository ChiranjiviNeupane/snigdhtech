package com.snigdh.Erp.Service.EmployeeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
//import com.snigdh.Erp.Controller.LeaveController.LeaveRemainingController;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.CompanyInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;

@Service("employeePersonalInformationService")
public class EmployeePersonalInformationService {

	private EmployeePersonalInformationRepository personalInformationRepository;

	@Autowired
	private CompanyInformationRepository co;

	@Autowired
	private MailService mail;

	@Autowired
	UserService genService;

	@Autowired
	UtilityService utService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	public EmployeePersonalInformationService(EmployeePersonalInformationRepository personalInformationRepository) {
		this.personalInformationRepository = personalInformationRepository;
	}

	public void save(EmployeePersonalInformation empObj) {
		personalInformationRepository.save(empObj);

	}

	public EmployeePersonalInformation retrieve(Integer id) {
		Optional<EmployeePersonalInformation> employee = personalInformationRepository.findById(id);
		return employee.get();
	}

	public String replaceEmployee(EmployeePersonalInformation empObj, Integer id) {
		EmployeePersonalInformation employee1 = personalInformationRepository.findDetailById(id);
		if (employee1.getEmail().equals(empObj.getEmail())
				|| (personalInformationRepository.existsByEmail(empObj.getEmail()) == false)) {
			return personalInformationRepository.findById(id).map(employee -> {
				employee.setTitle(empObj.getTitle()); // setName(newEmployee.getName());
				employee.setFirstName(empObj.getFirstName());
				employee.setOtherDegrees(empObj.getOtherDegrees());
				employee.setMiddleName(empObj.getMiddleName());
				employee.setSurname(empObj.getSurname());
				employee.setMaidenName(empObj.getMaidenName());
				employee.setSuffix(empObj.getSuffix());
				employee.setEmail(empObj.getEmail());
				employee.setNationality(empObj.getNationality());
				employee.setIdentificationName(empObj.getIdentificationName());
				employee.setIdentificationNumber(empObj.getIdentificationNumber());
				employee.setSex(empObj.getSex());
				employee.setMaritalStatus(empObj.getMaritalStatus());
				employee.setAcademicQualification(empObj.getAcademicQualification());
				employee.setUpdatedAt(new Date());
				employee.setUpdatedBy(genService.getUserEmail());
				employee.setPresentStatus(empObj.getPresentStatus());
				try {
					personalInformationRepository.save(employee);
					String subject = "Notification regarding Updated Account";
					String message = empObj.getEmail() + " has been updated by " + genService.getUserFirstName()+" "+genService.getUserMiddleName()+" "+genService.getSurName();
					mail.sendMail(empObj.getEmail(), genService.getUserEmail(), subject, message);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				return "Successfully updated";
			}).orElseGet(() -> {
				empObj.setId(id);
				try {
					personalInformationRepository.save(empObj);
					String subject = "Notification regarding adding Account";
					String message = "Your account has been added by " + genService.getUserEmail()
							+ ". Please Check your profile.";
					mail.sendMail(empObj.getEmail(), subject, message);

					mail.sendMail(genService.getUserEmail(), "Successfully updated.",
							"You have successfully updated account " + empObj.getEmail() + " .");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				return "Successfully added.";
			});
		} else if (personalInformationRepository.existsByEmail(empObj.getEmail())) {
			return ("Fail -> Email is already in use!");
		} else {
			return "Something went wrong";
		}
	}

	public Optional<EmployeePersonalInformation> deleteEmployee(Integer id) {
		return personalInformationRepository.findById(id).map(employee -> {
			employee.setPresentStatus(false);
			employee.setUpdatedAt(new Date());
			employee.setUpdatedBy(genService.getUserEmail());
			return personalInformationRepository.save(employee);
		});

	}

	public List<Object> findLineManager() {
		List<Object[]> empList = personalInformationRepository.findListOfEmployeeNameIdDepart();
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] empDetail : empList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("id", empDetail[0]);
			entityMap.put("name",
					utService.mergeName(empDetail[1].toString(), empDetail[2].toString(), empDetail[3].toString()));
			entityMap.put("department", empDetail[4]);
			arrayMap.add(entityMap);
		}

		return arrayMap;
	}

	public List<Object> findEmployeeDetailForManageList(boolean status) {
		List<Object[]> empList = personalInformationRepository.findEmployeeDetailWithCustomDetails(status);
		List<Object> arrayMap = new ArrayList<Object>();
		for (Object[] empDetail : empList) {
			HashMap<String, Object> entityMap = new HashMap<String, Object>();
			entityMap.put("id", empDetail[0]);
			entityMap.put("name",
					utService.mergeName(empDetail[1].toString(), empDetail[2].toString(), empDetail[3].toString()));
			entityMap.put("email", empDetail[4]);
			entityMap.put("office", empDetail[5]);
			entityMap.put("department", empDetail[6]);
			entityMap.put("position", empDetail[7]);
			entityMap.put("lineManager", convertIdToName(empDetail[8]));
			entityMap.put("terminationDate", empDetail[9]);
			arrayMap.add(entityMap);
		}
		return arrayMap;
	}

	public HashMap<String, Object> empDetail(int id) {
		EmployeePersonalInformation detail = personalInformationRepository.findDetailById(id);
		HashMap<String, Object> entityMap = new HashMap<String, Object>();
		entityMap.put("name", utService.mergeName(detail.getFirstName(), detail.getMiddleName(), detail.getSurname()));
		entityMap.put("academicQualification", detail.getAcademicQualification());
		entityMap.put("dateOfBirth", detail.getDateOfBirth());
		entityMap.put("email", detail.getEmail());
		entityMap.put("identificationName", detail.getIdentificationName());
		entityMap.put("identificationNumber", detail.getIdentificationNumber());
		entityMap.put("maidenName", detail.getMaidenName());
		entityMap.put("maritalStatus", detail.getMaritalStatus());
		entityMap.put("nationality", detail.getNationality());
		entityMap.put("sex", detail.getSex());
		entityMap.put("suffix", detail.getSuffix());
		entityMap.put("title", detail.getTitle());
		entityMap.put("createdBy", detail.getCreatedBy());
		entityMap.put("updatedBy", detail.getUpdatedBy());
		entityMap.put("companyInformations", detail.getCompanyInformations());
		entityMap.put("contactAddresses", detail.getContactAddresses());
		entityMap.put("permanentHomeAddresses", detail.getPermanentHomeAddresses());
		entityMap.put("contactInformations", detail.getContactInformations());
		entityMap.put("emergencyContactInformations", detail.getEmergencyContactInformations());
		entityMap.put("dependentInformations", detail.getDependentInformations());
		entityMap.put("otherInformations", detail.getOtherInformations());
		entityMap.put("workAddresses", detail.getWorkAddresses());
		entityMap.put("roles", detail.getRoles());

		CompanyInformation compInfo = co.findDetailByEmployeePersonalInformation_Id(id);
		entityMap.put("LineManager", convertIdToName(compInfo.getLineManager()));
		return entityMap;
	}

	// converting employeeId to employeeName
	public String convertIdToName(Object obj) {
		String idOfLineManagerStringValue = (String) obj;
		int idOfLineManagerIdValue = Integer.parseInt(idOfLineManagerStringValue);
		EmployeePersonalInformation name = personalInformationRepository.findDetailById(idOfLineManagerIdValue);
		return (utService.mergeName(name.getFirstName(), name.getMiddleName(), name.getSurname()));
	}

	// password change
	public String changePassword(EmployeePersonalInformation empObj) {
		int id = genService.getUserId();

		// get email for old password
		EmployeePersonalInformation passEmp = personalInformationRepository.findDetailById(id);
		String password = passEmp.getPassword();

		return personalInformationRepository.findById(id).map(employee -> {
			employee.setPassword(encoder.encode(empObj.getPassword()));
			employee.setUpdatedAt(new Date());
			employee.setUpdatedBy(genService.getUserEmail());
			try {

				if (encoder.matches(empObj.getEmail(), password)) {
					String subject = "Notification regarding Password Change";
					String message = "Your Password has been successfully changed.";
					mail.sendMail(genService.getUserEmail(), subject, message);
					personalInformationRepository.save(employee);
				} else {
					return "Password did not matched.";
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return "Success!";

		}).orElseGet(() -> {
			return "failed";
		});
	}

	// Forgot password
	public ResponseEntity<String> forgotPassword(EmployeePersonalInformation empObj) {
		// generated random string
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 8) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();

		// check if current mail is present
		if (personalInformationRepository.existsByEmail(empObj.getEmail())) {
			EmployeePersonalInformation idEmp = personalInformationRepository
					.findIdByEmailAndPresentStatus(empObj.getEmail(), true);

			// update password and send mail
			return personalInformationRepository.findById(idEmp.getId()).map(employee -> {
				employee.setPassword(encoder.encode(saltStr));
				employee.setUpdatedAt(new Date());
				employee.setUpdatedBy(empObj.getEmail());
				try {
					String subject = "Notification regarding Password Change";
					String message = "Your Password has been successfully changed. Your new Password is: " + saltStr
							+ " . Please change your password according to your comfort.";
					mail.sendMail(empObj.getEmail(), subject, message);
					personalInformationRepository.save(employee);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				return ResponseEntity.ok().body("Password Changed successfully!");


			}).orElseGet(() -> {
				return new ResponseEntity<String>("Fail -> Something is wrong!", HttpStatus.BAD_REQUEST);
			});
		} else {
			return new ResponseEntity<String>("Email does not exist!", HttpStatus.BAD_REQUEST);
		}
	}

	public HashMap<String, Object> empDetailNamePosDepart(Integer id) {
		EmployeePersonalInformation detail = personalInformationRepository.findDetailById(id);
		HashMap<String, Object> entityMap = new HashMap<String, Object>();
		entityMap.put("name", utService.mergeName(detail.getFirstName(), detail.getMiddleName(), detail.getSurname()));
		entityMap.put("companyInformations", detail.getCompanyInformations());
		return entityMap;
	}

	public Object getMyEmployeesData() {
		int id = genService.getUserId();
		List<Object[]> request = personalInformationRepository.getMyEmployeesData(String.valueOf(id));
		List<Object> arrayWithMap = new ArrayList<Object>();

		for (Object[] recent : request) {
			HashMap<String, Object> recentUserJson = new HashMap<String, Object>();
			recentUserJson.put("id", recent[0]);
			String name = utService.mergeName(String.valueOf(recent[1]), String.valueOf(recent[2]),
					String.valueOf(recent[3]));
			recentUserJson.put("name", name);
			recentUserJson.put("email", recent[4]);
			recentUserJson.put("office", recent[5]);
			recentUserJson.put("department", recent[6]);
			recentUserJson.put("position", recent[7]);
			arrayWithMap.add(recentUserJson);
		}
		return arrayWithMap;

	}

	public Optional<EmployeePersonalInformation> enableEmployee(Integer id) {
		return personalInformationRepository.findById(id).map(employee -> {
			employee.setPresentStatus(true);
			employee.setUpdatedAt(new Date());
			employee.setUpdatedBy(genService.getUserEmail());
			return personalInformationRepository.save(employee);
		});
	}

}
