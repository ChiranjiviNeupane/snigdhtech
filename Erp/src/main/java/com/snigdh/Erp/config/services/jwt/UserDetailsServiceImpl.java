package com.snigdh.Erp.config.services.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
 
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    EmployeePersonalInformationRepository userRepository;
 
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
    	
        EmployeePersonalInformation user = userRepository.findByEmailAndPresentStatus(username,true)
                	.orElseThrow(() -> 
                        new UsernameNotFoundException("User Not Found with email." + username)
        );
 
        return UserPrinciple.build(user);
    }
}