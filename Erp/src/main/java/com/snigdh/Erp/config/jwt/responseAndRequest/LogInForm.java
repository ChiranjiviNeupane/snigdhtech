package com.snigdh.Erp.config.jwt.responseAndRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
 
public class LogInForm {
    @NotBlank
    @Size(min=3, max = 60)
    private String email;
 
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String username) {
        this.email = username;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
}