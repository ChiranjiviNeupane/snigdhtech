package com.snigdh.Erp.config.jwt.responseAndRequest;
 
import java.util.Set;

import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Model.EmployeeModel.ContactAddress;
import com.snigdh.Erp.Model.EmployeeModel.ContactInformation;
import com.snigdh.Erp.Model.EmployeeModel.DependentInformation;
import com.snigdh.Erp.Model.EmployeeModel.EmergencyContactInformation;
import com.snigdh.Erp.Model.EmployeeModel.Multimedia;
import com.snigdh.Erp.Model.EmployeeModel.OtherInformation;
import com.snigdh.Erp.Model.EmployeeModel.PermanentHomeAddress;
import com.snigdh.Erp.Model.EmployeeModel.WorkAddress;



 
public class SignUpForm {
 
	private int id;
	
	private String academicQualification;

	private boolean presentStatus;

	private String dateOfBirth;
	
	private String email;

	private String firstName;

	private String identificationName;

	private String identificationNumber;
	
	private String maidenName;
	
	private String maritalStatus;

	private String middleName;

	private String nationality;

	private String password;

	private String sex;

	private String suffix;
	
	private String otherDegrees;

	private String surname;

	private String title;
	
	private Set<CompanyInformation> companyInformations;
	
	private Set<ContactAddress> contactAddresses;
	
	private Set<ContactInformation> contactInformations;
	
	private Set<DependentInformation> dependentInformations;
	
	private Set<EmergencyContactInformation> emergencyContactInformations;

	private Set<OtherInformation> otherInformations;

	private Set<PermanentHomeAddress> permanentHomeAddresses;
	
	private Set<String> role;

	private Set<WorkAddress> workAddresses;
	
	private Set<Multimedia> multimedia;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAcademicQualification() {
		return this.academicQualification;
	}

	public void setAcademicQualification(String academicQualification) {
		this.academicQualification = academicQualification;
	}

	public String getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIdentificationName() {
		return this.identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}

	public String getIdentificationNumber() {
		return this.identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getMaidenName() {
		return this.maidenName;
	}

	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isPresentStatus() {
		return presentStatus;
	}

	public void setPresentStatus(boolean presentStatus) {
		this.presentStatus = presentStatus;
	}


	public Set<CompanyInformation> getCompanyInformations() {
		return this.companyInformations;
	}

	public void setCompanyInformations(Set<CompanyInformation> companyInformations) {
		this.companyInformations = companyInformations;
	}


	public Set<ContactAddress> getContactAddresses() {
		return this.contactAddresses;
	}

	public void setContactAddresses(Set<ContactAddress> contactAddresses) {
		this.contactAddresses = contactAddresses;
	}



	public Set<ContactInformation> getContactInformations() {
		return this.contactInformations;
	}

	public void setContactInformations(Set<ContactInformation> contactInformations) {
		this.contactInformations = contactInformations;
	}


	public Set<DependentInformation> getDependentInformations() {
		return this.dependentInformations;
	}

	public void setDependentInformations(Set<DependentInformation> dependentInformations) {
		this.dependentInformations = dependentInformations;
	}


	public Set<EmergencyContactInformation> getEmergencyContactInformations() {
		return this.emergencyContactInformations;
	}

	public void setEmergencyContactInformations(Set<EmergencyContactInformation> emergencyContactInformations) {
		this.emergencyContactInformations = emergencyContactInformations;
	}


	public Set<OtherInformation> getOtherInformations() {
		return this.otherInformations;
	}

	public void setOtherInformations(Set<OtherInformation> otherInformations) {
		this.otherInformations = otherInformations;
	}


	public Set<PermanentHomeAddress> getPermanentHomeAddresses() {
		return this.permanentHomeAddresses;
	}

	public void setPermanentHomeAddresses(Set<PermanentHomeAddress> permanentHomeAddresses) {
		this.permanentHomeAddresses = permanentHomeAddresses;
	}

	public Set<String> getRole() {
    	return this.role;
    }
    
    public void setRole(Set<String> role) {
    	this.role = role;
    }

	public Set<WorkAddress> getWorkAddresses() {
		return this.workAddresses;
	}

	public void setWorkAddresses(Set<WorkAddress> workAddresses) {
		this.workAddresses = workAddresses;
	}
	public Set<Multimedia> getMultimedia() {
		return this.multimedia;
	}

	public void setMultimedia(Set<Multimedia> multimedia) {
		this.multimedia = multimedia;
	}

	public String getOtherDegrees() {
		return otherDegrees;
	}

	public void setOtherDegrees(String otherDegrees) {
		this.otherDegrees = otherDegrees;
	}


	

}
