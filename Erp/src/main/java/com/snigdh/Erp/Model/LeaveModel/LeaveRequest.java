package com.snigdh.Erp.Model.LeaveModel;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.Notification.Notification;

@Entity
@Table(name = "leave_request")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = LeaveRequest.class)
public class LeaveRequest {

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "on_leave_status")
	private boolean onLeaveStatus = false;

	@ManyToOne
	@JoinColumn(name = "leave_type_id")
	private LeaveType leaveType;
	
	@Column(name = "no_of_days")
	private double noOfDays;	

	@Column(name = "leave_reason")
	private String leaveReason;

	@Column(name = "leave_from")
	private String leaveFrom;

	@Column(name = "leave_till")
	private String leaveTill;

	@Column(name = "request_to")
	private Integer requestTo;
	
	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;

	@Column(name = "request_status")
	private String requestStatus = "Pending";

	@Column(name = "recommended_by")
	private String recommendedBy;
	
	@Column(name = "recommended_date")
	private String recommendedDate;
	
	@Column(name = "created_date")
	private String createdDate;

	@Column(name = "created_time")
	private String createdTime;

	@Column(name = "extend_status")
	private String extendStatus;

	@Column(name = "extend_till")
	private String extendTill;

	@Column(name = "extended_days")
	private double extendDays;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "cancel_from")
	private String cancelFrom;
	
	@Column(name = "cancel_status")
	private String cancelStatus;
	
	@Column(name = "cancel_date")
	private String cancelDate;
	
	@Column(name = "cancel_remarks")
	private String cancelRemarks;

	@Column(name = "extended_date")
	private String extendedDate;
	
	@OneToMany
	@Column(name = "leave_request_id")
	private Set<Notification> notification;

	@Column(name = "response_by")
	private String responseBy;
	
	@Column(name = "response_date")
	private String responseDate;
	
	@Column(name = "response_time")
	private String responseTime;
	
	@Column(name = "extend_remarks")
	private String extendRemarks;
	
	@Column(name = "extend_response_remarks")
	private String extendResponseRemarks;
	
	@Column(name = "cancel_response_date")
	private String cancelResponseDate;
	
	@Column(name = "cancel_response_by")
	private String cancelResponseBy;
	
	@Column(name = "cancel_response_remarks")
	private String cancelResponseRemarks;
	
	@Column(name = "extend_response_date")
	private String extendResponseDate;
	
	@Column(name = "extend_response_by")
	private String extendResponseBy;

	public String getExtendResponseBy() {
		return extendResponseBy;
	}

	public void setExtendResponseBy(String extendResponseBy) {
		this.extendResponseBy = extendResponseBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public double getExtendDays() {
		return extendDays;
	}

	public void setExtendDays(double extendDays) {
		this.extendDays = extendDays;
	}

	public String getExtendStatus() {
		return extendStatus;
	}

	public void setExtendStatus(String extendStatus) {
		this.extendStatus = extendStatus;
	}

	public String getExtendTill() {
		return extendTill;
	}

	public void setExtendTill(String extendTill) { 
		this.extendTill = extendTill;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public double getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(double noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getRecommendedBy() {
		return recommendedBy;
	}

	public void setRecommendedBy(String recommendedBy) {
		this.recommendedBy = recommendedBy;
	}

	public Integer getRequestTo() {
		return requestTo;
	}

	public void setRequestTo(Integer requestTo) {
		this.requestTo = requestTo;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isOnLeaveStatus() {
		return onLeaveStatus;
	}

	public void setOnLeaveStatus(boolean onLeaveStatus) {
		this.onLeaveStatus = onLeaveStatus;
	}

	public String getLeaveFrom() {
		return leaveFrom;
	}

	public void setLeaveFrom(String leaveFrom) {
		this.leaveFrom = leaveFrom;
	}

	public String getLeaveTill() {
		return leaveTill;
	}

	public void setLeaveTill(String leaveTill) {
		this.leaveTill = leaveTill;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public String getCancelFrom() {
		return cancelFrom;
	}

	public void setCancelFrom(String cancelFrom) {
		this.cancelFrom = cancelFrom;
	}

	public String getCancelStatus() {
		return cancelStatus;
	}

	public void setCancelStatus(String cancelStatus) {
		this.cancelStatus = cancelStatus;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getResponseBy() {
		return responseBy;
	}

	public void setResponseBy(String responseBy) {
		this.responseBy = responseBy;
	}

	public String getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getRecommendedDate() {
		return recommendedDate;
	}

	public void setRecommendedDate(String recommendedDate) {
		this.recommendedDate = recommendedDate;
	}
	
	public String getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getExtendedDate() {
		return extendedDate;
	}

	public void setExtendedDate(String extendedDate) {
		this.extendedDate = extendedDate;
	}

	public String getExtendRemarks() {
		return extendRemarks;
	}

	public String getCancelRemarks() {
		return cancelRemarks;
	}

	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	public String getCancelResponseDate() {
		return cancelResponseDate;
	}

	public void setCancelResponseDate(String cancelResponseDate) {
		this.cancelResponseDate = cancelResponseDate;
	}

	public String getCancelResponseBy() {
		return cancelResponseBy;
	}

	public void setCancelResponseBy(String cancelResponseBy) {
		this.cancelResponseBy = cancelResponseBy;
	}

	public String getExtendResponseRemarks() {
		return extendResponseRemarks;
	}

	public void setExtendResponseRemarks(String extendResponseRemarks) {
		this.extendResponseRemarks = extendResponseRemarks;
	}

	public String getCancelResponseRemarks() {
		return cancelResponseRemarks;
	}

	public void setCancelResponseRemarks(String cancelResponseRemarks) {
		this.cancelResponseRemarks = cancelResponseRemarks;
	}

	public void setExtendRemarks(String extendRemarks) {
		this.extendRemarks = extendRemarks;
	}

	public String getExtendResponseDate() {
		return extendResponseDate;
	}

	public void setExtendResponseDate(String extendResponseDate) {
		this.extendResponseDate = extendResponseDate;
	}
}
