package com.snigdh.Erp.Model.LeaveModel;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.Notification.Notification;

@Entity
@Table(name = "half_day_leave")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = HalfDayLeave.class)
public class HalfDayLeave {
	
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "on_leave_status")
	private boolean onLeaveStatus = false;
	
	@ManyToOne
	@JoinColumn(name = "leave_type_id")
	private LeaveType leaveType;
	
	@Column(name = "half_day_type")
	private String halfDayType;
	
	@Column(name = "leave_reason")
	private String leaveReason;

	@Column(name = "leave_date")
	private String leaveDate;

	@Column(name = "request_to")
	private Integer requestTo;

	@Column(name = "request_status")
	private String requestStatus = "Pending";
	
	@Column(name = "response_date")
	private String responseDate;
	
	@Column(name = "response_time")
	private String responseTime;
	
	@Column(name = "created_date")
	private String createdDate;
	
	@Column(name = "created_time")
	private String createdTime;

	@Column(name = "recommended_by")
	private String recommendedBy;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "response_by")
	private String responseBy;

	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;
	
	@OneToMany
	@Column(name = "half_day_leave_id")
	private Set<Notification> notification;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isOnLeaveStatus() {
		return onLeaveStatus;
	}

	public void setOnLeaveStatus(boolean onLeaveStatus) {
		this.onLeaveStatus = onLeaveStatus;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public String getHalfDayType() {
		return halfDayType;
	}

	public void setHalfDayType(String halfDayType) {
		this.halfDayType = halfDayType;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public String getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(String leaveDate) {
		this.leaveDate = leaveDate;
	}

	public Integer getRequestTo() {
		return requestTo;
	}

	public void setRequestTo(Integer requestTo) {
		this.requestTo = requestTo;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}

	public String getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getRecommendedBy() {
		return recommendedBy;
	}

	public void setRecommendedBy(String recommendedBy) {
		this.recommendedBy = recommendedBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getResponseBy() {
		return responseBy;
	}

	public void setResponseBy(String responseBy) {
		this.responseBy = responseBy;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public Set<Notification> getNotification() {
		return notification;
	}

	public void setNotification(Set<Notification> notification) {
		this.notification = notification;
	}
}
