package com.snigdh.Erp.Model.AttendanceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

@Entity
@Table(name = "attendance")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id", scope = Attendance.class)
public class Attendance {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="check_In_Time")
	private String checkInTime;
	
	@Column(name="check_out_time")
	private String checkOutTime;
	
	@Column(name="check_out_location")
	private String checkOutLocation;
	
	@Column(name="check_in_location")
	private String checkInLocation;
	
	@Column(name = "status")
	private boolean status;
	
	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;

	public Attendance() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(String checkInTime) {
		this.checkInTime = checkInTime;
	}

	public String getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(String checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public String getCheckOutLocation() {
		return checkOutLocation;
	}

	public void setCheckOutLocation(String checkOutLocation) {
		this.checkOutLocation = checkOutLocation;
	}

	public String getCheckInLocation() {
		return checkInLocation;
	}

	public void setCheckInLocation(String checkInLocation) {
		this.checkInLocation = checkInLocation;
	}
}
