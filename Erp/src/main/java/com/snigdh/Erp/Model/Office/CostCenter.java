package com.snigdh.Erp.Model.Office;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="cost_center")
@NamedQuery(name="CostCenter.findAll",query="select c FROM CostCenter c")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCenter.class)
public class CostCenter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(nullable= false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="cost_center_id")
	private Set<SourceOfFund> sourceOfFunds;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public Set<SourceOfFund> getSourceOfFunds() {
		return sourceOfFunds;
	}

	public void setSourceOfFunds(Set<SourceOfFund> sourceOfFunds) {
		this.sourceOfFunds = sourceOfFunds;
	}
	

}
