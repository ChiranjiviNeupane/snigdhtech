package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the dependent_information database table.
 * 
 */
@Entity
@Table(name="dependent_information")
@NamedQuery(name="DependentInformation.findAll", query="SELECT d FROM DependentInformation d")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id", scope = DependentInformation.class)
public class DependentInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;

	private String children1;

	private String children2;

	private String children3;

	@Column(name="father_name")
	private String fatherName;

	@Column(name="mother_name")
	private String motherName;

	private String spouse;

	//bi-directional many-to-one association to EmployeePersonalInformation
	@ManyToOne
	@JsonIgnore
	private EmployeePersonalInformation employeePersonalInformation;

	public DependentInformation() {
		this.updatedAt = new Date();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getChildren1() {
		return this.children1;
	}

	public void setChildren1(String children1) {
		this.children1 = children1;
	}

	public String getChildren2() {
		return this.children2;
	}

	public void setChildren2(String children2) {
		this.children2 = children2;
	}

	public String getChildren3() {
		return this.children3;
	}

	public void setChildren3(String children3) {
		this.children3 = children3;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return this.motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getSpouse() {
		return this.spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return this.employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

}