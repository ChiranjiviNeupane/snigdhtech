package com.snigdh.Erp.Model.TravelModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="travel_air_ticket")
@NamedQuery(name = "TravelAirTicket.findAll", query = "SELECT t FROM TravelAirTicket t")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TravelItenary.class)
public class TravelAirTicket {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "traveller_full_name")
	private String travellerFullName;
	
	@Column(name = "flight_from")
	private String flightFrom;
	
	@Column(name = "flight_to")
	private String flightTo;
	
	@Column(name = "departure_date")
	private String departureDate;
	
	@Column(name = "departure_time")
	private String departureTime;
	
	@Column(name = "return_date")
	private String returnDate;
	
	@Column(name = "return_time")
	private String returnTime;
	
	@Column(name = "remarks")
	private String remarks;
	
	@ManyToOne
	@JsonIgnore
	private Travel travel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTravellerFullName() {
		return travellerFullName;
	}

	public void setTravellerFullName(String travellerFullName) {
		this.travellerFullName = travellerFullName;
	}

	public String getFlightFrom() {
		return flightFrom;
	}

	public void setFlightFrom(String flightFrom) {
		this.flightFrom = flightFrom;
	}

	public String getFlightTo() {
		return flightTo;
	}

	public void setFlightTo(String flightTo) {
		this.flightTo = flightTo;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Travel getTravel() {
		return travel;
	}

	public void setTravel(Travel travel) {
		this.travel = travel;
	}
	

}
