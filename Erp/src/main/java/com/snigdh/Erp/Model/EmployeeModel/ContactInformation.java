package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the contact_information database table.
 * 
 */
@Entity
@Table(name="contact_information")
@NamedQuery(name="ContactInformation.findAll", query="SELECT c FROM ContactInformation c")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id", scope = ContactInformation.class)
public class ContactInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name="secondary_email",unique=true)
	private String secondaryEmail;

	private String mobile;

	private String phone;

	@Column(name="po_box")
	private String poBox;

	//bi-directional many-to-one association to EmployeePersonalInformation
	@ManyToOne
	@JsonIgnore
	private EmployeePersonalInformation employeePersonalInformation;

	public ContactInformation() {
		this.updatedAt=new Date();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getSecondaryEmail() {
		return this.secondaryEmail;
	}

	public void setSecondaryEmail(String email) {
		this.secondaryEmail = email;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPoBox() {
		return this.poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return this.employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

}