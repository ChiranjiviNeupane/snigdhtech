package com.snigdh.Erp.Model.AttendanceModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

@Entity
@Table(name = "lieu_request")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id", scope = LieuRequest.class)
public class LieuRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "request_status")
	private String requestStatus = "Pending";
	
	@Column(name = "request_to")
	private int requestTo;
	
	@Column(name = "cancel_request")
	private String cancelRequest;
	
	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public int getRequestTo() {
		return requestTo;
	}

	public void setRequestTo(int requestTo) {
		this.requestTo = requestTo;
	}

	public String getCancelRequest() {
		return cancelRequest;
	}

	public void setCancelRequest(String cancelRequest) {
		this.cancelRequest = cancelRequest;
	}
}