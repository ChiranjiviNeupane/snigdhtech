package com.snigdh.Erp.Model.Notification;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.TravelModel.Travel;

@Entity
@Table(name = "notification")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Notification.class)
public class Notification {

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "notification_status")
	private boolean onNotificationStatus = false;
	
	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;
	
	@ManyToOne
	@JoinColumn(name = "leave_request_id")
	private LeaveRequest leaveRequest;
	
	@ManyToOne
	@JoinColumn(name = "half_day_leave_id")
	private HalfDayLeave halfDayLeave;
	
	@ManyToOne
	@JoinColumn(name = "hourly_leave_id")
	private HourlyLeave hourlyLeave;
	
	@ManyToOne
	@JoinColumn(name = "travel_id")
	private Travel travel;
	
	@Column(name = "created_at")
	private LocalDate createdAt;

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isOnNotificationStatus() {
		return onNotificationStatus;
	}

	public void setOnNotificationStatus(boolean onNotificationStatus) {
		this.onNotificationStatus = onNotificationStatus;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public LeaveRequest getLeaveRequest() {
		return leaveRequest;
	}

	public void setLeaveRequest(LeaveRequest leaveRequest) {
		this.leaveRequest = leaveRequest;
	}

	public HalfDayLeave getHalfDayLeave() {
		return halfDayLeave;
	}

	public void setHalfDayLeave(HalfDayLeave halfDayLeave) {
		this.halfDayLeave = halfDayLeave;
	}

	public HourlyLeave getHourlyLeave() {
		return hourlyLeave;
	}

	public void setHourlyLeave(HourlyLeave hourlyLeave) {
		this.hourlyLeave = hourlyLeave;
	}

	public Travel getTravel() {
		return travel;
	}

	public void setTravel(Travel travel) {
		this.travel = travel;
	}
}
