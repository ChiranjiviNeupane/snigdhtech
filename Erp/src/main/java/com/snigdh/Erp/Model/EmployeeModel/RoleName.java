package com.snigdh.Erp.Model.EmployeeModel;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN, 
    ROLE_MANAGER
}