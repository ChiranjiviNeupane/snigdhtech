package com.snigdh.Erp.Model.TravelModel;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

@Entity
@Table(name = "travel")
@NamedQuery(name = "Travel.findAll", query = "SELECT t FROM Travel t")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Travel.class)
public class Travel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "purpose")
	private String purpose;

	@Column(name = "place_of_travel")
	private String placeOfTravel;

	@Column(name = "type")
	private String type;

	@Column(name = "team")
	private String team;

	@Column(name = "date_from")
	private String dateFrom;

	@Column(name = "date_to")
	private String dateTo;

	@Column(name = "on_travel")
	private boolean onTravel;

	@Column(name = "duration")
	private String duration;

	@Column(name = "total_advance_cost")
	private String totalAdvanceCost;
	
	@Column(name = "total_advance_cost_in_words")
	private String totalAdvanceCostInWords;

	@Column(name = "budget_code_comment")
	private String budgetCodeComment;

	@Column(name = "created_at")
	private String createdAt;

	@Column(name = "overall_status")
	private String overallStatus;

	@Column(name = "overall_status_changed_date")
	private String overallStatusChangedDate;

	// requests to .....
	@Column(name = "request_to_line_manager")
	private int requestToLineManager;

	@Column(name = "request_to_security_clearance")
	private int requestToSecurityClearance;

	@Column(name = "request_to_administrator")
	private int requestToAdministrator;

	@Column(name = "request_to_finance")
	private int requestToFinance;

	// responses and responses time & date of .......
	@Column(name = "line_manager_response")
	private String lineManagerResponse;

	@Column(name = "line_manager_response_date")
	private String lineManagerResponseDate;
	
	@Column(name = "line_manager_response_by")
	private String lineManagerResponseBy;

	@Column(name = "line_manager_response_remarks")
	private String lineManagerResponseRemarks;

	@Column(name = "administrator_response")
	private String administratorResponse;

	@Column(name = "administrator_response_date")
	private String administratorResponseDate;
	
	@Column(name = "administrator_response_by")
	private String administratorResponseBy;

	@Column(name = "administrator_response_remarks")
	private String administratorResponseRemarks;

	@Column(name = "finance_response")
	private String financeResponse;

	@Column(name = "finance_response_date")
	private String financeResponseDate;
	
	@Column(name = "finance_response_by")
	private String financeResponseBy;

	@Column(name = "finance_response_remarks")
	private String financeResponseRemarks;

	@Column(name = "security_clearance_response")
	private String securityClearanceResponse;

	@Column(name = "security_clearance_response_date")
	private String securityClearanceResponseDate;
	
	@Column(name = "security_clearance_response_by")
	private String securityClearanceResponseBy;

	@Column(name = "security_clearance_response_remarks")
	private String securityClearanceResponseRemarks;

	// extend parameters
	@Column(name = "extend_till")
	private String extendTill;
	
	@Column(name = "extend_overall_status")
	private String extendOverallStatus;
	
	@Column(name = "extend_total_advance")
	private String extendTotalAdvance;
	
	@Column(name = "extend_on")
	private String extendOn;
	
	@Column(name = "extend_remarks")
	private String extendRemarks;

	@Column(name = "line_manager_extend_response")
	private String lineManagerExtendResponse;

	@Column(name = "line_manager_extend_response_date")
	private String lineManagerExtendResponseDate;
	
	@Column(name = "line_manager_extend_response_by")
	private String lineManagerExtendResponseBy;

	@Column(name = "line_manager_extend_response_remarks")
	private String lineManagerExtendResponseRemarks;

	@Column(name = "finance_extend_response")
	private String financeExtendResponse;

	@Column(name = "finance_extend_response_date")
	private String financeExtendResponseDate;
	
	@Column(name = "finance_extend_response_by")
	private String financeExtendResponseBy;

	@Column(name = "finance_extend_response_remarks")
	private String financeExtendResponseRemarks;

	@Column(name = "security_clearance_extend_response")
	private String securityClearanceExtendResponse;

	@Column(name = "security_clearance_extend_response_date")
	private String securityClearanceExtendResponseDate;
	
	@Column(name = "security_clearance_extend_response_by")
	private String securityClearanceExtendResponseBy;

	@Column(name = "security_clearance_extend_response_remarks")
	private String securityClearanceExtendResponseRemarks;

	// recommend parameter
	@Column(name = "recommend_status")
	private boolean recommendStatus;
	
	@Column(name = "recommend_by")
	private String recommendBy;
	
	@Column(name = "recommend_to")
	private String recommendTo;

	@Column(name = "recommend_date")
	private String recommendDate;

	@Column(name = "recommend_response")
	private String recommendResponse;
	
	@Column(name = "recommend_response_by")
	private String recommendResponseBy;

	@Column(name = "recommend_response_date")
	private String recommendResponseDate;

	@Column(name = "recommend_response_remarks")
	private String recommendResponseRemarks;

	// cancel parameters
	@Column(name = "cancel_response")
	private String cancelResponse;
	
	@Column(name = "cancel_on")
	private String cancelOn;
	
	@Column(name = "cancel_remarks")
	private String cancelRemarks;

	@Column(name = "cancel_response_date")
	private String cancelResponseDate;
	
	@Column(name = "cancel_response_by")
	private String cancelResponseBy;

	@Column(name = "cancel_response_remarks")
	private String cancelResponseRemarks;

	//for Auth
	
	@Column(name = "typeAuth")
	private String typeAuth;
	
	@Column(name = "responseAuth")
	private String responseAuth;
	
	@Column(name = "responseRemarks")
	private String responseRemarks;
	
	
	@ManyToOne
	@JsonIgnore
	private EmployeePersonalInformation employeePersonalInformation;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "travel_id")
	private Set<TravelAdvance> travelAdvances;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "travel_id")
	private Set<TravelBudgetCode> budgetCodeSamples;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "travel_id")
	private Set<TravelItenary> travelItenarys;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "travel_id")
	private Set<TravelAirTicket> travelAirTickets;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "travel_id")
	private Set<TravelVehicle> travelVehicles;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPlaceOfTravel() {
		return placeOfTravel;
	}

	public void setPlaceOfTravel(String placeOfTravel) {
		this.placeOfTravel = placeOfTravel;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getTotalAdvanceCost() {
		return totalAdvanceCost;
	}

	public String getExtendTill() {
		return extendTill;
	}

	public void setExtendTill(String extendTravel) {
		this.extendTill = extendTravel;
	}

	public void setTotalAdvanceCost(String totalAdvanceCost) {
		this.totalAdvanceCost = totalAdvanceCost;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public Set<TravelAdvance> getTravelAdvances() {
		return travelAdvances;
	}

	public void setTravelAdvances(Set<TravelAdvance> travelAdvances) {
		this.travelAdvances = travelAdvances;
	}

	public Set<TravelItenary> getTravelItenarys() {
		return travelItenarys;
	}

	public void setTravelItenarys(Set<TravelItenary> travelItenarys) {
		this.travelItenarys = travelItenarys;
	}

	public Set<TravelAdvance> addTravelAdvance(Set<TravelAdvance> set) {
		getTravelAdvances().addAll(set);
		return set;
	}

	public TravelAdvance removeTravelAdvance(TravelAdvance travelAdvance) {
		getTravelAdvances().remove(travelAdvance);
		travelAdvance.setTravel(null);

		return travelAdvance;
	}

	public Set<TravelItenary> addTravelItenary(Set<TravelItenary> travelItenary) {
		getTravelItenarys().addAll(travelItenary);
		return travelItenary;
	}

	public TravelItenary removeTravelAdvance(TravelItenary travelItenary) {
		getTravelItenarys().remove(travelItenary);
		travelItenary.setTravel(null);
		return travelItenary;
	}

	public Set<TravelBudgetCode> getBudgetCodeSamples() {
		return budgetCodeSamples;
	}

	public void setBudgetCodeSamples(Set<TravelBudgetCode> budgetCodeSamples) {
		this.budgetCodeSamples = budgetCodeSamples;
	}

	public int getRequestToLineManager() {
		return requestToLineManager;
	}

	public void setRequestToLineManager(int requestToLineManager) {
		this.requestToLineManager = requestToLineManager;
	}

	public int getRequestToAdministrator() {
		return requestToAdministrator;
	}

	public void setRequestToAdministrator(int requestToAdministrator) {
		this.requestToAdministrator = requestToAdministrator;
	}

	public int getRequestToFinance() {
		return requestToFinance;
	}

	public void setRequestToFinance(int requestToFinance) {
		this.requestToFinance = requestToFinance;
	}

	public boolean isOnTravel() {
		return onTravel;
	}

	public void setOnTravel(boolean onTravel) {
		this.onTravel = onTravel;
	}

	public Set<TravelAirTicket> getTravelAirTickets() {
		return travelAirTickets;
	}

	public void setTravelAirTickets(Set<TravelAirTicket> travelAirTickets) {
		this.travelAirTickets = travelAirTickets;
	}

	public Set<TravelVehicle> getTravelVehicles() {
		return travelVehicles;
	}

	public void setTravelVehicles(Set<TravelVehicle> travelVehicles) {
		this.travelVehicles = travelVehicles;
	}

	public String getRecommendTo() {
		return recommendTo;
	}

	public void setRecommendTo(String recommendTo) {
		this.recommendTo = recommendTo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRequestToSecurityClearance() {
		return requestToSecurityClearance;
	}

	public void setRequestToSecurityClearance(int requestToSecurityClearance) {
		this.requestToSecurityClearance = requestToSecurityClearance;
	}

	public String getLineManagerResponse() {
		return lineManagerResponse;
	}

	public void setLineManagerResponse(String lineManagerResponse) {
		this.lineManagerResponse = lineManagerResponse;
	}

	public String getAdministratorResponse() {
		return administratorResponse;
	}

	public void setAdministratorResponse(String administratorResponse) {
		this.administratorResponse = administratorResponse;
	}

	public String getFinanceResponse() {
		return financeResponse;
	}

	public void setFinanceResponse(String financeResponse) {
		this.financeResponse = financeResponse;
	}

	public String getSecurityClearanceResponse() {
		return securityClearanceResponse;
	}

	public void setSecurityClearanceResponse(String securityClearanceResponse) {
		this.securityClearanceResponse = securityClearanceResponse;
	}

	public String getLineManagerExtendResponse() {
		return lineManagerExtendResponse;
	}

	public void setLineManagerExtendResponse(String lineManagerExtendResponse) {
		this.lineManagerExtendResponse = lineManagerExtendResponse;
	}

	public String getFinanceExtendResponse() {
		return financeExtendResponse;
	}

	public void setFinanceExtendResponse(String financeExtendResponse) {
		this.financeExtendResponse = financeExtendResponse;
	}

	public String getSecurityClearanceExtendResponse() {
		return securityClearanceExtendResponse;
	}

	public void setSecurityClearanceExtendResponse(String securityClearanceExtendResponse) {
		this.securityClearanceExtendResponse = securityClearanceExtendResponse;
	}

	public String getBudgetCodeComment() {
		return budgetCodeComment;
	}

	public void setBudgetCodeComment(String budgetCodeComment) {
		this.budgetCodeComment = budgetCodeComment;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getLineManagerResponseRemarks() {
		return lineManagerResponseRemarks;
	}

	public void setLineManagerResponseRemarks(String lineManagerResponseRemarks) {
		this.lineManagerResponseRemarks = lineManagerResponseRemarks;
	}

	public String getAdministratorResponseRemarks() {
		return administratorResponseRemarks;
	}

	public void setAdministratorResponseRemarks(String administratorResponseRemarks) {
		this.administratorResponseRemarks = administratorResponseRemarks;
	}

	public String getFinanceResponseRemarks() {
		return financeResponseRemarks;
	}

	public void setFinanceResponseRemarks(String financeResponseRemarks) {
		this.financeResponseRemarks = financeResponseRemarks;
	}

	public String getSecurityClearanceResponseRemarks() {
		return securityClearanceResponseRemarks;
	}

	public void setSecurityClearanceResponseRemarks(String securityClearanceResponseRemarks) {
		this.securityClearanceResponseRemarks = securityClearanceResponseRemarks;
	}

	public String getLineManagerExtendResponseRemarks() {
		return lineManagerExtendResponseRemarks;
	}

	public void setLineManagerExtendResponseRemarks(String lineManagerExtendResponseRemarks) {
		this.lineManagerExtendResponseRemarks = lineManagerExtendResponseRemarks;
	}

	public String getFinanceExtendResponseRemarks() {
		return financeExtendResponseRemarks;
	}

	public void setFinanceExtendResponseRemarks(String financeExtendResponseRemarks) {
		this.financeExtendResponseRemarks = financeExtendResponseRemarks;
	}

	public String getSecurityClearanceExtendResponseRemarks() {
		return securityClearanceExtendResponseRemarks;
	}

	public void setSecurityClearanceExtendResponseRemarks(String securityClearanceExtendResponseRemarks) {
		this.securityClearanceExtendResponseRemarks = securityClearanceExtendResponseRemarks;
	}

	public String getRecommendResponse() {
		return recommendResponse;
	}

	public void setRecommendResponse(String recommendResponse) {
		this.recommendResponse = recommendResponse;
	}

	public String getRecommendResponseRemarks() {
		return recommendResponseRemarks;
	}

	public void setRecommendResponseRemarks(String recommendResponseRemarks) {
		this.recommendResponseRemarks = recommendResponseRemarks;
	}

	public String getOverallStatus() {
		return overallStatus;
	}

	public void setOverallStatus(String overallStatus) {
		this.overallStatus = overallStatus;
	}

	public String getCancelResponse() {
		return cancelResponse;
	}

	public void setCancelResponse(String cancelResponse) {
		this.cancelResponse = cancelResponse;
	}

	public String getCancelResponseRemarks() {
		return cancelResponseRemarks;
	}

	public void setCancelResponseRemarks(String cancelResponseRemarks) {
		this.cancelResponseRemarks = cancelResponseRemarks;
	}

	public boolean isRecommendStatus() {
		return recommendStatus;
	}

	public void setRecommendStatus(boolean recommendStatus) {
		this.recommendStatus = recommendStatus;
	}

	public String getTotalAdvanceCostInWords() {
		return totalAdvanceCostInWords;
	}

	public void setTotalAdvanceCostInWords(String totalAdvanceCostInWords) {
		this.totalAdvanceCostInWords = totalAdvanceCostInWords;
	}

	public String getOverallStatusChangedDate() {
		return overallStatusChangedDate;
	}

	public void setOverallStatusChangedDate(String overallStatusChangedDate) {
		this.overallStatusChangedDate = overallStatusChangedDate;
	}

	public String getLineManagerResponseDate() {
		return lineManagerResponseDate;
	}

	public void setLineManagerResponseDate(String lineManagerResponseDate) {
		this.lineManagerResponseDate = lineManagerResponseDate;
	}

	public String getAdministratorResponseDate() {
		return administratorResponseDate;
	}

	public void setAdministratorResponseDate(String administratorResponseDate) {
		this.administratorResponseDate = administratorResponseDate;
	}

	public String getFinanceResponseDate() {
		return financeResponseDate;
	}

	public void setFinanceResponseDate(String financeResponseDate) {
		this.financeResponseDate = financeResponseDate;
	}

	public String getSecurityClearanceResponseDate() {
		return securityClearanceResponseDate;
	}

	public void setSecurityClearanceResponseDate(String securityClearanceResponseDate) {
		this.securityClearanceResponseDate = securityClearanceResponseDate;
	}

	public String getLineManagerExtendResponseDate() {
		return lineManagerExtendResponseDate;
	}

	public void setLineManagerExtendResponseDate(String lineManagerExtendResponseDate) {
		this.lineManagerExtendResponseDate = lineManagerExtendResponseDate;
	}

	public String getFinanceExtendResponseDate() {
		return financeExtendResponseDate;
	}

	public void setFinanceExtendResponseDate(String financeExtendResponseDate) {
		this.financeExtendResponseDate = financeExtendResponseDate;
	}

	public String getSecurityClearanceExtendResponseDate() {
		return securityClearanceExtendResponseDate;
	}

	public void setSecurityClearanceExtendResponseDate(String securityClearanceExtendResponseDate) {
		this.securityClearanceExtendResponseDate = securityClearanceExtendResponseDate;
	}

	public String getRecommendDate() {
		return recommendDate;
	}

	public void setRecommendDate(String recommendDate) {
		this.recommendDate = recommendDate;
	}

	public String getRecommendResponseDate() {
		return recommendResponseDate;
	}

	public void setRecommendResponseDate(String recommendResponseDate) {
		this.recommendResponseDate = recommendResponseDate;
	}

	public String getCancelResponseDate() {
		return cancelResponseDate;
	}

	public void setCancelResponseDate(String cancelResponseDate) {
		this.cancelResponseDate = cancelResponseDate;
	}

	public String getRecommendBy() {
		return recommendBy;
	}

	public void setRecommendBy(String recommendBy) {
		this.recommendBy = recommendBy;
	}

	public String getLineManagerResponseBy() {
		return lineManagerResponseBy;
	}

	public void setLineManagerResponseBy(String lineManagerResponseBy) {
		this.lineManagerResponseBy = lineManagerResponseBy;
	}

	public String getAdministratorResponseBy() {
		return administratorResponseBy;
	}

	public void setAdministratorResponseBy(String administratorResponseBy) {
		this.administratorResponseBy = administratorResponseBy;
	}

	public String getFinanceResponseBy() {
		return financeResponseBy;
	}

	public void setFinanceResponseBy(String financeResponseBy) {
		this.financeResponseBy = financeResponseBy;
	}

	public String getSecurityClearanceResponseBy() {
		return securityClearanceResponseBy;
	}

	public void setSecurityClearanceResponseBy(String securityClearanceResponseBy) {
		this.securityClearanceResponseBy = securityClearanceResponseBy;
	}

	public String getExtendOn() {
		return extendOn;
	}

	public void setExtendOn(String extendOn) {
		this.extendOn = extendOn;
	}

	public String getLineManagerExtendResponseBy() {
		return lineManagerExtendResponseBy;
	}

	public void setLineManagerExtendResponseBy(String lineManagerExtendResponseBy) {
		this.lineManagerExtendResponseBy = lineManagerExtendResponseBy;
	}

	public String getFinanceExtendResponseBy() {
		return financeExtendResponseBy;
	}

	public void setFinanceExtendResponseBy(String financeExtendResponseBy) {
		this.financeExtendResponseBy = financeExtendResponseBy;
	}

	public String getSecurityClearanceExtendResponseBy() {
		return securityClearanceExtendResponseBy;
	}

	public void setSecurityClearanceExtendResponseBy(String securityClearanceExtendResponseBy) {
		this.securityClearanceExtendResponseBy = securityClearanceExtendResponseBy;
	}

	public String getRecommendResponseBy() {
		return recommendResponseBy;
	}

	public void setRecommendResponseBy(String recommendResponseBy) {
		this.recommendResponseBy = recommendResponseBy;
	}

	public String getCancelOn() {
		return cancelOn;
	}

	public void setCancelOn(String cancelOn) {
		this.cancelOn = cancelOn;
	}

	public String getCancelResponseBy() {
		return cancelResponseBy;
	}

	public void setCancelResponseBy(String cancelResponseBy) {
		this.cancelResponseBy = cancelResponseBy;
	}

	public String getExtendRemarks() {
		return extendRemarks;
	}

	public void setExtendRemarks(String extendRemarks) {
		this.extendRemarks = extendRemarks;
	}

	public String getCancelRemarks() {
		return cancelRemarks;
	}

	public void setCancelRemarks(String cancelRemarks) {
		this.cancelRemarks = cancelRemarks;
	}

	public String getExtendOverallStatus() {
		return extendOverallStatus;
	}

	public void setExtendOverallStatus(String extendOverallStatus) {
		this.extendOverallStatus = extendOverallStatus;
	}

	public String getExtendTotalAdvance() {
		return extendTotalAdvance;
	}

	public void setExtendTotalAdvance(String extendTotalAdvance) {
		this.extendTotalAdvance = extendTotalAdvance;
	}

	public String getTypeAuth() {
		return typeAuth;
	}

	public void setTypeAuth(String typeAuth) {
		this.typeAuth = typeAuth;
	}

	public String getResponseAuth() {
		return responseAuth;
	}

	public void setResponseAuth(String responseAuth) {
		this.responseAuth = responseAuth;
	}

	public String getResponseRemarks() {
		return responseRemarks;
	}

	public void setResponseRemarks(String responseRemarks) {
		this.responseRemarks = responseRemarks;
	}
	
	
	

}
