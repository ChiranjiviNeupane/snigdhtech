package com.snigdh.Erp.Model.Delegate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

@Entity
@Table(name = "delegate")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Delegate.class)
public class Delegate implements Serializable {
	private static final long serialVersionUID = 1L;

	public Delegate() {
	}

	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;

	@Column(name = "delegate_to")
	private int delegateTo;

	@Column(name = "delegate_from")
	private String delegateFrom;

	@Column(name = "delegate_till")
	private String delegateTill;

	@Column(name = "on_delegate_status")
	private boolean onDelegateStatus = false;

	@Column(name = "request_status")
	private String requestStatus = "Approved";

	public String isRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public boolean isOnDelegateStatus() {
		return onDelegateStatus;
	}

	public void setOnDelegateStatus(boolean onDelegateStatus) {
		this.onDelegateStatus = onDelegateStatus;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public int getDelegateTo() {
		return delegateTo;
	}

	public void setDelegateTo(int delegateTo) {
		this.delegateTo = delegateTo;
	}

	public String getDelegateFrom() {
		return delegateFrom;
	}

	public void setDelegateFrom(String delegateFrom) {
		this.delegateFrom = delegateFrom;
	}

	public String getDelegateTill() {
		return delegateTill;
	}

	public void setDelegateTill(String delegateTill) {
		this.delegateTill = delegateTill;
	}

}
