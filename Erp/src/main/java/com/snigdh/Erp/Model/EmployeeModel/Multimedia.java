package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "multimedia")
@NamedQuery(name = "Multimedia.findAll", query = "SELECT m FROM Multimedia m")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Multimedia.class)
public class Multimedia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Basic(optional = false)
	@NotNull
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "path")
	private String path;

	@Column(name = "type")
	private String type;

	// only for local storage
	@Column(unique=true)
	private int userid;

	public int getUserId() {
		return userid;
	}

	public void setUserId(int userId) {
		this.userid = userId;
	}

	public Multimedia(String string, int id) {
		this.path = string;
		this.userid = id;
		this.updatedAt = new Date();
	}
	// end for local storgae

	@ManyToOne
	@JsonIgnore
	private EmployeePersonalInformation employeePersonalInformation;

	public Multimedia() {
		this.updatedAt = new Date();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}
}
