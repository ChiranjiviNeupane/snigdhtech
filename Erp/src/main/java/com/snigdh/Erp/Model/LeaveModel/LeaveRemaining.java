package com.snigdh.Erp.Model.LeaveModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;

@Entity
@Table(name = "leave_remaining")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property="id", scope = LeaveRemaining.class)
public class LeaveRemaining {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
		
	@ManyToOne
	@JoinColumn(name = "employee_personal_information_id")
	private EmployeePersonalInformation employeePersonalInformation;
	
	@Column(name="remaining_Leave")
	private double remainingLeave;
	
	@Column(name = "carry_over")
	private double carryOver = 0.0;
		
	@ManyToOne
	@JoinColumn(name = "leave_type_id")
	private LeaveType leaveType;
	
	public int getId() {
		return id; 
	}

	public double getCarryOver() {
		return carryOver;
	}

	public void setCarryOver(double carryOver) {
		this.carryOver = carryOver;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}

	public double getRemainingLeave() {
		return remainingLeave;
	}

	public void setRemainingLeave(double remainingLeave) {
		this.remainingLeave = remainingLeave;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	} 

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}
} 
