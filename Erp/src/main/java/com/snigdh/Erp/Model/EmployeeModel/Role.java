package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.NaturalId;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	 @Enumerated(EnumType.STRING)
	 @NaturalId
	 @Column(length = 60)
	private RoleName type;

	public Role() {
	}
	public Role(RoleName name) {
        this.type = name;
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	 public RoleName getType() {
	        return type;
	    }
	 
	    public void setType(RoleName name) {
	        this.type = name;
	    }
}