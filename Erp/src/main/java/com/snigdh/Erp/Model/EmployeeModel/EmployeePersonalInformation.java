package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snigdh.Erp.Model.AttendanceModel.Attendance;
import com.snigdh.Erp.Model.AttendanceModel.LieuRequest;
import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Model.Notification.Notification;
import com.snigdh.Erp.Model.TravelModel.Travel;

/**
 * The persistent class for the employee_personal_information database table.
 * 
 */
/**
 * @author chiranjivi
 *
 */
@Entity
@Table(name = "employee_personal_information")
@NamedQuery(name = "EmployeePersonalInformation.findAll", query = "SELECT e FROM EmployeePersonalInformation e")
public class EmployeePersonalInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable= false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
	
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
	

	@NotNull
	@Column(name = "created_by")
	private String createdBy;
	
	@NotNull
	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "academic_qualification")
	private String academicQualification;
	
	@Column(name = "other_degrees")
	private String otherDegrees;

	@Column(name = "date_of_birth")
	private String dateOfBirth;

	@Column(name = "present_status")
	private boolean presentStatus = true;

	@Column(unique=true)
	private String email;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "identification_name")
	private String identificationName;

	@Column(name = "identification_number")
	private String identificationNumber;

	@Column(name = "maiden_name")
	private String maidenName;

	@Column(name = "marital_status")
	private String maritalStatus;

	@Column(name = "middle_name")
	private String middleName;

	private String nationality;

	private String password;

	private String sex;

	private String suffix;

	private String surname;

	private String title;

	// bi-directional many-to-one association to CompanyInformation
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<CompanyInformation> companyInformations;

	// bi-directional many-to-one association to ContactAddress
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<ContactAddress> contactAddresses;

	// bi-directional many-to-one association to ContactInformation
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<ContactInformation> contactInformations;

	// bi-directional many-to-one association to DependentInformation
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<DependentInformation> dependentInformations;

	// bi-directional many-to-one association to EmergencyContactInformation
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<EmergencyContactInformation> emergencyContactInformations;

	// bi-directional many-to-one association to OtherInformation
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<OtherInformation> otherInformations;

	// bi-directional many-to-one association to PermanentHomeAddress
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<PermanentHomeAddress> permanentHomeAddresses;

	// bi-directional many-to-many association to Role
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "employee_personal_information_has_role", joinColumns = @JoinColumn(name = "employee_personal_information_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	//Leave Remaining
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name = "employee_personal_information_id")
	private Set<LeaveRemaining> leaveRemaining;
	
	// bi-directional many-to-one association to WorkAddress
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<WorkAddress> workAddresses;
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="employee_personal_information_id")
	private Set<Multimedia> multimedia;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "employee_personal_information_id")
	private Set<Travel> travel;

	// Attendance Relation 
	@OneToMany
	@JoinColumn(name="employee_personal_information_id")
	private Set<Attendance> attendance; 
	
	//Leave Request
	@OneToMany
	@JoinColumn(name = "employee_personal_information_id")
	private Set<LeaveRequest> leaveRequest;
	
	// Lieu Request
	@OneToMany
	@JoinColumn(name = "employee_personal_information_id")
	private Set<LieuRequest> lieuRequest;	
	
	// Leave Remaining
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "employee_personal_information_id")
	private Set<Notification> notification;
	
	// Delegate Relation
	@OneToMany
	@JoinColumn(name = "employee_personal_information_id")
	private Set<Delegate> Delegate;

	//getter Leave Remaining 
	public Set<LeaveRemaining> getLeaveRemaining() {
		return leaveRemaining;
	}

	//setter Leave Remaining 
	public void setLeaveRemaining(Set<LeaveRemaining> leaveRemaining) {
		this.leaveRemaining = leaveRemaining;
	}

	//getter Leave Request 
	public Set<LeaveRequest> getLeaveRequest() {
		return leaveRequest;
	}

	//setter Leave Request
	public void setLeaveRequest(Set<LeaveRequest> leaveRequest) {
		this.leaveRequest = leaveRequest;
	}

	public EmployeePersonalInformation() {
	}

	public EmployeePersonalInformation(String email2, String encode, String academicQualification2, String dateOfBirth2,
			String firstName2, String identificationName2, String identificationNumber2, String maidenName2,
			String maritalStatus2, String middleName2, String nationality2, String sex2, String suffix2,
			String surname2, String title2, Set<CompanyInformation> companyInformations2,
			Set<ContactAddress> contactAddresses2, Set<ContactInformation> contactInformations2,
			Set<DependentInformation> dependentInformations2,
			Set<EmergencyContactInformation> emergencyContactInformations2, Set<OtherInformation> otherInformations2,
			Set<PermanentHomeAddress> permanentHomeAddresses2,Set<WorkAddress> workAddresses2,Set<Multimedia> mul,String degree) {
					this.email=email2;
					this.password=encode;
					this.academicQualification=academicQualification2;
					this.dateOfBirth=dateOfBirth2;
					this.firstName=firstName2;
					this.identificationName=identificationName2;
					this.identificationNumber=identificationNumber2;
					this.maidenName=maidenName2;
					this.maritalStatus=maritalStatus2;
					this.middleName=middleName2;
					this.nationality=nationality2;
					this.sex=sex2;
					this.suffix=suffix2;
					this.surname=surname2;
					this.title=title2;
					this.companyInformations=companyInformations2;
					this.contactAddresses=contactAddresses2;
					this.contactInformations=contactInformations2;
					this.dependentInformations=dependentInformations2;
					this.emergencyContactInformations=emergencyContactInformations2;
					this.otherInformations=otherInformations2;
					this.permanentHomeAddresses=permanentHomeAddresses2;
					this.workAddresses=workAddresses2;
					this.multimedia=mul;
					this.otherDegrees=degree;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAcademicQualification() {
		return this.academicQualification;
	}

	public void setAcademicQualification(String academicQualification) {
		this.academicQualification = academicQualification;
	}

	public String getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIdentificationName() {
		return this.identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}

	public String getIdentificationNumber() {
		return this.identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getMaidenName() {
		return this.maidenName;
	}

	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public boolean getPresentStatus() {
		return presentStatus;
	}

	public void setPresentStatus(boolean presentStatus) {
		this.presentStatus = presentStatus;
	}
	

	public String getOtherDegrees() {
		return otherDegrees;
	}

	public void setOtherDegrees(String otherDegrees) {
		this.otherDegrees = otherDegrees;
	}

	public Set<CompanyInformation> getCompanyInformations() {
		return this.companyInformations;
	}

	public void setCompanyInformations(Set<CompanyInformation> companyInformations) {
		this.companyInformations = companyInformations;
	}

	public Set<ContactAddress> getContactAddresses() {
		return this.contactAddresses;
	}

	public void setContactAddresses(Set<ContactAddress> contactAddresses) {
		this.contactAddresses = contactAddresses;
	}

	public ContactAddress addContactAddress(ContactAddress contactAddress) {
		getContactAddresses().add(contactAddress);
		contactAddress.setEmployeePersonalInformation(this);

		return contactAddress;
	}

	public ContactAddress removeContactAddress(ContactAddress contactAddress) {
		getContactAddresses().remove(contactAddress);
		contactAddress.setEmployeePersonalInformation(null);

		return contactAddress;
	}

	public Set<ContactInformation> getContactInformations() {
		return this.contactInformations;
	}

	public void setContactInformations(Set<ContactInformation> contactInformations) {
		this.contactInformations = contactInformations;
	}

	public ContactInformation addContactInformation(ContactInformation contactInformation) {
		getContactInformations().add(contactInformation);
		contactInformation.setEmployeePersonalInformation(this);

		return contactInformation;
	}

	public ContactInformation removeContactInformation(ContactInformation contactInformation) {
		getContactInformations().remove(contactInformation);
		contactInformation.setEmployeePersonalInformation(null);

		return contactInformation;
	}

	public Set<DependentInformation> getDependentInformations() {
		return this.dependentInformations;
	}

	public void setDependentInformations(Set<DependentInformation> dependentInformations) {
		this.dependentInformations = dependentInformations;
	}

	public DependentInformation addDependentInformation(DependentInformation dependentInformation) {
		getDependentInformations().add(dependentInformation);
		dependentInformation.setEmployeePersonalInformation(this);

		return dependentInformation;
	}

	public DependentInformation removeDependentInformation(DependentInformation dependentInformation) {
		getDependentInformations().remove(dependentInformation);
		dependentInformation.setEmployeePersonalInformation(null);

		return dependentInformation;
	}

	public Set<EmergencyContactInformation> getEmergencyContactInformations() {
		return this.emergencyContactInformations;
	}

	public void setEmergencyContactInformations(Set<EmergencyContactInformation> emergencyContactInformations) {
		this.emergencyContactInformations = emergencyContactInformations;
	}

	public EmergencyContactInformation addEmergencyContactInformation(
			EmergencyContactInformation emergencyContactInformation) {
		getEmergencyContactInformations().add(emergencyContactInformation);
		emergencyContactInformation.setEmployeePersonalInformation(this);

		return emergencyContactInformation;
	}

	public EmergencyContactInformation removeEmergencyContactInformation(
			EmergencyContactInformation emergencyContactInformation) {
		getEmergencyContactInformations().remove(emergencyContactInformation);
		emergencyContactInformation.setEmployeePersonalInformation(null);

		return emergencyContactInformation;
	}

	public Set<OtherInformation> getOtherInformations() {
		return this.otherInformations;
	}

	public void setOtherInformations(Set<OtherInformation> otherInformations) {
		this.otherInformations = otherInformations;
	}

	public OtherInformation addOtherInformation(OtherInformation otherInformation) {
		getOtherInformations().add(otherInformation);
		otherInformation.setEmployeePersonalInformation(this);

		return otherInformation;
	}

	public OtherInformation removeOtherInformation(OtherInformation otherInformation) {
		getOtherInformations().remove(otherInformation);
		otherInformation.setEmployeePersonalInformation(null);

		return otherInformation;
	}

	public Set<PermanentHomeAddress> getPermanentHomeAddresses() {
		return this.permanentHomeAddresses;
	}

	/**
	 * @param permanentHomeAddresses
	 */
	public void setPermanentHomeAddresses(Set<PermanentHomeAddress> permanentHomeAddresses) {
		this.permanentHomeAddresses = permanentHomeAddresses;
	}

	public PermanentHomeAddress addPermanentHomeAddress(PermanentHomeAddress permanentHomeAddress) {
		getPermanentHomeAddresses().add(permanentHomeAddress);
		permanentHomeAddress.setEmployeePersonalInformation(this);

		return permanentHomeAddress;
	}

	public PermanentHomeAddress removePermanentHomeAddress(PermanentHomeAddress permanentHomeAddress) {
		getPermanentHomeAddresses().remove(permanentHomeAddress);
		permanentHomeAddress.setEmployeePersonalInformation(null);

		return permanentHomeAddress;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<WorkAddress> getWorkAddresses() {
		return this.workAddresses;
	}

	public void setWorkAddresses(Set<WorkAddress> workAddresses) {
		this.workAddresses = workAddresses;
	}

	public WorkAddress addWorkAddress(WorkAddress workAddress) {
		getWorkAddresses().add(workAddress);
		workAddress.setEmployeePersonalInformation(this);

		return workAddress;
	}

	public WorkAddress removeWorkAddress(WorkAddress workAddress) {
		getWorkAddresses().remove(workAddress);
		workAddress.setEmployeePersonalInformation(null);

		return workAddress;
	}

	public Set<Multimedia> getMultimedia() {
		return multimedia;
	}

	public void setMultimedia(Set<Multimedia> multimedia) {
		this.multimedia = multimedia;
	}
	public Multimedia addMultimedia(Multimedia multimedia) {
		getMultimedia().add(multimedia);
		multimedia.setEmployeePersonalInformation(this);

		return multimedia;
	}

	public Multimedia removeMultimedia(Multimedia multimedia) {
		getMultimedia().remove(multimedia);
		multimedia.setEmployeePersonalInformation(null);

		return multimedia;
	}
	
	public Set<LieuRequest> getLieuRequest() {
		return lieuRequest;
	}

	public void setLieuRequest(Set<LieuRequest> lieuRequest) {
		this.lieuRequest = lieuRequest;
	}


	public Set<Travel> getTravel() {
		return travel;
	}

	public void setTravel(Set<Travel> travel) {
		this.travel = travel;
	}

	public Travel addTravel(Travel travel) {
		getTravel().add(travel);
		travel.setEmployeePersonalInformation(this);

		return travel;
	}

	public Travel removeTravel(Travel travel) {
		getTravel().remove(travel);
		travel.setEmployeePersonalInformation(null);

		return travel;
	}

}