package com.snigdh.Erp.Model.EmployeeModel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the other_information database table.
 * 
 */
@Entity
@Table(name="other_information")
@NamedQuery(name="OtherInformation.findAll", query="SELECT o FROM OtherInformation o")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property="id", scope = OtherInformation.class)
public class OtherInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;	
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name="driving_liscence_number")
	private String drivingLiscenceNumber;

	@Column(name="insurance_policy_number")
	private String insurancePolicyNumber;

	@Column(name="major_alergy_or_sickness")
	private String majorAlergyOrSickness;

	@Column(name="other_professional_training_courses")
	private String otherProfessionalTrainingCourses;

	@Column(name="pan_number")
	private String panNumber;

	@Column(name="past_organisations")
	private String pastOrganisations;

	private String preferences;
	
	@Column(name="blood_group")
	private String bloodGroup;
	
	@Column(name="termination_date")
	private String terminationDate;
	
	private String ethnicity;
	
	private String religion;

	//bi-directional many-to-one association to EmployeePersonalInformation
	@ManyToOne
	@JsonIgnore
	private EmployeePersonalInformation employeePersonalInformation;

	public OtherInformation() {
		this.updatedAt=new Date();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getDrivingLiscenceNumber() {
		return this.drivingLiscenceNumber;
	}

	public void setDrivingLiscenceNumber(String drivingLiscenceNumber) {
		this.drivingLiscenceNumber = drivingLiscenceNumber;
	}

	public String getInsurancePolicyNumber() {
		return this.insurancePolicyNumber;
	}

	public void setInsurancePolicyNumber(String insurancePolicyNumber) {
		this.insurancePolicyNumber = insurancePolicyNumber;
	}

	public String getMajorAlergyOrSickness() {
		return this.majorAlergyOrSickness;
	}

	public void setMajorAlergyOrSickness(String majorAlergyOrSickness) {
		this.majorAlergyOrSickness = majorAlergyOrSickness;
	}

	public String getOtherProfessionalTrainingCourses() {
		return this.otherProfessionalTrainingCourses;
	}

	public void setOtherProfessionalTrainingCourses(String otherProfessionalTrainingCourses) {
		this.otherProfessionalTrainingCourses = otherProfessionalTrainingCourses;
	}

	public String getPanNumber() {
		return this.panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getPastOrganisations() {
		return this.pastOrganisations;
	}

	public void setPastOrganisations(String pastOrganisations) {
		this.pastOrganisations = pastOrganisations;
	}

	public String getPreferences() {
		return this.preferences;
	}

	public void setPreferences(String preferences) {
		this.preferences = preferences;
	}
	
	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public EmployeePersonalInformation getEmployeePersonalInformation() {
		return this.employeePersonalInformation;
	}

	public void setEmployeePersonalInformation(EmployeePersonalInformation employeePersonalInformation) {
		this.employeePersonalInformation = employeePersonalInformation;
	}
	
	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}


}