package com.snigdh.Erp.Model.TravelModel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "travel_budget_code")
@NamedQuery(name = "TravelBudgetCode.findAll", query = "SELECT t FROM TravelBudgetCode t")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TravelBudgetCode.class)
public class TravelBudgetCode implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "cost_center")
	private String costCenter;

	@Column(name = "source_of_fund")
	private String sourceOfFund;
	
	@Column(name = "activity")
	private String activity;
	
	@Column(name = "project")
	private String project;
	
	@Column(name = "analysis")
	private String analysis;
	

	
	@ManyToOne
	@JsonIgnore
	private Travel travel;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Travel getTravel() {
		return travel;
	}


	public void setTravel(Travel travel) {
		this.travel = travel;
	}



	public String getAnalysis() {
		return analysis;
	}


	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}


	public String getCostCenter() {
		return costCenter;
	}


	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}


	public String getSourceOfFund() {
		return sourceOfFund;
	}


	public void setSourceOfFund(String sourceOfFund) {
		this.sourceOfFund = sourceOfFund;
	}


	public String getActivity() {
		return activity;
	}


	public void setActivity(String activity) {
		this.activity = activity;
	}


	public String getProject() {
		return project;
	}


	public void setProject(String project) {
		this.project = project;
	}
	
	

}
