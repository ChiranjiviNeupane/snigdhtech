package com.snigdh.Erp.Controller.NotificationController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Service.NotificationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.NOTIFICATION)
public class NotificationController {

	@Autowired
	NotificationService notificationService;
	
	@GetMapping(value = "/getNotification")
	public void getNotification() {
		notificationService.getNotification();
	}
}
