package com.snigdh.Erp.Controller.OfficeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.Office.SourceOfFund;
import com.snigdh.Erp.Repository.OfficeRepository.SourceOfFundRepository;
import com.snigdh.Erp.Service.OfficeService.SourceOfFundService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.SOURCE_OF_FUND)
public class SourceOfFundController {
	@Autowired
	SourceOfFundRepository sourceFundRepo;
	
	@Autowired
	SourceOfFundService sourceFundService;
	
	@PostMapping(value = "/{costCenter}/save")
	public void save(@Valid @PathVariable(value="costCenter") Integer id, @RequestBody SourceOfFund sourceFund) {
		sourceFundService.save(sourceFund, id);
	}

	@GetMapping(value = "/findall")
	public List<SourceOfFund> findAll() {
		return sourceFundService.findAll();
	}

	@GetMapping(value = "/findone/{id}")
	public SourceOfFund retrieve(@PathVariable(value = "id") Integer id) {
		return sourceFundService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteContactAddress(@PathVariable SourceOfFund id) {
		sourceFundService.deleteValue(id);
	}

	@PutMapping(value = "/{id}/costCenter/{costCenter}/update")
	public SourceOfFund updateContactAddress(@PathVariable(value="id") Integer id, @PathVariable(value="costCenter") Integer costCenter,@RequestBody  SourceOfFund sourceFund) {
		return sourceFundService.updateValue(costCenter,id,sourceFund);
	}
	
	@GetMapping(value = "/findAll/travelRequest/costCenter/{costCenterId}")
	public List<Object> findTravelRequest(@PathVariable Integer costCenterId){
		return sourceFundService.findForTravelRequest(costCenterId);
	}
	
	@GetMapping(value = "/findAllListing")
	public List<Object> findListing(){
		return sourceFundService.findForListing();
	}

}
