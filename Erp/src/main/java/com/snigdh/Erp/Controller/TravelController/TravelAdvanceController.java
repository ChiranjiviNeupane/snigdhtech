package com.snigdh.Erp.Controller.TravelController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.TravelModel.TravelAdvance;
import com.snigdh.Erp.Service.TravelService.TravelAdvanceService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.TRAVEL + "/advance")
public class TravelAdvanceController {

	@Autowired
	TravelAdvanceService travelTransitService;

	@GetMapping(value = "/findall")
	public List<TravelAdvance> findAll() {
		return travelTransitService.findAll();
	}

	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody TravelAdvance otherInformation) {
		travelTransitService.save(otherInformation);
	}

	/*
	 * @GetMapping(value = "/findone/{id}") public Travel
	 * retrieve(@PathVariable(value = "id") Integer id) { return
	 * travelService.retrieve(id); }
	 */

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable TravelAdvance id) {
		travelTransitService.delete(id);
	}

	@PutMapping(value = "/update/{id}")
	public TravelAdvance update(@RequestBody TravelAdvance otherInformation, @PathVariable Integer id) {
		return travelTransitService.update(otherInformation, id);
	}

}
