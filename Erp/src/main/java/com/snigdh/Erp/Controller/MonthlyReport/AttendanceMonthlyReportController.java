package com.snigdh.Erp.Controller.MonthlyReport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Service.MonthlyReportService.AttendanceMonthlyReportService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.ATTMONREP)
public class AttendanceMonthlyReportController {

	@Autowired
	AttendanceMonthlyReportService attendanceService;
	
	@GetMapping(value = "/getMonthlyAttendance/{id}/{year}/{month}")
	public Object getMonthlyAttendance(@PathVariable(value = "id") int id, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return attendanceService.getMonthlyAttendance(id, year, month);
	}
	
	@GetMapping(value = "/getMonthlyAttendanceAllEmp/{year}/{month}")
	public Object getMonthlyAttendanceAllEmp(@PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return attendanceService.getMonthlyAttendanceAllEm(year, month);
	}
	
	@GetMapping(value = "/getMonthlyAttendanceByDept/{department}/{year}/{month}")
	public Object getMonthlyAttendanceByDept(@PathVariable(value = "department") String department, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return attendanceService.getMonthlyAttendanceByDept(department,year, month);
	}
	
	@GetMapping(value = "/getMonthlyAttendanceByDeptm/{department}/{year}/{month}")
	public Object getMonthlyAttendanceByDeptm(@PathVariable(value = "department") String department, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return attendanceService.getMonthlyAttendanceByDeptm(department,year, month);

	}
	
	@GetMapping(value = "/getMonthlyAttendMultipleId/{id}/{year}/{month}")
	public Object getMonthlyAttendMultipleId(@PathVariable(value = "id") String id, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return attendanceService.getMonthlyAttendMultipleId(id, year, month);
	}
	
	
}
