package com.snigdh.Erp.Controller.AttendanceController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.AttendanceModel.Attendance;
import com.snigdh.Erp.Repository.AttendanceRepository.AttendanceRepository;
import com.snigdh.Erp.Service.AttendanceService.AttendanceService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.ATTENDANCE)
public class AttendanceController {
	
	@Autowired
	AttendanceRepository repo;
	
	@Autowired
	AttendanceService attendanceService;
		
		
	@PostMapping(value = "/checkin")	
	public void getCheckInTime(Attendance attendanceObj) {		
		attendanceService.saveValue(attendanceObj);				
	}
	
	@PutMapping(value = "/checkout")
	public void getCheckOutTime(Attendance attendance) {
		attendanceService.updateValue(attendance);
	}

	@GetMapping(value="/myAttendance")
	public Object getAttendance() {
		return attendanceService.myAttendance();
	}

	@GetMapping(value="/allAttendance")
	public Object getAllAttendance() {
		return attendanceService.allAttendance();
	}
	
	@GetMapping(value="/myAttendanceStatus")
	public Object getmyAttendanceStatus() {
		return attendanceService.attendanceStatusWithRole();
	}

}
