package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.WorkAddress;
import com.snigdh.Erp.Service.EmployeeService.WorkAddressService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.WORK_ADDR)
public class WorkAddressController {

	@Autowired
	WorkAddressService workAddressService;

	@GetMapping(value = "/findall")
	public List<WorkAddress> findAll() {
		return workAddressService.findAll();
	}

	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody WorkAddress workAddress) {
		workAddressService.save(workAddress);
	}

	@GetMapping(value = "/findone/{id}")
	public WorkAddress retrieve(@PathVariable(value = "id") Integer id) {
		return workAddressService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable WorkAddress id) {
		workAddressService.delete(id);
	}

	@PutMapping(value = "/update/{id}")
	public WorkAddress update(@RequestBody WorkAddress workAddress, @PathVariable Integer id) {
		return workAddressService.update(workAddress, id);
	}

}
