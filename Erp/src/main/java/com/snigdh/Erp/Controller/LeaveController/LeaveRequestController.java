package com.snigdh.Erp.Controller.LeaveController;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.LeaveRequest;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.LeaveExtraServices;
import com.snigdh.Erp.Service.LeaveService.LeaveRequestService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.LEAVEREQUEST)
public class LeaveRequestController {

	@Autowired
	LeaveRequestService leaveRequstService;

	@Autowired
	UtilityService utilityService;

	@Autowired
	private UserService generalService;

	@Autowired
	LeaveRequestRepository requestRepository;

	@Autowired
	LeaveExtraServices extraService;

	// Line Manager Get Pending Leave Requests
	// Get Method For Leave Request line manager
	@GetMapping(value = "/getLeaveRequest")
	public Object getLeaveRequest() {
		int id = generalService.getUserId();
		return leaveRequstService.getLeaveRequest(id, "Pending");
	}

	// Line Manager Get All Leave responses
	@GetMapping(value = "/getAllLeaveResponses")
	public Object getAllLeaveResponses() {
		int id = generalService.getUserId();
		return extraService.getAllLeaveResponses(id);
	}

	// employee gets all his requested leave
	@GetMapping(value = "/getMyLeaveRequest")
	public Object getMyLeaveRequest() {
		int id = generalService.getUserId();
		return leaveRequstService.getMyLeaveRequest(id); 
	}

	// employee gets all his requested leave
	@GetMapping(value = "/getEmployeeById/{id}")
	public Object getEmployeeById(@PathVariable(value = "id") Integer id) {
		return leaveRequstService.getMyLeaveRequest(id);
	}

	// Employee On Leave
	@GetMapping(value = "/getEmployeeOnLeave")
	public Object getEmployeeOnLeave() {
		return leaveRequstService.getEmployeeOnLeave();
	}

	// get logged in employee all Leave Date
	@GetMapping(value = "/getEmployeeLeaveDate")
	public Object getEmployeeLeaveDate() {
		int id = generalService.getUserId();
		return leaveRequstService.getEmployeeLeaveDate(id);
	}

	// HR Can Leave Request Any Employee By Id
	@PostMapping(value = "/makeLeaveRequest/{id}")
	public ResponseEntity<String> saveLeaveRequestHr(@Valid @RequestBody LeaveRequest leavereq,
			@PathVariable(value = "id") Integer id) {
		return leaveRequstService.saveLeaveRequest(leavereq, id);
	}

	// Save Employee leave Request
	@PostMapping(value = "/makeLeaveRequest")
	public ResponseEntity<String> saveLeaveRequestEmp(@Valid @RequestBody LeaveRequest leavereq) {
		int id = generalService.getUserId();
		return leaveRequstService.saveLeaveRequest(leavereq, id);
	}

	// Line Manager Accept/Reject Leave Request
	@PutMapping(value = "/updateLeaveRequest/{id}")
	public ResponseEntity<String> updateLeaveRequest(@Valid @RequestBody LeaveRequest leavereq,
			@PathVariable Integer id) {
		return leaveRequstService.updateLeaveRequest(leavereq, id);
	}

	// cancel leave request
	@PutMapping(value = "/cancelLeaveRequest/{id}")
	public ResponseEntity<String> cancelLeaveReq(@Valid @RequestBody(required = false) LeaveRequest leavereq,
			@PathVariable(value = "id") Integer id) {
		return leaveRequstService.cancelLeaveRequest(leavereq, id);
	}
	
	// cancel leave request
		@PutMapping(value = "/cancelLeaveRequestPartial/{id}")
		public ResponseEntity<String> cancelLeaveRequestPartial(@Valid @RequestBody(required = false) LeaveRequest leavereq,
				@PathVariable(value = "id") Integer id) {
			return leaveRequstService.cancelLeaveRequestPartial(leavereq, id);
		}

	// Extend leave request
	@PutMapping(value = "/extendLeaveRequest/{id}")
	public ResponseEntity<String> extendLeaveRequest(@Valid @RequestBody LeaveRequest leavereq,
			@PathVariable Integer id) {
		return leaveRequstService.extendedLeaveRequest(leavereq, id);
	}

	// update extended leave
	@PutMapping(value = "/updateExtendLeave/{id}")
	public void updateExtendLeave(@Valid @RequestBody LeaveRequest leavereq, @PathVariable Integer id) {
		leaveRequstService.updateExtendLeave(leavereq, id);
	}

	// Leave Cancel Authorization
	@PutMapping(value = "/cancelLeaveAuthorization/{id}")
	public void cancelLeaveAuthorization(@Valid @RequestBody LeaveRequest leavereq, @PathVariable Integer id) {
		leaveRequstService.cancelLeaveAuthorization(leavereq, id);
	}

	// find by id
	@GetMapping(value = "/findRequestById/{id}")
	public Object findMyRequest(@PathVariable(value = "id") Integer id) {
		return leaveRequstService.findRequestById(id);
	}
	
	//find by id year month
	@GetMapping(value = "/getMonthlyLeave/{id}/{year}/{month}")
	public Object getMonthlyLeave(@PathVariable(value = "id") int id, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return leaveRequstService.getAllMonthlyLeaveDate(id, year, month);
	}
	
	@GetMapping(value = "/getMonthlyLeaveAllEmp/{year}/{month}")
	public Object getMonthlyLeaveMultipleId(@PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return leaveRequstService.getMonthlyLeaveAllEmp(year, month);
	}
	
	@GetMapping(value = "/getMonthlyLeaveByDeptm/{department}/{year}/{month}")
	public Object getMonthlyLeaveByDeptm(@PathVariable(value = "department") String department, @PathVariable(value = "year") String year,
			@PathVariable(value = "month") String month) {
		return leaveRequstService.getMonthlyLeaveByDeptm(department,year, month);
	}

}
