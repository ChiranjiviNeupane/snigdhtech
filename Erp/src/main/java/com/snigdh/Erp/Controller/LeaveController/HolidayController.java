package com.snigdh.Erp.Controller.LeaveController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.Holiday;
import com.snigdh.Erp.Service.LeaveService.HolidayService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.HOLIDAY)
public class HolidayController {

	@Autowired
	HolidayService holidayService;

	// save holiday
	@PostMapping(value = "/saveHoliday")
	public String saveHoliday(@Valid @RequestBody Holiday holiday) {
		return holidayService.saveHoliday(holiday);
	}

	// update holiday
	@PutMapping(value = "/updateHoliday/{id}")
	public void updateHoliday(@Valid @RequestBody Holiday holiday, @PathVariable(value = "id") Integer id) {
		holidayService.updateHoliday(holiday, id);
	}

	// get holiday
	@GetMapping(value = "/getHoliday")
	public List<Holiday> findHoliday() {
		return holidayService.getHoliday();
	}
	
	//get holiday dates
	@GetMapping(value = "/getHolidayDates")
	public Object getHolidayDates() {
		return holidayService.getHolidayDates();
	}

	// delete holiday
	@DeleteMapping(value = "/deleteHoliday/{id}")
	public void deleteHoliday(@PathVariable(value = "id") Integer id) {
		holidayService.deleteHoliday(id);
	}
}