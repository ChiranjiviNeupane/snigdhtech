package com.snigdh.Erp.Controller.EmployeeController;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.CompanyInformation;
import com.snigdh.Erp.Service.EmployeeService.CompanyInformationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.COMP_INFO)
public class CompanyInformationController {
	
	@Autowired
	CompanyInformationService ciservice;
	
	@GetMapping(value = "/findall")
	public List<CompanyInformation> findAll(){
		return ciservice.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody CompanyInformation compInfoObj) {
		ciservice.saveValue(compInfoObj);
	}
	
	@GetMapping(value = "/findone/{id}")
	public HashMap<String, Object> retrieve(@PathVariable(value = "id") Integer id) {
		return ciservice.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable CompanyInformation id) {
		ciservice.deleteValue(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public CompanyInformation updateValue(@RequestBody CompanyInformation companyInformation,@PathVariable Integer id) {
		return ciservice.updateValue(companyInformation, id);
	}
	
	
}
