package com.snigdh.Erp.Controller.OfficeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.Office.CostCenter;
import com.snigdh.Erp.Repository.OfficeRepository.CostCenterRepository;
import com.snigdh.Erp.Service.OfficeService.CostCenterService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.COST_CENTER)
public class CostCenterController {
	@Autowired
	CostCenterRepository costCenterRepo;
	
	@Autowired
	CostCenterService costCenterService;
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody CostCenter costCenter) {
		costCenterService.save(costCenter);
	}

	@GetMapping(value = "/findall")
	public List<CostCenter> findAll() {
		return costCenterService.findAll();
	}

	@GetMapping(value = "/findone/{id}")
	public CostCenter retrieve(@PathVariable(value = "id") Integer id) {
		return costCenterService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteContactAddress(@PathVariable CostCenter id) {
		costCenterService.deleteValue(id);
	}

	@PutMapping(value = "/update/{id}")
	public CostCenter updateContactAddress(@RequestBody CostCenter costCenter, @PathVariable Integer id) {
		return costCenterService.updateValue(costCenter, id);
	}
	
	@GetMapping(value = "findAll/travelRequest")
	public List<Object> findTravelRequest(){
		return costCenterService.findForTravelRequest();
	}
}
