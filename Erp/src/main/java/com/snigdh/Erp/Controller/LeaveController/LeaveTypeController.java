package com.snigdh.Erp.Controller.LeaveController;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.LeaveType;
import com.snigdh.Erp.Service.LeaveService.LeaveTypeService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.LEAVETYPE)
public class LeaveTypeController {

	@Autowired
	LeaveTypeService leaveTypeService;

	//find by id
	@GetMapping(value = "/findById/{id}")
	public Optional<LeaveType> findByid(@PathVariable Integer id) {
		return leaveTypeService.findById(id);
	}
	
	// save type of leave
	@PostMapping(value = "/saveLeaveType")
	public String saveLeaveType(@Valid @RequestBody LeaveType leaveType) {
		return leaveTypeService.saveLeaveType(leaveType);
	}

	// update type of leave
	@PutMapping(value = "/updateLeaveType/{id}")
	public void updateLeaveType(@RequestBody LeaveType leaveType, @PathVariable Integer id) {
		leaveTypeService.updateLeaveType(leaveType, id);
	}

	// delete type of leave
	@DeleteMapping(value = "/deleteLeaveType/{id}")
	public void deleteLeaveType(@PathVariable LeaveType id) {
		leaveTypeService.deleteLeaveType(id);
	}

	// find type of leave using type name
	@PostMapping(value = "/findByLeaveType")
	public LeaveType findByleaveType(@Valid @RequestBody LeaveType leaveType) {
		return leaveTypeService.findByleaveType(leaveType);
	}

	// find all type of leave
	@GetMapping(value = "/findAllLeave")
	public List<LeaveType> findall() {
		return leaveTypeService.findall();
	}
}
