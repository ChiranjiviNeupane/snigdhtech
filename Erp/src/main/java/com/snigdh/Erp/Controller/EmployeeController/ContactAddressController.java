package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.ContactAddress;
import com.snigdh.Erp.Repository.EmployeeRepository.ContactAddressRepository;
import com.snigdh.Erp.Service.EmployeeService.ContactAddressService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.CONT_ADDR)
public class ContactAddressController {
	@Autowired
	ContactAddressRepository contactAddressRepository;
	
	@Autowired
	ContactAddressService caService;

	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody ContactAddress contactAddressObj) {
		caService.saveValue(contactAddressObj);
	}

	@GetMapping(value = "/findall")
	public List<ContactAddress> findAll() {
		return caService.findAll();
	}

	@GetMapping(value = "/findone/{id}")
	public ContactAddress retrieve(@PathVariable(value = "id") Integer id) {
		return caService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteContactAddress(@PathVariable ContactAddress id) {
		caService.deleteContactAddress(id);
	}

	@PutMapping(value = "/update/{id}")
	public ContactAddress updateContactAddress(@RequestBody ContactAddress contactAddress, @PathVariable Integer id) {
		return caService.updateContactAddress(contactAddress, id);
	}
}