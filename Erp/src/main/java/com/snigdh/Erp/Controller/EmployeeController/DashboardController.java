package com.snigdh.Erp.Controller.EmployeeController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Service.EmployeeService.DashboardService;


@RestController
public class DashboardController {
	
	@Autowired
	DashboardService dash;
	
	@GetMapping(value = "/hrm/dashboard")
	public Object getDashboardContent() {
		return dash.getDashBoard();
	}
	

}
