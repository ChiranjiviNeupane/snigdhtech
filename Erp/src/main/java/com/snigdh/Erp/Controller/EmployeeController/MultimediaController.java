package com.snigdh.Erp.Controller.EmployeeController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.snigdh.Erp.Model.EmployeeModel.Multimedia;
import com.snigdh.Erp.Repository.EmployeeRepository.MultimediaRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.EmployeeService.MultimediaService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.MULMEDIA)
public class MultimediaController {

	@Autowired
	MultimediaService mulService;

	
	//for local storage of image
	@Autowired
	UserService user;

	@Autowired
	MultimediaRepository repo;

	private static String UPLOADED_FOLDER = "C://Users//SnigdhTech_1//Desktop//Snigdh_Project//snigdh_erp_backend//Erp//src//main//resources//static//userImages//";

	@PostMapping("/upload")
	public ResponseEntity<String> uploadData(@RequestParam("file") MultipartFile file) throws Exception {

		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}

		String originalName = file.getOriginalFilename();
		Multimedia mul = new Multimedia(UPLOADED_FOLDER + originalName, user.getUserId());
		repo.save(mul);
		saveUploadedFiles(Arrays.asList(file));
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}

	private void saveUploadedFiles(List<MultipartFile> files) throws IOException {

		for (MultipartFile file : files) {

			if (file.isEmpty()) {
				continue;
			}

			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

		}

	}
	//end of local storage of image

	
	
	@GetMapping(value = "/findall")
	public List<Multimedia> findAll() {
		return mulService.findAll();
	}

	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody Multimedia compInfoObj) {
		mulService.saveValue(compInfoObj);
	}

	@GetMapping(value = "/findone/{id}")
	public Multimedia retrieve(@PathVariable(value = "id") Integer id) {
		return mulService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable Multimedia id) {
		mulService.deleteValue(id);
	}

	@PutMapping(value = "/update/{id}")
	public Multimedia updateValue(@RequestBody Multimedia companyInformation, @PathVariable Integer id) {
		return mulService.updateValue(companyInformation, id);
	}

}
