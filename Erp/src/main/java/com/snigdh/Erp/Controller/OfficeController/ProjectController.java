package com.snigdh.Erp.Controller.OfficeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.Office.Project;
import com.snigdh.Erp.Repository.OfficeRepository.ProjectRepository;
import com.snigdh.Erp.Service.OfficeService.ProjectService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.PROJECT)
public class ProjectController {
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	ProjectService projectService;
	
	@PostMapping(value = "/{sourceOfFund}/save")
	public void save(@Valid @PathVariable(value="sourceOfFund") Integer id, @RequestBody Project project) {
		projectService.save(project,id);
	}

	@GetMapping(value = "/findall")
	public List<Project> findAll() {
		return projectService.findAll();
	}

	@GetMapping(value = "/findone/{id}")
	public Project retrieve(@PathVariable(value = "id") Integer id) {
		return projectService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteContactAddress(@PathVariable Project id) {
		projectService.deleteValue(id);
	}

	@PutMapping(value = "/{id}/sourceOfFund/{sourceFund}/update")
	public Project updateContactAddress(@RequestBody Project project, @PathVariable(value="id") Integer id, @PathVariable(value="sourceFund") Integer sourceFund) {
		return projectService.updateValue(project, id,sourceFund);
	}
	
	@GetMapping(value = "/findAll/travelRequest/sourceOfFund/{fundId}")
	public List<Object> findTravelRequest(@PathVariable Integer fundId){
		return projectService.findForTravelRequest(fundId);
	}
	
	@GetMapping(value = "/findAllListing")
	public List<Object> findListing(){
		return projectService.findForListing();
	}


}
