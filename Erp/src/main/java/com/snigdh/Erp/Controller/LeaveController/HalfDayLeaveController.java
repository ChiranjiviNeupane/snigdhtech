package com.snigdh.Erp.Controller.LeaveController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.HalfDayLeave;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.HalfDayLeaveService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.HALFDAYLEAVE)
public class HalfDayLeaveController {

	@Autowired
	UtilityService utilityService;

	@Autowired
	private UserService generalService;

	@Autowired
	private HalfDayLeaveService halfDayLeaveService;

	// Employee leave Request
	@PostMapping(value = "/halfDayLeaveRequest")
	public ResponseEntity<String> saveLeaveRequestEmp(@Valid @RequestBody HalfDayLeave halfLeaveRequest) {
		int id = generalService.getUserId();
		return halfDayLeaveService.saveLeaveRequest(halfLeaveRequest, id);
	}

	// Line Manager Accept/Reject Leave Request
	@PutMapping(value = "/updateHalfDayLeave/{id}")
	public void updateLeaveRequest(@Valid @RequestBody HalfDayLeave leavereq, @PathVariable Integer id) {
		halfDayLeaveService.updateLeaveRequest(leavereq, id);
	}

	@GetMapping(value = "/getLeave")
	public List<HalfDayLeave> getleave(Integer id) {
		return halfDayLeaveService.getLeave(id);
	}

	// find by id
	@GetMapping(value = "/findRequestById/{id}")
	public Object findMyRequest(@PathVariable(value = "id") Integer id) {
		return halfDayLeaveService.findRequestById(id);
	}
}
