package com.snigdh.Erp.Controller.EmployeeController;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.Authorization;
import com.snigdh.Erp.Service.EmployeeService.AuthorizationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.AUTHORIZATION)
public class AuthorizationController {
	
	@Autowired
	AuthorizationService authorizationService;
	
	//Get
	@GetMapping(value = "/getValues")
	public Object getValues() {
		return authorizationService.getValues();
	}
	
	//save
	@PostMapping(value = "/saveValues")
	public void saveValues(@Valid @RequestBody Authorization authorization) {
		authorizationService.saveValues(authorization);;
	}
	
	//update
	@PutMapping(value = "/updateValues/{id}")
	public void updateValues(@Valid @RequestBody Authorization authorization,@PathVariable(value = "id") Integer id) {
		authorizationService.updateValues(authorization, id);
	}

}
