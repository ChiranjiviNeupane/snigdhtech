package com.snigdh.Erp.Controller.OfficeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.Office.Activity;
import com.snigdh.Erp.Repository.OfficeRepository.ActivityRepository;
import com.snigdh.Erp.Service.OfficeService.ActivityService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.ACTIVITY)
public class ActivityController {
	
	@Autowired
	ActivityRepository activityRepo;
	
	@Autowired
	ActivityService activityService;
	
	@PostMapping(value = "/{project}/save")
	public void save(@Valid @PathVariable(value="project") Integer id, @RequestBody Activity activity) {
		activityService.save(activity,id);
	}

	@GetMapping(value = "/findall")
	public List<Activity> findAll() {
		return activityService.findAll();
	}

	@GetMapping(value = "/findone/{id}")
	public Activity retrieve(@PathVariable(value = "id") Integer id) {
		return activityService.retrieve(id);
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deleteContactAddress(@PathVariable Activity id) {
		activityService.deleteValue(id);
	}

	@PutMapping(value = "/{id}/project/{projectId}/update")
	public Activity updateContactAddress(@RequestBody Activity activity, @PathVariable(value="id") Integer id,@PathVariable(value="projectId") Integer projectId) {
		return activityService.updateValue(activity, id,projectId);
	}
	
	@GetMapping(value = "/findAll/travelRequest/project/{projectId}")
	public List<Object> findTravelRequest(@PathVariable Integer projectId){
		return activityService.findForTravelRequest(projectId);
	}
	
	@GetMapping(value = "/findAllListing") 
	public List<Object> findListing(){
		return activityService.findForListing();
	}

}
