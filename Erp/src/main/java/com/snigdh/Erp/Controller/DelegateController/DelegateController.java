package com.snigdh.Erp.Controller.DelegateController;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.Delegate.Delegate;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.DelegateService.DelegateService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.DELEGATE)
public class DelegateController {

	@Autowired
	DelegateService delegateService;

	@Autowired
	UtilityService utilityService;

	@Autowired
	private UserService generalService;

	@PostMapping(value = "/saveDelegate")
	public String saveDelegation(@Valid @RequestBody Delegate delegation) {
		return delegateService.saveDelegation(delegation);
	}

	// employee gets details of all delegetes which is delegated to other by him
	@GetMapping(value = "/getDelegateToOther")
	public Object getDelegateToOther() {
		return delegateService.getDelegateToOther();
	}

	// employee gets details of all delegetes which is delegated to him by other
	@GetMapping(value = "/getDelegateByOther")
	public Object getDelegateByOther() {
		return delegateService.getDelegateByOther();
	}

	/*
	 * // Accept/Reject Delegate Request
	 * 
	 * @PutMapping(value = "/updateDelegate/{id}") public void
	 * updateLeaveRequest(@Valid @RequestBody Delegate delegate, @PathVariable
	 * Integer id) { delegateService.updateDelegate(delegate, id); }
	 */
	
	//Cancel Delegation
	@PutMapping(value = "/cancelDelegate/{id}")
	public String CancelDelegate(@PathVariable(value = "id") Integer id) {
		return delegateService.CancelDelegate(id);
	}
	
	//Extend Delegation
	@PutMapping(value = "/extendDelegate/{id}")
	public String extendDelegate(@Valid @RequestBody Delegate delegation, @PathVariable(value = "id") Integer id) {
		return delegateService.extendDelegate(delegation, id);
	}

}
