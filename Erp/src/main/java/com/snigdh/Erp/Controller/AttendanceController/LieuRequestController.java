package com.snigdh.Erp.Controller.AttendanceController;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.AttendanceModel.LieuRequest;
import com.snigdh.Erp.Service.AttendanceService.LieuRequestService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.LIEUREQUEST)
public class LieuRequestController {

	@Autowired
	LieuRequestService lieuRequestService;

	// request line manager to work on holiday
	@PostMapping(value = "/saveRequest")
	public ResponseEntity<String> saveRequest(@Valid @RequestBody LieuRequest lieuRequest) {
		return lieuRequestService.saveRequest(lieuRequest);
	}
	
	@PutMapping(value = "/updateRequest")
	public void updateRequest(@Valid @RequestBody LieuRequest lieuRequest, @PathVariable(value = "id") Integer id) {
		lieuRequestService.updateRequest(lieuRequest, id);
	}
	
	//get method for line manager
	@GetMapping(value = "/getLeaveRequest")
	public Object getLieuResponses() {
		return null;//lieuRequestService.getLieuResponses();
	}
	
	//Cancel Request
	public void cancelRequest(@PathVariable(value = "id") Integer id) {
		lieuRequestService.cancelRequest(id);
	}
}