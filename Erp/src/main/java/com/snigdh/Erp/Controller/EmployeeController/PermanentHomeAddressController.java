package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.PermanentHomeAddress;
import com.snigdh.Erp.Service.EmployeeService.PermanentHomeAddressService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.PERM_HOME_ADDR)
public class PermanentHomeAddressController {

	@Autowired
	PermanentHomeAddressService homeAddressService;
	
	@GetMapping(value = "/findall")
	public List<PermanentHomeAddress> findAll(){
		return homeAddressService.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody PermanentHomeAddress address) {
		homeAddressService.save(address);
	}
	
	@GetMapping(value = "/findone/{id}")
	public PermanentHomeAddress retrieve(@PathVariable(value="id") Integer id) {
		return homeAddressService.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable PermanentHomeAddress id) {
		homeAddressService.delete(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public PermanentHomeAddress update(@RequestBody PermanentHomeAddress permanentHomeAddress, @PathVariable Integer id) {
		return homeAddressService.update(permanentHomeAddress, id);
	}	
}
