package com.snigdh.Erp.Controller.TravelController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.TravelModel.TravelItenary;
import com.snigdh.Erp.Service.TravelService.TravelItenerayService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.TRAVEL+"/itenary")
public class TravelItenaryController {
	
	@Autowired
	TravelItenerayService travelItenaryService;
	
	@GetMapping(value = "/findall")
	public List<TravelItenary> findAll(){
		return travelItenaryService.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody TravelItenary otherInformation) {
		travelItenaryService.save(otherInformation);
	}
	
/*	@GetMapping(value = "/findone/{id}")
	public Travel retrieve(@PathVariable(value = "id") Integer id) {
		return travelService.retrieve(id);
	}*/
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable TravelItenary id) {
		travelItenaryService.delete(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public TravelItenary update(@RequestBody TravelItenary otherInformation, @PathVariable Integer id) {
		return travelItenaryService.update(otherInformation, id);
	}

}
