package com.snigdh.Erp.Controller.TravelController;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.TravelModel.TravelBudgetCode;
import com.snigdh.Erp.Repository.TravelRepository.TravelBudgetCodeRepository;

@RestController
@RequestMapping(value = "/hrm/test")
public class TravelBudgetCodeController {
	@Autowired
	TravelBudgetCodeRepository budgetRepo;
	
	@GetMapping(value = "/findall")
	public List<TravelBudgetCode> findAll(){
		return budgetRepo.findAll();
	}
	
	@PostMapping(value = "/save")
	public String save(@Valid @RequestBody TravelBudgetCode travel) {
		budgetRepo.save(travel);
		return "success";
	}
	
	@GetMapping(value = "/findone/{id}")
	public Optional<TravelBudgetCode> retrieve(@PathVariable(value = "id") Integer id) {
		return budgetRepo.findById(id);
	}
	
	@GetMapping(value = "/findOneDetail/{id}")
	public Object retrieveDetail(@PathVariable(value = "id") Integer id) {
//		 Optional<TravelBudgetCode> code = budgetRepo.findById(id);
		 Object test=budgetRepo.getCostCenter();
//		 System.out.println();
		 return test;
	}

}
