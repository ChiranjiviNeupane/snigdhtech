package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.DependentInformation;
import com.snigdh.Erp.Service.EmployeeService.DependentInformationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.DEPND_INFO)
public class DependentInformationController {
	@Autowired
	DependentInformationService diService;
	
	@GetMapping(value = "/findall")
	public List<DependentInformation> findAll() {
		return diService.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody DependentInformation dependentInformation) {
		diService.save(dependentInformation);
	}
	
	@GetMapping(value = "/findone/{id}")
	public DependentInformation retrieve(@PathVariable(value = "id") Integer id ) {
		
		return diService.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable DependentInformation id) {
		diService.delete(id);
	}
	
	
	@PutMapping(value = "/update/{id}")
	public DependentInformation updateValue(@RequestBody DependentInformation dependentInformation,@PathVariable Integer id){
		return diService.updateValue(dependentInformation, id);	
	}
	
}
