package com.snigdh.Erp.Controller.LeaveController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.LeaveRemaining;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRemainingRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveRequestRepository;
import com.snigdh.Erp.Repository.LeaveRepository.LeaveTypeRepository;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.LeaveService.LeaveRemainingService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.LEAVEREMAINING)
public class LeaveRemainingController {

	@Autowired
	EmployeePersonalInformationRepository empRepo;

	@Autowired
	LeaveRemainingService leaveRemainingService;

	@Autowired
	LeaveRequestRepository request;

	@Autowired
	LeaveRemainingRepository leaveRemainingRepository;

	@Autowired
	LeaveTypeRepository leaveTypeRepo;

	@Autowired
	private UserService generalService;

	// find all remaining leave
	@GetMapping(value = "/findAllRemainingLeave")
	public List<Object> findAllRemainingLeave() {
		return leaveRemainingService.findAllRemainingLeave();
	}

	// find remaining leave of desired employee
	@GetMapping(value = "/findRemainingLeave/{eid}")
	public Object findRemainingLeaveById(@PathVariable(value = "eid") Integer eid) {
		return leaveRemainingService.findRemainingLeaveById(eid);
	}

	// find my remaining leave
	@GetMapping(value = "/findMyRemainingLeave")
	public Object findEmpRemainingLeave() {
		int eid = generalService.getUserId();
		return leaveRemainingService.findRemainingLeaveById(eid);
	}

	// save remaining leave by hr using emp id
	@PostMapping(value = "/saveRemainingLeaveHr/{id}")
	public void saveRemainingLeaveHr(@PathVariable(value = "id") Integer id) {
		leaveRemainingService.saveLeaveBalance(id);
	}

	// save my remaining leave
	@PostMapping(value = "/saveRemainingLeave")
	public void saveRemainingLeave() {
		int id = generalService.getUserId();
		leaveRemainingService.saveRemainingLeave(id);
	}
}
