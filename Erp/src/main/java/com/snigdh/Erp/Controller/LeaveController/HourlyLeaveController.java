package com.snigdh.Erp.Controller.LeaveController;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.LeaveModel.HourlyLeave;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.UtilityService;
import com.snigdh.Erp.Service.LeaveService.HourlyLeaveService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.HOURLYLEAVE)
public class HourlyLeaveController {

	@Autowired
	UtilityService utilityService;

	@Autowired
	private UserService generalService;

	@Autowired
	private HourlyLeaveService leaveService;

	// save hourly leave
	@PostMapping(value = "/saveLeaveRequest")
	public ResponseEntity<String> saveLeaveRequest(@Valid @RequestBody HourlyLeave leave) {
		int id = generalService.getUserId();
		return leaveService.saveLeaveRequest(leave, id);
	}

	// Line Manager Accept/Reject Leave Request
	@PutMapping(value = "/updateLeaveRequest/{id}")
	public void updateLeaveRequest(@Valid @RequestBody HourlyLeave leave, @PathVariable Integer id) {
		leaveService.updateLeaveRequest(leave, id);
	}

	/*
	 * // Line Manager Get Leave Requests
	 * 
	 * @GetMapping(value = "/getLeaveRequest") public Object getLeaveRequest() { int
	 * id = generalService.getUserId(); return leaveService.getLeaveRequest(id,
	 * "Pending"); }
	 */

	// getMyLeaveRequest
	@GetMapping(value = "/getMyLeaveRequest")
	public Object getMyLeaveRequest() {
		int id = generalService.getUserId();
		return leaveService.getMyLeaveRequest(id);
	}

	// cancel leave request
	@PutMapping(value = "/cancelLeaveRequest/{id}")
	public void cancelLeaveRequest(@PathVariable(value = "id") Integer id) {
		leaveService.cancelLeaveRequest(id);
	}

	// find by id
	@GetMapping(value = "/findRequestById/{id}")
	public Object findMyRequest(@PathVariable(value = "id") Integer id) {
		return leaveService.findRequestById(id);
	}
}