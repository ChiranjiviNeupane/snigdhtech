package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.OtherInformation;
import com.snigdh.Erp.Service.EmployeeService.OtherInformationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.OTHER_INFO)
public class OtherInformationController {

	@Autowired
	OtherInformationService otherInformationService;
	
	@GetMapping(value = "/findall")
	public List<OtherInformation> findAll(){
		return otherInformationService.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody OtherInformation otherInformation) {
		otherInformationService.save(otherInformation);
	}
	
	@GetMapping(value = "/findone/{id}")
	public OtherInformation retrieve(@PathVariable(value = "id") Integer id) {
		return otherInformationService.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable OtherInformation id) {
		otherInformationService.delete(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public OtherInformation update(@RequestBody OtherInformation otherInformation, @PathVariable Integer id) {
		return otherInformationService.update(otherInformation, id);
	}
	
}
