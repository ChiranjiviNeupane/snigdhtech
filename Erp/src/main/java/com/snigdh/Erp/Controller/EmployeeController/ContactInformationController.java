package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.ContactInformation;
import com.snigdh.Erp.Repository.EmployeeRepository.ContactInformationRepository;
import com.snigdh.Erp.Service.EmployeeService.ContactInformationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.CONT_INFO)
public class ContactInformationController {

	@Autowired
	ContactInformationRepository contactInformationRepository;
	
	@Autowired
	ContactInformationService cInfoService;
	
	@GetMapping(value = "/findall")
	public List<ContactInformation> findAll(){
		return cInfoService.findAll();
	}
	
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody ContactInformation contactInfoObj) {
		cInfoService.save(contactInfoObj);
	}
	
	
	@GetMapping(value = "/findone/{id}")
	public ContactInformation retrieve(@PathVariable(value = "id") Integer id) {
		return cInfoService.retrieve(id);
	}
	
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable ContactInformation id) {
		cInfoService.delete(id);
	}
	
	
	@PutMapping(value = "/update/{id}")
	public ContactInformation updateValue(@RequestBody ContactInformation contactInformation, @PathVariable Integer id) {
		return cInfoService.updateValue(contactInformation, id);
	}
}
