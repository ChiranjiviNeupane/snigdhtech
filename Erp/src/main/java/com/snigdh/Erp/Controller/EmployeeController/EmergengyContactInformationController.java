package com.snigdh.Erp.Controller.EmployeeController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.EmergencyContactInformation;
import com.snigdh.Erp.Service.EmployeeService.EmergencyContactInformationService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.EMG_CONT)
public class EmergengyContactInformationController {
	@Autowired
	EmergencyContactInformationService emergencyContactInformationService;
	
	@GetMapping(value = "/findall")
	public List<EmergencyContactInformation> findAll(){
		return emergencyContactInformationService.findAll();
	}
	
	@PostMapping(value = "/save")
	public void save(@Valid @RequestBody EmergencyContactInformation emergencyContactInformation) {
		emergencyContactInformationService.save(emergencyContactInformation);
	}
	
	@GetMapping(value = "/findone/{id}")
	public EmergencyContactInformation retrieve(@PathVariable(value = "id") Integer id) {
		return emergencyContactInformationService.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable EmergencyContactInformation id) {
		emergencyContactInformationService.delete(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public EmergencyContactInformation update(@RequestBody EmergencyContactInformation emergencyContactInformation,@PathVariable Integer id) {
		return emergencyContactInformationService.update(emergencyContactInformation, id);
	}
	
	
}
