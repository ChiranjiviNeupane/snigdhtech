package com.snigdh.Erp.Controller.TravelController;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.TravelModel.Travel;
import com.snigdh.Erp.Service.TravelService.TravelService;
import com.snigdh.Erp.Utils.Constants;

@RestController
@RequestMapping(value = Constants.TRAVEL+"/request")
public class TravelController {
	@Autowired
	TravelService travelService;
	
	
	@GetMapping(value = "/findall")
	public List<Travel> findAll(){
		return travelService.findAll();
	}
	
	@PostMapping(value = "/save")
	public ResponseEntity<String> save(@Valid @RequestBody Travel travel) {
		return travelService.save(travel);
	}
	
	@GetMapping(value = "/findone/{id}")
	public Travel retrieve(@PathVariable(value = "id") Integer id) {
		return travelService.retrieve(id);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public void delete(@PathVariable Travel id) {
		travelService.delete(id);
	}
	
	@PutMapping(value = "/update/{id}")
	public ResponseEntity<String> update(@RequestBody Travel travel, @PathVariable Integer id) {
		return travelService.update(travel, id);
	}
	
	@GetMapping(value="/onTravel")
	public Object onTravel() {
		return travelService.onTravel();
	}
	@GetMapping(value="/myTravel")
	public List<Object> findMyTravel(){
		return travelService.findMyTravel();
	}
	
	@GetMapping(value="/myResponses")
	public List<Object> findMyResponses(){
		return travelService.findMyResponses();
	}
	
	@GetMapping(value="/requestsToMe")
	public List<Object> requests(){
		return travelService.requestsToMe();
	}
	
	@GetMapping(value="/reportByDate/{from}/{to}")
	public List<Object> reportByDate(@PathVariable(value = "from") String from,@PathVariable(value = "to") String to){
		return travelService.reportByDate(from,to);
	}
	
	@GetMapping(value="/reportByDateAndEmp/{from}/{to}/{empId}")
	public List<Object> reportByDateAndEmp(@PathVariable(value = "from") String from,@PathVariable(value = "to") String to,@PathVariable(value = "empId") EmployeePersonalInformation empId){
		return travelService.reportByDateAndEmp(from,to,empId);
	}
	
	@GetMapping(value="/reportByDateAndDepart/{from}/{to}/{depart}")
	public List<Object> reportByDateAndDepart(@PathVariable(value = "from") String from,@PathVariable(value = "to") String to,@PathVariable(value = "depart") String depart){
		return travelService.reportByDateAndDepart(from,to,depart); 
	}
	
}
