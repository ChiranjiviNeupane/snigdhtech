package com.snigdh.Erp.Controller.EmployeeController;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snigdh.Erp.Model.EmployeeModel.EmployeePersonalInformation;
import com.snigdh.Erp.Model.EmployeeModel.OtherInformation;
import com.snigdh.Erp.Model.EmployeeModel.Role;
import com.snigdh.Erp.Model.EmployeeModel.RoleName;
import com.snigdh.Erp.Repository.EmployeeRepository.EmployeePersonalInformationRepository;
import com.snigdh.Erp.Repository.EmployeeRepository.RoleRepository;
import com.snigdh.Erp.Service.MailService;
import com.snigdh.Erp.Service.UserService;
import com.snigdh.Erp.Service.EmployeeService.EmployeePersonalInformationService;
import com.snigdh.Erp.Service.EmployeeService.OtherInformationService;
import com.snigdh.Erp.Utils.Constants;
import com.snigdh.Erp.config.jwt.responseAndRequest.SignUpForm;

@RestController
@RequestMapping(value = Constants.BASE_URL)
public class EmployeePersonalInformationController {
	@Autowired
	PasswordEncoder encoder;

	@Autowired
	MailService mail;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	EmployeePersonalInformationRepository emp;

	@Autowired
	EmployeePersonalInformationService service;

	@Autowired
	OtherInformationService otherService;

	@Autowired
	UserService genService;

	@PreAuthorize("hasRole('ADMIN')") 
	@PostMapping("/save")
	public ResponseEntity<String> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {

		if (emp.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<String>("Fail -> Email is already in use!", HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		EmployeePersonalInformation user = new EmployeePersonalInformation(signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()), signUpRequest.getAcademicQualification(),
				signUpRequest.getDateOfBirth(), signUpRequest.getFirstName(), signUpRequest.getIdentificationName(),
				signUpRequest.getIdentificationNumber(), signUpRequest.getMaidenName(),
				signUpRequest.getMaritalStatus(), signUpRequest.getMiddleName(), signUpRequest.getNationality(),
				signUpRequest.getSex(), signUpRequest.getSuffix(), signUpRequest.getSurname(), signUpRequest.getTitle(),
				signUpRequest.getCompanyInformations(), signUpRequest.getContactAddresses(),
				signUpRequest.getContactInformations(), signUpRequest.getDependentInformations(),
				signUpRequest.getEmergencyContactInformations(), signUpRequest.getOtherInformations(),
				signUpRequest.getPermanentHomeAddresses(), signUpRequest.getWorkAddresses(),
				signUpRequest.getMultimedia(), signUpRequest.getOtherDegrees());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByType(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);

				break;
			case "manager":
				Role managerRole = roleRepository.findByType(RoleName.ROLE_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(managerRole);

				break;
			default:
				Role userRole = roleRepository.findByType(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});

		try {
			user.setRoles(roles);
			user.setCreatedAt(new Date());
			user.setCreatedBy(genService.getUserEmail());
			user.setUpdatedBy(genService.getUserEmail());
			user.setUpdatedAt(new Date());
			user.setPresentStatus(true);

			// mail
			String subject = "Notification regarding new User";
			String message = "New Employee with mail " + signUpRequest.getEmail()
					+ " and name "+signUpRequest.getFirstName()+" "+signUpRequest.getMiddleName()+" "+signUpRequest.getSurname()+" has been added from your account.";
			mail.sendMail(genService.getUserEmail(), subject, message);
			mail.sendMail(signUpRequest.getEmail(), "New Account",
					"your account has been added to the system. Now you can login using these credentials: Email- "
							+ signUpRequest.getEmail() + "Password- " + signUpRequest.getPassword() + " .");
			// mail end

			emp.save(user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return ResponseEntity.ok().body("User registered successfully!");

	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/findone/{id}")
	public EmployeePersonalInformation retrieveEmployee(@PathVariable(value = "id") Integer id) {
		return service.retrieve(id);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('USER') ")
	@PutMapping("/update/{id}")
	String replaceEmployee(@RequestBody EmployeePersonalInformation newEmployee, @PathVariable Integer id) {
		return service.replaceEmployee(newEmployee, id);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/delete/{id}")
	Optional<EmployeePersonalInformation> deleteEmployee(@RequestBody OtherInformation oth, @PathVariable Integer id) {
		otherService.update(oth, id);
		return service.deleteEmployee(id);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/statusDisableToEnable/{id}")
	Optional<EmployeePersonalInformation> EnableEmployee(@PathVariable Integer id) {
		return service.enableEmployee(id);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('USER')")
	@GetMapping("/findLineManager")
	public List<Object> lineManager() {
		return service.findLineManager();
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/findCurrentEmpManageList")
	public List<Object> empCurrentManageList() {
		boolean status = true;
		return service.findEmployeeDetailForManageList(status);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/findExEmpManageList")
	public List<Object> empExManageList() {
		boolean status = false;
		return service.findEmployeeDetailForManageList(status);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/findEmpDetail/{id}")
	public HashMap<String, Object> empDetail(@PathVariable Integer id) {
		return service.empDetail(id);
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('USER')")
	@GetMapping("/myProfile")
	public HashMap<String, Object> myProfile() { 
		return service.empDetail(genService.getUserId());
	}

	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('USER')")
	@PutMapping("/changePassword")
	public String changePassword(@RequestBody EmployeePersonalInformation newEmployeePassword) {
		return service.changePassword(newEmployeePassword);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/changeRole/{id}")
	public String changeRassword(@PathVariable Integer id,@RequestBody SignUpForm signUpRequest) {
		EmployeePersonalInformation employee1 = emp.findDetailById(id);
		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByType(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);

				break;
			case "manager":
				Role managerRole = roleRepository.findByType(RoleName.ROLE_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(managerRole);

				break;
			default:
				Role userRole = roleRepository.findByType(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});
		employee1.setRoles(roles);
		emp.save(employee1);
		return "success";
	}

	@PreAuthorize(" hasRole('ADMIN') or hasRole('MANAGER') or hasRole('USER')")
	@GetMapping("/findEmpName/{id}")
	public Object empDetailNamePosDepart(@PathVariable Integer id) {
		return service.empDetailNamePosDepart(id);
	}

	/* line manager get all his employees data */
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER')")
	@GetMapping("/getMyEmployeesData")
	public Object getMyEmployeesData() {
		return service.getMyEmployeesData();
	}

}
